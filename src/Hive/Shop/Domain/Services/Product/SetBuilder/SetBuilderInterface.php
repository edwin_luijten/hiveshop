<?php namespace Shop\Domain\Services\Product\SetBuilder;

interface SetBuilderInterface
{
    /**
     * Get all permutations of option set.
     *
     * @param array   $setTuples
     * @param boolean $isRecursiveStep
     *
     * @return array The product set of tuples.
     */
    public function build(array $setTuples, $isRecursiveStep = false);
}
