<?php namespace Shop\Domain\Services;

/**
 * Class ServiceFactory
 *
 * @package Shop\Domain\Services
 *
 * @todo    optimize ReflectionClass::newInstanceArgs � Creates a new class instance from given arguments.
 */
class ServiceFactory
{

    private $service;
    private $serviceMetaData = [];

    const CREATOR_SERVICE = AbstractCreatorService::class;
    const UPDATER_SERVICE = AbstractUpdaterService::class;

    public function __construct($service)
    {
        $this->service = $this->validate($service);
    }

    /**
     * Get the service
     * __call can also be used
     *
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Check if the service is valid
     *
     * @param string $service
     *
     * @return mixed
     */
    private function validate($service)
    {
        $this->setServiceMetaData($service);

        $service = $this->resolveService($service);

        if (empty($this->serviceMetaData['parentClass'])) {
            die('invalid service');
        }

        $serviceType = $this->serviceMetaData['parentClass']->name;

        if ($serviceType !== self::CREATOR_SERVICE || $serviceType !== self::UPDATER_SERVICE) {
            // Invalid service
        }

        if ($serviceType == self::CREATOR_SERVICE) {
            // Creator service must have a create method
            if (! $this->serviceMetaData['type'] == 'create') {
            }
        }

        if ($serviceType == self::UPDATER_SERVICE) {
            // Creator service must have an update method
            if (! $this->serviceMetaData['type'] == 'update') {
            }
        }

        return $service;
    }

    /**
     * Resolve the service with its dependencies and create a new instance
     *
     * @param $service
     *
     * @return mixed
     */
    private function resolveService($service)
    {
        return new $service(...$this->serviceMetaData['dependencies']);
    }

    /**
     * Set service meta data, this way we have required information at hand
     *
     * @param $service
     */
    private function setServiceMetaData($service)
    {
        $reflector = new \ReflectionClass($service);

        $params = $reflector->getConstructor()->getParameters();

        $this->serviceMetaData['dependencies'] = [];

        foreach ($params as $param) {
            // Resolve dependencies from laravel's ioc container
            $this->serviceMetaData['dependencies'][] = app($param->getClass()->name);
        }

        $this->serviceMetaData['parentClass'] = $reflector->getParentClass();

        if ($reflector->hasMethod('create')) {
            $this->serviceMetaData['type'] = 'create';
        } elseif ($reflector->hasMethod('update')) {
            $this->serviceMetaData['type'] = 'create';
        }
    }

    public function __call($method, $parameters)
    {
        if (method_exists($this->service, $method)) {
            return call_user_func_array([$this->service, $method], $parameters);
        }
    }
}
