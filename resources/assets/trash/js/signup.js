(function () {
	null == window.HiveShopSignup && (window.HiveShopSignup = {})
}).call(this), function () {
	var t, e = function (t, e) {
		return function () {
			return t.apply(e, arguments)
		}
	};

	t = function () {
		function t() {
			this.signupCompleted = e(this.signupCompleted, this), this.waitPeriodOver = e(this.waitPeriodOver, this)
		}

		var n, i;
		return t.name = "WaitScreen", n = 0, i = new $.Deferred, t.prototype.init = function () {
			return this.setup()
		}, t.prototype.setup = function () {
			var t, e;
			return $("body").scrollTop(0), $(".wait-page-creating").addClass("active"), this.initWaitPage(), t = this.submitSignup(), e = i.promise(), $.when(t, e).done(this.signupCompleted).fail(this.signupError)
		}, t.prototype.initWaitPage = function () {
			return this.cycleWaitPageMessage(), setInterval(this.cycleWaitPageMessage, 2000), setTimeout(this.waitPeriodOver, 2000)
		}, t.prototype.cycleWaitPageMessage = function () {
			var t;
			return t = $(".js-section-loading p").length, n > 0 && $(".js-section-loading > *:eq(" + (n - 1) + ")").addClass("exit"), t >= n ? ($(".js-section-loading > *:eq(" + n + ")").addClass("enter"), n++) : void 0
		}, t.prototype.waitPeriodOver = function () {
			return i.resolve()
		}, t.prototype.cycleWaitPageMessage = function () {
			var t;
			return t = $(".js-section-loading p").length, n > 0 && $(".js-section-loading > *:eq(" + (n - 1) + ")").addClass("exit"), t >= n ? ($(".js-section-loading > *:eq(" + n + ")").addClass("enter"), n++) : void 0
		}, t.prototype.animateWaitExit = function () {
			return $(".differential").addClass("is-exiting")
		}, t.prototype.submitSignup = function () {
			return $.ajax({type: "POST", url: "/services/signup/create", dataType: "json", data: signupParams})
		}, t.prototype.signupError = function () {
			return $(".signup-page-wrapper").hide(), $(".wait-page-creating").removeClass("active"), $(".wait-page-error").addClass("active")
		}, t.prototype.signupCompleted = function (t) {
			var e;
			return $(".wait-page-creating .heading-1").html("Success! Your store is ready to go"), null != t ? (e = t[0], window.location = e.signup.admin_url) : void 0
		}, t.prototype.checkDefaultProvince = function () {
			var t, e;
			return e = $('#detailsForm').find('[name="province"]'), t = e.data("default"), e.find("option:contains(" + t + "):first").length ? e.val(t) : void 0
		}, t.prototype.onCountrySelection = function (t) {

			var e, n, i, r;
			if (e = CountryDB.getCountry($(t.target).val()), this.requirePostalCode = -1 === CountryDB.validatedPostalCodes.indexOf(e.code) ? !1 : !0, i = $('#detailsForm').find('[name="province"]'), null != e && null != (r = e.zones) ? r.length : void 0) {
				$('#detailsForm').find("#zoneLabel").text(e.zone_label), $('#detailsForm').find("#province").show(), i.empty();
				for (n in e.zones)i.append("<option value='" + e.zones[n].name + "'>" + e.zones[n].name + "</option>");
				return this.checkDefaultProvince()
			}
			return $('#detailsForm').find("#province").hide(), i.empty();
		}, t
	}(), HiveShopSignup.WaitScreen = t
}, function () {
	var t;
	$(function () {

	}), window.HiveShopSignup.init = function () {
		return t()
	}, t = function () {
		return window.HiveShopSignup.waitScreen = new HiveShopSignup.WaitScreen, $(".signup-page-wrapper").length ? window.HiveShopSignup.signupForm = new HiveShopSignup.SignupForm($(".signup-page-wrapper"), window.HiveShopSignup.waitScreen, new HiveShopSignup.ShopAvailablityChecker) : $(".wait-page-wrapper").length ? HiveShopSignup.waitScreen.init() : window.HiveShopSignup.detailsForm = new HiveShopSignup.DetailsForm($(".tenfield .card-wrapper"), HiveShopSignup.errors)
	}
}.call(this);