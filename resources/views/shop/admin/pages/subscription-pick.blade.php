@extends('shop.admin.template')

@section('page-breadcrumb')
    <ul>
        <li><i class="fa fa-cog"></i> {{ _('Settings') }} / {{ _('Account') }} / </li>
        <li class="action">{{ _('Pick a plan for your store')  }}</li>
    </ul>
@endsection

@section('content')
    <form id="billing-preferences" data-toggle="validator" action="{{ URL::route('admin.settings.account', ['subdomain' => $store->getSubdomain()]) }}" method="post">
    <header class="navbar navbar-fixed-top navbar-default">
        <ul class="nav navbar-nav-custom">
            <li class="hidden-xs animation-fadeInQuick navbar-breadcrumb">
                @yield('page-breadcrumb')
            </li>
        </ul>
        <ul class="nav navbar-nav-custom pull-right">
            <li><button type="submit" class="btn btn-default btn-effect-ripple disabled" disabled="disabled">{{ _('Save') }}</button></li>
        </ul>
        <div id="progress">

        </div>
    </header>
    <div id="page-content">
        <div class="row section">
            <div class="col-md-3">
                <h3>{{ _('Billing information') }}</h3>

                <p class="text-muted"><small>{{ _('Please provide your payment details.') }}</small></p>

                <div class="row">
                    <div class="col-md-6">
                        <img src="/assets/c/img/pay-pro-logo.svg" title="Pay Pro" class="img-responsive"/>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-section">

                        <!-- Current Address -->
                        <div class="row">
                            <div class="col-md-8">
                                <h4>{{ _('Payment preferences') }}</h4>
                                <div class="form-group">
                                    <div class="radio">
                                        <label for="ideal">
                                            <input type="radio" name="method" value="ideal" id="ideal" class="billing-preference" checked="checked" /> {{ _('IDEAL') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group billing-details" id="ideal-details">
                                    <label for="issuer">{{ _('Choose your bank') }}</label>
                                    <select name="issuer" class="form-control" id="issuer">
                                        @foreach($issuers as $issuer)
                                            <option value="{{ $issuer->getId() }}">{{ $issuer->getName() }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="radio">
                                        <label for="directdebit">
                                            <input type="radio" name="method" value="directdebit" id="directdebit" class="billing-preference" /> {{ _('Continuing Authorization') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="row hidden billing-details" id="directdebit-details">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="bank-account">{{ _('Bank account') }}</label>
                                            <input type="text" name="bank-account" class="form-control" id="bank-account" />
                                        </div>
                                        <div class="form-group">
                                            <label for="in-name-of">{{ _('In name of') }}</label>
                                            <input type="text" name="in-name-of" class="form-control" id="in-name-of" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="address">{{ _('Your current billing address') }}</label>
                                <address>
                                    {{ $account->getFullName() }}<br/>
                                    {{ $store->getAddress()->getStreet() }}<br/>
                                    {{ $store->getAddress()->getCity() }}, {{ $store->getAddress()->getPostal() }}<br/>
                                    {{ _($store->getAddress()->getCountry()->getName()) }}
                                </address>
                            </div>
                        </div>
                        <div class="hidden">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="city">{{ _('Street') }}</label>
                                        <input type="text" name="street" value="{{ $store->getAddress()->getStreet() }}" class="form-control" id="street" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">{{ _('City') }}</label>
                                        <input type="text" name="city" value="{{ $store->getAddress()->getCity() }}" class="form-control" id="city" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="postal">{{ _('Postal/Zipcode') }}</label>
                                        <input type="text" name="postal" value="{{ $store->getAddress()->getPostal() }}" class="form-control" id="postal" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">{{ _('Country') }}</label>
                                        <input type="text" name="country" class="form-control" id="country" value="{{ $store->getAddress()->getCountry()->getName() }}" />
                                    </div>
                                </div>
                                <div class="col-md-6">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="row">
                <div class="col-md-12 text-right">
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-default btn-effect-ripple disabled" disabled="disabled">{{ _('Save') }}</button>
                </div>
            </footer>
        </div>
    </div>

    </form>
@endsection

@section('script')
    <script>
        UI.form($('#billing-preferences'));
        App.billingPreferences();
    </script>
@endsection