<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

use Shop\Domain\Entity\Account\Account;
use Shop\Domain\Entity\Account\AccountId;
use Shop\Domain\Entity\Account\AccountRepositoryInterface;
use Shop\Domain\Entity\History\AuthHistory;

class AccountRepository extends AbstractBaseRepository implements AccountRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return AccountId::generate();
    }

    public function getEntity()
    {
        return new Account();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('a')->from(Account::class, 'a');

        return $queryBuilder->getQuery()->execute();
    }

    public function getByStore($store)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('a', 'aa')->from(Account::class, 'a')
            ->where(
                $queryBuilder->expr()->eq('a.store', ':store')
            )
        ->setParameter('store', $store)
            ->leftJoin(
                'a.authHistory',
                'aa',
                'WITH',
                'a.id = aa.account AND aa.createdAt = (SELECT MAX(aat.createdAt) FROM ' . AuthHistory::class
                . ' AS aat WHERE aat.account = a.id)'
            );

        return $queryBuilder->getQuery()->execute();
    }
    /**
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function getPaginated($page = 1, $limit = 15)
    {
        if ($limit > $this->maxResultsPerPage) {
            $limit = $this->maxResultsPerPage;
        }

        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('a')->from(Account::class, 'a');

        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pager = new Pagerfanta($adapter);
        $pager->setCurrentPage($page);
        $pager->setMaxPerPage($limit);

        return $pager;
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('a')->from(Account::class, 'a')
            ->where($queryBuilder->expr()->eq('a.id', ':id'))
            ->setParameter('id', $id);

        return $queryBuilder->getQuery()->getSingleResult();
    }

    public function getByEmail($email)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('a')->from(Account::class, 'a')
            ->where($queryBuilder->expr()->eq('a.email', ':email'))
            ->setParameter('email', $email);

        return $queryBuilder->getQuery()->getSingleResult();
    }

    public function getCount()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('count(a.id)')->from(Account::class, 'a');

        return (integer) $queryBuilder->getQuery()->getSingleScalarResult();
    }
}
