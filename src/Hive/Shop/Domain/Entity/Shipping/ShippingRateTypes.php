<?php namespace Shop\Domain\Entity\Shipping;

class ShippingRateTypes
{
    const WEIGHT_BASED = 'weight_based';
    const PRICE_BASED = 'price_based';
}
