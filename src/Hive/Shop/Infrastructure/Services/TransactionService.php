<?php namespace Shop\Infrastructure\Services;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Services\ServiceInterface;

class TransactionService
{
    /**
     * @var array
     */
    private $services = [];
    /**
     * @var EntityManager
     */
    private $entityManager;
    /**
     * @var array
     */
    private $result = [];

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Add a service to the transaction
     *
     * @param \Shop\Domain\Services\ServiceInterface $service
     */
    public function addService(ServiceInterface $service)
    {
        $this->services[get_class($service)] = $service;
    }

    /**
     * Run the transaction with the given services
     *
     * @return array service instances
     *
     * @throws \Exception
     */
    public function run()
    {
        $this->entityManager->transactional(function ($em) {
            foreach ($this->services as $key => $service) {
                $this->result[$key] = $service->run($em);
            }
        });

        return $this->result;
    }
}
