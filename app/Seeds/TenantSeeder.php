<?php namespace Shop\Domain\Services\Seeds;

use Shop\Domain\Entity\Store\StoreRepositoryInterface;
use Shop\Domain\Services\Store\CreateStore;

class StoreSeeder
{

    private $storeRepository;
    private $createStore;

    public function __construct(StoreRepositoryInterface $storeRepository, CreateStore $createStore)
    {
        $this->storeRepository = $storeRepository;
        $this->createStore = $createStore;
    }

    public function seed()
    {
        $maxStores = 10;
        $stores = [];

        $i = 0;

        while ($i <= $maxStores) {
            $i++;

            $stores[] = [
                'name'      => 'store_' . $i . str_random(3),
            ];
        }

        foreach ($stores as $store) {
            $this->createStore->create($store['name']);
        }
    }
}