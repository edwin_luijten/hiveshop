<?php namespace Shop\Domain\Entity\Geo;

use Shop\Infrastructure\Repositories\RepositoryInterface;

interface IpToCountryRepositoryInterface extends RepositoryInterface
{
    public function getByIp($ip);
}
