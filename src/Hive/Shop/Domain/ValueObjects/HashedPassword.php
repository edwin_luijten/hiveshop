<?php namespace Shop\Domain\ValueObjects;

use Assert\Assertion;

class HashedPassword extends AbstractValueObject
{
    /**
     * @var string
     */
    private $value;

    /**
     * Create a new Hashed Password
     *
     * @param string $value
     *
     * @return \Shop\Domain\ValueObjects\HashedPassword
     */
    public function __construct($value)
    {
        Assertion::string($value);

        $this->value = $value;
    }

    /**
     * Create a new instance from a native form
     *
     * @param mixed $native
     * @return ValueObject
     */
    public static function fromNative($native)
    {
        return new HashedPassword($native);
    }

    /**
     * Determine equality with another Value Object
     *
     * @param AbstractValueObject $object
     * @return bool
     */
    public function equals(AbstractValueObject $object)
    {
        return $this == $object;
    }
    /**
     * Return the object as a string
     *
     * @return string
     */
    public function toString()
    {
        return $this->value;
    }

    /**
     * Return the object as a string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}
