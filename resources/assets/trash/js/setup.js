(function () {
	null == window.ShopifySignup && (window.ShopifySignup = {})
}).call(this), function () {
	window.SignupCardEvents = function () {
		function t() {
			this._listeners = []
		}

		return t.name = "SignupCardEvents", t.prototype.registerListener = function (t, e) {
			return this._listeners.push({context: t, subscribesTo: e})
		}, t.prototype.trigger = function (t, e) {
			var n;
			return n = !0, $.each(this._listeners, function (i, r) {
				var o;
				return null != r.subscribesTo[t] ? (o = r.subscribesTo[t].call(r.context, e), n = n && o) : void 0
			}), n
		}, t
	}()
}.call(this), function () {
	var t, e, n = function (t, e) {
		return function () {
			return t.apply(e, arguments)
		}
	};
	e = {
		minLength: function (t) {
			var e, n;
			return n = this.data("min-length"), this.val().length < n ? (this.addClass("error"), e = "" + this.data("fieldname") + " is too short (minimum is " + n + " characters)", t.push(e), !1) : !0
		}, maxLength: function (t) {
			var e, n;
			return n = this.data("max-length"), this.val().length > n ? (this.addClass("error"), e = "" + this.data("fieldname") + " is too long (maximum is " + n + " characters)", t.push(e), !1) : !0
		}, noSpaces: function (t) {
			var e;
			return /[\s]/.test(this.val()) ? (this.addClass("error"), e = "" + this.data("fieldname") + " has invalid characters. Please don't use spaces.", t.push(e), !1) : !0
		}, email: function (t) {
			var e;
			return /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(this.val()) ? !0 : (this.addClass("error"), e = "" + this.data("fieldname") + " is invalid", t.push(e), !1)
		}
	}, t = function () {
		function t(t, e, i) {
			this.el = t, this.waitScreen = e, this.shopAvailabilityChecker = i, this.onSubmitSuccess = n(this.onSubmitSuccess, this), this.onSubmitForm = n(this.onSubmitForm, this), this.el.find("form").on("submit", this.onSubmitForm)
		}

		var i, r, o;
		return t.name = "SignupForm", i = '[name="email"]', r = '[name="password"]', o = '[name="shop_name"]', t.prototype.animateExit = function () {
			return this.el.find(".card").addClass("card-exit-down")
		}, t.prototype.onSubmitForm = function (t) {
			return t.preventDefault(), this.el.find('[type="submit"]').prop("disabled", !0), this.validate().done(this.onSubmitSuccess)
		}, t.prototype.onSubmitSuccess = function () {
			return signupParams.signup.email = this.el.find(i).val(), signupParams.signup.password = this.el.find(r).val(), signupParams.signup.shop_name = this.el.find(o).val(), this.animateExit(), this.waitScreen.init()
		}, t.prototype.validate = function () {
			var t, n, r, a, s, u = this;
			return s = $.Deferred(), n = [], this.el.find(".error").removeClass("error"), this.el.find(".field_with_errors").removeClass("field_with_errors"), this.el.find("[data-validate]").each(function (t, i) {
				var r, o;
				return o = $(i).data("validate").split(" "), r = !0, $.each(o, function (t, o) {
					return null != e[o] ? r = r && e[o].call($(i), n, u.el) : void 0
				})
			}), t = function () {
				return n.push("Shop name is not available"), u.el.find(o).addClass("error")
			}, this.el.find(o).hasClass("error") || (r = this.shopAvailabilityChecker.isShopAvailable(this.el.find(o).val(), this.el.find(i).val()).then(function (e) {
				return e ? void 0 : t()
			}, t)), a = function () {
				return u.populateErrorsList(n), 0 === n.length ? s.resolve() : (s.reject(), u.el.find('[type="submit"]').prop("disabled", !1))
			}, r ? r.then(a) : a(), s
		}, t.prototype.populateErrorsList = function (t) {
			var e, n, i;
			return t.length ? (this.el.find(".errors").toggleClass("active", !0), i = 1 === t.length && "was" || "were", n = 1 === t.length && "error" || "errors", this.el.find(".error-count-message").text("There " + i + " " + t.length + " " + n + " on this form:"), e = this.el.find(".error-list").empty(), $.each(t, function () {
				return e.append("<li>" + this + "</li>")
			})) : this.el.find(".errors").toggleClass("active", !1)
		}, t
	}(), null == window.ShopifySignup && (window.ShopifySignup = {}), ShopifySignup.SignupForm = t
}.call(this), function () {
	ShopifySignup.util = {
		init: function () {
			return $("html").removeClass("no-js").addClass("js")
		}
	}, $(function () {
		return ShopifySignup.util.init()
	})
}.call(this), function () {
	var t;
	t = function () {
		function t() {
		}

		return t.name = "ShopAvailablityChecker", t.prototype.isShopAvailable = function (t, e) {
			return $.getJSON("/services/signup/check_availability.json", {
				shop_name: t,
				email: e
			}).promise().then(function (t) {
				return "available" === t.status
			})
		}, t
	}(), null == window.ShopifySignup && (window.ShopifySignup = {}), ShopifySignup.ShopAvailablityChecker = t
}.call(this), function () {
	var t, e = function (t, e) {
		return function () {
			return t.apply(e, arguments)
		}
	};
	t = function () {
		function t() {
			this.signupCompleted = e(this.signupCompleted, this), this.waitPeriodOver = e(this.waitPeriodOver, this)
		}

		var n, i;
		return t.name = "WaitScreen", n = 0, i = new $.Deferred, t.prototype.init = function () {
			return this.setup()
		}, t.prototype.setup = function () {
			var t, e;
			return $("body").scrollTop(0), $(".wait-page-creating").addClass("active"), this.initWaitPage(), t = this.submitSignup(), e = i.promise(), $.when(t, e).done(this.signupCompleted).fail(this.signupError)
		}, t.prototype.initWaitPage = function () {
			return this.cycleWaitPageMessage(), setInterval(this.cycleWaitPageMessage, 2e3), setTimeout(this.waitPeriodOver, 8e3)
		}, t.prototype.waitPeriodOver = function () {
			return i.resolve()
		}, t.prototype.cycleWaitPageMessage = function () {
			var t;
			return t = $(".js-section-loading p").length, n > 0 && $(".js-section-loading > *:eq(" + (n - 1) + ")").addClass("exit"), t >= n ? ($(".js-section-loading > *:eq(" + n + ")").addClass("enter"), n++) : void 0
		}, t.prototype.animateWaitExit = function () {
			return $(".differential").addClass("is-exiting")
		}, t.prototype.submitSignup = function () {
			return $.ajax({type: "POST", url: "/services/signup/create", dataType: "json", data: signupParams})
		}, t.prototype.signupError = function () {
			return $(".signup-page-wrapper").hide(), $(".wait-page-creating").removeClass("active"), $(".wait-page-error").addClass("active")
		}, t.prototype.signupCompleted = function (t) {
			var e;
			return this.animateWaitExit(), $(".wait-page-creating .heading-1").html("Success! Your store is ready to go"), null != t ? (e = t[0], window.location = e.signup.admin_url) : void 0
		}, t
	}(), ShopifySignup.WaitScreen = t
}.call(this), function () {
	var t, e, n, i = function (t, e) {
		return function () {
			return t.apply(e, arguments)
		}
	}, r = {}.hasOwnProperty, o = function (t, e) {
		function n() {
			this.constructor = t
		}

		for (var i in e)r.call(e, i) && (t[i] = e[i]);
		return n.prototype = e.prototype, t.prototype = new n, t.__super__ = e.prototype, t
	};
	t = "card-active", e = "card-enter", n = "card-exit", window.SignupCards = function (r) {
		function a(t) {
			var n = this;
			this.cardContainer = t, this.previousCard = i(this.previousCard, this), this.nextCard = i(this.nextCard, this), a.__super__.constructor.apply(this, arguments), this.$body = $("body"), this.$cardContainer = $(this.cardContainer), this.$cards = this.$cardContainer.find(".card"), this.activeIndex = this.activeCardIndex(), this.activeCard = this.$cards.eq(this.activeIndex), this.$cards.first().removeClass(e), setTimeout(function () {
				return n.trigger("card:entering:" + n.$cards.first().data("card-id") + ":after", 0)
			}), this.$cards.filter(":gt(0)").hide(), this.bindEventHandlers(), this.populateProgressIndicator()
		}

		return o(a, r), a.name = "SignupCards", a.prototype.activeCardIndex = function () {
			return this.$cards.filter("." + t).length ? this.$cards.index(this.$cards.filter("." + t)) : (this.$cards.first().addClass(t), 0)
		}, a.prototype.bindEventHandlers = function () {
			return $('[data-nav="next"]').on("click", this.nextCard), $('[data-nav="prev"]').on("click", this.previousCard), $('[data-nav="submit"]').on("click", function () {
				return $(this).closest("form").trigger("submit")
			})
		}, a.prototype.nextCard = function (t) {
			return (null != t ? t.preventDefault() : void 0) || this.jumpToIndex(this.activeIndex + 1)
		}, a.prototype.previousCard = function (t) {
			return (null != t ? t.preventDefault() : void 0) || this.jumpToIndex(this.activeIndex - 1)
		}, a.prototype.isLastCard = function () {
			return this.activeIndex === this.$cards.length - 1
		}, a.prototype.jumpToId = function (t) {
			var e;
			return e = this.$cards.index(this.$cards.filter("[data-card-id='" + t + "']")), this.jumpToIndex(e)
		}, a.prototype.jumpToIndex = function (i) {
			var r, o, a, s;
			if (!(i >= this.$cards.length || i === this.activeIndex) && (r = this.$cards.eq(i).data("card-id"), a = this.$cards.eq(this.activeIndex).data("card-id"), o = this.trigger("card:entering:" + r), s = this.trigger("card:leaving:" + a, {
					from: this.activeIndex,
					to: i
				}), o && s))return i > this.activeIndex ? (this.$cards.eq(this.activeIndex).one("webkitTransitionEnd msTransitionEnd transitionend", function () {
				return $(this).hasClass(t) ? void 0 : $(this).hide()
			}), this.$cards.each(function (r) {
				return i > r ? $(this).addClass(n).removeClass("" + t + " " + e) : void 0
			}), this.$cards.eq(i).show()[0].offsetWidth, this.$cards.eq(i).removeClass(e).addClass(t)) : (this.$cards.eq(this.activeIndex).one("webkitTransitionEnd msTransitionEnd transitionend", function () {
				return $(this).hasClass(t) ? void 0 : $(this).hide()
			}), this.$cards.each(function (r) {
				return r > i ? $(this).addClass(e).removeClass("" + t + " " + n) : void 0
			}), this.$cards.eq(i).show()[0].offsetWidth, this.$cards.eq(i).removeClass(n).addClass(t)), this.activeIndex = i, this.activeCard = this.$cards.eq(i), this.$body.scrollTop(0), this.trigger("card:entering:" + r + ":after"), this.trigger("card:leaving:" + a + ":after")
		}, a.prototype.populateProgressIndicator = function () {
			var t, e;
			for (t = 1, e = this.$cards.length; e >= 1 ? e >= t : t >= e; e >= 1 ? t++ : t--)this.$cardContainer.find(".progress-indicator").append("<li/>");
			return this.$cardContainer.find(".progress-indicator").each(function (t) {
				return $(this).find("li:eq(" + t + ")").addClass("active")
			})
		}, a
	}(SignupCardEvents)
}.call(this), function () {
	var t, e, n = function (t, e) {
		return function () {
			return t.apply(e, arguments)
		}
	};
	e = 13, t = function () {
		function t(t, e) {
			this.el = t, this.errors = e, this.validate = n(this.validate, this), this.onBeforeUnload = n(this.onBeforeUnload, this), this.onFormSubmit = n(this.onFormSubmit, this), this.onCountrySelection = n(this.onCountrySelection, this), this.onBackgroundChange = n(this.onBackgroundChange, this), window.windowLoaded = $.Deferred(), $(window).load(function () {
				return windowLoaded.resolve()
			}), this.enableLiveValidation = !0, this.requirePostalCode = !1, this.promptBeforeUnload = !0, this.form = this.el.find("form"), this.initializeHelpers(), this.bindEventHandlers(), this.onCountrySelection({target: this.form.find('[name="account_setup[country]"]')}), this.onBackgroundChange({target: this.form.find("#account_setup_background")[0]}), this.navigation = new SignupCards(this.el), this.navigation.registerListener(this, {
				"card:entering:essentials:after": this.onEssentialsCard,
				"card:entering:address:after": this.onAddressCard,
				"card:leaving:address": this.onLeavingAddress,
				"card:entering:customize:after": this.onCustomizeCard
			}), this.processErrors()
		}

		return t.name = "DetailsForm", t.prototype.bindEventHandlers = function () {
			var t = this;
			return this.form.on("change", ".js-marketing-checkbox", function () {
				return $(this).closest(".js-box").toggleClass("is-active", $(this).is(":checked"))
			}), this.form.on("click", ".js-box", function (t) {
				var e;
				if ("checkbox" !== t.target.type && "hidden" !== t.target.type)return t.preventDefault(), e = $(this).find(".js-marketing-checkbox:first"), e.prop("checked", !e.prop("checked")).trigger("change")
			}), this.form.on("change", '[name="account_setup[country]"]', this.onCountrySelection), this.form.on("change keypress", "input, select, button", function (n) {
				var i;
				return i = n.keyCode || n.which, i === e ? t.handleEnterKey(n) : (t.form.find(".error").length && t.enableLiveValidation && t.validate(), !0)
			}), this.form.on("change", "#account_setup_background", this.onBackgroundChange), this.form.on("submit", this.onFormSubmit), $(window).on("beforeunload", this.onBeforeUnload)
		}, t.prototype.checkDefaultProvince = function () {
			var t, e;
			return e = this.form.find('[name="account_setup[province]"]'), t = e.data("default"), e.find("option:contains(" + t + "):first").length ? e.val(t) : void 0
		}, t.prototype.filterToEmpty = function () {
			return !$(this).val().length > 0
		}, t.prototype.handleEnterKey = function (t) {
			return t.preventDefault(), $(":focus").is('[data-nav="prev"]') ? this.navigation.previousCard() : this.navigation.isLastCard() ? this.submitButton().prop("disabled") || this.form.trigger("submit") : this.navigation.nextCard(), !1
		}, t.prototype.initializeHelpers = function () {
			return CountryDB.getCountry = function (t) {
				var e;
				return e = $.grep(this.countries, function (e) {
					return e.name === t || e.code === t
				}), e[0]
			}
		}, t.prototype.onEssentialsCard = function () {
			var t = this;
			return windowLoaded.then(function () {
				return t.form.find('input[type="checkbox"]:first').focus()
			})
		}, t.prototype.onAddressCard = function () {
			var t = this;
			return setTimeout(function () {
				var e;
				return e = t.navigation.activeCard, windowLoaded.then(e.find(".error").length ? function () {
					return e.find(".error:first input").first().focus()
				} : e.find('input[type="text"]').filter(t.filterToEmpty).length ? function () {
					return e.find('input[type="text"]').filter(t.filterToEmpty).first().focus()
				} : function () {
					return e.find('input[type="text"]:first').focus()
				})
			}, 0)
		}, t.prototype.onBackgroundChange = function (t) {
			var e;
			return e = this.form.find(".js-platform-options"), "I am already selling online using a different system" === t.target.value ? e.addClass("input-animate--open") : this.form.find(".js-platform-options").removeClass("input-animate--open")
		}, t.prototype.onLeavingAddress = function (t) {
			var e, n = this;
			return t.from < t.to ? (e = this.validate(), windowLoaded.then(function () {
				return e ? void 0 : n.form.find(".error:first input").focus()
			}), e) : !0
		}, t.prototype.onCustomizeCard = function () {
			var t = this;
			return windowLoaded.then(function () {
				return t.navigation.activeCard.find("select:first").focus()
			})
		}, t.prototype.onCountrySelection = function (t) {
			var e, n, i, r;
			if (e = CountryDB.getCountry($(t.target).val()), this.requirePostalCode = -1 === CountryDB.validatedPostalCodes.indexOf(e.code) ? !1 : !0, i = this.form.find('[name="account_setup[province]"]'), null != e && null != (r = e.zones) ? r.length : void 0) {
				this.form.find("#zoneLabel").text(e.zone_label), this.form.find("#province").show(), i.empty();
				for (n in e.zones)i.append("<option value='" + e.zones[n].name + "'>" + e.zones[n].name + "</option>");
				return this.checkDefaultProvince()
			}
			return this.form.find("#province").hide(), i.empty()
		}, t.prototype.onFormSubmit = function (t) {
			var e = this;
			return this.validate() ? (this.promptBeforeUnload = !1, this.submitButton().prop("disabled", !0)) : (t.preventDefault(), windowLoaded.then(function () {
				return e.form.find(".error:first input").focus()
			}))
		}, t.prototype.onBeforeUnload = function () {
			return this.promptBeforeUnload ? "Your store setup is not yet complete. You will have to start over if you leave this page." : void 0
		}, t.prototype.processErrors = function () {
			var t, e = this;
			return this.errors ? (this.enableLiveValidation = !1, t = [], $.each(this.errors, function (n, i) {
				var r, o;
				return r = e.form.find("[data-canonical*='" + n + "']"), r.length ? (o = r.data("fieldname") + " " + i[0], t.push(o), e.failedValidation(r, o)) : void 0
			}), this.populateErrorsList(t), this.navigation.jumpToId("address")) : void 0
		}, t.prototype.populateErrorsList = function (t) {
			var e, n, i;
			return t.length ? (this.form.find(".errors").toggleClass("active", !0), i = 1 === t.length && "was" || "were", n = 1 === t.length && "error" || "errors", this.form.find(".error-count-message").text("There " + i + " " + t.length + " " + n + " on this form:"), e = this.form.find(".error-list").empty(), $.each(t, function () {
				return e.append("<li>" + this + "</li>")
			})) : this.form.find(".errors").toggleClass("active", !1)
		}, t.prototype.submitButton = function () {
			return this.form.find('[data-nav="submit"], [type="submit"]')
		}, t.prototype.validate = function () {
			var t, e = this;
			return t = [], this.form.find("[data-validate]").each(function (n, i) {
				var r, o;
				return o = $(i).data("validate").split(" "), r = !0, $.each(o, function (n, o) {
					return null != e.validators[o] ? r = r && e.validators[o].call($(i), t, e) : void 0
				}), r ? $(i).parents("label").removeClass("error") : void 0
			}), this.populateErrorsList(t), !t.length
		}, t.prototype.failedValidation = function (t, e) {
			return "function" == typeof t.parents ? t.parents("label").addClass("error").find(".error-message").text(e) : void 0
		}, t.prototype.validators = {
			nonZero: function (t, e) {
				var n;
				return $.trim(this.val()).length ? !0 : (n = "Please enter your " + this.data("fieldname"), t.push(n), e.failedValidation(this, n), !1)
			}, postalCode: function (t, e) {
				var n;
				return e.requirePostalCode ? /^([a-zA-Z0-9 -]{2,})$/.test(this.val()) ? !0 : (n = "Please enter a valid " + this.data("fieldname"), t.push(n), e.failedValidation(this, n), !1) : !0
			}, phoneNumber: function (t, e) {
				var n;
				return this.val().replace(/[^0-9]+/g, "").length > 6 ? !0 : (n = "Please enter a valid phone number", t.push(n), e.failedValidation(this, n), !1)
			}
		}, t
	}(), null == window.ShopifySignup && (window.ShopifySignup = {}), ShopifySignup.DetailsForm = t
}.call(this), function () {
	"use strict";
	function t(e, i) {
		function r(t, e) {
			return function () {
				return t.apply(e, arguments)
			}
		}

		var o;
		if (i = i || {}, this.trackingClick = !1, this.trackingClickStart = 0, this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, this.touchBoundary = i.touchBoundary || 10, this.layer = e, this.tapDelay = i.tapDelay || 200, !t.notNeeded(e)) {
			for (var a = ["onMouse", "onClick", "onTouchStart", "onTouchMove", "onTouchEnd", "onTouchCancel"], s = this, u = 0, c = a.length; c > u; u++)s[a[u]] = r(s[a[u]], s);
			n && (e.addEventListener("mouseover", this.onMouse, !0), e.addEventListener("mousedown", this.onMouse, !0), e.addEventListener("mouseup", this.onMouse, !0)), e.addEventListener("click", this.onClick, !0), e.addEventListener("touchstart", this.onTouchStart, !1), e.addEventListener("touchmove", this.onTouchMove, !1), e.addEventListener("touchend", this.onTouchEnd, !1), e.addEventListener("touchcancel", this.onTouchCancel, !1), Event.prototype.stopImmediatePropagation || (e.removeEventListener = function (t, n, i) {
				var r = Node.prototype.removeEventListener;
				"click" === t ? r.call(e, t, n.hijacked || n, i) : r.call(e, t, n, i)
			}, e.addEventListener = function (t, n, i) {
				var r = Node.prototype.addEventListener;
				"click" === t ? r.call(e, t, n.hijacked || (n.hijacked = function (t) {
					t.propagationStopped || n(t)
				}), i) : r.call(e, t, n, i)
			}), "function" == typeof e.onclick && (o = e.onclick, e.addEventListener("click", function (t) {
				o(t)
			}, !1), e.onclick = null)
		}
	}

	var e = navigator.userAgent.indexOf("Windows Phone") >= 0, n = navigator.userAgent.indexOf("Android") > 0 && !e, i = /iP(ad|hone|od)/.test(navigator.userAgent) && !e, r = i && /OS 4_\d(_\d)?/.test(navigator.userAgent), o = i && /OS ([6-9]|\d{2})_\d/.test(navigator.userAgent), a = navigator.userAgent.indexOf("BB10") > 0;
	t.prototype.needsClick = function (t) {
		switch (t.nodeName.toLowerCase()) {
			case"button":
			case"select":
			case"textarea":
				if (t.disabled)return !0;
				break;
			case"input":
				if (i && "file" === t.type || t.disabled)return !0;
				break;
			case"label":
			case"iframe":
			case"video":
				return !0
		}
		return /\bneedsclick\b/.test(t.className)
	}, t.prototype.needsFocus = function (t) {
		switch (t.nodeName.toLowerCase()) {
			case"textarea":
				return !0;
			case"select":
				return !n;
			case"input":
				switch (t.type) {
					case"button":
					case"checkbox":
					case"file":
					case"image":
					case"radio":
					case"submit":
						return !1
				}
				return !t.disabled && !t.readOnly;
			default:
				return /\bneedsfocus\b/.test(t.className)
		}
	}, t.prototype.sendClick = function (t, e) {
		var n, i;
		document.activeElement && document.activeElement !== t && document.activeElement.blur(), i = e.changedTouches[0], n = document.createEvent("MouseEvents"), n.initMouseEvent(this.determineEventType(t), !0, !0, window, 1, i.screenX, i.screenY, i.clientX, i.clientY, !1, !1, !1, !1, 0, null), n.forwardedTouchEvent = !0, t.dispatchEvent(n)
	}, t.prototype.determineEventType = function (t) {
		return n && "select" === t.tagName.toLowerCase() ? "mousedown" : "click"
	}, t.prototype.focus = function (t) {
		var e;
		i && t.setSelectionRange && 0 !== t.type.indexOf("date") && "time" !== t.type && "month" !== t.type ? (e = t.value.length, t.setSelectionRange(e, e)) : t.focus()
	}, t.prototype.updateScrollParent = function (t) {
		var e, n;
		if (e = t.fastClickScrollParent, !e || !e.contains(t)) {
			n = t;
			do {
				if (n.scrollHeight > n.offsetHeight) {
					e = n, t.fastClickScrollParent = n;
					break
				}
				n = n.parentElement
			} while (n)
		}
		e && (e.fastClickLastScrollTop = e.scrollTop)
	}, t.prototype.getTargetElementFromEventTarget = function (t) {
		return t.nodeType === Node.TEXT_NODE ? t.parentNode : t
	}, t.prototype.onTouchStart = function (t) {
		var e, n, o;
		if (t.targetTouches.length > 1)return !0;
		if (e = this.getTargetElementFromEventTarget(t.target), n = t.targetTouches[0], i) {
			if (o = window.getSelection(), o.rangeCount && !o.isCollapsed)return !0;
			if (!r) {
				if (n.identifier && n.identifier === this.lastTouchIdentifier)return t.preventDefault(), !1;
				this.lastTouchIdentifier = n.identifier, this.updateScrollParent(e)
			}
		}
		return this.trackingClick = !0, this.trackingClickStart = t.timeStamp, this.targetElement = e, this.touchStartX = n.pageX, this.touchStartY = n.pageY, t.timeStamp - this.lastClickTime < this.tapDelay && t.preventDefault(), !0
	}, t.prototype.touchHasMoved = function (t) {
		var e = t.changedTouches[0], n = this.touchBoundary;
		return Math.abs(e.pageX - this.touchStartX) > n || Math.abs(e.pageY - this.touchStartY) > n ? !0 : !1
	}, t.prototype.onTouchMove = function (t) {
		return this.trackingClick ? ((this.targetElement !== this.getTargetElementFromEventTarget(t.target) || this.touchHasMoved(t)) && (this.trackingClick = !1, this.targetElement = null), !0) : !0
	}, t.prototype.findControl = function (t) {
		return void 0 !== t.control ? t.control : t.htmlFor ? document.getElementById(t.htmlFor) : t.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea")
	}, t.prototype.onTouchEnd = function (t) {
		var e, a, s, u, c, d = this.targetElement;
		if (!this.trackingClick)return !0;
		if (t.timeStamp - this.lastClickTime < this.tapDelay)return this.cancelNextClick = !0, !0;
		if (this.cancelNextClick = !1, this.lastClickTime = t.timeStamp, a = this.trackingClickStart, this.trackingClick = !1, this.trackingClickStart = 0, o && (c = t.changedTouches[0], d = document.elementFromPoint(c.pageX - window.pageXOffset, c.pageY - window.pageYOffset) || d, d.fastClickScrollParent = this.targetElement.fastClickScrollParent), s = d.tagName.toLowerCase(), "label" === s) {
			if (e = this.findControl(d)) {
				if (this.focus(d), n)return !1;
				d = e
			}
		} else if (this.needsFocus(d))return t.timeStamp - a > 100 || i && window.top !== window && "input" === s ? (this.targetElement = null, !1) : (this.focus(d), this.sendClick(d, t), i && "select" === s || (this.targetElement = null, t.preventDefault()), !1);
		return i && !r && (u = d.fastClickScrollParent, u && u.fastClickLastScrollTop !== u.scrollTop) ? !0 : (this.needsClick(d) || (t.preventDefault(), this.sendClick(d, t)), !1)
	}, t.prototype.onTouchCancel = function () {
		this.trackingClick = !1, this.targetElement = null
	}, t.prototype.onMouse = function (t) {
		return this.targetElement ? t.forwardedTouchEvent ? !0 : t.cancelable && (!this.needsClick(this.targetElement) || this.cancelNextClick) ? (t.stopImmediatePropagation ? t.stopImmediatePropagation() : t.propagationStopped = !0, t.stopPropagation(), t.preventDefault(), !1) : !0 : !0
	}, t.prototype.onClick = function (t) {
		var e;
		return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, !0) : "submit" === t.target.type && 0 === t.detail ? !0 : (e = this.onMouse(t), e || (this.targetElement = null), e)
	}, t.prototype.destroy = function () {
		var t = this.layer;
		n && (t.removeEventListener("mouseover", this.onMouse, !0), t.removeEventListener("mousedown", this.onMouse, !0), t.removeEventListener("mouseup", this.onMouse, !0)), t.removeEventListener("click", this.onClick, !0), t.removeEventListener("touchstart", this.onTouchStart, !1), t.removeEventListener("touchmove", this.onTouchMove, !1), t.removeEventListener("touchend", this.onTouchEnd, !1), t.removeEventListener("touchcancel", this.onTouchCancel, !1)
	}, t.notNeeded = function (t) {
		var e, i, r;
		if ("undefined" == typeof window.ontouchstart)return !0;
		if (i = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1]) {
			if (!n)return !0;
			if (e = document.querySelector("meta[name=viewport]")) {
				if (-1 !== e.content.indexOf("user-scalable=no"))return !0;
				if (i > 31 && document.documentElement.scrollWidth <= window.outerWidth)return !0
			}
		}
		if (a && (r = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/), r[1] >= 10 && r[2] >= 3 && (e = document.querySelector("meta[name=viewport]")))) {
			if (-1 !== e.content.indexOf("user-scalable=no"))return !0;
			if (document.documentElement.scrollWidth <= window.outerWidth)return !0
		}
		return "none" === t.style.msTouchAction ? !0 : !1
	}, t.attach = function (e, n) {
		return new t(e, n)
	}, "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function () {
		return t
	}) : "undefined" != typeof module && module.exports ? (module.exports = t.attach, module.exports.FastClick = t) : window.FastClick = t
}(), function () {
	var t;
	$(function () {
		return FastClick.attach(document.body)
	}), window.ShopifySignup.init = function () {
		return $("html").hasClass("oldie") ? $(".js-unsupported-override").on("click", function () {
			return $("html").removeClass("oldie"), t()
		}) : t()
	}, t = function () {
		return window.ShopifySignup.waitScreen = new ShopifySignup.WaitScreen, $(".signup-page-wrapper").length ? window.ShopifySignup.signupForm = new ShopifySignup.SignupForm($(".signup-page-wrapper"), window.ShopifySignup.waitScreen, new ShopifySignup.ShopAvailablityChecker) : $(".wait-page-wrapper").length ? ShopifySignup.waitScreen.init() : window.ShopifySignup.detailsForm = new ShopifySignup.DetailsForm($(".tenfield .card-wrapper"), ShopifySignup.errors)
	}
}.call(this);