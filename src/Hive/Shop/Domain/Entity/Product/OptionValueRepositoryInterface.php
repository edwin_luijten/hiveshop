<?php namespace Shop\Domain\Entity\Product;

use Shop\Infrastructure\Repositories\RepositoryInterface;

interface OptionValueRepositoryInterface extends RepositoryInterface
{

}
