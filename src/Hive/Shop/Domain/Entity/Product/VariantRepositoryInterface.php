<?php namespace Shop\Domain\Entity\Product;

use Shop\Infrastructure\Repositories\RepositoryInterface;

interface VariantRepositoryInterface extends RepositoryInterface
{

}
