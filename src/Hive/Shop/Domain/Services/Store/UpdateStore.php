<?php namespace Shop\Domain\Services\Store;

use Doctrine\ORM\EntityManager;
use Illuminate\Support\Str;
use Shop\Domain\Entity\Geo\Address;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Store\StoreId;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;
use Shop\Domain\Services\AbstractUpdaterService;
use Shop\Domain\Services\Geo\CreateAddress;
use Shop\Domain\Services\ServiceFactory;
use Shop\Domain\Services\ServiceInterface;

class UpdateStore extends AbstractUpdaterService implements ServiceInterface
{
    protected $store;

    private $entityManager;
    private $storeRepository;
    private $countryRepository;

    public function __construct(
        EntityManager $entityManager,
        StoreRepositoryInterface $storeRepository,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->entityManager = $entityManager;
        $this->storeRepository = $storeRepository;
        $this->countryRepository = $countryRepository;
    }

    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param \Shop\Domain\Entity\Store\StoreId $storeId
     * @param $name
     * @param null $aPhone
     * @param null $aStreet
     * @param null $aCountry
     * @param null $aLegalName
     * @param null $aCity
     * @param null $aPostal
     * @param null $sTimezone
     * @param null $sCurrency
     * @param null $sWeightUnit
     * @param null $sAccountEmail
     * @param null $sCustomerEmail
     * @param null $sUnitSystem
     * @param null $sOrderIdPrefix
     * @param null $sOrderIdSuffix
     *
     * @return $this
     */
    public function update(
        StoreId $storeId,
        $name,
        $aPhone = null,
        $aStreet = null,
        $aCountry = null,
        $aLegalName = null,
        $aCity = null,
        $aPostal = null,
        $sTimezone = null,
        $sCurrency = null,
        $sWeightUnit = null,
        $sAccountEmail = null,
        $sCustomerEmail = null,
        $sUnitSystem = null,
        $sOrderIdPrefix = null,
        $sOrderIdSuffix = null
    ) {
        $store = $this->storeRepository->getById($storeId->toString());
        $store->setName($name);

        // Address
        $country = $this->countryRepository->getByCode($aCountry);
        $address = $store->getAddress();

        if (!is_null($aCountry)) {
            $address->setCountry($country);
        }

        $address->setLegalName($aLegalName);
        $address->setPhone($aPhone);
        $address->setStreet($aStreet);
        $address->setCity($aCity);
        $address->setPostal($aPostal);

        // Settings
        $settings = $store->getSettings();
        $settings->setTimezone($sTimezone);
        $settings->setCurrency((is_null($sCurrency) ? $country->getCurrency() : $sCurrency));
        $settings->setWeightUnit($sWeightUnit);
        $settings->setAccountEmail($sAccountEmail);
        $settings->setCustomerEmail($sCustomerEmail);
        $settings->setUnitSystem((is_null($sUnitSystem) ? $country->getUnitSystem() : $sUnitSystem));
        $settings->setOrderIdPrefix($sOrderIdPrefix);
        $settings->setOrderIdSuffix($sOrderIdSuffix);

        $this->store = $store;

        return $this;
    }

    public function run()
    {
        $store = $this->getStore();

        $this->entityManager->merge($store);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        $store = $this->getStore();

        $this->entityManager->detach($store);

        return $store;
    }
}
