<?php namespace Shop\Infrastructure\Transformers;

use League\Fractal\TransformerAbstract;
use Shop\Domain\Entity\Product\Option;

class OptionTransformer extends TransformerAbstract
{
    public function transform(Option $option)
    {
        return [
            'id'              => $option->getId()->toString(),
            'name'            => $option->getName(),
        ];
    }
}
