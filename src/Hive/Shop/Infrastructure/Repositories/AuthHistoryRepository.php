<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\History\AuthHistory;
use Shop\Domain\Entity\History\AuthHistoryRepositoryInterface;

class AuthHistoryRepository extends AbstractBaseRepository implements AuthHistoryRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return AuthHistoryId::generate();
    }
    
    public function getEntity()
    {
        return new AuthHistory();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('a')->from(AuthHistory::class, 'a')
            ->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        //@todo implement getPaginated
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('a')->from(AuthHistory::class, 'a')
            ->where(
                $queryBuilder->expr()->eq('a.id', ':id')
            )
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Get last login of an account
     *
     * @param $id
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getLastLogin($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('a')->from(AuthHistory::class, 'a')
            ->where(
                $queryBuilder->expr()->eq('a.account', ':id')
            )
            ->orderBy('a.created_at', 'DESC')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param $id
     * @param integer $limit
     *
     * @return mixed
     */
    public function getByAccountId($id, $limit = 5)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('a')->from(AuthHistory::class, 'a')
            ->where(
                $queryBuilder->expr()->eq('a.account', ':id')
            )
            ->setParameter('id', $id)
            ->orderBy('a.createdAt', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->execute();
    }

    public function getByStoreId($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('a')->from(AuthHistory::class, 'a')
            ->where(
                $queryBuilder->expr()->eq('a.store', ':id')
            )
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    public function getCount()
    {
        //@todo implement getCount()
    }
}
