<?php namespace App\Http\Controllers\Shop\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStoreDetailsRequest;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\UrlGenerator;
use JMS\Serializer\Serializer;
use Omnipay\Omnipay;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Plan\PlanRepositoryInterface;
use Shop\Domain\Services\ServiceFactory;
use Shop\Domain\Services\Geo\UpdateAddress;
use Shop\Domain\Services\Store\UpdateSetting;

class PlanController extends Controller
{
    private $response;
    private $urlGenerator;
    private $guard;
    private $planRepository;

    public function __construct(
        Response $response,
        UrlGenerator $urlGenerator,
        Guard $guard,
        PlanRepositoryInterface $planRepository
    ) {
        $this->response = $response;
        $this->urlGenerator = $urlGenerator;
        $this->guard = $guard;
        $this->planRepository = $planRepository;
    }

    public function getPlan(Request $request)
    {
        return view('shop.admin.pages.settings-account-plan', [
            'plans' => $this->planRepository->getAll()
        ]);
    }

    public function postPlan()
    {
        $gateway = Omnipay::create('PayPro');
        $gateway->initialize([
            'apiKey' => env('PAYPRO_API_KEY'),
            'productId' => null,
            'testMode' => env('APP_DEBUG'),
        ]);
    }
}
