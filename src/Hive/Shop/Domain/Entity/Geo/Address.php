<?php namespace Shop\Domain\Entity\Geo;

use Assert\Assertion;
use Shop\Domain\AggregateRoot;
use Shop\Domain\Entity\Account\Account;
use Shop\Domain\Entity\Store\Store;

class Address implements AggregateRoot
{
    /**
     * @var AddressId
     */
    private $id;

    /**
     * @var string
     */
    private $legalName;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $postal;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var \Shop\Domain\Entity\Store\Store
     */
    private $store;

    /**
     * @var \Shop\Domain\Entity\Account\Account
     */
    private $account;

    /**
     * @return AddressId
     */
    public function getId()
    {
        return AddressId::fromString($this->id);
    }

    /**
     * @param AddressId $id
     */
    public function setId(AddressId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return string
     */
    public function getLegalName()
    {
        return $this->legalName;
    }

    /**
     * @param string $legalName
     */
    public function setLegalName($legalName)
    {
        $this->legalName = $legalName;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPostal()
    {
        return $this->postal;
    }

    /**
     * @param string $postal
     */
    public function setPostal($postal)
    {
        $this->postal = $postal;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
    }

    /**
     * @return \Shop\Domain\Entity\Store\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param \Shop\Domain\Entity\Store\Store $store
     */
    public function setStore(Store $store)
    {
        $this->store = $store;
    }

    /**
     * @return \Shop\Domain\Entity\Account\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param \Shop\Domain\Entity\Account\Account $account
     */
    public function setAccount(Account $account)
    {
        $this->account = $account;
    }
}
