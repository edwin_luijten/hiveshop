<?php namespace Shop\Domain\Services\Store;

use Doctrine\ORM\EntityManager;
use Illuminate\Support\Str;
use Shop\Domain\Entity\Geo\Address;
use Shop\Domain\Entity\Geo\AddressRepositoryInterface;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Store\Setting;
use Shop\Domain\Entity\Store\SettingRepositoryInterface;
use Shop\Domain\Entity\Store\Store;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;

class CreateStore extends AbstractCreatorService implements ServiceInterface
{
    private $entityManager;
    private $storeRepository;
    private $addressRepository;
    private $settingRepository;
    private $countryRepository;
    protected $store;

    public function __construct(
        EntityManager $entityManager,
        StoreRepositoryInterface $storeRepository,
        AddressRepositoryInterface $addressRepository,
        SettingRepositoryInterface $settingRepository,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->entityManager = $entityManager;
        $this->storeRepository = $storeRepository;
        $this->addressRepository = $addressRepository;
        $this->settingRepository = $settingRepository;
        $this->countryRepository = $countryRepository;
    }

    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param string $name
     *
     * @param null $aPhone
     * @param null $aStreet
     * @param null $aCountry
     * @param null $aLegalName
     * @param null $aCity
     * @param null $aPostal
     * @param null $sTimezone
     * @param null $sCurrency
     * @param null $sWeightUnit
     * @param null $sAccountEmail
     * @param null $sCustomerEmail
     * @param null $sUnitSystem
     * @param null $sOrderIdPrefix
     * @param null $sOrderIdSuffix
     *
     * @return \Shop\Domain\Entity\Product\Store
     */
    public function create(
        $name,
        $aPhone = null,
        $aStreet = null,
        $aCountry = null,
        $aLegalName = null,
        $aCity = null,
        $aPostal = null,
        $sTimezone = null,
        $sCurrency = null,
        $sWeightUnit = null,
        $sAccountEmail = null,
        $sCustomerEmail = null,
        $sUnitSystem = null,
        $sOrderIdPrefix = null,
        $sOrderIdSuffix = null
    )
    {
        $store = new Store();
        $store->setId($this->storeRepository->nextUuid());
        $store->setName($name);
        $store->setSubdomain(Str::slug($name));

        // Address
        $address = new Address();
        $address->setId($this->addressRepository->nextUuid());

        if (!is_null($aCountry)) {
            $address->setCountry($this->countryRepository->getByCode($aCountry));
        }

        $address->setLegalName($aLegalName);
        $address->setPhone($aPhone);
        $address->setStreet($aStreet);
        $address->setCity($aCity);
        $address->setPostal($aPostal);

        $store->setAddress($address);

        // Settings
        $settings = new Setting();
        $settings->setId($this->settingRepository->nextUuid());
        $settings->setTimezone($sTimezone);
        $settings->setCurrency(!is_null($sCurrency) ? $sCurrency : $address->getCountry()->getCurrency());
        $settings->setWeightUnit($sWeightUnit);
        $settings->setAccountEmail($sAccountEmail);
        $settings->setCustomerEmail($sCustomerEmail);
        $settings->setUnitSystem(!is_null($sUnitSystem) ? $sUnitSystem : $address->getCountry()->getUnitSystem());
        $settings->setOrderIdPrefix($sOrderIdPrefix);
        $settings->setOrderIdSuffix($sOrderIdSuffix);

        $store->setSettings($settings);

        $this->store = $store;

        return $this;
    }

    public function run()
    {
        $store = $this->getStore();

        // Store the store
        $this->entityManager->persist($store);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getStore();
    }
}
