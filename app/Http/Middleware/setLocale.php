<?php

namespace App\Http\Middleware;

use Closure;
use CommerceGuys\Intl\Currency\CurrencyRepository;
use Illuminate\Contracts\Auth\Guard;
use Locale;

class setLocale
{

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Locale::setDefault(config('app.locale'));
        setlocale(LC_ALL, config('app.locale'));

        $settings = new \stdClass();
        $settings->locale = config('app.locale');

        if ($this->auth->check()) {
            $storeSettings = $this->auth->user()->getStore()->getSettings();

            $currencyRepository = new CurrencyRepository();
            $currency = $currencyRepository->get($storeSettings->getCurrency());

            $settings->currency = $storeSettings->getCurrency();
            $settings->currencySymbol = $currency->getSymbol();
            $settings->weightUnit = $storeSettings->getWeightUnit();
            $settings->timezone = $storeSettings->getTimezone();
            $settings->unitSystem = $storeSettings->getUnitSystem();
        }

        view()->share('localeSettings', $settings);
        $request->session()->put('localeSettings', $settings);

        return $next($request);
    }
}
