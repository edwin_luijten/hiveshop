<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;
use Illuminate\Contracts\Cache\Factory;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Shop\Domain\Entity\Product\Product;
use Shop\Domain\Entity\Product\ProductId;
use Shop\Domain\Entity\Product\ProductRepositoryInterface;

class ProductRepository extends AbstractBaseRepository implements ProductRepositoryInterface
{

    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return ProductId::generate();
    }

    public function getEntity()
    {
        return new Product();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('p', 'po', 'pv', 'pvo')->from(Product::class, 'p')
            ->leftJoin('p.options', 'po')
            ->leftJoin('p.variants', 'pv')
            ->innerJoin('pv.optionValues', 'pvo');

        return $queryBuilder->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function getPaginated($page = 1, $limit = 15)
    {
        if ($limit > $this->maxResultsPerPage) {
            $limit = $this->maxResultsPerPage;
        }

        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('p', 'po', 'pov')->from(Product::class, 'p')
            ->leftJoin('p.options', 'po')
            ->leftJoin('p.optionValues', 'pov')
        ;

        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pager = new Pagerfanta($adapter);
        $pager->setCurrentPage($page);
        $pager->setMaxPerPage($limit);

        return $pager;
    }

    /**
     * @param int $limit
     *
     * @return array
     */
    public function getRandomItems($limit = 15)
    {
        if ($limit > $this->maxResultsPerPage) {
            $limit = $this->maxResultsPerPage;
        }

        //Get the number of rows from your table
        $rows = $this->entityManager->createQuery('SELECT COUNT(p.id) FROM ' . Product::class . ' p')
            ->getSingleScalarResult();

        $offset = max(0, rand(0, $rows - $limit - 1));

        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('DISTINCT p', 'pv')->from(Product::class, 'p')
            ->leftJoin('p.variants', 'pv')
            ->groupBy('p.id')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        return $queryBuilder->getQuery()->execute();
    }


    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('p', 'po', 'pov', 'pv')->from(Product::class, 'p')
            ->where(
                $queryBuilder->expr()->eq('p.id', ':id')
            )->leftJoin('p.optionValues', 'pov')
            ->leftJoin('p.options', 'po')
            ->leftJoin('p.variants', 'pv')

            ->setParameter('id', $id);

        return $queryBuilder->getQuery()->getSingleResult();
    }

    public function getBySlug($slug)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('p', 'po', 'pv', 'pov')->from(Product::class, 'p')
            ->where(
                $queryBuilder->expr()->eq('p.slug', ':slug')
            )
            ->leftJoin('p.optionValues', 'pov')
            ->leftJoin('p.options', 'po')
            ->leftJoin('p.variants', 'pv')

            ->setParameter('slug', $slug)
        ;
        return $queryBuilder->getQuery()->getFirstResult();
    }

    public function getCount()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('count(p.id)')->from(Product::class, 'p');

        return (integer) $queryBuilder->getQuery()->getSingleScalarResult();
    }
}
