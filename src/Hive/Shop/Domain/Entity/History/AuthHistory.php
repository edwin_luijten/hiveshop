<?php namespace Shop\Domain\Entity\History;

use Shop\Domain\Entity\Account\Account;
use Shop\Domain\Entity\Store\Store;
use Src\Doctrine\Traits\Timestamps;

class AuthHistory
{

    use Timestamps;

    /**
     * @var AuthHistoryId;
     */
    private $id;
    /**
     * @var string
     */
    private $ip;
    /**
     * @var string
     */
    private $isp;
    /**
     * @var string
     */
    private $location;
    /**
     * @var string
     */
    private $browser;
    /**
     * @var string
     */
    private $os;
    /**
     * @var \Shop\Domain\Entity\Account\Account
     */
    private $account;
    /**
     * @var \Shop\Domain\Entity\Store\Store
     */
    private $store;

    /**
     * @return AuthHistoryId
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param AuthHistoryId $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getIsp()
    {
        return $this->isp;
    }

    /**
     * @param string $isp
     */
    public function setIsp($isp)
    {
        $this->isp = $isp;
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getBrowser()
    {
        return $this->browser;
    }

    /**
     * @param string $browser
     */
    public function setBrowser($browser)
    {
        $this->browser = $browser;
    }

    /**
     * @return string
     */
    public function getOs()
    {
        return $this->os;
    }

    /**
     * @param string $os
     */
    public function setOs($os)
    {
        $this->os = $os;
    }

    /**
     * @return \Shop\Domain\Entity\Account\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param \Shop\Domain\Entity\Account\Account $account
     */
    public function setAccount(Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return \Shop\Domain\Entity\Store\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param \Shop\Domain\Entity\Store\Store $store
     */
    public function setStore(Store $store)
    {
        $this->store = $store;
    }
}