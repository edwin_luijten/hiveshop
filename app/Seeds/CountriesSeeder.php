<?php namespace App\Seeds;

use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Services\Geo\CreateCountry;

class CountriesSeeder
{

    private $countryRepository;
    private $createCountry;

    public function __construct(CountryRepositoryInterface $countryRepository, CreateCountry $createCountry)
    {
        $this->countryRepository = $countryRepository;
        $this->createCountry = $createCountry;
    }

    public function seed()
    {
        $countries = json_decode(file_get_contents(storage_path('app/countriesDB.json')), true);

        if (!empty($countries)) {
            foreach ($countries as $country) {
                $this->createCountry->create(
                    $country['name'],
                    $country['code'],
                    $country['tax'],
                    array_get($country, 'tax_name'),
                    $country['currency'],
                    $country['unit_system'],
                    array_get($country, 'zone_label'),
                    array_get($country, 'zone_key'),
                    array_get($country, 'group'),
                    array_get($country, 'example_zip'),
                    array_get($country, 'zones', []),
                    array_get($country, 'enabled', false)
                )->run()->flush();
            }


        }
    }
}