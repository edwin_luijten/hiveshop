<?php namespace App\Console\Commands;

use Shop\Domain\Services\Seeds\ProductSeeder;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Stopwatch\Stopwatch;

class ProductsSeederCommand extends BaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'seeder:products';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the products table with 100 products each time this command is run.';

    /**
     * The Entity Manager
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $productSeeder;

    public function __construct(ProductSeeder $productSeeder)
    {
        parent::__construct();

        $this->productSeeder = $productSeeder;
    }

    public function fire()
    {
        $amount = (is_null($this->option('amount')) ? 100 : $this->option('amount'));

        $stopwatch = new Stopwatch();
        $stopwatch->start('productSeeding');

        $this->info('Seeding ' . $amount . ' products');

        $products = $this->productSeeder->seed($amount);

        $event = $stopwatch->stop('productSeeding');

        $this->info('Done. ' . count($products) . ' products added.');
        $this->info(
            PHP_EOL . 'Stopwatch: ' . PHP_EOL .
            'Duration: ' . $this->formatDuration($event->getDuration()) . PHP_EOL .
            'Memory used: ' . $this->formatMemory($event->getMemory())
        );
    }

    protected function getArguments()
    {
        return [

        ];
    }

    protected function getOptions()
    {
        return [
            ['amount', 100, InputOption::VALUE_OPTIONAL, 'Amount of products to seed, default 100']
        ];
    }
}