<?php
if (!function_exists('get_subdomain'))
{
    /**
     * Get the first subdomain from a given url
     *
     * @param $url
     *
     * @return mixed
     */
    function get_subdomain($url = null)
    {
        if(is_null($url))
        {
            $url = current_page_url();
        }

        preg_match('/(?:http[s]*\:\/\/)*(.*?)\.(?=[^\/]*\..{2,5})/i', $url, $match);

        return $match[1];
    }
}

if (!function_exists('current_page_url'))
{
    function current_page_url()
    {
        $pageURL = 'http';
        if (isset($_SERVER['HTTPS']) && $_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
}
if (! function_exists('is_active_url')) {
    /**
     * Check if url is active
     *
     * @param        $path
     * @param string $active
     *
     * @return string
     */
    function is_active_url($path, $active = 'active')
    {
        return call_user_func_array('\Illuminate\Support\Facades\Request::is', (array) $path) ? $active : '';
    }
}

function route_contains($string)
{
    return strstr(Route::currentRouteName(), $string);
}

if (!function_exists('object_get')) {
    /**
     * Get an item from an object
     *
     * @param  object   $object
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function object_get($object, $key, $default = null)
    {
        if (is_null($key) && !is_object($object)) {
            return $object;
        }

        if (isset($object->{$key})) {
            return $object->{$key};
        }

        return $default;
    }
}

if(!function_exists('trial_countdown')) {
    /**
     * Get remaining days for a trial period
     *
     * @param $createdAt
     * @param int $trialDuration
     * @return int
     */
    function trial_countdown($createdAt, $trialDuration = 14)
    {
        $now = \Carbon\Carbon::now()->startOfDay();
        $end = \Carbon\Carbon::parse($createdAt->format('Y-m-d'))->endOfDay()->addDays($trialDuration);
        $diff = $now->diffInDays($end);

        return $diff;
    }
}

if(!function_exists('subscription_countdown')) {
    /**
     * Get remaining days for a subscription period
     *
     * @param $startDate
     * @param $endDate
     * @return int
     */
    function subscription_countdown($startDate, $endDate)
    {
        $start = \Carbon\Carbon::parse($startDate->format('Y-m-d'))->startOfDay();
        $end = \Carbon\Carbon::parse($endDate->format('Y-m-d'))->endOfDay();
        $diff = $start->diffInDays($end);

        return $diff;
    }
}

if(!function_exists('date_for_human'))
{
    function date_for_human($date, $treshHold = null)
    {
        $carbon = \Carbon\Carbon::parse($date);

        if (!is_null($treshHold)) {
            $now = \Carbon\Carbon::now();

            if ($now->diffInDays($carbon) >= $treshHold) {
                return $carbon->format('d-m-Y \a\t H:i:s');
            }
        }

        return $carbon->diffForHumans() . ' ' . sprintf(_('at %s'), $carbon->format('H:i:s'));
    }
}