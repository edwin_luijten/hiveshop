@extends('hiveshop.template')

@section('content')

<section>
    <header class="text-center">
        <div class="container page-section">
            <h1>Try Hiveshop for 14 days free, with no risk</h1>

            <p>
                After your trial ends, pick a plan that offers the features and credit card rate you want.
            </p>

            <button class="btn btn-lg btn-success" name="button" type="button">Start your free trial</button>
        </div>
    </header>

    <div class="container">
        <div class="page-section">

            <div class="grid row">
                @foreach($plans as $plan)
                    @if($plan->getName() !== 'Starter')
                        <div class="pricing-item grid-item col-md-4">
                            <header>
                                @if($plan->getHighlight())
                                    <div class="pricing-item-popular">Most popular</div>
                                @endif
                                <h3>{{ $plan->getName() }}</h3>

                                <div class="pricing-item-price">
                                    <sup>{{ $plan->getPrice()->getSymbol() }}</sup>
                                    {{ $plan->getPrice()->formatter(['places' => 0])->getDecimal() }}
                                    <span>/pm</span>
                                </div>

                                <h4>Credit card rate</h4>
                                <ul>
                                    <li>
                                        Online: {{ $plan->getOnlineFeePercentage() }}% + {{ (!is_null($plan->getOnlineFeePrice()) ? $plan->getOnlineFeePrice()->getFormatted() :  $plan->getPrice()->getSymbol() . '0'  ) }}
                                    </li>
                                    <li>
                                        In Person: {{ $plan->getInPersonFeePercentage() }}% + {{ (!is_null($plan->getInPersonFeePrice()) ? $plan->getInPersonFeePrice()->getFormatted() : $plan->getPrice()->getSymbol() . '0' ) }}
                                    </li>
                                </ul>
                                <h4>Features</h4>
                                <ul>
                                    @foreach($features as $feature)

                                        @if($feature->getType() === 'disk-space' || $feature->getType() === 'product-limit')
                                            @foreach($plan->getFeatures() as $planFeature)
                                                @if($planFeature->getId() === $feature->getId())
                                                    <li>{{ $feature->getName() }}</li>
                                                @endif
                                            @endforeach
                                        @else
                                            <li>
                                                @if(!$plan->hasFeature($feature))
                                                    <s class="text-muted">{{ $feature->getName() }}</s>
                                                @else
                                                    {{ $feature->getName() }}
                                                @endif
                                            </li>
                                        @endif

                                    @endforeach
                                </ul>
                            </header>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>

        <div class="page-section text-center">
            <h2>All of our plans include these great features</h2>

            <div class="row">
                <div class="col-md-4">
                    <img src="/assets/h/img/products-small.jpg" /><br/>
                    Unlimited products
                </div>
                <div class="col-md-4">
                    <img src="/assets/h/img/support-small.jpg" /><br/>
                    27/7 Support
                </div>
                <div class="col-md-4">
                    <img src="/assets/h/img/discounts-small.jpg" /><br/>
                    Discount code engine
                </div>
            </div>

            <div class="row page-section">
                Looking for something simple to get started?
                <div class="col-md-6 col-md-offset-3 pricing-item text-center">
                    @foreach($plans as $plan)
                        @if($plan->getName() === 'Starter')

                            <header>
                                @if($plan->getHighlight())
                                    <div class="pricing-item-popular">Most popular</div>
                                @endif
                                <h3>{{ $plan->getName() }}</h3>

                                <div class="pricing-item-price">
                                    <sup>{{ $plan->getPrice()->getSymbol() }}</sup>
                                    {{ $plan->getPrice()->formatter(['places' => 0])->getDecimal() }}
                                    <span>/pm</span>
                                </div>

                                <h4>Credit card rate</h4>
                                <ul>
                                    <li>
                                        Online: {{ $plan->getOnlineFeePercentage() }}% + {{ (!is_null($plan->getOnlineFeePrice()) ? $plan->getOnlineFeePrice()->getFormatted() :  $plan->getPrice()->getSymbol() . '0'  ) }}
                                    </li>
                                    <li>
                                        In Person: {{ $plan->getInPersonFeePercentage() }}% + {{ (!is_null($plan->getInPersonFeePrice()) ? $plan->getInPersonFeePrice()->getFormatted() : $plan->getPrice()->getSymbol() . '0' ) }}
                                    </li>
                                </ul>
                                <h4>Features</h4>
                                <ul>
                                    @foreach($features as $feature)

                                        @if($feature->getType() === 'disk-space' || $feature->getType() === 'product-limit')
                                            @foreach($plan->getFeatures() as $planFeature)
                                                @if($planFeature->getId() === $feature->getId())
                                                    <li>{{ $feature->getName() }}</li>
                                                @endif
                                            @endforeach
                                        @else
                                            <li>
                                                @if(!$plan->hasFeature($feature))
                                                    <s class="text-muted">{{ $feature->getName() }}</s>
                                                @else
                                                    {{ $feature->getName() }}
                                                @endif
                                            </li>
                                        @endif

                                    @endforeach
                                </ul>
                            </header>

                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="page-section text-center">
            <h3>Start your free 14-day trial today!</h3>

            <button class="btn btn-lg btn-success" name="button" type="button">Start your free trial</button>
        </div>

        <div class="page-section row">
            <div class="col-md-6">
                <h3>General Questions</h3>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Is there a setup fee?</h4>
                        <p>
                            No. There are no setup fees on any of our plans.
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h4>Do I need to enter my credit card details to sign up?</h4>
                        <p>
                            No. You can sign up and use Hiveshop for 14 days without entering your credit card details. At the end of your trial, or when you decide to launch your store, you will need to pick a plan and enter your credit card details.
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h4>Can I cancel my account at any time?</h4>
                        <p>
                            Yes. If you ever decide that Hiveshop isn't the best ecommerce platform for your business, simply cancel your account from your control panel.
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h4>How long are your contracts?</h4>
                        <p>
                            All Hiveshop plans are month to month unless you sign up for an annual or biennial plan.
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h4>Do you offer any discounted plans?</h4>
                        <p>
                            Yes, we offer a 10% discount on an annual and a 20% discount on a biennial plans, when they are paid upfront.
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h4>Can I change my plan later on?</h4>
                        <p>
                            Absolutely! You can upgrade or downgrade your plan at any time.
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h4>Is Hiveshop PCI Compliant or PCI Certified?</h4>
                        <p>
                            Yes. Hiveshop is certified Level 1 PCI DSS compliant. This means all your data and customer information is ultra secure.
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h4>Do I have to sell the same products in-store as I do online?</h4>
                        <p>
                            No, you can choose exactly what products to sell online, in-store, or both.
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h4>What is the rate if I type in a credit card?</h4>
                        <p>
                            If you chose to type in a credit card number instead of using the Hiveshop credit card reader, we charge a fixed rate of 3.0% + 30¢. This high rate is due to the higher fraud risk associated with such transactions.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h3>Online Questions</h3>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Can I use my own domain name?</h4>
                        <p>
                            Yes. Your online store can use an existing domain name that you own, or you can purchase a domain name within Hiveshop's control panel. We also provide a free myhiveshop.com domain name to all stores on sign up.
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h4>What are your bandwidth fees?</h4>
                        <p>
                            There are none. All Hiveshop plans include unlimited bandwidth for free.
                        </p>
                    </div>
                    <div class="col-md-12">
                        <h4>Do I need a web host?</h4>
                        <p>
                        No. All Hiveshop plans include secure, unlimited ecommerce hosting for your online store. Hiveshop uses the best servers, networks and a global CDN to ensure your ecommerce site is reliable and fast.
                        </p>
                    </div>
                </div>

            </div>
        </div>

        <!-- Form -->
        <div class="container page-section">
            <h2 class="text-center">Start your 14 day trial!</h2>

            <div class="row">
                <form method="post" action="{{ URL::route('services.signup.setup') }}" class="marketing-form" id="user-signup">
                    <div class="col-xs-12 col-md-2 col-md-offset-2">
                        <input type="email" name="email" class="form-control" placeholder="Your email" data-validation="required email">
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <input type="text" name="password" class="form-control" placeholder="Password" data-validation="required">
                    </div>
                    <div class="col-xs-12 col-md-2">
                        <input type="text" name="store-name"class="form-control" placeholder="Store name" data-validation="required">
                    </div>
                    <div class="col-xs-12 col-md-2">

                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-success btn-large btn-block" name="register">Create your store</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- end form -->
    </div>
</section>

@endsection