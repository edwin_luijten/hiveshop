@extends('shop.admin.template')

@section('page-breadcrumb')
    <ul>
        <li><i class="fa fa-truck"></i> {{ _('Settings') }} /</li>
        <li class="action">{{ _('Shipping') }}</li>
    </ul>
@endsection

@section('content')

    <header class="navbar navbar-fixed-top navbar-default">
        <ul class="nav navbar-nav-custom">
            <li class="hidden-xs animation-fadeInQuick navbar-breadcrumb">
                @yield('page-breadcrumb')
            </li>
        </ul>
        <div id="progress">

        </div>
    </header>
    <div id="page-content">
        <div class="row section">
            <div class="col-md-12 setting-summary">
                <p>{{ sprintf(_('Your current shipping address is %s, %s, %s, %s'), $store->getAddress()->getStreet(), $store->getAddress()->getPostal(), $store->getAddress()->getCity(), $store->getAddress()->getCountry()->getCode()) }}</p>
            </div>
        </div>
        <div class="row section">
            <div class="col-md-3">
                <h3>{{ _('Shipping rates') }}</h3>

                <p class="text-muted"><small>{{ _('Add the countries you will be shipping to and configure their shipping rates.') }}</small></p>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-section">
                        @if(!empty($shippingRates))
                            @foreach($shippingRates as $country)
                                <div class="row shipping-rate">
                                    <div class="col-md-12 country">
                                        <span class="flag flag-{{ strtolower($country['code']) }}"></span><strong>{{ $country['name'] }}</strong> - <a href="javascript:void(0)" data-confirm-delete="show" data-target="#confirm-delete" data-title="{{ sprintf(_('Remove Shipping Rates For %s'), $country['name']) }}" data-message="{{ sprintf(_('Are you sure you wish to remove %s of from the list of shipping countries? <strong>This will also remove all tax settings for %s.</strong>'), $country['name'], $country['name']) }}">{{ _('remove country') }}</a>
                                    </div>
                                    @if(!empty($country['rates']))
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table class="table">
                                                    @foreach($country['rates'] as $rate)
                                                        <tbody>
                                                            <tr>
                                                                <td><a href="javascript:void(0)" data-toggle-view="" data-target="#rate-{{ $rate->getId() }}">{{ $rate->getName() }}</a></td>
                                                                <td>
                                                                    @if($rate->getType() === 'weight_based')
                                                                        {{ $rate->getWeightLow() }} {{ $localeSettings->weightUnit }} - {{ $rate->getWeightHigh() }} {{ $localeSettings->weightUnit }}
                                                                    @elseif($rate->getType() === 'price_based')
                                                                        {{ $rate->getMinOrderSubtotal()->getFormatted() }} - {{ (!is_null($rate->getMaxOrderSubtotal()) ? $rate->getMaxOrderSubtotal()->getFormatted() : _('and up')) }}
                                                                    @endif
                                                                </td>
                                                                <td><strong>{{ $rate->getPrice()->getFormatted() }}</strong></td>
                                                            </tr>
                                                            <tr class="hidden" id="rate-{{ $rate->getId() }}">
                                                                <td colspan="9">
                                                                    <form class="table-dropdown">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="shipping-rate-name">{{ _('Shipping rate name')  }}</label>
                                                                                    <input type="text" name="name" value="{{ $rate->getName() }}" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <div class="form-group">
                                                                                    <label for="criteria">{{ _('Criteria') }}</label>
                                                                                    <select name="type" class="form-control">
                                                                                        <option value="weight_based" {{ $rate->getType() === 'weight_based' ? 'selected="selected"' : '' }}>{{ _('Based on order weight') }}</option>
                                                                                        <option value="price_based" {{ $rate->getType() === 'price_based' ? 'selected="selected"' : '' }}>{{ _('Based on order price') }}</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                            @if($rate->getType() === 'weight_based')
                                                                                <div class="row">
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="">{{ _('Weight range') }}</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" name="weight_low" value="{{ $rate->getWeightLow() }}" class="form-control" />
                                                                                                <span class="input-group-addon" id="">{{ $localeSettings->weightUnit }}</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="" class="hide-from-view">{{ _('High weight range') }}</label>
                                                                                            <div class="input-group">
                                                                                                <input type="text" name="weight_high"  value="{{ $rate->getWeightHigh() }}" class="form-control" />
                                                                                                <span class="input-group-addon" id="">{{ $localeSettings->weightUnit }}</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @elseif($rate->getType() === 'price_based')
                                                                                <div class="row">
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="">{{ _('Price range') }}</label>
                                                                                            <div class="input-group">
                                                                                                <span class="input-group-addon" id="">{{ $localeSettings->currencySymbol }}</span>
                                                                                                <input type="text" name="min_order_subtotal" value="{{ (!is_null($rate->getMinOrderSubtotal()) ? $rate->getMinOrderSubtotal()->getDecimal() : '') }}" class="form-control" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3">
                                                                                        <div class="form-group">
                                                                                            <label for="" class="hide-from-view">{{ _('Maximum price range') }}</label>
                                                                                            <div class="input-group">
                                                                                                <span class="input-group-addon" id="">{{ $localeSettings->currencySymbol }}</span>
                                                                                                <input type="text" name="max_order_subtotal" value="{{ (!is_null($rate->getMaxOrderSubtotal()) ? $rate->getMaxOrderSubtotal()->getDecimal() : '') }}" class="form-control" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-6 text-left">

                                                                            </div>
                                                                            <div class="col-md-6 text-right">
                                                                                <a href="javascript:void(0)" data-toggle-view="close" data-target="#rate-{{ $rate->getId() }}" class="btn btn-default">{{ _('Cancel') }}</a>
                                                                                <input type="hidden" name="rate" value="{{ $rate->getId() }}"/>
                                                                                {!! csrf_field() !!}
                                                                                <button type="submit" class="btn btn-success">{{ _('Save') }}</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('outside-wrapper')
    <div id="confirm-delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body">
                    Content..
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-effect-ripple btn-default" data-dismiss="modal">{{ _('Cancel') }}</button>
                    <button type="button" class="btn btn-effect-ripple btn-danger">{{ _('Delete') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script type="text/javascript">
    UI.form($('#shipping-settings'));
</script>
@endsection