<?php namespace Shop\Domain\Services\Account;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Account\Account;
use Shop\Domain\Entity\Account\AccountId;
use Shop\Domain\Entity\Account\AccountRepositoryInterface;
use Shop\Domain\Entity\Plan\Plan;
use Shop\Domain\Entity\Store\Store;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\HashingService;
use Shop\Domain\Services\ServiceInterface;
use Shop\Domain\ValueObjects\Email;
use Shop\Domain\ValueObjects\Password;

class CreateAccount extends AbstractCreatorService implements ServiceInterface
{
    use ServiceTrait;

    protected $account;

    private $entityManager;
    private $hashingService;

    public function __construct(
        EntityManager $entityManager,
        HashingService $hashingService
    ) {
        $this->entityManager = $entityManager;
        $this->hashingService = $hashingService;
    }

    /**
     * @param                                      $email
     * @param                                      $password
     * @param \Shop\Domain\Entity\Plan\Plan        $plan
     * @param \Shop\Domain\Entity\Store\Store|null $store
     *
     * @return $this
     */
    public function create($email, $password, Plan $plan, Store $store = null)
    {
        $account = new Account();
        $account->setId(AccountId::generate());
        $account->setEmail(new Email($email));
        $account->setPassword($this->hashingService->hash(new Password($password)));
        $account->setStore($store);
        $account->setPlan($plan);

        $this->account = $account;

        return $this;
    }
}
