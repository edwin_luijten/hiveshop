<?php namespace Shop\Domain\Services;

interface ServiceInterface
{

    /**
     * persist or merge the entity
     */
    public function run();

    /**
     * flush the entity manager
     */
    public function flush();
}
