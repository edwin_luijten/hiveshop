<?php namespace Shop\Domain\Entity\Shipping;

use Shop\Infrastructure\Repositories\RepositoryInterface;

interface ShippingRateRepositoryInterface extends RepositoryInterface
{
    public function getByStoreId($store);
}
