<?php namespace Shop\Domain\Entity\Product;

use Doctrine\Common\Collections\ArrayCollection;

use Shop\Domain\AggregateRoot;
use Shop\Domain\Entity\Store\Traits\StoreAware;
use Src\Doctrine\Traits\SoftDeletes;
use Src\Doctrine\Traits\Timestamps;

use Shop\Domain\ValueObjects\Price;

class Variant implements AggregateRoot
{
    use Timestamps;
    use SoftDeletes;
    use StoreAware;

    /**
     * @var VariantId
     */
    private $id;
    /**
     * @var integer
     */
    private $price;
    /**
     * @var bool
     */
    private $isMaster = false;
    /**
     * @var Product
     */
    private $product;
    /**
     * @var ArrayCollection
     */
    private $optionValues;

    public function __construct()
    {
        $this->optionValues = new ArrayCollection();
    }

    /**
     * @return \Shop\Domain\Identifier
     */
    public function getId()
    {
        return VariantId::fromString($this->id);
    }

    /**
     * @param \Shop\Domain\Entity\Product\VariantId $id
     */
    public function setId(VariantId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return \Shop\Domain\Entity\Product\price
     */
    public function getPrice()
    {
        return new Price($this->price);
    }

    /**
     * @param $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function getIsMaster()
    {
        return $this->isMaster;
    }

    /**
     * @param $isMaster
     */
    public function setIsMaster($isMaster)
    {
        $this->isMaster = (boolean) $isMaster;
    }

    /**
     * @return bool
     */
    public function isMaster()
    {
        return $this->getIsMaster();
    }

    /**
     * @param \Shop\Domain\Entity\Product\Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return ArrayCollection
     */
    public function getOptionValues()
    {
        return $this->optionValues;
    }

    /**
     * @param \Shop\Domain\Entity\Product\OptionValue $optionValue
     *
     * @return bool
     */
    public function hasOptionValue(OptionValue $optionValue)
    {
        return $this->optionValues->contains($optionValue);
    }

    /**
     * @param \Shop\Domain\Entity\Product\OptionValue $optionValue
     */
    public function addOptionValue(OptionValue $optionValue)
    {
        if (!$this->hasOptionValue($optionValue)) {
            $this->optionValues->add($optionValue);
        }
    }
}
