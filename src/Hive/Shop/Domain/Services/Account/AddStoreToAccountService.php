<?php namespace Shop\Domain\Services\Account;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Account\AccountRepositoryInterface;
use Shop\Domain\Entity\Store\Store;
use Shop\Domain\Services\AbstractUpdaterService;
use Shop\Domain\Services\HashingService;
use Shop\Domain\Services\ServiceInterface;

class AddStoreToAccountService extends AbstractUpdaterService implements ServiceInterface
{
    use ServiceTrait;

    protected $account;

    private $entityManager;
    private $accountRepository;
    private $hasingService;

    public function __construct(
        EntityManager $entityManager,
        AccountRepositoryInterface $accountRepository,
        HashingService $hashingService
    ) {
        $this->entityManager = $entityManager;
        $this->accountRepository = $accountRepository;
        $this->hasingService = $hashingService;
    }

    public function update($accountId, Store $store = null)
    {
        $account = $this->accountRepository->getById($accountId);

        $account->setStore($store);

        $this->account = $account;

        return $this;
    }
}
