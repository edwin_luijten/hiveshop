<?php namespace App\Http\Controllers\Shop\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\UrlGenerator;
use Shop\Domain\Entity\Account\AccountRepositoryInterface;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\History\AuthHistoryRepositoryInterface;
use Shop\Domain\Services\Account\UpdateAccount;
use Shop\Domain\Services\ServiceFactory;
use Shop\Domain\Services\Store\UpdateStore;

class GeneralController extends Controller
{
    private $response;
    private $urlGenerator;
    private $guard;
    private $accountRepository;
    private $authHistoryRepository;
    private $countryRepository;

    public function __construct(
        Response $response,
        UrlGenerator $urlGenerator,
        Guard $guard,
        AccountRepositoryInterface $accountRepository,
        AuthHistoryRepositoryInterface $authHistoryRepository,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->response = $response;
        $this->urlGenerator = $urlGenerator;
        $this->guard = $guard;
        $this->accountRepository = $accountRepository;
        $this->authHistoryRepository = $authHistoryRepository;
        $this->countryRepository = $countryRepository;
    }

    public function getGeneral(Request $request)
    {
        return view('shop.admin.pages.settings-general', [
            'account'   => $request->user(),
            'store'     => $request->user()->getStore(),
            'countries' => $this->countryRepository->getAll(),
            'timezones' => $this->getTimezoneList(),
            'currencies' => json_decode(file_get_contents(storage_path('app/currenciesDB.json'))),
            'defaultWeightList' => [
                'metric' => [
                    'kg' => 'Kilogram (kg)',
                    'g' => 'Gram (g)',
                ],
                'imperial' => [
                    'lb' => 'Pound (lb)',
                    'oz' => 'Ounce (oz)',
                ]
            ]
        ]);
    }

    public function postGeneral(Request $request)
    {
        $updateStoreService = new ServiceFactory(UpdateStore::class);
        $updateStoreService->update(
            $request->user()->getStore()->getId(),
            $request->get('store-name'),
            $request->get('phone'),
            $request->get('street'),
            $request->get('country'),
            $request->get('legal-name'),
            $request->get('city'),
            $request->get('postal'),
            $request->get('timezone'),
            $request->get('currency'),
            $request->get('weight-unit'),
            $request->get('account-email'),
            $request->get('customer-email'),
            $request->get('unit-system'),
            $request->get('order-id-prefix'),
            $request->get('order-id-suffix')
        )->run()->flush();

        if ($request->ajax()) {
            return $this->response->create([
                'message' => _('Settings saved'),
            ], 200);
        } else {
            return redirect()->route('admin.settings.general', [
                'subdomain' => $request->user()->getStore()->getSubdomain()
            ]);
        }
    }

    public function getAccount(Request $request)
    {
        return view('shop.admin.pages.settings-account', [
            'accounts' => $this->accountRepository->getByStore($request->user()->getStore()),
        ]);
    }

    public function postAccount()
    {

    }

    public function getProfile(Request $request)
    {
        return view('shop.admin.pages.settings-account-profile', [
            'authHistory' => $this->authHistoryRepository->getByAccountId($request->user()->getId()->toString()),
        ]);
    }

    public function postProfile(UpdateProfileRequest $request)
    {
        $input = $request->only('first-name', 'last-name', 'email', 'password');

        $updateAccountService = new ServiceFactory(UpdateAccount::class);
        $updateAccountService->update(
            $request->user()->getId()->toString(),
            array_get($input, 'email'),
            array_get($input, 'password'),
            array_get($input, 'first-name'),
            array_get($input, 'last-name')
        )->run()->flush();

        if ($request->ajax()) {
            return $this->response->create([
                'message' => _('Settings saved'),
            ], 200);
        } else {
            return redirect()->route('admin.settings.profile', [
                'subdomain' => $request->user()->getStore()->getSubdomain(),
                'id' => $request->user()->getId()->toString()
            ]);
        }
    }
}
