<?php
    $productUrl = URL::route('product', ['id' => $product->getId()->toString()]);
?>

<div class="col-md-4 col-sm-4 col-xs-6 col-mini-12">
    <div class="b-product-preview">
        <div class="b-product-preview__img view view-sixth">
            <img data-retina="" src="/assets/s/img/shop/shop_1.png" alt="" width="263">

            <div class="b-item-hover-action f-center mask">
                <div class="b-item-hover-action__inner">
                    <div class="b-item-hover-action__inner-btn_group">
                        <a href="#" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-heart"></i></a>
                        <a href="#" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-shopping-cart"></i></a>
                        <a href="{{ $productUrl }}" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="b-product-preview__content">
            <div class="b-product-preview__content_col">
                <span class="b-product-preview__content_price f-product-preview__content_price f-primary-b">
                    {{ $product->getMasterVariant()->getPrice()->getFormatted() }}
                </span>
            </div>
            <div class="b-product-preview__content_col">
                <a href="{{ $productUrl }}" class="f-product-preview__content_title">{{ $product->getName() }}</a>
                <div class="f-product-preview__content_category f-primary-b">
                    <a href="">Women</a> / <a href="">Clothe</a>
                </div>
            </div>
        </div>
    </div>
</div>