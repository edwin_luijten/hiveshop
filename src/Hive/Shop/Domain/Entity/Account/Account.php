<?php namespace Shop\Domain\Entity\Account;

use Doctrine\Common\Collections\ArrayCollection;
use Shop\Domain\AggregateHistory;
use Shop\Domain\AggregateRoot;
use Shop\Domain\Entity\History\AuthHistory;
use Shop\Domain\Entity\Subscription\Subscription;
use Shop\Domain\Entity\Store\Traits\StoreAware;
use Shop\Domain\ValueObjects\Email;
use Src\Doctrine\Traits\Authentication;
use Src\Doctrine\Traits\Timestamps;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

class Account implements AggregateRoot, Authenticatable, CanResetPassword
{
    use Timestamps;
    use Authentication;
    use StoreAware;

    /**
     * @var AccountId
     */
    private $id;
    /**
     * @var \Shop\Domain\ValueObjects\Email
     */
    private $email;
    /**
     * @var string
     */
    private $type = AccountTypes::PROSPECT;
    /**
     * @var string
     */
    private $firstName;
    /**
     * @var string
     */
    private $lastName;
    /**
     * @var \Shop\Domain\Entity\Geo\Address
     */
    private $addresses;
    /**
     * @var \Shop\Domain\Entity\Subscription\Subscription;
     */
    private $subscription;
    /**
     * @var ArrayCollection
     */
    private $authHistory;

    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->authHistory = new ArrayCollection();
    }

    /**
     * @return \Shop\Domain\Identifier
     */
    public function getId()
    {
        return AccountId::fromString($this->id);
    }

    /**
     * @param \Shop\Domain\Entity\Account\AccountId $id
     */
    public function setId(AccountId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return \Shop\Domain\ValueObjects\ValueObject
     */
    public function getEmail()
    {
        return Email::fromNative($this->email);
    }

    /**
     * @param \Shop\Domain\ValueObjects\Email $email
     */
    public function setEmail(Email $email)
    {
        $this->email = $email->toString();
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }
    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return \Shop\Domain\Entity\Account\ArrayCollection|\Shop\Domain\Entity\Geo\Address
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * @return \Shop\Domain\Entity\Subscription\Subscription
     */
    public function getSubscription()
    {
        return $this->subscription;
    }

    /**
     * @param \Shop\Domain\Entity\Subscription\Subscription $subscription
     */
    public function setSubscription(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @return ArrayCollection
     */
    public function getAuthHistory()
    {
        return $this->authHistory;
    }

    /**
     * @param ArrayCollection $authHistory
     */
    public function setAuthHistory(ArrayCollection $authHistory)
    {
        $this->authHistory = $authHistory;
    }

    /**
     * @param AuthHistory $authHistory
     */
    public function AddAuthHistory(AuthHistory $authHistory)
    {
        if(!$this->authHistory->contains($authHistory))
        {
            $this->authHistory->add($authHistory);
        }
    }
}
