<?php namespace Shop\Domain\Services\Product;

use Shop\Domain\Entity\Product\Product;
use Shop\Domain\Entity\Product\Variant;
use Shop\Domain\Entity\Product\VariantRepositoryInterface;
use Shop\Domain\Services\Product\SetBuilder\CartesianSetBuilder;

class VariantGenerator
{
    /**
     * Variant manager.
     *
     * @var \Shop\Infrastructure\Repositories\RepositoryInterface
     */
    protected $variantRepository;

    /**
     * @var SetBuilderInterface
     */
    private $setBuilder;

    /**
     * Constructor.
     *
     * @param VariantRepositoryInterface $variantRepository
     * @param CartesianSetBuilder $setBuilder
     */
    public function __construct(VariantRepositoryInterface $variantRepository, CartesianSetBuilder $setBuilder)
    {
        $this->variantRepository = $variantRepository;
        $this->setBuilder = $setBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function generate(Product $product)
    {
        if (!$product->hasOptions()) {
            throw new \InvalidArgumentException('Cannot generate variants for an object without options.');
        }

        $optionSet = [];
        $optionMap = [];

        foreach ($product->getOptions() as $k => $option) {
            foreach ($option->getValues() as $value) {
                $optionSet[$k][] = $value->getId()->toString();
                $optionMap[$value->getId()->toString()] = $value;
            }
        }

        $permutations = $this->setBuilder->build($optionSet);
        $i = 0;

        foreach ($permutations as $permutation) {
            $variant = $this->variantRepository->getEntity();
            $variant->setProduct($product);

            if ($i === 0) {
                $variant->setIsMaster(true);
            }

            if (is_array($permutation)) {
                foreach ($permutation as $id) {
                    $variant->addOptionValue($optionMap[$id]);
                }
            } else {
                $variant->addOptionValue($optionMap[$permutation]);
            }

            $product->addVariant($variant);

            $this->process($product, $variant);

            $i++;
        }
    }

    /**
     * Override if needed.
     *
     * @param Product $variable
     * @param Variant $variant
     */
    protected function process(Product $variable, Variant $variant)
    {
    }
}
