<?php namespace Shop\Domain\Entity\Store;

use Shop\Infrastructure\Repositories\RepositoryInterface;

interface StoreRepositoryInterface extends RepositoryInterface
{

}
