<?php namespace Shop\Domain\Entity\History;

use Shop\Infrastructure\Repositories\RepositoryInterface;

interface AuthHistoryRepositoryInterface extends RepositoryInterface
{
    public function getLastLogin($id);
}
