@extends('shop.admin.template')

@section('page-breadcrumb')
    <ul>
        <li><i class="fa fa-cog"></i> {{ _('Settings') }} / {{ _('Account') }} / </li>
        <li class="action">{{ _('Pick a plan for your store')  }}</li>
    </ul>
@endsection

@section('content')

    <header class="navbar navbar-fixed-top navbar-default">
        <ul class="nav navbar-nav-custom">
            <li class="hidden-xs animation-fadeInQuick navbar-breadcrumb">
                @yield('page-breadcrumb')
            </li>
        </ul>

        <div id="progress">

        </div>
    </header>
    <div id="page-content">
        <h1 class="text-center">{{ _('Pick a plan to use when your free trial ends') }}</h1>
        <h2 class="text-center text-muted">{{ _('There is no risk - if Hiveshop isn\'t right for you, cancel before August 4th and we won\'t charge you.') }}</h2>

        <div class="page-section row">
            <div class="col-md-8 col-md-offset-2 grid align-end">
                @foreach($plans as $plan)
                    @if($plan->getSlug() !== 'starter' && $plan->getSlug() !== 'trial')
                        <div class="grid-item">
                            <div class="card text-center {{ $plan->getHighLight() ? 'highlight': '' }}">
                                @if($plan->getHighLight())
                                    <header class="card-section">
                                        <h3>{{ _('Most popular') }}</h3>
                                    </header>
                                @endif
                                <div class="card-section">
                                    <h2 class="pricing-tag">
                                        <span class="pricing-tag-small">{{ $plan->getPrice()->getSymbol() }}</span>
                                        {{ $plan->getPrice()->formatter(['places' => 0])->getDecimal() }}
                                        <span class="pricing-tag-small">/pm</span>
                                    </h2>

                                    <a href="{{ URL::route('admin.subscription.pick', ['subdomain' => $subdomain, 'plan' => $plan->getSlug(), 'period' => 'monthly']) }}" class="btn btn-lg btn-primary btn-effect-ripple">{{ _('Choose this plan') }}</a>

                                    <img class="pricing-img {{ $plan->getSlug() }}" src="/assets/sa/img/plan-{{ $plan->getSlug() }}.svg" />

                                    <h3>{{ $plan->getName() }}</h3>

                                    <p class="pricing-description"><small>{{ $plan->getDescription() }}</small></p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>

        @foreach($plans as $plan)
            @if($plan->getSlug() === 'starter')
            <div class="row">
                <div class="col-md-8 col-md-offset-2 grid">
                    <div class="grid-item">
                        <div class="card card-aside">
                            <div class="grid grid-vertically-centered">
                                <div class="next-grid-item next-grid-item-no-flex">
                                    <h2 class="pricing-tag">
                                        <span class="pricing-tag-small">{{ $plan->getPrice()->getSymbol() }}</span>
                                        {{ $plan->getPrice()->formatter(['places' => 0])->getDecimal() }}
                                        <span class="pricing-tag-small">/pm</span>
                                    </h2>
                                </div>
                                <div class="grid-item">
                                    <h3>
                                        {{ $plan->getName() }}
                                    </h3>
                                    {{ $plan->getDescription() }}
                                </div>
                                <div class="grid-item grid-item-no-flex">
                                    <a href="" class="btn btn-primary">{{ _('Choose this plan') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @endforeach

    </div>
@endsection