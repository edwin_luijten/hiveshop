(function () {
	this.JST || (this.JST = {}), this.JST["templates/forgot_password/errors"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push(""), _.each(err, function (e, t) {
			__p.push('\n  <span class="error hide">', I18n.t("home.forgot_password.errors." + field + "." + t, {err: err}), "</span>\n")
		}), __p.push("\n");
		return __p.join("")
	}
}).call(this), function () {
	this.JST || (this.JST = {}), this.JST["templates/login/errors"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push(""), _.each(err, function (e, t) {
			__p.push('\n  <span class="error hide">', I18n.t("home.login.errors." + field + "." + t, {err: err}), "</span>\n")
		}), __p.push("\n");
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["templates/login/subdomain_suggest"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push('<li class="suggest is-visible">', I18n.t("home.login.suggestions.subdomain", {suggest: suggest}), "</li>\n");
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["templates/logout/header"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push("On your way out? This is what <strong>", city, '</strong> is like at the moment (or <a href="/login">log back in</a>)!\n');
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["templates/logout/product"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push("<p>", picture, "</p>\n<p>", product.copy, "</p>\n");
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["templates/logout/weather"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push('<div class="weather-block">\n  <h4>', I18n.t("home.logout.temperature"), '</h4>\n  <p class="temp">', temp, "&#x00B0;", unit, "</p>\n  <p>", I18n.t("home.logout.segments.weather")[segment], '</p>\n</div>\n\n<div class="weather-block">\n  <h4>', I18n.t("home.logout.weather"), '</h4>\n  <p class="temp"><span class="icon icon-weather-', icon, '"></span></p>\n  <p>', weatherDesc, "</p>\n</div>\n");
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["templates/modal/offline-video"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push('<div class="video-modal">\n  <iframe width="1280" height="720" src="//www.youtube.com/embed/', modalEmbedId, '?autoplay=1" frameborder="0" allowfullscreen></iframe>\n</div>\n');
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["templates/modal/online_examples"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push('<div class="modal-storeexample grid-container">\n    <picture class="modal-storeexample__screenshot grid-item grid-6 grid--tablet-8">\n      <!--[if IE 9]><video style="display: none;"><![endif]-->\n      <source srcset="', modalImage, " 1x, ", modalImageRetina, ' 2x">\n      <!--[if IE 9]></video><![endif]-->\n      <img srcset="', modalImage, " 1x, ", modalImageRetina, ' 2x" alt="', modalTitle, '">\n    </picture>\n\n    <div class="modal-storeexample__content grid-item grid-2 grid--tablet-8 grid--last">\n      <nav class="modal-storeexample__nav">\n        <button type="button" class="modal-nav-prev icon-arrow-left">\n          ', I18n.t("online.examples.modal.nav_prev"), '\n        </button>\n        <button type="button" class="modal-nav-next icon-arrow-right">\n          ', I18n.t("online.examples.modal.nav_next"), '\n        </button>\n      </nav>\n      <div class="modal-storeexample__store-info">\n        <h1 class="modal-storeexample__modal-heading modal__heading" id="ModalTitle">\n          ', modalTitle, '\n        </h1>\n        <a class="modal-storeexample__modal-uri" href="', modalUri, '" target="_blank">\n          ', modalUriText || modalUri.replace(/^(http?\:\/\/)?(www\.)?/, ""), '\n        </a>\n      </div>\n      <div class="modal-storeexample__cta">\n        <h2 class="modal-storeexample__cta-heading">\n          ', I18n.t("online.examples.modal.cta_title"), "\n        </h2>\n        <button type=\"button\"\n                class=\"modal-storeexample__cta-button marketing-button js-open-signup\"\n                data-ga-event='Showcase modal sign up' data-ga-action='clicked' data-ga-label='", themeSlug, "'>\n          ", buttonText, "\n        </button>\n      </div>\n    </div>\n</div>\n");
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["templates/modal/partner-video"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push('<div class="video-modal">\n  <iframe width="1280" height="720" src="//www.youtube.com/embed/', modalEmbedId, '?autoplay=1" frameborder="0" allowfullscreen></iframe>\n</div>\n');
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["templates/picture"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push("<picture"), classes && __p.push(' class="', classes, '"'), __p.push('>\n  <!--[if IE 9]><video style="display: none;"><![endif]-->\n  '), desktop && __p.push('<source srcset="', desktop, " 1x, ", desktop2x, ' 2x" media="(min-width: 990px)">'), __p.push("\n  "), tablet && __p.push('<source srcset="', tablet, " 1x, ", tablet2x, ' 2x" media="(min-width: 750px)">'), __p.push('\n  <!--[if IE 9]></video><![endif]-->\n  <img srcset="', mobile, " 1x, ", mobile2x, ' 2x" alt="', alt, '">\n</picture>\n');
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["templates/store_examples/collection"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push(""), __p.push("\n"), _.each(this.activeItems, function (e) {
			__p.push('\n  <li class="', e.classes, '">\n    <a href="#" class="store-examples-item__screenshot"\n      data-modal-uri="', e.uri, '"\n      data-modal-image="', e.image_large_uri, '"\n      data-modal-image-retina="', e.image_large_retina_uri, '"\n      data-modal-title="', e.title, '"\n      data-theme-slug="', e.theme_slug, '"\n      data-modal-uri-text="', e.link_text, '">\n      <picture>\n        <!--[if IE 9]><video style="display: none;"><![endif]-->\n        <source srcset="', e.image_uri, ", ", e.image_uri_retina, ' 2x">\n        <!--[if IE 9]></video><![endif]-->\n        <img srcset="', e.image_uri, ", ", e.image_uri_retina, ' 2x" alt="', e.title, '">\n      </picture>\n    </a>\n    <h3 class="store-examples-item__title">', e.title, '</h3>\n    <a class="store-examples-item__url" href="', e.uri, '" target="_blank">', e.uri.replace(/^(http?\:\/\/)?(www\.)?/, ""), "</a>\n  </li>\n")
		}), __p.push("\n");
		return __p.join("")
	}
}.call(this), function (e, t) {
	"object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function (e) {
		if (!e.document)throw new Error("jQuery requires a window with a document");
		return t(e)
	} : t(e)
}("undefined" != typeof window ? window : this, function (e, t) {
	function n(e) {
		var t = "length"in e && e.length, n = Z.type(e);
		return "function" === n || Z.isWindow(e) ? !1 : 1 === e.nodeType && t ? !0 : "array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e
	}

	function i(e, t, n) {
		if (Z.isFunction(t))return Z.grep(e, function (e, i) {
			return !!t.call(e, i, e) !== n
		});
		if (t.nodeType)return Z.grep(e, function (e) {
			return e === t !== n
		});
		if ("string" == typeof t) {
			if (at.test(t))return Z.filter(t, e, n);
			t = Z.filter(t, e)
		}
		return Z.grep(e, function (e) {
			return J.call(t, e) >= 0 !== n
		})
	}

	function r(e, t) {
		for (; (e = e[t]) && 1 !== e.nodeType;);
		return e
	}

	function o(e) {
		var t = dt[e] = {};
		return Z.each(e.match(ft) || [], function (e, n) {
			t[n] = !0
		}), t
	}

	function s() {
		Y.removeEventListener("DOMContentLoaded", s, !1), e.removeEventListener("load", s, !1), Z.ready()
	}

	function a() {
		Object.defineProperty(this.cache = {}, 0, {
			get: function () {
				return {}
			}
		}), this.expando = Z.expando + a.uid++
	}

	function u(e, t, n) {
		var i;
		if (void 0 === n && 1 === e.nodeType)if (i = "data-" + t.replace(wt, "-$1").toLowerCase(), n = e.getAttribute(i), "string" == typeof n) {
			try {
				n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : bt.test(n) ? Z.parseJSON(n) : n
			} catch (r) {
			}
			yt.set(e, t, n)
		} else n = void 0;
		return n
	}

	function l() {
		return !0
	}

	function c() {
		return !1
	}

	function p() {
		try {
			return Y.activeElement
		} catch (e) {
		}
	}

	function h(e, t) {
		return Z.nodeName(e, "table") && Z.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
	}

	function f(e) {
		return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e
	}

	function d(e) {
		var t = Ft.exec(e.type);
		return t ? e.type = t[1] : e.removeAttribute("type"), e
	}

	function m(e, t) {
		for (var n = 0, i = e.length; i > n; n++)vt.set(e[n], "globalEval", !t || vt.get(t[n], "globalEval"))
	}

	function g(e, t) {
		var n, i, r, o, s, a, u, l;
		if (1 === t.nodeType) {
			if (vt.hasData(e) && (o = vt.access(e), s = vt.set(t, o), l = o.events)) {
				delete s.handle, s.events = {};
				for (r in l)for (n = 0, i = l[r].length; i > n; n++)Z.event.add(t, r, l[r][n])
			}
			yt.hasData(e) && (a = yt.access(e), u = Z.extend({}, a), yt.set(t, u))
		}
	}

	function v(e, t) {
		var n = e.getElementsByTagName ? e.getElementsByTagName(t || "*") : e.querySelectorAll ? e.querySelectorAll(t || "*") : [];
		return void 0 === t || t && Z.nodeName(e, t) ? Z.merge([e], n) : n
	}

	function y(e, t) {
		var n = t.nodeName.toLowerCase();
		"input" === n && St.test(e.type) ? t.checked = e.checked : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
	}

	function b(t, n) {
		var i, r = Z(n.createElement(t)).appendTo(n.body), o = e.getDefaultComputedStyle && (i = e.getDefaultComputedStyle(r[0])) ? i.display : Z.css(r[0], "display");
		return r.detach(), o
	}

	function w(e) {
		var t = Y, n = Pt[e];
		return n || (n = b(e, t), "none" !== n && n || (Ht = (Ht || Z("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = Ht[0].contentDocument, t.write(), t.close(), n = b(e, t), Ht.detach()), Pt[e] = n), n
	}

	function _(e, t, n) {
		var i, r, o, s, a = e.style;
		return n = n || zt(e), n && (s = n.getPropertyValue(t) || n[t]), n && ("" !== s || Z.contains(e.ownerDocument, e) || (s = Z.style(e, t)), Bt.test(s) && Rt.test(t) && (i = a.width, r = a.minWidth, o = a.maxWidth, a.minWidth = a.maxWidth = a.width = s, s = n.width, a.width = i, a.minWidth = r, a.maxWidth = o)), void 0 !== s ? s + "" : s
	}

	function x(e, t) {
		return {
			get: function () {
				return e() ? void delete this.get : (this.get = t).apply(this, arguments)
			}
		}
	}

	function k(e, t) {
		if (t in e)return t;
		for (var n = t[0].toUpperCase() + t.slice(1), i = t, r = Kt.length; r--;)if (t = Kt[r] + n, t in e)return t;
		return i
	}

	function S(e, t, n) {
		var i = Wt.exec(t);
		return i ? Math.max(0, i[1] - (n || 0)) + (i[2] || "px") : t
	}

	function C(e, t, n, i, r) {
		for (var o = n === (i ? "border" : "content") ? 4 : "width" === t ? 1 : 0, s = 0; 4 > o; o += 2)"margin" === n && (s += Z.css(e, n + xt[o], !0, r)), i ? ("content" === n && (s -= Z.css(e, "padding" + xt[o], !0, r)), "margin" !== n && (s -= Z.css(e, "border" + xt[o] + "Width", !0, r))) : (s += Z.css(e, "padding" + xt[o], !0, r), "padding" !== n && (s += Z.css(e, "border" + xt[o] + "Width", !0, r)));
		return s
	}

	function T(e, t, n) {
		var i = !0, r = "width" === t ? e.offsetWidth : e.offsetHeight, o = zt(e), s = "border-box" === Z.css(e, "boxSizing", !1, o);
		if (0 >= r || null == r) {
			if (r = _(e, t, o), (0 > r || null == r) && (r = e.style[t]), Bt.test(r))return r;
			i = s && (G.boxSizingReliable() || r === e.style[t]), r = parseFloat(r) || 0
		}
		return r + C(e, t, n || (s ? "border" : "content"), i, o) + "px"
	}

	function $(e, t) {
		for (var n, i, r, o = [], s = 0, a = e.length; a > s; s++)i = e[s], i.style && (o[s] = vt.get(i, "olddisplay"), n = i.style.display, t ? (o[s] || "none" !== n || (i.style.display = ""), "" === i.style.display && kt(i) && (o[s] = vt.access(i, "olddisplay", w(i.nodeName)))) : (r = kt(i), "none" === n && r || vt.set(i, "olddisplay", r ? n : Z.css(i, "display"))));
		for (s = 0; a > s; s++)i = e[s], i.style && (t && "none" !== i.style.display && "" !== i.style.display || (i.style.display = t ? o[s] || "" : "none"));
		return e
	}

	function E(e, t, n, i, r) {
		return new E.prototype.init(e, t, n, i, r)
	}

	function j() {
		return setTimeout(function () {
			Gt = void 0
		}), Gt = Z.now()
	}

	function A(e, t) {
		var n, i = 0, r = {height: e};
		for (t = t ? 1 : 0; 4 > i; i += 2 - t)n = xt[i], r["margin" + n] = r["padding" + n] = e;
		return t && (r.opacity = r.width = e), r
	}

	function N(e, t, n) {
		for (var i, r = (nn[t] || []).concat(nn["*"]), o = 0, s = r.length; s > o; o++)if (i = r[o].call(n, t, e))return i
	}

	function D(e, t, n) {
		var i, r, o, s, a, u, l, c, p = this, h = {}, f = e.style, d = e.nodeType && kt(e), m = vt.get(e, "fxshow");
		n.queue || (a = Z._queueHooks(e, "fx"), null == a.unqueued && (a.unqueued = 0, u = a.empty.fire, a.empty.fire = function () {
			a.unqueued || u()
		}), a.unqueued++, p.always(function () {
			p.always(function () {
				a.unqueued--, Z.queue(e, "fx").length || a.empty.fire()
			})
		})), 1 === e.nodeType && ("height"in t || "width"in t) && (n.overflow = [f.overflow, f.overflowX, f.overflowY], l = Z.css(e, "display"), c = "none" === l ? vt.get(e, "olddisplay") || w(e.nodeName) : l, "inline" === c && "none" === Z.css(e, "float") && (f.display = "inline-block")), n.overflow && (f.overflow = "hidden", p.always(function () {
			f.overflow = n.overflow[0], f.overflowX = n.overflow[1], f.overflowY = n.overflow[2]
		}));
		for (i in t)if (r = t[i], Qt.exec(r)) {
			if (delete t[i], o = o || "toggle" === r, r === (d ? "hide" : "show")) {
				if ("show" !== r || !m || void 0 === m[i])continue;
				d = !0
			}
			h[i] = m && m[i] || Z.style(e, i)
		} else l = void 0;
		if (Z.isEmptyObject(h))"inline" === ("none" === l ? w(e.nodeName) : l) && (f.display = l); else {
			m ? "hidden"in m && (d = m.hidden) : m = vt.access(e, "fxshow", {}), o && (m.hidden = !d), d ? Z(e).show() : p.done(function () {
				Z(e).hide()
			}), p.done(function () {
				var t;
				vt.remove(e, "fxshow");
				for (t in h)Z.style(e, t, h[t])
			});
			for (i in h)s = N(d ? m[i] : 0, i, p), i in m || (m[i] = s.start, d && (s.end = s.start, s.start = "width" === i || "height" === i ? 1 : 0))
		}
	}

	function I(e, t) {
		var n, i, r, o, s;
		for (n in e)if (i = Z.camelCase(n), r = t[i], o = e[n], Z.isArray(o) && (r = o[1], o = e[n] = o[0]), n !== i && (e[i] = o, delete e[n]), s = Z.cssHooks[i], s && "expand"in s) {
			o = s.expand(o), delete e[i];
			for (n in o)n in e || (e[n] = o[n], t[n] = r)
		} else t[i] = r
	}

	function M(e, t, n) {
		var i, r, o = 0, s = tn.length, a = Z.Deferred().always(function () {
			delete u.elem
		}), u = function () {
			if (r)return !1;
			for (var t = Gt || j(), n = Math.max(0, l.startTime + l.duration - t), i = n / l.duration || 0, o = 1 - i, s = 0, u = l.tweens.length; u > s; s++)l.tweens[s].run(o);
			return a.notifyWith(e, [l, o, n]), 1 > o && u ? n : (a.resolveWith(e, [l]), !1)
		}, l = a.promise({
			elem: e,
			props: Z.extend({}, t),
			opts: Z.extend(!0, {specialEasing: {}}, n),
			originalProperties: t,
			originalOptions: n,
			startTime: Gt || j(),
			duration: n.duration,
			tweens: [],
			createTween: function (t, n) {
				var i = Z.Tween(e, l.opts, t, n, l.opts.specialEasing[t] || l.opts.easing);
				return l.tweens.push(i), i
			},
			stop: function (t) {
				var n = 0, i = t ? l.tweens.length : 0;
				if (r)return this;
				for (r = !0; i > n; n++)l.tweens[n].run(1);
				return t ? a.resolveWith(e, [l, t]) : a.rejectWith(e, [l, t]), this
			}
		}), c = l.props;
		for (I(c, l.opts.specialEasing); s > o; o++)if (i = tn[o].call(l, e, c, l.opts))return i;
		return Z.map(c, N, l), Z.isFunction(l.opts.start) && l.opts.start.call(e, l), Z.fx.timer(Z.extend(u, {
			elem: e,
			anim: l,
			queue: l.opts.queue
		})), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always)
	}

	function L(e) {
		return function (t, n) {
			"string" != typeof t && (n = t, t = "*");
			var i, r = 0, o = t.toLowerCase().match(ft) || [];
			if (Z.isFunction(n))for (; i = o[r++];)"+" === i[0] ? (i = i.slice(1) || "*", (e[i] = e[i] || []).unshift(n)) : (e[i] = e[i] || []).push(n)
		}
	}

	function F(e, t, n, i) {
		function r(a) {
			var u;
			return o[a] = !0, Z.each(e[a] || [], function (e, a) {
				var l = a(t, n, i);
				return "string" != typeof l || s || o[l] ? s ? !(u = l) : void 0 : (t.dataTypes.unshift(l), r(l), !1)
			}), u
		}

		var o = {}, s = e === wn;
		return r(t.dataTypes[0]) || !o["*"] && r("*")
	}

	function O(e, t) {
		var n, i, r = Z.ajaxSettings.flatOptions || {};
		for (n in t)void 0 !== t[n] && ((r[n] ? e : i || (i = {}))[n] = t[n]);
		return i && Z.extend(!0, e, i), e
	}

	function q(e, t, n) {
		for (var i, r, o, s, a = e.contents, u = e.dataTypes; "*" === u[0];)u.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
		if (i)for (r in a)if (a[r] && a[r].test(i)) {
			u.unshift(r);
			break
		}
		if (u[0]in n)o = u[0]; else {
			for (r in n) {
				if (!u[0] || e.converters[r + " " + u[0]]) {
					o = r;
					break
				}
				s || (s = r)
			}
			o = o || s
		}
		return o ? (o !== u[0] && u.unshift(o), n[o]) : void 0
	}

	function H(e, t, n, i) {
		var r, o, s, a, u, l = {}, c = e.dataTypes.slice();
		if (c[1])for (s in e.converters)l[s.toLowerCase()] = e.converters[s];
		for (o = c.shift(); o;)if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && i && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift())if ("*" === o)o = u; else if ("*" !== u && u !== o) {
			if (s = l[u + " " + o] || l["* " + o], !s)for (r in l)if (a = r.split(" "), a[1] === o && (s = l[u + " " + a[0]] || l["* " + a[0]])) {
				s === !0 ? s = l[r] : l[r] !== !0 && (o = a[0], c.unshift(a[1]));
				break
			}
			if (s !== !0)if (s && e["throws"])t = s(t); else try {
				t = s(t)
			} catch (p) {
				return {state: "parsererror", error: s ? p : "No conversion from " + u + " to " + o}
			}
		}
		return {state: "success", data: t}
	}

	function P(e, t, n, i) {
		var r;
		if (Z.isArray(t))Z.each(t, function (t, r) {
			n || Cn.test(e) ? i(e, r) : P(e + "[" + ("object" == typeof r ? t : "") + "]", r, n, i)
		}); else if (n || "object" !== Z.type(t))i(e, t); else for (r in t)P(e + "[" + r + "]", t[r], n, i)
	}

	function R(e) {
		return Z.isWindow(e) ? e : 9 === e.nodeType && e.defaultView
	}

	var B = [], z = B.slice, U = B.concat, W = B.push, J = B.indexOf, X = {}, V = X.toString, K = X.hasOwnProperty, G = {}, Y = e.document, Q = "2.1.4", Z = function (e, t) {
		return new Z.fn.init(e, t)
	}, et = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, tt = /^-ms-/, nt = /-([\da-z])/gi, it = function (e, t) {
		return t.toUpperCase()
	};
	Z.fn = Z.prototype = {
		jquery: Q, constructor: Z, selector: "", length: 0, toArray: function () {
			return z.call(this)
		}, get: function (e) {
			return null != e ? 0 > e ? this[e + this.length] : this[e] : z.call(this)
		}, pushStack: function (e) {
			var t = Z.merge(this.constructor(), e);
			return t.prevObject = this, t.context = this.context, t
		}, each: function (e, t) {
			return Z.each(this, e, t)
		}, map: function (e) {
			return this.pushStack(Z.map(this, function (t, n) {
				return e.call(t, n, t)
			}))
		}, slice: function () {
			return this.pushStack(z.apply(this, arguments))
		}, first: function () {
			return this.eq(0)
		}, last: function () {
			return this.eq(-1)
		}, eq: function (e) {
			var t = this.length, n = +e + (0 > e ? t : 0);
			return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
		}, end: function () {
			return this.prevObject || this.constructor(null)
		}, push: W, sort: B.sort, splice: B.splice
	}, Z.extend = Z.fn.extend = function () {
		var e, t, n, i, r, o, s = arguments[0] || {}, a = 1, u = arguments.length, l = !1;
		for ("boolean" == typeof s && (l = s, s = arguments[a] || {}, a++), "object" == typeof s || Z.isFunction(s) || (s = {}), a === u && (s = this, a--); u > a; a++)if (null != (e = arguments[a]))for (t in e)n = s[t], i = e[t], s !== i && (l && i && (Z.isPlainObject(i) || (r = Z.isArray(i))) ? (r ? (r = !1, o = n && Z.isArray(n) ? n : []) : o = n && Z.isPlainObject(n) ? n : {}, s[t] = Z.extend(l, o, i)) : void 0 !== i && (s[t] = i));
		return s
	}, Z.extend({
		expando: "jQuery" + (Q + Math.random()).replace(/\D/g, ""), isReady: !0, error: function (e) {
			throw new Error(e)
		}, noop: function () {
		}, isFunction: function (e) {
			return "function" === Z.type(e)
		}, isArray: Array.isArray, isWindow: function (e) {
			return null != e && e === e.window
		}, isNumeric: function (e) {
			return !Z.isArray(e) && e - parseFloat(e) + 1 >= 0
		}, isPlainObject: function (e) {
			return "object" !== Z.type(e) || e.nodeType || Z.isWindow(e) ? !1 : e.constructor && !K.call(e.constructor.prototype, "isPrototypeOf") ? !1 : !0
		}, isEmptyObject: function (e) {
			var t;
			for (t in e)return !1;
			return !0
		}, type: function (e) {
			return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? X[V.call(e)] || "object" : typeof e
		}, globalEval: function (e) {
			var t, n = eval;
			e = Z.trim(e), e && (1 === e.indexOf("use strict") ? (t = Y.createElement("script"), t.text = e, Y.head.appendChild(t).parentNode.removeChild(t)) : n(e))
		}, camelCase: function (e) {
			return e.replace(tt, "ms-").replace(nt, it)
		}, nodeName: function (e, t) {
			return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
		}, each: function (e, t, i) {
			var r, o = 0, s = e.length, a = n(e);
			if (i) {
				if (a)for (; s > o && (r = t.apply(e[o], i), r !== !1); o++); else for (o in e)if (r = t.apply(e[o], i), r === !1)break
			} else if (a)for (; s > o && (r = t.call(e[o], o, e[o]), r !== !1); o++); else for (o in e)if (r = t.call(e[o], o, e[o]), r === !1)break;
			return e
		}, trim: function (e) {
			return null == e ? "" : (e + "").replace(et, "")
		}, makeArray: function (e, t) {
			var i = t || [];
			return null != e && (n(Object(e)) ? Z.merge(i, "string" == typeof e ? [e] : e) : W.call(i, e)), i
		}, inArray: function (e, t, n) {
			return null == t ? -1 : J.call(t, e, n)
		}, merge: function (e, t) {
			for (var n = +t.length, i = 0, r = e.length; n > i; i++)e[r++] = t[i];
			return e.length = r, e
		}, grep: function (e, t, n) {
			for (var i, r = [], o = 0, s = e.length, a = !n; s > o; o++)i = !t(e[o], o), i !== a && r.push(e[o]);
			return r
		}, map: function (e, t, i) {
			var r, o = 0, s = e.length, a = n(e), u = [];
			if (a)for (; s > o; o++)r = t(e[o], o, i), null != r && u.push(r); else for (o in e)r = t(e[o], o, i), null != r && u.push(r);
			return U.apply([], u)
		}, guid: 1, proxy: function (e, t) {
			var n, i, r;
			return "string" == typeof t && (n = e[t], t = e, e = n), Z.isFunction(e) ? (i = z.call(arguments, 2), r = function () {
				return e.apply(t || this, i.concat(z.call(arguments)))
			}, r.guid = e.guid = e.guid || Z.guid++, r) : void 0
		}, now: Date.now, support: G
	}), Z.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (e, t) {
		X["[object " + t + "]"] = t.toLowerCase()
	});
	var rt = function (e) {
		function t(e, t, n, i) {
			var r, o, s, a, u, l, p, f, d, m;
			if ((t ? t.ownerDocument || t : P) !== D && N(t), t = t || D, n = n || [], a = t.nodeType, "string" != typeof e || !e || 1 !== a && 9 !== a && 11 !== a)return n;
			if (!i && M) {
				if (11 !== a && (r = yt.exec(e)))if (s = r[1]) {
					if (9 === a) {
						if (o = t.getElementById(s), !o || !o.parentNode)return n;
						if (o.id === s)return n.push(o), n
					} else if (t.ownerDocument && (o = t.ownerDocument.getElementById(s)) && q(t, o) && o.id === s)return n.push(o), n
				} else {
					if (r[2])return Q.apply(n, t.getElementsByTagName(e)), n;
					if ((s = r[3]) && _.getElementsByClassName)return Q.apply(n, t.getElementsByClassName(s)), n
				}
				if (_.qsa && (!L || !L.test(e))) {
					if (f = p = H, d = t, m = 1 !== a && e, 1 === a && "object" !== t.nodeName.toLowerCase()) {
						for (l = C(e), (p = t.getAttribute("id")) ? f = p.replace(wt, "\\$&") : t.setAttribute("id", f), f = "[id='" + f + "'] ", u = l.length; u--;)l[u] = f + h(l[u]);
						d = bt.test(e) && c(t.parentNode) || t, m = l.join(",")
					}
					if (m)try {
						return Q.apply(n, d.querySelectorAll(m)), n
					} catch (g) {
					} finally {
						p || t.removeAttribute("id")
					}
				}
			}
			return $(e.replace(ut, "$1"), t, n, i)
		}

		function n() {
			function e(n, i) {
				return t.push(n + " ") > x.cacheLength && delete e[t.shift()], e[n + " "] = i
			}

			var t = [];
			return e
		}

		function i(e) {
			return e[H] = !0, e
		}

		function r(e) {
			var t = D.createElement("div");
			try {
				return !!e(t)
			} catch (n) {
				return !1
			} finally {
				t.parentNode && t.parentNode.removeChild(t), t = null
			}
		}

		function o(e, t) {
			for (var n = e.split("|"), i = e.length; i--;)x.attrHandle[n[i]] = t
		}

		function s(e, t) {
			var n = t && e, i = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || X) - (~e.sourceIndex || X);
			if (i)return i;
			if (n)for (; n = n.nextSibling;)if (n === t)return -1;
			return e ? 1 : -1
		}

		function a(e) {
			return function (t) {
				var n = t.nodeName.toLowerCase();
				return "input" === n && t.type === e
			}
		}

		function u(e) {
			return function (t) {
				var n = t.nodeName.toLowerCase();
				return ("input" === n || "button" === n) && t.type === e
			}
		}

		function l(e) {
			return i(function (t) {
				return t = +t, i(function (n, i) {
					for (var r, o = e([], n.length, t), s = o.length; s--;)n[r = o[s]] && (n[r] = !(i[r] = n[r]))
				})
			})
		}

		function c(e) {
			return e && "undefined" != typeof e.getElementsByTagName && e
		}

		function p() {
		}

		function h(e) {
			for (var t = 0, n = e.length, i = ""; n > t; t++)i += e[t].value;
			return i
		}

		function f(e, t, n) {
			var i = t.dir, r = n && "parentNode" === i, o = B++;
			return t.first ? function (t, n, o) {
				for (; t = t[i];)if (1 === t.nodeType || r)return e(t, n, o)
			} : function (t, n, s) {
				var a, u, l = [R, o];
				if (s) {
					for (; t = t[i];)if ((1 === t.nodeType || r) && e(t, n, s))return !0
				} else for (; t = t[i];)if (1 === t.nodeType || r) {
					if (u = t[H] || (t[H] = {}), (a = u[i]) && a[0] === R && a[1] === o)return l[2] = a[2];
					if (u[i] = l, l[2] = e(t, n, s))return !0
				}
			}
		}

		function d(e) {
			return e.length > 1 ? function (t, n, i) {
				for (var r = e.length; r--;)if (!e[r](t, n, i))return !1;
				return !0
			} : e[0]
		}

		function m(e, n, i) {
			for (var r = 0, o = n.length; o > r; r++)t(e, n[r], i);
			return i
		}

		function g(e, t, n, i, r) {
			for (var o, s = [], a = 0, u = e.length, l = null != t; u > a; a++)(o = e[a]) && (!n || n(o, i, r)) && (s.push(o), l && t.push(a));
			return s
		}

		function v(e, t, n, r, o, s) {
			return r && !r[H] && (r = v(r)), o && !o[H] && (o = v(o, s)), i(function (i, s, a, u) {
				var l, c, p, h = [], f = [], d = s.length, v = i || m(t || "*", a.nodeType ? [a] : a, []), y = !e || !i && t ? v : g(v, h, e, a, u), b = n ? o || (i ? e : d || r) ? [] : s : y;
				if (n && n(y, b, a, u), r)for (l = g(b, f), r(l, [], a, u), c = l.length; c--;)(p = l[c]) && (b[f[c]] = !(y[f[c]] = p));
				if (i) {
					if (o || e) {
						if (o) {
							for (l = [], c = b.length; c--;)(p = b[c]) && l.push(y[c] = p);
							o(null, b = [], l, u)
						}
						for (c = b.length; c--;)(p = b[c]) && (l = o ? et(i, p) : h[c]) > -1 && (i[l] = !(s[l] = p))
					}
				} else b = g(b === s ? b.splice(d, b.length) : b), o ? o(null, s, b, u) : Q.apply(s, b)
			})
		}

		function y(e) {
			for (var t, n, i, r = e.length, o = x.relative[e[0].type], s = o || x.relative[" "], a = o ? 1 : 0, u = f(function (e) {
				return e === t
			}, s, !0), l = f(function (e) {
				return et(t, e) > -1
			}, s, !0), c = [function (e, n, i) {
				var r = !o && (i || n !== E) || ((t = n).nodeType ? u(e, n, i) : l(e, n, i));
				return t = null, r
			}]; r > a; a++)if (n = x.relative[e[a].type])c = [f(d(c), n)]; else {
				if (n = x.filter[e[a].type].apply(null, e[a].matches), n[H]) {
					for (i = ++a; r > i && !x.relative[e[i].type]; i++);
					return v(a > 1 && d(c), a > 1 && h(e.slice(0, a - 1).concat({value: " " === e[a - 2].type ? "*" : ""})).replace(ut, "$1"), n, i > a && y(e.slice(a, i)), r > i && y(e = e.slice(i)), r > i && h(e))
				}
				c.push(n)
			}
			return d(c)
		}

		function b(e, n) {
			var r = n.length > 0, o = e.length > 0, s = function (i, s, a, u, l) {
				var c, p, h, f = 0, d = "0", m = i && [], v = [], y = E, b = i || o && x.find.TAG("*", l), w = R += null == y ? 1 : Math.random() || .1, _ = b.length;
				for (l && (E = s !== D && s); d !== _ && null != (c = b[d]); d++) {
					if (o && c) {
						for (p = 0; h = e[p++];)if (h(c, s, a)) {
							u.push(c);
							break
						}
						l && (R = w)
					}
					r && ((c = !h && c) && f--, i && m.push(c))
				}
				if (f += d, r && d !== f) {
					for (p = 0; h = n[p++];)h(m, v, s, a);
					if (i) {
						if (f > 0)for (; d--;)m[d] || v[d] || (v[d] = G.call(u));
						v = g(v)
					}
					Q.apply(u, v), l && !i && v.length > 0 && f + n.length > 1 && t.uniqueSort(u)
				}
				return l && (R = w, E = y), m
			};
			return r ? i(s) : s
		}

		var w, _, x, k, S, C, T, $, E, j, A, N, D, I, M, L, F, O, q, H = "sizzle" + 1 * new Date, P = e.document, R = 0, B = 0, z = n(), U = n(), W = n(), J = function (e, t) {
			return e === t && (A = !0), 0
		}, X = 1 << 31, V = {}.hasOwnProperty, K = [], G = K.pop, Y = K.push, Q = K.push, Z = K.slice, et = function (e, t) {
			for (var n = 0, i = e.length; i > n; n++)if (e[n] === t)return n;
			return -1
		}, tt = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", nt = "[\\x20\\t\\r\\n\\f]", it = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", rt = it.replace("w", "w#"), ot = "\\[" + nt + "*(" + it + ")(?:" + nt + "*([*^$|!~]?=)" + nt + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + rt + "))|)" + nt + "*\\]", st = ":(" + it + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ot + ")*)|.*)\\)|)", at = new RegExp(nt + "+", "g"), ut = new RegExp("^" + nt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + nt + "+$", "g"), lt = new RegExp("^" + nt + "*," + nt + "*"), ct = new RegExp("^" + nt + "*([>+~]|" + nt + ")" + nt + "*"), pt = new RegExp("=" + nt + "*([^\\]'\"]*?)" + nt + "*\\]", "g"), ht = new RegExp(st), ft = new RegExp("^" + rt + "$"), dt = {
			ID: new RegExp("^#(" + it + ")"),
			CLASS: new RegExp("^\\.(" + it + ")"),
			TAG: new RegExp("^(" + it.replace("w", "w*") + ")"),
			ATTR: new RegExp("^" + ot),
			PSEUDO: new RegExp("^" + st),
			CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + nt + "*(even|odd|(([+-]|)(\\d*)n|)" + nt + "*(?:([+-]|)" + nt + "*(\\d+)|))" + nt + "*\\)|)", "i"),
			bool: new RegExp("^(?:" + tt + ")$", "i"),
			needsContext: new RegExp("^" + nt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + nt + "*((?:-\\d)?\\d*)" + nt + "*\\)|)(?=[^-]|$)", "i")
		}, mt = /^(?:input|select|textarea|button)$/i, gt = /^h\d$/i, vt = /^[^{]+\{\s*\[native \w/, yt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, bt = /[+~]/, wt = /'|\\/g, _t = new RegExp("\\\\([\\da-f]{1,6}" + nt + "?|(" + nt + ")|.)", "ig"), xt = function (e, t, n) {
			var i = "0x" + t - 65536;
			return i !== i || n ? t : 0 > i ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
		}, kt = function () {
			N()
		};
		try {
			Q.apply(K = Z.call(P.childNodes), P.childNodes), K[P.childNodes.length].nodeType
		} catch (St) {
			Q = {
				apply: K.length ? function (e, t) {
					Y.apply(e, Z.call(t))
				} : function (e, t) {
					for (var n = e.length, i = 0; e[n++] = t[i++];);
					e.length = n - 1
				}
			}
		}
		_ = t.support = {}, S = t.isXML = function (e) {
			var t = e && (e.ownerDocument || e).documentElement;
			return t ? "HTML" !== t.nodeName : !1
		}, N = t.setDocument = function (e) {
			var t, n, i = e ? e.ownerDocument || e : P;
			return i !== D && 9 === i.nodeType && i.documentElement ? (D = i, I = i.documentElement, n = i.defaultView, n && n !== n.top && (n.addEventListener ? n.addEventListener("unload", kt, !1) : n.attachEvent && n.attachEvent("onunload", kt)), M = !S(i), _.attributes = r(function (e) {
				return e.className = "i", !e.getAttribute("className")
			}), _.getElementsByTagName = r(function (e) {
				return e.appendChild(i.createComment("")), !e.getElementsByTagName("*").length
			}), _.getElementsByClassName = vt.test(i.getElementsByClassName), _.getById = r(function (e) {
				return I.appendChild(e).id = H, !i.getElementsByName || !i.getElementsByName(H).length
			}), _.getById ? (x.find.ID = function (e, t) {
				if ("undefined" != typeof t.getElementById && M) {
					var n = t.getElementById(e);
					return n && n.parentNode ? [n] : []
				}
			}, x.filter.ID = function (e) {
				var t = e.replace(_t, xt);
				return function (e) {
					return e.getAttribute("id") === t
				}
			}) : (delete x.find.ID, x.filter.ID = function (e) {
				var t = e.replace(_t, xt);
				return function (e) {
					var n = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
					return n && n.value === t
				}
			}), x.find.TAG = _.getElementsByTagName ? function (e, t) {
				return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : _.qsa ? t.querySelectorAll(e) : void 0
			} : function (e, t) {
				var n, i = [], r = 0, o = t.getElementsByTagName(e);
				if ("*" === e) {
					for (; n = o[r++];)1 === n.nodeType && i.push(n);
					return i
				}
				return o
			}, x.find.CLASS = _.getElementsByClassName && function (e, t) {
				return M ? t.getElementsByClassName(e) : void 0
			}, F = [], L = [], (_.qsa = vt.test(i.querySelectorAll)) && (r(function (e) {
				I.appendChild(e).innerHTML = "<a id='" + H + "'></a><select id='" + H + "-\f]' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && L.push("[*^$]=" + nt + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || L.push("\\[" + nt + "*(?:value|" + tt + ")"), e.querySelectorAll("[id~=" + H + "-]").length || L.push("~="), e.querySelectorAll(":checked").length || L.push(":checked"), e.querySelectorAll("a#" + H + "+*").length || L.push(".#.+[+~]")
			}), r(function (e) {
				var t = i.createElement("input");
				t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && L.push("name" + nt + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || L.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), L.push(",.*:")
			})), (_.matchesSelector = vt.test(O = I.matches || I.webkitMatchesSelector || I.mozMatchesSelector || I.oMatchesSelector || I.msMatchesSelector)) && r(function (e) {
				_.disconnectedMatch = O.call(e, "div"), O.call(e, "[s!='']:x"), F.push("!=", st)
			}), L = L.length && new RegExp(L.join("|")), F = F.length && new RegExp(F.join("|")), t = vt.test(I.compareDocumentPosition), q = t || vt.test(I.contains) ? function (e, t) {
				var n = 9 === e.nodeType ? e.documentElement : e, i = t && t.parentNode;
				return e === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(i)))
			} : function (e, t) {
				if (t)for (; t = t.parentNode;)if (t === e)return !0;
				return !1
			}, J = t ? function (e, t) {
				if (e === t)return A = !0, 0;
				var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
				return n ? n : (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & n || !_.sortDetached && t.compareDocumentPosition(e) === n ? e === i || e.ownerDocument === P && q(P, e) ? -1 : t === i || t.ownerDocument === P && q(P, t) ? 1 : j ? et(j, e) - et(j, t) : 0 : 4 & n ? -1 : 1)
			} : function (e, t) {
				if (e === t)return A = !0, 0;
				var n, r = 0, o = e.parentNode, a = t.parentNode, u = [e], l = [t];
				if (!o || !a)return e === i ? -1 : t === i ? 1 : o ? -1 : a ? 1 : j ? et(j, e) - et(j, t) : 0;
				if (o === a)return s(e, t);
				for (n = e; n = n.parentNode;)u.unshift(n);
				for (n = t; n = n.parentNode;)l.unshift(n);
				for (; u[r] === l[r];)r++;
				return r ? s(u[r], l[r]) : u[r] === P ? -1 : l[r] === P ? 1 : 0
			}, i) : D
		}, t.matches = function (e, n) {
			return t(e, null, null, n)
		}, t.matchesSelector = function (e, n) {
			if ((e.ownerDocument || e) !== D && N(e), n = n.replace(pt, "='$1']"), !(!_.matchesSelector || !M || F && F.test(n) || L && L.test(n)))try {
				var i = O.call(e, n);
				if (i || _.disconnectedMatch || e.document && 11 !== e.document.nodeType)return i
			} catch (r) {
			}
			return t(n, D, null, [e]).length > 0
		}, t.contains = function (e, t) {
			return (e.ownerDocument || e) !== D && N(e), q(e, t)
		}, t.attr = function (e, t) {
			(e.ownerDocument || e) !== D && N(e);
			var n = x.attrHandle[t.toLowerCase()], i = n && V.call(x.attrHandle, t.toLowerCase()) ? n(e, t, !M) : void 0;
			return void 0 !== i ? i : _.attributes || !M ? e.getAttribute(t) : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
		}, t.error = function (e) {
			throw new Error("Syntax error, unrecognized expression: " + e)
		}, t.uniqueSort = function (e) {
			var t, n = [], i = 0, r = 0;
			if (A = !_.detectDuplicates, j = !_.sortStable && e.slice(0), e.sort(J), A) {
				for (; t = e[r++];)t === e[r] && (i = n.push(r));
				for (; i--;)e.splice(n[i], 1)
			}
			return j = null, e
		}, k = t.getText = function (e) {
			var t, n = "", i = 0, r = e.nodeType;
			if (r) {
				if (1 === r || 9 === r || 11 === r) {
					if ("string" == typeof e.textContent)return e.textContent;
					for (e = e.firstChild; e; e = e.nextSibling)n += k(e)
				} else if (3 === r || 4 === r)return e.nodeValue
			} else for (; t = e[i++];)n += k(t);
			return n
		}, x = t.selectors = {
			cacheLength: 50,
			createPseudo: i,
			match: dt,
			attrHandle: {},
			find: {},
			relative: {
				">": {dir: "parentNode", first: !0},
				" ": {dir: "parentNode"},
				"+": {dir: "previousSibling", first: !0},
				"~": {dir: "previousSibling"}
			},
			preFilter: {
				ATTR: function (e) {
					return e[1] = e[1].replace(_t, xt), e[3] = (e[3] || e[4] || e[5] || "").replace(_t, xt), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
				}, CHILD: function (e) {
					return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
				}, PSEUDO: function (e) {
					var t, n = !e[6] && e[2];
					return dt.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && ht.test(n) && (t = C(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
				}
			},
			filter: {
				TAG: function (e) {
					var t = e.replace(_t, xt).toLowerCase();
					return "*" === e ? function () {
						return !0
					} : function (e) {
						return e.nodeName && e.nodeName.toLowerCase() === t
					}
				}, CLASS: function (e) {
					var t = z[e + " "];
					return t || (t = new RegExp("(^|" + nt + ")" + e + "(" + nt + "|$)")) && z(e, function (e) {
						return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "")
					})
				}, ATTR: function (e, n, i) {
					return function (r) {
						var o = t.attr(r, e);
						return null == o ? "!=" === n : n ? (o += "", "=" === n ? o === i : "!=" === n ? o !== i : "^=" === n ? i && 0 === o.indexOf(i) : "*=" === n ? i && o.indexOf(i) > -1 : "$=" === n ? i && o.slice(-i.length) === i : "~=" === n ? (" " + o.replace(at, " ") + " ").indexOf(i) > -1 : "|=" === n ? o === i || o.slice(0, i.length + 1) === i + "-" : !1) : !0
					}
				}, CHILD: function (e, t, n, i, r) {
					var o = "nth" !== e.slice(0, 3), s = "last" !== e.slice(-4), a = "of-type" === t;
					return 1 === i && 0 === r ? function (e) {
						return !!e.parentNode
					} : function (t, n, u) {
						var l, c, p, h, f, d, m = o !== s ? "nextSibling" : "previousSibling", g = t.parentNode, v = a && t.nodeName.toLowerCase(), y = !u && !a;
						if (g) {
							if (o) {
								for (; m;) {
									for (p = t; p = p[m];)if (a ? p.nodeName.toLowerCase() === v : 1 === p.nodeType)return !1;
									d = m = "only" === e && !d && "nextSibling"
								}
								return !0
							}
							if (d = [s ? g.firstChild : g.lastChild], s && y) {
								for (c = g[H] || (g[H] = {}), l = c[e] || [], f = l[0] === R && l[1], h = l[0] === R && l[2], p = f && g.childNodes[f]; p = ++f && p && p[m] || (h = f = 0) || d.pop();)if (1 === p.nodeType && ++h && p === t) {
									c[e] = [R, f, h];
									break
								}
							} else if (y && (l = (t[H] || (t[H] = {}))[e]) && l[0] === R)h = l[1]; else for (; (p = ++f && p && p[m] || (h = f = 0) || d.pop()) && ((a ? p.nodeName.toLowerCase() !== v : 1 !== p.nodeType) || !++h || (y && ((p[H] || (p[H] = {}))[e] = [R, h]), p !== t)););
							return h -= r, h === i || h % i === 0 && h / i >= 0
						}
					}
				}, PSEUDO: function (e, n) {
					var r, o = x.pseudos[e] || x.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
					return o[H] ? o(n) : o.length > 1 ? (r = [e, e, "", n], x.setFilters.hasOwnProperty(e.toLowerCase()) ? i(function (e, t) {
						for (var i, r = o(e, n), s = r.length; s--;)i = et(e, r[s]), e[i] = !(t[i] = r[s])
					}) : function (e) {
						return o(e, 0, r)
					}) : o
				}
			},
			pseudos: {
				not: i(function (e) {
					var t = [], n = [], r = T(e.replace(ut, "$1"));
					return r[H] ? i(function (e, t, n, i) {
						for (var o, s = r(e, null, i, []), a = e.length; a--;)(o = s[a]) && (e[a] = !(t[a] = o))
					}) : function (e, i, o) {
						return t[0] = e, r(t, null, o, n), t[0] = null, !n.pop()
					}
				}), has: i(function (e) {
					return function (n) {
						return t(e, n).length > 0
					}
				}), contains: i(function (e) {
					return e = e.replace(_t, xt), function (t) {
						return (t.textContent || t.innerText || k(t)).indexOf(e) > -1
					}
				}), lang: i(function (e) {
					return ft.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(_t, xt).toLowerCase(), function (t) {
						var n;
						do if (n = M ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang"))return n = n.toLowerCase(), n === e || 0 === n.indexOf(e + "-"); while ((t = t.parentNode) && 1 === t.nodeType);
						return !1
					}
				}), target: function (t) {
					var n = e.location && e.location.hash;
					return n && n.slice(1) === t.id
				}, root: function (e) {
					return e === I
				}, focus: function (e) {
					return e === D.activeElement && (!D.hasFocus || D.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
				}, enabled: function (e) {
					return e.disabled === !1
				}, disabled: function (e) {
					return e.disabled === !0
				}, checked: function (e) {
					var t = e.nodeName.toLowerCase();
					return "input" === t && !!e.checked || "option" === t && !!e.selected
				}, selected: function (e) {
					return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
				}, empty: function (e) {
					for (e = e.firstChild; e; e = e.nextSibling)if (e.nodeType < 6)return !1;
					return !0
				}, parent: function (e) {
					return !x.pseudos.empty(e)
				}, header: function (e) {
					return gt.test(e.nodeName)
				}, input: function (e) {
					return mt.test(e.nodeName)
				}, button: function (e) {
					var t = e.nodeName.toLowerCase();
					return "input" === t && "button" === e.type || "button" === t
				}, text: function (e) {
					var t;
					return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
				}, first: l(function () {
					return [0]
				}), last: l(function (e, t) {
					return [t - 1]
				}), eq: l(function (e, t, n) {
					return [0 > n ? n + t : n]
				}), even: l(function (e, t) {
					for (var n = 0; t > n; n += 2)e.push(n);
					return e
				}), odd: l(function (e, t) {
					for (var n = 1; t > n; n += 2)e.push(n);
					return e
				}), lt: l(function (e, t, n) {
					for (var i = 0 > n ? n + t : n; --i >= 0;)e.push(i);
					return e
				}), gt: l(function (e, t, n) {
					for (var i = 0 > n ? n + t : n; ++i < t;)e.push(i);
					return e
				})
			}
		}, x.pseudos.nth = x.pseudos.eq;
		for (w in{radio: !0, checkbox: !0, file: !0, password: !0, image: !0})x.pseudos[w] = a(w);
		for (w in{submit: !0, reset: !0})x.pseudos[w] = u(w);
		return p.prototype = x.filters = x.pseudos, x.setFilters = new p, C = t.tokenize = function (e, n) {
			var i, r, o, s, a, u, l, c = U[e + " "];
			if (c)return n ? 0 : c.slice(0);
			for (a = e, u = [], l = x.preFilter; a;) {
				(!i || (r = lt.exec(a))) && (r && (a = a.slice(r[0].length) || a), u.push(o = [])), i = !1, (r = ct.exec(a)) && (i = r.shift(), o.push({
					value: i,
					type: r[0].replace(ut, " ")
				}), a = a.slice(i.length));
				for (s in x.filter)!(r = dt[s].exec(a)) || l[s] && !(r = l[s](r)) || (i = r.shift(), o.push({
					value: i,
					type: s,
					matches: r
				}), a = a.slice(i.length));
				if (!i)break
			}
			return n ? a.length : a ? t.error(e) : U(e, u).slice(0)
		}, T = t.compile = function (e, t) {
			var n, i = [], r = [], o = W[e + " "];
			if (!o) {
				for (t || (t = C(e)), n = t.length; n--;)o = y(t[n]), o[H] ? i.push(o) : r.push(o);
				o = W(e, b(r, i)), o.selector = e
			}
			return o
		}, $ = t.select = function (e, t, n, i) {
			var r, o, s, a, u, l = "function" == typeof e && e, p = !i && C(e = l.selector || e);
			if (n = n || [], 1 === p.length) {
				if (o = p[0] = p[0].slice(0), o.length > 2 && "ID" === (s = o[0]).type && _.getById && 9 === t.nodeType && M && x.relative[o[1].type]) {
					if (t = (x.find.ID(s.matches[0].replace(_t, xt), t) || [])[0], !t)return n;
					l && (t = t.parentNode), e = e.slice(o.shift().value.length)
				}
				for (r = dt.needsContext.test(e) ? 0 : o.length; r-- && (s = o[r], !x.relative[a = s.type]);)if ((u = x.find[a]) && (i = u(s.matches[0].replace(_t, xt), bt.test(o[0].type) && c(t.parentNode) || t))) {
					if (o.splice(r, 1), e = i.length && h(o), !e)return Q.apply(n, i), n;
					break
				}
			}
			return (l || T(e, p))(i, t, !M, n, bt.test(e) && c(t.parentNode) || t), n
		}, _.sortStable = H.split("").sort(J).join("") === H, _.detectDuplicates = !!A, N(), _.sortDetached = r(function (e) {
			return 1 & e.compareDocumentPosition(D.createElement("div"))
		}), r(function (e) {
			return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
		}) || o("type|href|height|width", function (e, t, n) {
			return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
		}), _.attributes && r(function (e) {
			return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
		}) || o("value", function (e, t, n) {
			return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
		}), r(function (e) {
			return null == e.getAttribute("disabled")
		}) || o(tt, function (e, t, n) {
			var i;
			return n ? void 0 : e[t] === !0 ? t.toLowerCase() : (i = e.getAttributeNode(t)) && i.specified ? i.value : null
		}), t
	}(e);
	Z.find = rt, Z.expr = rt.selectors, Z.expr[":"] = Z.expr.pseudos, Z.unique = rt.uniqueSort, Z.text = rt.getText, Z.isXMLDoc = rt.isXML, Z.contains = rt.contains;
	var ot = Z.expr.match.needsContext, st = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, at = /^.[^:#\[\.,]*$/;
	Z.filter = function (e, t, n) {
		var i = t[0];
		return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === i.nodeType ? Z.find.matchesSelector(i, e) ? [i] : [] : Z.find.matches(e, Z.grep(t, function (e) {
			return 1 === e.nodeType
		}))
	}, Z.fn.extend({
		find: function (e) {
			var t, n = this.length, i = [], r = this;
			if ("string" != typeof e)return this.pushStack(Z(e).filter(function () {
				for (t = 0; n > t; t++)if (Z.contains(r[t], this))return !0
			}));
			for (t = 0; n > t; t++)Z.find(e, r[t], i);
			return i = this.pushStack(n > 1 ? Z.unique(i) : i), i.selector = this.selector ? this.selector + " " + e : e, i
		}, filter: function (e) {
			return this.pushStack(i(this, e || [], !1))
		}, not: function (e) {
			return this.pushStack(i(this, e || [], !0))
		}, is: function (e) {
			return !!i(this, "string" == typeof e && ot.test(e) ? Z(e) : e || [], !1).length
		}
	});
	var ut, lt = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, ct = Z.fn.init = function (e, t) {
		var n, i;
		if (!e)return this;
		if ("string" == typeof e) {
			if (n = "<" === e[0] && ">" === e[e.length - 1] && e.length >= 3 ? [null, e, null] : lt.exec(e), !n || !n[1] && t)return !t || t.jquery ? (t || ut).find(e) : this.constructor(t).find(e);
			if (n[1]) {
				if (t = t instanceof Z ? t[0] : t, Z.merge(this, Z.parseHTML(n[1], t && t.nodeType ? t.ownerDocument || t : Y, !0)), st.test(n[1]) && Z.isPlainObject(t))for (n in t)Z.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
				return this
			}
			return i = Y.getElementById(n[2]), i && i.parentNode && (this.length = 1, this[0] = i), this.context = Y, this.selector = e, this
		}
		return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : Z.isFunction(e) ? "undefined" != typeof ut.ready ? ut.ready(e) : e(Z) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), Z.makeArray(e, this))
	};
	ct.prototype = Z.fn, ut = Z(Y);
	var pt = /^(?:parents|prev(?:Until|All))/, ht = {children: !0, contents: !0, next: !0, prev: !0};
	Z.extend({
		dir: function (e, t, n) {
			for (var i = [], r = void 0 !== n; (e = e[t]) && 9 !== e.nodeType;)if (1 === e.nodeType) {
				if (r && Z(e).is(n))break;
				i.push(e)
			}
			return i
		}, sibling: function (e, t) {
			for (var n = []; e; e = e.nextSibling)1 === e.nodeType && e !== t && n.push(e);
			return n
		}
	}), Z.fn.extend({
		has: function (e) {
			var t = Z(e, this), n = t.length;
			return this.filter(function () {
				for (var e = 0; n > e; e++)if (Z.contains(this, t[e]))return !0
			})
		}, closest: function (e, t) {
			for (var n, i = 0, r = this.length, o = [], s = ot.test(e) || "string" != typeof e ? Z(e, t || this.context) : 0; r > i; i++)for (n = this[i]; n && n !== t; n = n.parentNode)if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && Z.find.matchesSelector(n, e))) {
				o.push(n);
				break
			}
			return this.pushStack(o.length > 1 ? Z.unique(o) : o)
		}, index: function (e) {
			return e ? "string" == typeof e ? J.call(Z(e), this[0]) : J.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
		}, add: function (e, t) {
			return this.pushStack(Z.unique(Z.merge(this.get(), Z(e, t))))
		}, addBack: function (e) {
			return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
		}
	}), Z.each({
		parent: function (e) {
			var t = e.parentNode;
			return t && 11 !== t.nodeType ? t : null
		}, parents: function (e) {
			return Z.dir(e, "parentNode")
		}, parentsUntil: function (e, t, n) {
			return Z.dir(e, "parentNode", n)
		}, next: function (e) {
			return r(e, "nextSibling")
		}, prev: function (e) {
			return r(e, "previousSibling")
		}, nextAll: function (e) {
			return Z.dir(e, "nextSibling")
		}, prevAll: function (e) {
			return Z.dir(e, "previousSibling")
		}, nextUntil: function (e, t, n) {
			return Z.dir(e, "nextSibling", n)
		}, prevUntil: function (e, t, n) {
			return Z.dir(e, "previousSibling", n)
		}, siblings: function (e) {
			return Z.sibling((e.parentNode || {}).firstChild, e)
		}, children: function (e) {
			return Z.sibling(e.firstChild)
		}, contents: function (e) {
			return e.contentDocument || Z.merge([], e.childNodes)
		}
	}, function (e, t) {
		Z.fn[e] = function (n, i) {
			var r = Z.map(this, t, n);
			return "Until" !== e.slice(-5) && (i = n), i && "string" == typeof i && (r = Z.filter(i, r)), this.length > 1 && (ht[e] || Z.unique(r), pt.test(e) && r.reverse()), this.pushStack(r)
		}
	});
	var ft = /\S+/g, dt = {};
	Z.Callbacks = function (e) {
		e = "string" == typeof e ? dt[e] || o(e) : Z.extend({}, e);
		var t, n, i, r, s, a, u = [], l = !e.once && [], c = function (o) {
			for (t = e.memory && o, n = !0, a = r || 0, r = 0, s = u.length, i = !0; u && s > a; a++)if (u[a].apply(o[0], o[1]) === !1 && e.stopOnFalse) {
				t = !1;
				break
			}
			i = !1, u && (l ? l.length && c(l.shift()) : t ? u = [] : p.disable())
		}, p = {
			add: function () {
				if (u) {
					var n = u.length;
					!function o(t) {
						Z.each(t, function (t, n) {
							var i = Z.type(n);
							"function" === i ? e.unique && p.has(n) || u.push(n) : n && n.length && "string" !== i && o(n)
						})
					}(arguments), i ? s = u.length : t && (r = n, c(t))
				}
				return this
			}, remove: function () {
				return u && Z.each(arguments, function (e, t) {
					for (var n; (n = Z.inArray(t, u, n)) > -1;)u.splice(n, 1), i && (s >= n && s--, a >= n && a--)
				}), this
			}, has: function (e) {
				return e ? Z.inArray(e, u) > -1 : !(!u || !u.length)
			}, empty: function () {
				return u = [], s = 0, this
			}, disable: function () {
				return u = l = t = void 0, this
			}, disabled: function () {
				return !u
			}, lock: function () {
				return l = void 0, t || p.disable(), this
			}, locked: function () {
				return !l
			}, fireWith: function (e, t) {
				return !u || n && !l || (t = t || [], t = [e, t.slice ? t.slice() : t], i ? l.push(t) : c(t)), this
			}, fire: function () {
				return p.fireWith(this, arguments), this
			}, fired: function () {
				return !!n
			}
		};
		return p
	}, Z.extend({
		Deferred: function (e) {
			var t = [["resolve", "done", Z.Callbacks("once memory"), "resolved"], ["reject", "fail", Z.Callbacks("once memory"), "rejected"], ["notify", "progress", Z.Callbacks("memory")]], n = "pending", i = {
				state: function () {
					return n
				}, always: function () {
					return r.done(arguments).fail(arguments), this
				}, then: function () {
					var e = arguments;
					return Z.Deferred(function (n) {
						Z.each(t, function (t, o) {
							var s = Z.isFunction(e[t]) && e[t];
							r[o[1]](function () {
								var e = s && s.apply(this, arguments);
								e && Z.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[o[0] + "With"](this === i ? n.promise() : this, s ? [e] : arguments)
							})
						}), e = null
					}).promise()
				}, promise: function (e) {
					return null != e ? Z.extend(e, i) : i
				}
			}, r = {};
			return i.pipe = i.then, Z.each(t, function (e, o) {
				var s = o[2], a = o[3];
				i[o[1]] = s.add, a && s.add(function () {
					n = a
				}, t[1 ^ e][2].disable, t[2][2].lock), r[o[0]] = function () {
					return r[o[0] + "With"](this === r ? i : this, arguments), this
				}, r[o[0] + "With"] = s.fireWith
			}), i.promise(r), e && e.call(r, r), r
		}, when: function (e) {
			var t, n, i, r = 0, o = z.call(arguments), s = o.length, a = 1 !== s || e && Z.isFunction(e.promise) ? s : 0, u = 1 === a ? e : Z.Deferred(), l = function (e, n, i) {
				return function (r) {
					n[e] = this, i[e] = arguments.length > 1 ? z.call(arguments) : r, i === t ? u.notifyWith(n, i) : --a || u.resolveWith(n, i)
				}
			};
			if (s > 1)for (t = new Array(s), n = new Array(s), i = new Array(s); s > r; r++)o[r] && Z.isFunction(o[r].promise) ? o[r].promise().done(l(r, i, o)).fail(u.reject).progress(l(r, n, t)) : --a;
			return a || u.resolveWith(i, o), u.promise()
		}
	});
	var mt;
	Z.fn.ready = function (e) {
		return Z.ready.promise().done(e), this
	}, Z.extend({
		isReady: !1, readyWait: 1, holdReady: function (e) {
			e ? Z.readyWait++ : Z.ready(!0)
		}, ready: function (e) {
			(e === !0 ? --Z.readyWait : Z.isReady) || (Z.isReady = !0, e !== !0 && --Z.readyWait > 0 || (mt.resolveWith(Y, [Z]), Z.fn.triggerHandler && (Z(Y).triggerHandler("ready"), Z(Y).off("ready"))))
		}
	}), Z.ready.promise = function (t) {
		return mt || (mt = Z.Deferred(), "complete" === Y.readyState ? setTimeout(Z.ready) : (Y.addEventListener("DOMContentLoaded", s, !1), e.addEventListener("load", s, !1))), mt.promise(t)
	}, Z.ready.promise();
	var gt = Z.access = function (e, t, n, i, r, o, s) {
		var a = 0, u = e.length, l = null == n;
		if ("object" === Z.type(n)) {
			r = !0;
			for (a in n)Z.access(e, t, a, n[a], !0, o, s)
		} else if (void 0 !== i && (r = !0, Z.isFunction(i) || (s = !0), l && (s ? (t.call(e, i), t = null) : (l = t, t = function (e, t, n) {
				return l.call(Z(e), n)
			})), t))for (; u > a; a++)t(e[a], n, s ? i : i.call(e[a], a, t(e[a], n)));
		return r ? e : l ? t.call(e) : u ? t(e[0], n) : o
	};
	Z.acceptData = function (e) {
		return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType
	}, a.uid = 1, a.accepts = Z.acceptData, a.prototype = {
		key: function (e) {
			if (!a.accepts(e))return 0;
			var t = {}, n = e[this.expando];
			if (!n) {
				n = a.uid++;
				try {
					t[this.expando] = {value: n}, Object.defineProperties(e, t)
				} catch (i) {
					t[this.expando] = n, Z.extend(e, t)
				}
			}
			return this.cache[n] || (this.cache[n] = {}), n
		}, set: function (e, t, n) {
			var i, r = this.key(e), o = this.cache[r];
			if ("string" == typeof t)o[t] = n; else if (Z.isEmptyObject(o))Z.extend(this.cache[r], t); else for (i in t)o[i] = t[i];
			return o
		}, get: function (e, t) {
			var n = this.cache[this.key(e)];
			return void 0 === t ? n : n[t]
		}, access: function (e, t, n) {
			var i;
			return void 0 === t || t && "string" == typeof t && void 0 === n ? (i = this.get(e, t), void 0 !== i ? i : this.get(e, Z.camelCase(t))) : (this.set(e, t, n), void 0 !== n ? n : t)
		}, remove: function (e, t) {
			var n, i, r, o = this.key(e), s = this.cache[o];
			if (void 0 === t)this.cache[o] = {}; else {
				Z.isArray(t) ? i = t.concat(t.map(Z.camelCase)) : (r = Z.camelCase(t), t in s ? i = [t, r] : (i = r, i = i in s ? [i] : i.match(ft) || [])), n = i.length;
				for (; n--;)delete s[i[n]]
			}
		}, hasData: function (e) {
			return !Z.isEmptyObject(this.cache[e[this.expando]] || {})
		}, discard: function (e) {
			e[this.expando] && delete this.cache[e[this.expando]]
		}
	};
	var vt = new a, yt = new a, bt = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, wt = /([A-Z])/g;
	Z.extend({
		hasData: function (e) {
			return yt.hasData(e) || vt.hasData(e)
		}, data: function (e, t, n) {
			return yt.access(e, t, n)
		}, removeData: function (e, t) {
			yt.remove(e, t)
		}, _data: function (e, t, n) {
			return vt.access(e, t, n)
		}, _removeData: function (e, t) {
			vt.remove(e, t)
		}
	}), Z.fn.extend({
		data: function (e, t) {
			var n, i, r, o = this[0], s = o && o.attributes;
			if (void 0 === e) {
				if (this.length && (r = yt.get(o), 1 === o.nodeType && !vt.get(o, "hasDataAttrs"))) {
					for (n = s.length; n--;)s[n] && (i = s[n].name, 0 === i.indexOf("data-") && (i = Z.camelCase(i.slice(5)), u(o, i, r[i])));
					vt.set(o, "hasDataAttrs", !0)
				}
				return r
			}
			return "object" == typeof e ? this.each(function () {
				yt.set(this, e)
			}) : gt(this, function (t) {
				var n, i = Z.camelCase(e);
				if (o && void 0 === t) {
					if (n = yt.get(o, e), void 0 !== n)return n;
					if (n = yt.get(o, i), void 0 !== n)return n;
					if (n = u(o, i, void 0), void 0 !== n)return n
				} else this.each(function () {
					var n = yt.get(this, i);
					yt.set(this, i, t), -1 !== e.indexOf("-") && void 0 !== n && yt.set(this, e, t)
				})
			}, null, t, arguments.length > 1, null, !0)
		}, removeData: function (e) {
			return this.each(function () {
				yt.remove(this, e)
			})
		}
	}), Z.extend({
		queue: function (e, t, n) {
			var i;
			return e ? (t = (t || "fx") + "queue", i = vt.get(e, t), n && (!i || Z.isArray(n) ? i = vt.access(e, t, Z.makeArray(n)) : i.push(n)), i || []) : void 0
		}, dequeue: function (e, t) {
			t = t || "fx";
			var n = Z.queue(e, t), i = n.length, r = n.shift(), o = Z._queueHooks(e, t), s = function () {
				Z.dequeue(e, t)
			};
			"inprogress" === r && (r = n.shift(), i--), r && ("fx" === t && n.unshift("inprogress"), delete o.stop, r.call(e, s, o)), !i && o && o.empty.fire()
		}, _queueHooks: function (e, t) {
			var n = t + "queueHooks";
			return vt.get(e, n) || vt.access(e, n, {
				empty: Z.Callbacks("once memory").add(function () {
					vt.remove(e, [t + "queue", n])
				})
			})
		}
	}), Z.fn.extend({
		queue: function (e, t) {
			var n = 2;
			return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? Z.queue(this[0], e) : void 0 === t ? this : this.each(function () {
				var n = Z.queue(this, e, t);
				Z._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && Z.dequeue(this, e)
			})
		}, dequeue: function (e) {
			return this.each(function () {
				Z.dequeue(this, e)
			})
		}, clearQueue: function (e) {
			return this.queue(e || "fx", [])
		}, promise: function (e, t) {
			var n, i = 1, r = Z.Deferred(), o = this, s = this.length, a = function () {
				--i || r.resolveWith(o, [o])
			};
			for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; s--;)n = vt.get(o[s], e + "queueHooks"), n && n.empty && (i++, n.empty.add(a));
			return a(), r.promise(t)
		}
	});
	var _t = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, xt = ["Top", "Right", "Bottom", "Left"], kt = function (e, t) {
		return e = t || e, "none" === Z.css(e, "display") || !Z.contains(e.ownerDocument, e)
	}, St = /^(?:checkbox|radio)$/i;
	!function () {
		var e = Y.createDocumentFragment(), t = e.appendChild(Y.createElement("div")), n = Y.createElement("input");
		n.setAttribute("type", "radio"), n.setAttribute("checked", "checked"), n.setAttribute("name", "t"), t.appendChild(n), G.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, t.innerHTML = "<textarea>x</textarea>", G.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue
	}();
	var Ct = "undefined";
	G.focusinBubbles = "onfocusin"in e;
	var Tt = /^key/, $t = /^(?:mouse|pointer|contextmenu)|click/, Et = /^(?:focusinfocus|focusoutblur)$/, jt = /^([^.]*)(?:\.(.+)|)$/;
	Z.event = {
		global: {},
		add: function (e, t, n, i, r) {
			var o, s, a, u, l, c, p, h, f, d, m, g = vt.get(e);
			if (g)for (n.handler && (o = n, n = o.handler, r = o.selector), n.guid || (n.guid = Z.guid++), (u = g.events) || (u = g.events = {}), (s = g.handle) || (s = g.handle = function (t) {
				return typeof Z !== Ct && Z.event.triggered !== t.type ? Z.event.dispatch.apply(e, arguments) : void 0
			}), t = (t || "").match(ft) || [""], l = t.length; l--;)a = jt.exec(t[l]) || [], f = m = a[1], d = (a[2] || "").split(".").sort(), f && (p = Z.event.special[f] || {}, f = (r ? p.delegateType : p.bindType) || f, p = Z.event.special[f] || {}, c = Z.extend({
				type: f,
				origType: m,
				data: i,
				handler: n,
				guid: n.guid,
				selector: r,
				needsContext: r && Z.expr.match.needsContext.test(r),
				namespace: d.join(".")
			}, o), (h = u[f]) || (h = u[f] = [], h.delegateCount = 0, p.setup && p.setup.call(e, i, d, s) !== !1 || e.addEventListener && e.addEventListener(f, s, !1)), p.add && (p.add.call(e, c), c.handler.guid || (c.handler.guid = n.guid)), r ? h.splice(h.delegateCount++, 0, c) : h.push(c), Z.event.global[f] = !0)
		},
		remove: function (e, t, n, i, r) {
			var o, s, a, u, l, c, p, h, f, d, m, g = vt.hasData(e) && vt.get(e);
			if (g && (u = g.events)) {
				for (t = (t || "").match(ft) || [""], l = t.length; l--;)if (a = jt.exec(t[l]) || [], f = m = a[1], d = (a[2] || "").split(".").sort(), f) {
					for (p = Z.event.special[f] || {}, f = (i ? p.delegateType : p.bindType) || f, h = u[f] || [], a = a[2] && new RegExp("(^|\\.)" + d.join("\\.(?:.*\\.|)") + "(\\.|$)"), s = o = h.length; o--;)c = h[o], !r && m !== c.origType || n && n.guid !== c.guid || a && !a.test(c.namespace) || i && i !== c.selector && ("**" !== i || !c.selector) || (h.splice(o, 1), c.selector && h.delegateCount--, p.remove && p.remove.call(e, c));
					s && !h.length && (p.teardown && p.teardown.call(e, d, g.handle) !== !1 || Z.removeEvent(e, f, g.handle), delete u[f])
				} else for (f in u)Z.event.remove(e, f + t[l], n, i, !0);
				Z.isEmptyObject(u) && (delete g.handle, vt.remove(e, "events"))
			}
		},
		trigger: function (t, n, i, r) {
			var o, s, a, u, l, c, p, h = [i || Y], f = K.call(t, "type") ? t.type : t, d = K.call(t, "namespace") ? t.namespace.split(".") : [];
			if (s = a = i = i || Y, 3 !== i.nodeType && 8 !== i.nodeType && !Et.test(f + Z.event.triggered) && (f.indexOf(".") >= 0 && (d = f.split("."), f = d.shift(), d.sort()), l = f.indexOf(":") < 0 && "on" + f, t = t[Z.expando] ? t : new Z.Event(f, "object" == typeof t && t), t.isTrigger = r ? 2 : 3, t.namespace = d.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + d.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = i), n = null == n ? [t] : Z.makeArray(n, [t]), p = Z.event.special[f] || {}, r || !p.trigger || p.trigger.apply(i, n) !== !1)) {
				if (!r && !p.noBubble && !Z.isWindow(i)) {
					for (u = p.delegateType || f, Et.test(u + f) || (s = s.parentNode); s; s = s.parentNode)h.push(s), a = s;
					a === (i.ownerDocument || Y) && h.push(a.defaultView || a.parentWindow || e)
				}
				for (o = 0; (s = h[o++]) && !t.isPropagationStopped();)t.type = o > 1 ? u : p.bindType || f, c = (vt.get(s, "events") || {})[t.type] && vt.get(s, "handle"), c && c.apply(s, n), c = l && s[l], c && c.apply && Z.acceptData(s) && (t.result = c.apply(s, n), t.result === !1 && t.preventDefault());
				return t.type = f, r || t.isDefaultPrevented() || p._default && p._default.apply(h.pop(), n) !== !1 || !Z.acceptData(i) || l && Z.isFunction(i[f]) && !Z.isWindow(i) && (a = i[l], a && (i[l] = null), Z.event.triggered = f, i[f](), Z.event.triggered = void 0, a && (i[l] = a)), t.result
			}
		},
		dispatch: function (e) {
			e = Z.event.fix(e);
			var t, n, i, r, o, s = [], a = z.call(arguments), u = (vt.get(this, "events") || {})[e.type] || [], l = Z.event.special[e.type] || {};
			if (a[0] = e, e.delegateTarget = this, !l.preDispatch || l.preDispatch.call(this, e) !== !1) {
				for (s = Z.event.handlers.call(this, e, u), t = 0; (r = s[t++]) && !e.isPropagationStopped();)for (e.currentTarget = r.elem, n = 0; (o = r.handlers[n++]) && !e.isImmediatePropagationStopped();)(!e.namespace_re || e.namespace_re.test(o.namespace)) && (e.handleObj = o, e.data = o.data, i = ((Z.event.special[o.origType] || {}).handle || o.handler).apply(r.elem, a), void 0 !== i && (e.result = i) === !1 && (e.preventDefault(), e.stopPropagation()));
				return l.postDispatch && l.postDispatch.call(this, e), e.result
			}
		},
		handlers: function (e, t) {
			var n, i, r, o, s = [], a = t.delegateCount, u = e.target;
			if (a && u.nodeType && (!e.button || "click" !== e.type))for (; u !== this; u = u.parentNode || this)if (u.disabled !== !0 || "click" !== e.type) {
				for (i = [], n = 0; a > n; n++)o = t[n], r = o.selector + " ", void 0 === i[r] && (i[r] = o.needsContext ? Z(r, this).index(u) >= 0 : Z.find(r, this, null, [u]).length), i[r] && i.push(o);
				i.length && s.push({elem: u, handlers: i})
			}
			return a < t.length && s.push({elem: this, handlers: t.slice(a)}), s
		},
		props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
		fixHooks: {},
		keyHooks: {
			props: "char charCode key keyCode".split(" "), filter: function (e, t) {
				return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
			}
		},
		mouseHooks: {
			props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
			filter: function (e, t) {
				var n, i, r, o = t.button;
				return null == e.pageX && null != t.clientX && (n = e.target.ownerDocument || Y, i = n.documentElement, r = n.body, e.pageX = t.clientX + (i && i.scrollLeft || r && r.scrollLeft || 0) - (i && i.clientLeft || r && r.clientLeft || 0), e.pageY = t.clientY + (i && i.scrollTop || r && r.scrollTop || 0) - (i && i.clientTop || r && r.clientTop || 0)), e.which || void 0 === o || (e.which = 1 & o ? 1 : 2 & o ? 3 : 4 & o ? 2 : 0), e
			}
		},
		fix: function (e) {
			if (e[Z.expando])return e;
			var t, n, i, r = e.type, o = e, s = this.fixHooks[r];
			for (s || (this.fixHooks[r] = s = $t.test(r) ? this.mouseHooks : Tt.test(r) ? this.keyHooks : {}), i = s.props ? this.props.concat(s.props) : this.props, e = new Z.Event(o), t = i.length; t--;)n = i[t], e[n] = o[n];
			return e.target || (e.target = Y), 3 === e.target.nodeType && (e.target = e.target.parentNode), s.filter ? s.filter(e, o) : e
		},
		special: {
			load: {noBubble: !0}, focus: {
				trigger: function () {
					return this !== p() && this.focus ? (this.focus(), !1) : void 0
				}, delegateType: "focusin"
			}, blur: {
				trigger: function () {
					return this === p() && this.blur ? (this.blur(), !1) : void 0
				}, delegateType: "focusout"
			}, click: {
				trigger: function () {
					return "checkbox" === this.type && this.click && Z.nodeName(this, "input") ? (this.click(), !1) : void 0
				}, _default: function (e) {
					return Z.nodeName(e.target, "a")
				}
			}, beforeunload: {
				postDispatch: function (e) {
					void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result)
				}
			}
		},
		simulate: function (e, t, n, i) {
			var r = Z.extend(new Z.Event, n, {type: e, isSimulated: !0, originalEvent: {}});
			i ? Z.event.trigger(r, null, t) : Z.event.dispatch.call(t, r), r.isDefaultPrevented() && n.preventDefault()
		}
	}, Z.removeEvent = function (e, t, n) {
		e.removeEventListener && e.removeEventListener(t, n, !1)
	}, Z.Event = function (e, t) {
		return this instanceof Z.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && e.returnValue === !1 ? l : c) : this.type = e, t && Z.extend(this, t), this.timeStamp = e && e.timeStamp || Z.now(), void(this[Z.expando] = !0)) : new Z.Event(e, t)
	}, Z.Event.prototype = {
		isDefaultPrevented: c,
		isPropagationStopped: c,
		isImmediatePropagationStopped: c,
		preventDefault: function () {
			var e = this.originalEvent;
			this.isDefaultPrevented = l, e && e.preventDefault && e.preventDefault()
		},
		stopPropagation: function () {
			var e = this.originalEvent;
			this.isPropagationStopped = l, e && e.stopPropagation && e.stopPropagation()
		},
		stopImmediatePropagation: function () {
			var e = this.originalEvent;
			this.isImmediatePropagationStopped = l, e && e.stopImmediatePropagation && e.stopImmediatePropagation(), this.stopPropagation()
		}
	}, Z.each({
		mouseenter: "mouseover",
		mouseleave: "mouseout",
		pointerenter: "pointerover",
		pointerleave: "pointerout"
	}, function (e, t) {
		Z.event.special[e] = {
			delegateType: t, bindType: t, handle: function (e) {
				var n, i = this, r = e.relatedTarget, o = e.handleObj;
				return (!r || r !== i && !Z.contains(i, r)) && (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n
			}
		}
	}), G.focusinBubbles || Z.each({focus: "focusin", blur: "focusout"}, function (e, t) {
		var n = function (e) {
			Z.event.simulate(t, e.target, Z.event.fix(e), !0)
		};
		Z.event.special[t] = {
			setup: function () {
				var i = this.ownerDocument || this, r = vt.access(i, t);
				r || i.addEventListener(e, n, !0), vt.access(i, t, (r || 0) + 1)
			}, teardown: function () {
				var i = this.ownerDocument || this, r = vt.access(i, t) - 1;
				r ? vt.access(i, t, r) : (i.removeEventListener(e, n, !0), vt.remove(i, t))
			}
		}
	}), Z.fn.extend({
		on: function (e, t, n, i, r) {
			var o, s;
			if ("object" == typeof e) {
				"string" != typeof t && (n = n || t, t = void 0);
				for (s in e)this.on(s, t, n, e[s], r);
				return this
			}
			if (null == n && null == i ? (i = t, n = t = void 0) : null == i && ("string" == typeof t ? (i = n, n = void 0) : (i = n, n = t, t = void 0)), i === !1)i = c; else if (!i)return this;
			return 1 === r && (o = i, i = function (e) {
				return Z().off(e), o.apply(this, arguments)
			}, i.guid = o.guid || (o.guid = Z.guid++)), this.each(function () {
				Z.event.add(this, e, i, n, t)
			})
		}, one: function (e, t, n, i) {
			return this.on(e, t, n, i, 1)
		}, off: function (e, t, n) {
			var i, r;
			if (e && e.preventDefault && e.handleObj)return i = e.handleObj, Z(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
			if ("object" == typeof e) {
				for (r in e)this.off(r, t, e[r]);
				return this
			}
			return (t === !1 || "function" == typeof t) && (n = t, t = void 0), n === !1 && (n = c), this.each(function () {
				Z.event.remove(this, e, n, t)
			})
		}, trigger: function (e, t) {
			return this.each(function () {
				Z.event.trigger(e, t, this)
			})
		}, triggerHandler: function (e, t) {
			var n = this[0];
			return n ? Z.event.trigger(e, t, n, !0) : void 0
		}
	});
	var At = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, Nt = /<([\w:]+)/, Dt = /<|&#?\w+;/, It = /<(?:script|style|link)/i, Mt = /checked\s*(?:[^=]|=\s*.checked.)/i, Lt = /^$|\/(?:java|ecma)script/i, Ft = /^true\/(.*)/, Ot = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, qt = {
		option: [1, "<select multiple='multiple'>", "</select>"],
		thead: [1, "<table>", "</table>"],
		col: [2, "<table><colgroup>", "</colgroup></table>"],
		tr: [2, "<table><tbody>", "</tbody></table>"],
		td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
		_default: [0, "", ""]
	};
	qt.optgroup = qt.option, qt.tbody = qt.tfoot = qt.colgroup = qt.caption = qt.thead, qt.th = qt.td, Z.extend({
		clone: function (e, t, n) {
			var i, r, o, s, a = e.cloneNode(!0), u = Z.contains(e.ownerDocument, e);
			if (!(G.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || Z.isXMLDoc(e)))for (s = v(a), o = v(e), i = 0, r = o.length; r > i; i++)y(o[i], s[i]);
			if (t)if (n)for (o = o || v(e), s = s || v(a), i = 0, r = o.length; r > i; i++)g(o[i], s[i]); else g(e, a);
			return s = v(a, "script"), s.length > 0 && m(s, !u && v(e, "script")), a
		}, buildFragment: function (e, t, n, i) {
			for (var r, o, s, a, u, l, c = t.createDocumentFragment(), p = [], h = 0, f = e.length; f > h; h++)if (r = e[h], r || 0 === r)if ("object" === Z.type(r))Z.merge(p, r.nodeType ? [r] : r); else if (Dt.test(r)) {
				for (o = o || c.appendChild(t.createElement("div")), s = (Nt.exec(r) || ["", ""])[1].toLowerCase(), a = qt[s] || qt._default, o.innerHTML = a[1] + r.replace(At, "<$1></$2>") + a[2], l = a[0]; l--;)o = o.lastChild;
				Z.merge(p, o.childNodes), o = c.firstChild, o.textContent = ""
			} else p.push(t.createTextNode(r));
			for (c.textContent = "", h = 0; r = p[h++];)if ((!i || -1 === Z.inArray(r, i)) && (u = Z.contains(r.ownerDocument, r), o = v(c.appendChild(r), "script"), u && m(o), n))for (l = 0; r = o[l++];)Lt.test(r.type || "") && n.push(r);
			return c
		}, cleanData: function (e) {
			for (var t, n, i, r, o = Z.event.special, s = 0; void 0 !== (n = e[s]); s++) {
				if (Z.acceptData(n) && (r = n[vt.expando], r && (t = vt.cache[r]))) {
					if (t.events)for (i in t.events)o[i] ? Z.event.remove(n, i) : Z.removeEvent(n, i, t.handle);
					vt.cache[r] && delete vt.cache[r]
				}
				delete yt.cache[n[yt.expando]]
			}
		}
	}), Z.fn.extend({
		text: function (e) {
			return gt(this, function (e) {
				return void 0 === e ? Z.text(this) : this.empty().each(function () {
					(1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) && (this.textContent = e)
				})
			}, null, e, arguments.length)
		}, append: function () {
			return this.domManip(arguments, function (e) {
				if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
					var t = h(this, e);
					t.appendChild(e)
				}
			})
		}, prepend: function () {
			return this.domManip(arguments, function (e) {
				if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
					var t = h(this, e);
					t.insertBefore(e, t.firstChild)
				}
			})
		}, before: function () {
			return this.domManip(arguments, function (e) {
				this.parentNode && this.parentNode.insertBefore(e, this)
			})
		}, after: function () {
			return this.domManip(arguments, function (e) {
				this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
			})
		}, remove: function (e, t) {
			for (var n, i = e ? Z.filter(e, this) : this, r = 0; null != (n = i[r]); r++)t || 1 !== n.nodeType || Z.cleanData(v(n)), n.parentNode && (t && Z.contains(n.ownerDocument, n) && m(v(n, "script")), n.parentNode.removeChild(n));
			return this
		}, empty: function () {
			for (var e, t = 0; null != (e = this[t]); t++)1 === e.nodeType && (Z.cleanData(v(e, !1)), e.textContent = "");
			return this
		}, clone: function (e, t) {
			return e = null == e ? !1 : e, t = null == t ? e : t, this.map(function () {
				return Z.clone(this, e, t)
			})
		}, html: function (e) {
			return gt(this, function (e) {
				var t = this[0] || {}, n = 0, i = this.length;
				if (void 0 === e && 1 === t.nodeType)return t.innerHTML;
				if ("string" == typeof e && !It.test(e) && !qt[(Nt.exec(e) || ["", ""])[1].toLowerCase()]) {
					e = e.replace(At, "<$1></$2>");
					try {
						for (; i > n; n++)t = this[n] || {}, 1 === t.nodeType && (Z.cleanData(v(t, !1)), t.innerHTML = e);
						t = 0
					} catch (r) {
					}
				}
				t && this.empty().append(e)
			}, null, e, arguments.length)
		}, replaceWith: function () {
			var e = arguments[0];
			return this.domManip(arguments, function (t) {
				e = this.parentNode, Z.cleanData(v(this)), e && e.replaceChild(t, this)
			}), e && (e.length || e.nodeType) ? this : this.remove()
		}, detach: function (e) {
			return this.remove(e, !0)
		}, domManip: function (e, t) {
			e = U.apply([], e);
			var n, i, r, o, s, a, u = 0, l = this.length, c = this, p = l - 1, h = e[0], m = Z.isFunction(h);
			if (m || l > 1 && "string" == typeof h && !G.checkClone && Mt.test(h))return this.each(function (n) {
				var i = c.eq(n);
				m && (e[0] = h.call(this, n, i.html())), i.domManip(e, t)
			});
			if (l && (n = Z.buildFragment(e, this[0].ownerDocument, !1, this), i = n.firstChild, 1 === n.childNodes.length && (n = i), i)) {
				for (r = Z.map(v(n, "script"), f), o = r.length; l > u; u++)s = n, u !== p && (s = Z.clone(s, !0, !0), o && Z.merge(r, v(s, "script"))), t.call(this[u], s, u);
				if (o)for (a = r[r.length - 1].ownerDocument, Z.map(r, d), u = 0; o > u; u++)s = r[u], Lt.test(s.type || "") && !vt.access(s, "globalEval") && Z.contains(a, s) && (s.src ? Z._evalUrl && Z._evalUrl(s.src) : Z.globalEval(s.textContent.replace(Ot, "")))
			}
			return this
		}
	}), Z.each({
		appendTo: "append",
		prependTo: "prepend",
		insertBefore: "before",
		insertAfter: "after",
		replaceAll: "replaceWith"
	}, function (e, t) {
		Z.fn[e] = function (e) {
			for (var n, i = [], r = Z(e), o = r.length - 1, s = 0; o >= s; s++)n = s === o ? this : this.clone(!0), Z(r[s])[t](n), W.apply(i, n.get());
			return this.pushStack(i)
		}
	});
	var Ht, Pt = {}, Rt = /^margin/, Bt = new RegExp("^(" + _t + ")(?!px)[a-z%]+$", "i"), zt = function (t) {
		return t.ownerDocument.defaultView.opener ? t.ownerDocument.defaultView.getComputedStyle(t, null) : e.getComputedStyle(t, null)
	};
	!function () {
		function t() {
			s.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", s.innerHTML = "", r.appendChild(o);
			var t = e.getComputedStyle(s, null);
			n = "1%" !== t.top, i = "4px" === t.width, r.removeChild(o)
		}

		var n, i, r = Y.documentElement, o = Y.createElement("div"), s = Y.createElement("div");
		s.style && (s.style.backgroundClip = "content-box", s.cloneNode(!0).style.backgroundClip = "", G.clearCloneStyle = "content-box" === s.style.backgroundClip, o.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;position:absolute", o.appendChild(s), e.getComputedStyle && Z.extend(G, {
			pixelPosition: function () {
				return t(), n
			}, boxSizingReliable: function () {
				return null == i && t(), i
			}, reliableMarginRight: function () {
				var t, n = s.appendChild(Y.createElement("div"));
				return n.style.cssText = s.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", n.style.marginRight = n.style.width = "0", s.style.width = "1px", r.appendChild(o), t = !parseFloat(e.getComputedStyle(n, null).marginRight), r.removeChild(o), s.removeChild(n), t
			}
		}))
	}(), Z.swap = function (e, t, n, i) {
		var r, o, s = {};
		for (o in t)s[o] = e.style[o], e.style[o] = t[o];
		r = n.apply(e, i || []);
		for (o in t)e.style[o] = s[o];
		return r
	};
	var Ut = /^(none|table(?!-c[ea]).+)/, Wt = new RegExp("^(" + _t + ")(.*)$", "i"), Jt = new RegExp("^([+-])=(" + _t + ")", "i"), Xt = {
		position: "absolute",
		visibility: "hidden",
		display: "block"
	}, Vt = {letterSpacing: "0", fontWeight: "400"}, Kt = ["Webkit", "O", "Moz", "ms"];
	Z.extend({
		cssHooks: {
			opacity: {
				get: function (e, t) {
					if (t) {
						var n = _(e, "opacity");
						return "" === n ? "1" : n
					}
				}
			}
		},
		cssNumber: {
			columnCount: !0,
			fillOpacity: !0,
			flexGrow: !0,
			flexShrink: !0,
			fontWeight: !0,
			lineHeight: !0,
			opacity: !0,
			order: !0,
			orphans: !0,
			widows: !0,
			zIndex: !0,
			zoom: !0
		},
		cssProps: {"float": "cssFloat"},
		style: function (e, t, n, i) {
			if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
				var r, o, s, a = Z.camelCase(t), u = e.style;
				return t = Z.cssProps[a] || (Z.cssProps[a] = k(u, a)), s = Z.cssHooks[t] || Z.cssHooks[a], void 0 === n ? s && "get"in s && void 0 !== (r = s.get(e, !1, i)) ? r : u[t] : (o = typeof n, "string" === o && (r = Jt.exec(n)) && (n = (r[1] + 1) * r[2] + parseFloat(Z.css(e, t)), o = "number"), null != n && n === n && ("number" !== o || Z.cssNumber[a] || (n += "px"), G.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (u[t] = "inherit"), s && "set"in s && void 0 === (n = s.set(e, n, i)) || (u[t] = n)), void 0)
			}
		},
		css: function (e, t, n, i) {
			var r, o, s, a = Z.camelCase(t);
			return t = Z.cssProps[a] || (Z.cssProps[a] = k(e.style, a)), s = Z.cssHooks[t] || Z.cssHooks[a], s && "get"in s && (r = s.get(e, !0, n)), void 0 === r && (r = _(e, t, i)), "normal" === r && t in Vt && (r = Vt[t]), "" === n || n ? (o = parseFloat(r), n === !0 || Z.isNumeric(o) ? o || 0 : r) : r
		}
	}), Z.each(["height", "width"], function (e, t) {
		Z.cssHooks[t] = {
			get: function (e, n, i) {
				return n ? Ut.test(Z.css(e, "display")) && 0 === e.offsetWidth ? Z.swap(e, Xt, function () {
					return T(e, t, i)
				}) : T(e, t, i) : void 0
			}, set: function (e, n, i) {
				var r = i && zt(e);
				return S(e, n, i ? C(e, t, i, "border-box" === Z.css(e, "boxSizing", !1, r), r) : 0)
			}
		}
	}), Z.cssHooks.marginRight = x(G.reliableMarginRight, function (e, t) {
		return t ? Z.swap(e, {display: "inline-block"}, _, [e, "marginRight"]) : void 0
	}), Z.each({margin: "", padding: "", border: "Width"}, function (e, t) {
		Z.cssHooks[e + t] = {
			expand: function (n) {
				for (var i = 0, r = {}, o = "string" == typeof n ? n.split(" ") : [n]; 4 > i; i++)r[e + xt[i] + t] = o[i] || o[i - 2] || o[0];
				return r
			}
		}, Rt.test(e) || (Z.cssHooks[e + t].set = S)
	}), Z.fn.extend({
		css: function (e, t) {
			return gt(this, function (e, t, n) {
				var i, r, o = {}, s = 0;
				if (Z.isArray(t)) {
					for (i = zt(e), r = t.length; r > s; s++)o[t[s]] = Z.css(e, t[s], !1, i);
					return o
				}
				return void 0 !== n ? Z.style(e, t, n) : Z.css(e, t)
			}, e, t, arguments.length > 1)
		}, show: function () {
			return $(this, !0)
		}, hide: function () {
			return $(this)
		}, toggle: function (e) {
			return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
				kt(this) ? Z(this).show() : Z(this).hide()
			})
		}
	}), Z.Tween = E, E.prototype = {
		constructor: E, init: function (e, t, n, i, r, o) {
			this.elem = e, this.prop = n, this.easing = r || "swing", this.options = t, this.start = this.now = this.cur(), this.end = i, this.unit = o || (Z.cssNumber[n] ? "" : "px")
		}, cur: function () {
			var e = E.propHooks[this.prop];
			return e && e.get ? e.get(this) : E.propHooks._default.get(this)
		}, run: function (e) {
			var t, n = E.propHooks[this.prop];
			return this.pos = t = this.options.duration ? Z.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : E.propHooks._default.set(this), this
		}
	}, E.prototype.init.prototype = E.prototype, E.propHooks = {
		_default: {
			get: function (e) {
				var t;
				return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = Z.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
			}, set: function (e) {
				Z.fx.step[e.prop] ? Z.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[Z.cssProps[e.prop]] || Z.cssHooks[e.prop]) ? Z.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
			}
		}
	}, E.propHooks.scrollTop = E.propHooks.scrollLeft = {
		set: function (e) {
			e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
		}
	}, Z.easing = {
		linear: function (e) {
			return e
		}, swing: function (e) {
			return .5 - Math.cos(e * Math.PI) / 2
		}
	}, Z.fx = E.prototype.init, Z.fx.step = {};
	var Gt, Yt, Qt = /^(?:toggle|show|hide)$/, Zt = new RegExp("^(?:([+-])=|)(" + _t + ")([a-z%]*)$", "i"), en = /queueHooks$/, tn = [D], nn = {
		"*": [function (e, t) {
			var n = this.createTween(e, t), i = n.cur(), r = Zt.exec(t), o = r && r[3] || (Z.cssNumber[e] ? "" : "px"), s = (Z.cssNumber[e] || "px" !== o && +i) && Zt.exec(Z.css(n.elem, e)), a = 1, u = 20;
			if (s && s[3] !== o) {
				o = o || s[3], r = r || [], s = +i || 1;
				do a = a || ".5", s /= a, Z.style(n.elem, e, s + o); while (a !== (a = n.cur() / i) && 1 !== a && --u)
			}
			return r && (s = n.start = +s || +i || 0, n.unit = o, n.end = r[1] ? s + (r[1] + 1) * r[2] : +r[2]), n
		}]
	};
	Z.Animation = Z.extend(M, {
		tweener: function (e, t) {
			Z.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
			for (var n, i = 0, r = e.length; r > i; i++)n = e[i], nn[n] = nn[n] || [], nn[n].unshift(t)
		}, prefilter: function (e, t) {
			t ? tn.unshift(e) : tn.push(e)
		}
	}), Z.speed = function (e, t, n) {
		var i = e && "object" == typeof e ? Z.extend({}, e) : {
			complete: n || !n && t || Z.isFunction(e) && e,
			duration: e,
			easing: n && t || t && !Z.isFunction(t) && t
		};
		return i.duration = Z.fx.off ? 0 : "number" == typeof i.duration ? i.duration : i.duration in Z.fx.speeds ? Z.fx.speeds[i.duration] : Z.fx.speeds._default, (null == i.queue || i.queue === !0) && (i.queue = "fx"), i.old = i.complete, i.complete = function () {
			Z.isFunction(i.old) && i.old.call(this), i.queue && Z.dequeue(this, i.queue)
		}, i
	}, Z.fn.extend({
		fadeTo: function (e, t, n, i) {
			return this.filter(kt).css("opacity", 0).show().end().animate({opacity: t}, e, n, i)
		}, animate: function (e, t, n, i) {
			var r = Z.isEmptyObject(e), o = Z.speed(t, n, i), s = function () {
				var t = M(this, Z.extend({}, e), o);
				(r || vt.get(this, "finish")) && t.stop(!0)
			};
			return s.finish = s, r || o.queue === !1 ? this.each(s) : this.queue(o.queue, s)
		}, stop: function (e, t, n) {
			var i = function (e) {
				var t = e.stop;
				delete e.stop, t(n)
			};
			return "string" != typeof e && (n = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function () {
				var t = !0, r = null != e && e + "queueHooks", o = Z.timers, s = vt.get(this);
				if (r)s[r] && s[r].stop && i(s[r]); else for (r in s)s[r] && s[r].stop && en.test(r) && i(s[r]);
				for (r = o.length; r--;)o[r].elem !== this || null != e && o[r].queue !== e || (o[r].anim.stop(n), t = !1, o.splice(r, 1));
				(t || !n) && Z.dequeue(this, e)
			})
		}, finish: function (e) {
			return e !== !1 && (e = e || "fx"), this.each(function () {
				var t, n = vt.get(this), i = n[e + "queue"], r = n[e + "queueHooks"], o = Z.timers, s = i ? i.length : 0;
				for (n.finish = !0, Z.queue(this, e, []), r && r.stop && r.stop.call(this, !0), t = o.length; t--;)o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
				for (t = 0; s > t; t++)i[t] && i[t].finish && i[t].finish.call(this);
				delete n.finish
			})
		}
	}), Z.each(["toggle", "show", "hide"], function (e, t) {
		var n = Z.fn[t];
		Z.fn[t] = function (e, i, r) {
			return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(A(t, !0), e, i, r)
		}
	}), Z.each({
		slideDown: A("show"),
		slideUp: A("hide"),
		slideToggle: A("toggle"),
		fadeIn: {opacity: "show"},
		fadeOut: {opacity: "hide"},
		fadeToggle: {opacity: "toggle"}
	}, function (e, t) {
		Z.fn[e] = function (e, n, i) {
			return this.animate(t, e, n, i)
		}
	}), Z.timers = [], Z.fx.tick = function () {
		var e, t = 0, n = Z.timers;
		for (Gt = Z.now(); t < n.length; t++)e = n[t], e() || n[t] !== e || n.splice(t--, 1);
		n.length || Z.fx.stop(), Gt = void 0
	}, Z.fx.timer = function (e) {
		Z.timers.push(e), e() ? Z.fx.start() : Z.timers.pop()
	}, Z.fx.interval = 13, Z.fx.start = function () {
		Yt || (Yt = setInterval(Z.fx.tick, Z.fx.interval))
	}, Z.fx.stop = function () {
		clearInterval(Yt), Yt = null
	}, Z.fx.speeds = {slow: 600, fast: 200, _default: 400}, Z.fn.delay = function (e, t) {
		return e = Z.fx ? Z.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function (t, n) {
			var i = setTimeout(t, e);
			n.stop = function () {
				clearTimeout(i)
			}
		})
	}, function () {
		var e = Y.createElement("input"), t = Y.createElement("select"), n = t.appendChild(Y.createElement("option"));
		e.type = "checkbox", G.checkOn = "" !== e.value, G.optSelected = n.selected, t.disabled = !0, G.optDisabled = !n.disabled, e = Y.createElement("input"), e.value = "t", e.type = "radio", G.radioValue = "t" === e.value
	}();
	var rn, on, sn = Z.expr.attrHandle;
	Z.fn.extend({
		attr: function (e, t) {
			return gt(this, Z.attr, e, t, arguments.length > 1)
		}, removeAttr: function (e) {
			return this.each(function () {
				Z.removeAttr(this, e)
			})
		}
	}), Z.extend({
		attr: function (e, t, n) {
			var i, r, o = e.nodeType;
			if (e && 3 !== o && 8 !== o && 2 !== o)return typeof e.getAttribute === Ct ? Z.prop(e, t, n) : (1 === o && Z.isXMLDoc(e) || (t = t.toLowerCase(), i = Z.attrHooks[t] || (Z.expr.match.bool.test(t) ? on : rn)), void 0 === n ? i && "get"in i && null !== (r = i.get(e, t)) ? r : (r = Z.find.attr(e, t), null == r ? void 0 : r) : null !== n ? i && "set"in i && void 0 !== (r = i.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : void Z.removeAttr(e, t))
		}, removeAttr: function (e, t) {
			var n, i, r = 0, o = t && t.match(ft);
			if (o && 1 === e.nodeType)for (; n = o[r++];)i = Z.propFix[n] || n, Z.expr.match.bool.test(n) && (e[i] = !1), e.removeAttribute(n)
		}, attrHooks: {
			type: {
				set: function (e, t) {
					if (!G.radioValue && "radio" === t && Z.nodeName(e, "input")) {
						var n = e.value;
						return e.setAttribute("type", t), n && (e.value = n), t
					}
				}
			}
		}
	}), on = {
		set: function (e, t, n) {
			return t === !1 ? Z.removeAttr(e, n) : e.setAttribute(n, n), n
		}
	}, Z.each(Z.expr.match.bool.source.match(/\w+/g), function (e, t) {
		var n = sn[t] || Z.find.attr;
		sn[t] = function (e, t, i) {
			var r, o;
			return i || (o = sn[t], sn[t] = r, r = null != n(e, t, i) ? t.toLowerCase() : null, sn[t] = o), r
		}
	});
	var an = /^(?:input|select|textarea|button)$/i;
	Z.fn.extend({
		prop: function (e, t) {
			return gt(this, Z.prop, e, t, arguments.length > 1)
		}, removeProp: function (e) {
			return this.each(function () {
				delete this[Z.propFix[e] || e]
			})
		}
	}), Z.extend({
		propFix: {"for": "htmlFor", "class": "className"}, prop: function (e, t, n) {
			var i, r, o, s = e.nodeType;
			if (e && 3 !== s && 8 !== s && 2 !== s)return o = 1 !== s || !Z.isXMLDoc(e), o && (t = Z.propFix[t] || t, r = Z.propHooks[t]), void 0 !== n ? r && "set"in r && void 0 !== (i = r.set(e, n, t)) ? i : e[t] = n : r && "get"in r && null !== (i = r.get(e, t)) ? i : e[t]
		}, propHooks: {
			tabIndex: {
				get: function (e) {
					return e.hasAttribute("tabindex") || an.test(e.nodeName) || e.href ? e.tabIndex : -1
				}
			}
		}
	}), G.optSelected || (Z.propHooks.selected = {
		get: function (e) {
			var t = e.parentNode;
			return t && t.parentNode && t.parentNode.selectedIndex, null
		}
	}), Z.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
		Z.propFix[this.toLowerCase()] = this
	});
	var un = /[\t\r\n\f]/g;
	Z.fn.extend({
		addClass: function (e) {
			var t, n, i, r, o, s, a = "string" == typeof e && e, u = 0, l = this.length;
			if (Z.isFunction(e))return this.each(function (t) {
				Z(this).addClass(e.call(this, t, this.className))
			});
			if (a)for (t = (e || "").match(ft) || []; l > u; u++)if (n = this[u], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(un, " ") : " ")) {
				for (o = 0; r = t[o++];)i.indexOf(" " + r + " ") < 0 && (i += r + " ");
				s = Z.trim(i), n.className !== s && (n.className = s)
			}
			return this
		}, removeClass: function (e) {
			var t, n, i, r, o, s, a = 0 === arguments.length || "string" == typeof e && e, u = 0, l = this.length;
			if (Z.isFunction(e))return this.each(function (t) {
				Z(this).removeClass(e.call(this, t, this.className))
			});
			if (a)for (t = (e || "").match(ft) || []; l > u; u++)if (n = this[u], i = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(un, " ") : "")) {
				for (o = 0; r = t[o++];)for (; i.indexOf(" " + r + " ") >= 0;)i = i.replace(" " + r + " ", " ");
				s = e ? Z.trim(i) : "", n.className !== s && (n.className = s)
			}
			return this
		}, toggleClass: function (e, t) {
			var n = typeof e;
			return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : this.each(Z.isFunction(e) ? function (n) {
				Z(this).toggleClass(e.call(this, n, this.className, t), t)
			} : function () {
				if ("string" === n)for (var t, i = 0, r = Z(this), o = e.match(ft) || []; t = o[i++];)r.hasClass(t) ? r.removeClass(t) : r.addClass(t); else(n === Ct || "boolean" === n) && (this.className && vt.set(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : vt.get(this, "__className__") || "")
			})
		}, hasClass: function (e) {
			for (var t = " " + e + " ", n = 0, i = this.length; i > n; n++)if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(un, " ").indexOf(t) >= 0)return !0;
			return !1
		}
	});
	var ln = /\r/g;
	Z.fn.extend({
		val: function (e) {
			var t, n, i, r = this[0];
			{
				if (arguments.length)return i = Z.isFunction(e), this.each(function (n) {
					var r;
					1 === this.nodeType && (r = i ? e.call(this, n, Z(this).val()) : e, null == r ? r = "" : "number" == typeof r ? r += "" : Z.isArray(r) && (r = Z.map(r, function (e) {
						return null == e ? "" : e + ""
					})), t = Z.valHooks[this.type] || Z.valHooks[this.nodeName.toLowerCase()], t && "set"in t && void 0 !== t.set(this, r, "value") || (this.value = r))
				});
				if (r)return t = Z.valHooks[r.type] || Z.valHooks[r.nodeName.toLowerCase()], t && "get"in t && void 0 !== (n = t.get(r, "value")) ? n : (n = r.value, "string" == typeof n ? n.replace(ln, "") : null == n ? "" : n)
			}
		}
	}), Z.extend({
		valHooks: {
			option: {
				get: function (e) {
					var t = Z.find.attr(e, "value");
					return null != t ? t : Z.trim(Z.text(e))
				}
			}, select: {
				get: function (e) {
					for (var t, n, i = e.options, r = e.selectedIndex, o = "select-one" === e.type || 0 > r, s = o ? null : [], a = o ? r + 1 : i.length, u = 0 > r ? a : o ? r : 0; a > u; u++)if (n = i[u], !(!n.selected && u !== r || (G.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && Z.nodeName(n.parentNode, "optgroup"))) {
						if (t = Z(n).val(), o)return t;
						s.push(t)
					}
					return s
				}, set: function (e, t) {
					for (var n, i, r = e.options, o = Z.makeArray(t), s = r.length; s--;)i = r[s], (i.selected = Z.inArray(i.value, o) >= 0) && (n = !0);
					return n || (e.selectedIndex = -1), o
				}
			}
		}
	}), Z.each(["radio", "checkbox"], function () {
		Z.valHooks[this] = {
			set: function (e, t) {
				return Z.isArray(t) ? e.checked = Z.inArray(Z(e).val(), t) >= 0 : void 0
			}
		}, G.checkOn || (Z.valHooks[this].get = function (e) {
			return null === e.getAttribute("value") ? "on" : e.value
		})
	}), Z.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (e, t) {
		Z.fn[t] = function (e, n) {
			return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
		}
	}), Z.fn.extend({
		hover: function (e, t) {
			return this.mouseenter(e).mouseleave(t || e)
		}, bind: function (e, t, n) {
			return this.on(e, null, t, n)
		}, unbind: function (e, t) {
			return this.off(e, null, t)
		}, delegate: function (e, t, n, i) {
			return this.on(t, e, n, i)
		}, undelegate: function (e, t, n) {
			return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
		}
	});
	var cn = Z.now(), pn = /\?/;
	Z.parseJSON = function (e) {
		return JSON.parse(e + "")
	}, Z.parseXML = function (e) {
		var t, n;
		if (!e || "string" != typeof e)return null;
		try {
			n = new DOMParser, t = n.parseFromString(e, "text/xml")
		} catch (i) {
			t = void 0
		}
		return (!t || t.getElementsByTagName("parsererror").length) && Z.error("Invalid XML: " + e), t
	};
	var hn = /#.*$/, fn = /([?&])_=[^&]*/, dn = /^(.*?):[ \t]*([^\r\n]*)$/gm, mn = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, gn = /^(?:GET|HEAD)$/, vn = /^\/\//, yn = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, bn = {}, wn = {}, _n = "*/".concat("*"), xn = e.location.href, kn = yn.exec(xn.toLowerCase()) || [];
	Z.extend({
		active: 0,
		lastModified: {},
		etag: {},
		ajaxSettings: {
			url: xn,
			type: "GET",
			isLocal: mn.test(kn[1]),
			global: !0,
			processData: !0,
			async: !0,
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			accepts: {
				"*": _n,
				text: "text/plain",
				html: "text/html",
				xml: "application/xml, text/xml",
				json: "application/json, text/javascript"
			},
			contents: {xml: /xml/, html: /html/, json: /json/},
			responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
			converters: {"* text": String, "text html": !0, "text json": Z.parseJSON, "text xml": Z.parseXML},
			flatOptions: {url: !0, context: !0}
		},
		ajaxSetup: function (e, t) {
			return t ? O(O(e, Z.ajaxSettings), t) : O(Z.ajaxSettings, e)
		},
		ajaxPrefilter: L(bn),
		ajaxTransport: L(wn),
		ajax: function (e, t) {
			function n(e, t, n, s) {
				var u, c, v, y, w, x = t;
				2 !== b && (b = 2, a && clearTimeout(a), i = void 0, o = s || "", _.readyState = e > 0 ? 4 : 0, u = e >= 200 && 300 > e || 304 === e, n && (y = q(p, _, n)), y = H(p, y, _, u), u ? (p.ifModified && (w = _.getResponseHeader("Last-Modified"), w && (Z.lastModified[r] = w), w = _.getResponseHeader("etag"), w && (Z.etag[r] = w)), 204 === e || "HEAD" === p.type ? x = "nocontent" : 304 === e ? x = "notmodified" : (x = y.state, c = y.data, v = y.error, u = !v)) : (v = x, (e || !x) && (x = "error", 0 > e && (e = 0))), _.status = e, _.statusText = (t || x) + "", u ? d.resolveWith(h, [c, x, _]) : d.rejectWith(h, [_, x, v]), _.statusCode(g), g = void 0, l && f.trigger(u ? "ajaxSuccess" : "ajaxError", [_, p, u ? c : v]), m.fireWith(h, [_, x]), l && (f.trigger("ajaxComplete", [_, p]), --Z.active || Z.event.trigger("ajaxStop")))
			}

			"object" == typeof e && (t = e, e = void 0), t = t || {};
			var i, r, o, s, a, u, l, c, p = Z.ajaxSetup({}, t), h = p.context || p, f = p.context && (h.nodeType || h.jquery) ? Z(h) : Z.event, d = Z.Deferred(), m = Z.Callbacks("once memory"), g = p.statusCode || {}, v = {}, y = {}, b = 0, w = "canceled", _ = {
				readyState: 0,
				getResponseHeader: function (e) {
					var t;
					if (2 === b) {
						if (!s)for (s = {}; t = dn.exec(o);)s[t[1].toLowerCase()] = t[2];
						t = s[e.toLowerCase()]
					}
					return null == t ? null : t
				},
				getAllResponseHeaders: function () {
					return 2 === b ? o : null
				},
				setRequestHeader: function (e, t) {
					var n = e.toLowerCase();
					return b || (e = y[n] = y[n] || e, v[e] = t), this
				},
				overrideMimeType: function (e) {
					return b || (p.mimeType = e), this
				},
				statusCode: function (e) {
					var t;
					if (e)if (2 > b)for (t in e)g[t] = [g[t], e[t]]; else _.always(e[_.status]);
					return this
				},
				abort: function (e) {
					var t = e || w;
					return i && i.abort(t), n(0, t), this
				}
			};
			if (d.promise(_).complete = m.add, _.success = _.done, _.error = _.fail, p.url = ((e || p.url || xn) + "").replace(hn, "").replace(vn, kn[1] + "//"), p.type = t.method || t.type || p.method || p.type, p.dataTypes = Z.trim(p.dataType || "*").toLowerCase().match(ft) || [""], null == p.crossDomain && (u = yn.exec(p.url.toLowerCase()), p.crossDomain = !(!u || u[1] === kn[1] && u[2] === kn[2] && (u[3] || ("http:" === u[1] ? "80" : "443")) === (kn[3] || ("http:" === kn[1] ? "80" : "443")))), p.data && p.processData && "string" != typeof p.data && (p.data = Z.param(p.data, p.traditional)), F(bn, p, t, _), 2 === b)return _;
			l = Z.event && p.global, l && 0 === Z.active++ && Z.event.trigger("ajaxStart"), p.type = p.type.toUpperCase(), p.hasContent = !gn.test(p.type), r = p.url, p.hasContent || (p.data && (r = p.url += (pn.test(r) ? "&" : "?") + p.data, delete p.data), p.cache === !1 && (p.url = fn.test(r) ? r.replace(fn, "$1_=" + cn++) : r + (pn.test(r) ? "&" : "?") + "_=" + cn++)), p.ifModified && (Z.lastModified[r] && _.setRequestHeader("If-Modified-Since", Z.lastModified[r]), Z.etag[r] && _.setRequestHeader("If-None-Match", Z.etag[r])), (p.data && p.hasContent && p.contentType !== !1 || t.contentType) && _.setRequestHeader("Content-Type", p.contentType), _.setRequestHeader("Accept", p.dataTypes[0] && p.accepts[p.dataTypes[0]] ? p.accepts[p.dataTypes[0]] + ("*" !== p.dataTypes[0] ? ", " + _n + "; q=0.01" : "") : p.accepts["*"]);
			for (c in p.headers)_.setRequestHeader(c, p.headers[c]);
			if (p.beforeSend && (p.beforeSend.call(h, _, p) === !1 || 2 === b))return _.abort();
			w = "abort";
			for (c in{success: 1, error: 1, complete: 1})_[c](p[c]);
			if (i = F(wn, p, t, _)) {
				_.readyState = 1, l && f.trigger("ajaxSend", [_, p]), p.async && p.timeout > 0 && (a = setTimeout(function () {
					_.abort("timeout")
				}, p.timeout));
				try {
					b = 1, i.send(v, n)
				} catch (x) {
					if (!(2 > b))throw x;
					n(-1, x)
				}
			} else n(-1, "No Transport");
			return _
		},
		getJSON: function (e, t, n) {
			return Z.get(e, t, n, "json")
		},
		getScript: function (e, t) {
			return Z.get(e, void 0, t, "script")
		}
	}), Z.each(["get", "post"], function (e, t) {
		Z[t] = function (e, n, i, r) {
			return Z.isFunction(n) && (r = r || i, i = n, n = void 0), Z.ajax({
				url: e,
				type: t,
				dataType: r,
				data: n,
				success: i
			})
		}
	}), Z._evalUrl = function (e) {
		return Z.ajax({url: e, type: "GET", dataType: "script", async: !1, global: !1, "throws": !0})
	}, Z.fn.extend({
		wrapAll: function (e) {
			var t;
			return Z.isFunction(e) ? this.each(function (t) {
				Z(this).wrapAll(e.call(this, t))
			}) : (this[0] && (t = Z(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
				for (var e = this; e.firstElementChild;)e = e.firstElementChild;
				return e
			}).append(this)), this)
		}, wrapInner: function (e) {
			return this.each(Z.isFunction(e) ? function (t) {
				Z(this).wrapInner(e.call(this, t))
			} : function () {
				var t = Z(this), n = t.contents();
				n.length ? n.wrapAll(e) : t.append(e)
			})
		}, wrap: function (e) {
			var t = Z.isFunction(e);
			return this.each(function (n) {
				Z(this).wrapAll(t ? e.call(this, n) : e)
			})
		}, unwrap: function () {
			return this.parent().each(function () {
				Z.nodeName(this, "body") || Z(this).replaceWith(this.childNodes)
			}).end()
		}
	}), Z.expr.filters.hidden = function (e) {
		return e.offsetWidth <= 0 && e.offsetHeight <= 0
	}, Z.expr.filters.visible = function (e) {
		return !Z.expr.filters.hidden(e)
	};
	var Sn = /%20/g, Cn = /\[\]$/, Tn = /\r?\n/g, $n = /^(?:submit|button|image|reset|file)$/i, En = /^(?:input|select|textarea|keygen)/i;
	Z.param = function (e, t) {
		var n, i = [], r = function (e, t) {
			t = Z.isFunction(t) ? t() : null == t ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
		};
		if (void 0 === t && (t = Z.ajaxSettings && Z.ajaxSettings.traditional), Z.isArray(e) || e.jquery && !Z.isPlainObject(e))Z.each(e, function () {
			r(this.name, this.value)
		}); else for (n in e)P(n, e[n], t, r);
		return i.join("&").replace(Sn, "+")
	}, Z.fn.extend({
		serialize: function () {
			return Z.param(this.serializeArray())
		}, serializeArray: function () {
			return this.map(function () {
				var e = Z.prop(this, "elements");
				return e ? Z.makeArray(e) : this
			}).filter(function () {
				var e = this.type;
				return this.name && !Z(this).is(":disabled") && En.test(this.nodeName) && !$n.test(e) && (this.checked || !St.test(e))
			}).map(function (e, t) {
				var n = Z(this).val();
				return null == n ? null : Z.isArray(n) ? Z.map(n, function (e) {
					return {name: t.name, value: e.replace(Tn, "\r\n")}
				}) : {name: t.name, value: n.replace(Tn, "\r\n")}
			}).get()
		}
	}), Z.ajaxSettings.xhr = function () {
		try {
			return new XMLHttpRequest
		} catch (e) {
		}
	};
	var jn = 0, An = {}, Nn = {0: 200, 1223: 204}, Dn = Z.ajaxSettings.xhr();
	e.attachEvent && e.attachEvent("onunload", function () {
		for (var e in An)An[e]()
	}), G.cors = !!Dn && "withCredentials"in Dn, G.ajax = Dn = !!Dn, Z.ajaxTransport(function (e) {
		var t;
		return G.cors || Dn && !e.crossDomain ? {
			send: function (n, i) {
				var r, o = e.xhr(), s = ++jn;
				if (o.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)for (r in e.xhrFields)o[r] = e.xhrFields[r];
				e.mimeType && o.overrideMimeType && o.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
				for (r in n)o.setRequestHeader(r, n[r]);
				t = function (e) {
					return function () {
						t && (delete An[s], t = o.onload = o.onerror = null, "abort" === e ? o.abort() : "error" === e ? i(o.status, o.statusText) : i(Nn[o.status] || o.status, o.statusText, "string" == typeof o.responseText ? {text: o.responseText} : void 0, o.getAllResponseHeaders()))
					}
				}, o.onload = t(), o.onerror = t("error"), t = An[s] = t("abort");
				try {
					o.send(e.hasContent && e.data || null)
				} catch (a) {
					if (t)throw a
				}
			}, abort: function () {
				t && t()
			}
		} : void 0
	}), Z.ajaxSetup({
		accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},
		contents: {script: /(?:java|ecma)script/},
		converters: {
			"text script": function (e) {
				return Z.globalEval(e), e
			}
		}
	}), Z.ajaxPrefilter("script", function (e) {
		void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET")
	}), Z.ajaxTransport("script", function (e) {
		if (e.crossDomain) {
			var t, n;
			return {
				send: function (i, r) {
					t = Z("<script>").prop({
						async: !0,
						charset: e.scriptCharset,
						src: e.url
					}).on("load error", n = function (e) {
						t.remove(), n = null, e && r("error" === e.type ? 404 : 200, e.type)
					}), Y.head.appendChild(t[0])
				}, abort: function () {
					n && n()
				}
			}
		}
	});
	var In = [], Mn = /(=)\?(?=&|$)|\?\?/;
	Z.ajaxSetup({
		jsonp: "callback", jsonpCallback: function () {
			var e = In.pop() || Z.expando + "_" + cn++;
			return this[e] = !0, e
		}
	}), Z.ajaxPrefilter("json jsonp", function (t, n, i) {
		var r, o, s, a = t.jsonp !== !1 && (Mn.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && Mn.test(t.data) && "data");
		return a || "jsonp" === t.dataTypes[0] ? (r = t.jsonpCallback = Z.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, a ? t[a] = t[a].replace(Mn, "$1" + r) : t.jsonp !== !1 && (t.url += (pn.test(t.url) ? "&" : "?") + t.jsonp + "=" + r), t.converters["script json"] = function () {
			return s || Z.error(r + " was not called"), s[0]
		}, t.dataTypes[0] = "json", o = e[r], e[r] = function () {
			s = arguments
		}, i.always(function () {
			e[r] = o, t[r] && (t.jsonpCallback = n.jsonpCallback, In.push(r)), s && Z.isFunction(o) && o(s[0]), s = o = void 0
		}), "script") : void 0
	}), Z.parseHTML = function (e, t, n) {
		if (!e || "string" != typeof e)return null;
		"boolean" == typeof t && (n = t, t = !1), t = t || Y;
		var i = st.exec(e), r = !n && [];
		return i ? [t.createElement(i[1])] : (i = Z.buildFragment([e], t, r), r && r.length && Z(r).remove(), Z.merge([], i.childNodes))
	};
	var Ln = Z.fn.load;
	Z.fn.load = function (e, t, n) {
		if ("string" != typeof e && Ln)return Ln.apply(this, arguments);
		var i, r, o, s = this, a = e.indexOf(" ");
		return a >= 0 && (i = Z.trim(e.slice(a)), e = e.slice(0, a)), Z.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (r = "POST"), s.length > 0 && Z.ajax({
			url: e,
			type: r,
			dataType: "html",
			data: t
		}).done(function (e) {
			o = arguments, s.html(i ? Z("<div>").append(Z.parseHTML(e)).find(i) : e)
		}).complete(n && function (e, t) {
			s.each(n, o || [e.responseText, t, e])
		}), this
	}, Z.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
		Z.fn[t] = function (e) {
			return this.on(t, e)
		}
	}), Z.expr.filters.animated = function (e) {
		return Z.grep(Z.timers, function (t) {
			return e === t.elem
		}).length
	};
	var Fn = e.document.documentElement;
	Z.offset = {
		setOffset: function (e, t, n) {
			var i, r, o, s, a, u, l, c = Z.css(e, "position"), p = Z(e), h = {};
			"static" === c && (e.style.position = "relative"), a = p.offset(), o = Z.css(e, "top"), u = Z.css(e, "left"), l = ("absolute" === c || "fixed" === c) && (o + u).indexOf("auto") > -1, l ? (i = p.position(), s = i.top, r = i.left) : (s = parseFloat(o) || 0, r = parseFloat(u) || 0), Z.isFunction(t) && (t = t.call(e, n, a)), null != t.top && (h.top = t.top - a.top + s), null != t.left && (h.left = t.left - a.left + r), "using"in t ? t.using.call(e, h) : p.css(h)
		}
	}, Z.fn.extend({
		offset: function (e) {
			if (arguments.length)return void 0 === e ? this : this.each(function (t) {
				Z.offset.setOffset(this, e, t)
			});
			var t, n, i = this[0], r = {top: 0, left: 0}, o = i && i.ownerDocument;
			if (o)return t = o.documentElement, Z.contains(t, i) ? (typeof i.getBoundingClientRect !== Ct && (r = i.getBoundingClientRect()), n = R(o), {
				top: r.top + n.pageYOffset - t.clientTop,
				left: r.left + n.pageXOffset - t.clientLeft
			}) : r
		}, position: function () {
			if (this[0]) {
				var e, t, n = this[0], i = {top: 0, left: 0};
				return "fixed" === Z.css(n, "position") ? t = n.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), Z.nodeName(e[0], "html") || (i = e.offset()), i.top += Z.css(e[0], "borderTopWidth", !0), i.left += Z.css(e[0], "borderLeftWidth", !0)), {
					top: t.top - i.top - Z.css(n, "marginTop", !0),
					left: t.left - i.left - Z.css(n, "marginLeft", !0)
				}
			}
		}, offsetParent: function () {
			return this.map(function () {
				for (var e = this.offsetParent || Fn; e && !Z.nodeName(e, "html") && "static" === Z.css(e, "position");)e = e.offsetParent;
				return e || Fn
			})
		}
	}), Z.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function (t, n) {
		var i = "pageYOffset" === n;
		Z.fn[t] = function (r) {
			return gt(this, function (t, r, o) {
				var s = R(t);
				return void 0 === o ? s ? s[n] : t[r] : void(s ? s.scrollTo(i ? e.pageXOffset : o, i ? o : e.pageYOffset) : t[r] = o)
			}, t, r, arguments.length, null)
		}
	}), Z.each(["top", "left"], function (e, t) {
		Z.cssHooks[t] = x(G.pixelPosition, function (e, n) {
			return n ? (n = _(e, t), Bt.test(n) ? Z(e).position()[t] + "px" : n) : void 0
		})
	}), Z.each({Height: "height", Width: "width"}, function (e, t) {
		Z.each({padding: "inner" + e, content: t, "": "outer" + e}, function (n, i) {
			Z.fn[i] = function (i, r) {
				var o = arguments.length && (n || "boolean" != typeof i), s = n || (i === !0 || r === !0 ? "margin" : "border");
				return gt(this, function (t, n, i) {
					var r;
					return Z.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (r = t.documentElement, Math.max(t.body["scroll" + e], r["scroll" + e], t.body["offset" + e], r["offset" + e], r["client" + e])) : void 0 === i ? Z.css(t, n, s) : Z.style(t, n, i, s)
				}, t, o ? i : void 0, o, null)
			}
		})
	}), Z.fn.size = function () {
		return this.length
	}, Z.fn.andSelf = Z.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
		return Z
	});
	var On = e.jQuery, qn = e.$;
	return Z.noConflict = function (t) {
		return e.$ === Z && (e.$ = qn), t && e.jQuery === Z && (e.jQuery = On), Z
	}, typeof t === Ct && (e.jQuery = e.$ = Z), Z
}), function () {
	function e(e, t, n) {
		for (var i = (n || 0) - 1, r = e ? e.length : 0; ++i < r;)if (e[i] === t)return i;
		return -1
	}

	function t(t, n) {
		var i = typeof n;
		if (t = t.cache, "boolean" == i || null == n)return t[n] ? 0 : -1;
		"number" != i && "string" != i && (i = "object");
		var r = "number" == i ? n : v + n;
		return t = (t = t[i]) && t[r], "object" == i ? t && e(t, n) > -1 ? 0 : -1 : t ? 0 : -1
	}

	function n(e) {
		var t = this.cache, n = typeof e;
		if ("boolean" == n || null == e)t[e] = !0; else {
			"number" != n && "string" != n && (n = "object");
			var i = "number" == n ? e : v + e, r = t[n] || (t[n] = {});
			"object" == n ? (r[i] || (r[i] = [])).push(e) : r[i] = !0
		}
	}

	function i(e) {
		return e.charCodeAt(0)
	}

	function r(e, t) {
		for (var n = e.criteria, i = t.criteria, r = -1, o = n.length; ++r < o;) {
			var s = n[r], a = i[r];
			if (s !== a) {
				if (s > a || "undefined" == typeof s)return 1;
				if (a > s || "undefined" == typeof a)return -1
			}
		}
		return e.index - t.index
	}

	function o(e) {
		var t = -1, i = e.length, r = e[0], o = e[i / 2 | 0], s = e[i - 1];
		if (r && "object" == typeof r && o && "object" == typeof o && s && "object" == typeof s)return !1;
		var a = u();
		a["false"] = a["null"] = a["true"] = a.undefined = !1;
		var l = u();
		for (l.array = e, l.cache = a, l.push = n; ++t < i;)l.push(e[t]);
		return l
	}

	function s(e) {
		return "\\" + X[e]
	}

	function a() {
		return d.pop() || []
	}

	function u() {
		return m.pop() || {
			array: null,
			cache: null,
			criteria: null,
			"false": !1,
			index: 0,
			"null": !1,
			number: null,
			object: null,
			push: null,
			string: null,
			"true": !1,
			undefined: !1,
			value: null
		}
	}

	function l(e) {
		e.length = 0, d.length < b && d.push(e)
	}

	function c(e) {
		var t = e.cache;
		t && c(t), e.array = e.cache = e.criteria = e.object = e.number = e.string = e.value = null, m.length < b && m.push(e)
	}

	function p(e, t, n) {
		t || (t = 0), "undefined" == typeof n && (n = e ? e.length : 0);
		for (var i = -1, r = n - t || 0, o = Array(0 > r ? 0 : r); ++i < r;)o[i] = e[t + i];
		return o
	}

	function h(n) {
		function d(e) {
			return e && "object" == typeof e && !Qi(e) && Mi.call(e, "__wrapped__") ? e : new m(e)
		}

		function m(e, t) {
			this.__chain__ = !!t, this.__wrapped__ = e
		}

		function b(e) {
			function t() {
				if (i) {
					var e = p(i);
					Li.apply(e, arguments)
				}
				if (this instanceof t) {
					var o = K(n.prototype), s = n.apply(o, e || arguments);
					return At(s) ? s : o
				}
				return n.apply(r, e || arguments)
			}

			var n = e[0], i = e[2], r = e[4];
			return Yi(t, e), t
		}

		function X(e, t, n, i, r) {
			if (n) {
				var o = n(e);
				if ("undefined" != typeof o)return o
			}
			var s = At(e);
			if (!s)return e;
			var u = $i.call(e);
			if (!z[u])return e;
			var c = Ki[u];
			switch (u) {
				case F:
				case O:
					return new c(+e);
				case H:
				case B:
					return new c(e);
				case R:
					return o = c(e.source, C.exec(e)), o.lastIndex = e.lastIndex, o
			}
			var h = Qi(e);
			if (t) {
				var f = !i;
				i || (i = a()), r || (r = a());
				for (var d = i.length; d--;)if (i[d] == e)return r[d];
				o = h ? c(e.length) : {}
			} else o = h ? p(e) : or({}, e);
			return h && (Mi.call(e, "index") && (o.index = e.index), Mi.call(e, "input") && (o.input = e.input)), t ? (i.push(e), r.push(o), (h ? Gt : ur)(e, function (e, s) {
				o[s] = X(e, t, n, i, r)
			}), f && (l(i), l(r)), o) : o
		}

		function K(e) {
			return At(e) ? Pi(e) : {}
		}

		function G(e, t, n) {
			if ("function" != typeof e)return Qn;
			if ("undefined" == typeof t || !("prototype"in e))return e;
			var i = e.__bindData__;
			if ("undefined" == typeof i && (Gi.funcNames && (i = !e.name), i = i || !Gi.funcDecomp, !i)) {
				var r = Di.call(e);
				Gi.funcNames || (i = !T.test(r)), i || (i = A.test(r), Yi(e, i))
			}
			if (i === !1 || i !== !0 && 1 & i[1])return e;
			switch (n) {
				case 1:
					return function (n) {
						return e.call(t, n)
					};
				case 2:
					return function (n, i) {
						return e.call(t, n, i)
					};
				case 3:
					return function (n, i, r) {
						return e.call(t, n, i, r)
					};
				case 4:
					return function (n, i, r, o) {
						return e.call(t, n, i, r, o)
					}
			}
			return Ln(e, t)
		}

		function Y(e) {
			function t() {
				var e = u ? s : this;
				if (r) {
					var d = p(r);
					Li.apply(d, arguments)
				}
				if ((o || c) && (d || (d = p(arguments)), o && Li.apply(d, o), c && d.length < a))return i |= 16, Y([n, h ? i : -4 & i, d, null, s, a]);
				if (d || (d = arguments), l && (n = e[f]), this instanceof t) {
					e = K(n.prototype);
					var m = n.apply(e, d);
					return At(m) ? m : e
				}
				return n.apply(e, d)
			}

			var n = e[0], i = e[1], r = e[2], o = e[3], s = e[4], a = e[5], u = 1 & i, l = 2 & i, c = 4 & i, h = 8 & i, f = n;
			return Yi(t, e), t
		}

		function Q(n, i) {
			var r = -1, s = ut(), a = n ? n.length : 0, u = a >= y && s === e, l = [];
			if (u) {
				var p = o(i);
				p ? (s = t, i = p) : u = !1
			}
			for (; ++r < a;) {
				var h = n[r];
				s(i, h) < 0 && l.push(h)
			}
			return u && c(i), l
		}

		function et(e, t, n, i) {
			for (var r = (i || 0) - 1, o = e ? e.length : 0, s = []; ++r < o;) {
				var a = e[r];
				if (a && "object" == typeof a && "number" == typeof a.length && (Qi(a) || ht(a))) {
					t || (a = et(a, t, n));
					var u = -1, l = a.length, c = s.length;
					for (s.length += l; ++u < l;)s[c++] = a[u]
				} else n || s.push(a)
			}
			return s
		}

		function tt(e, t, n, i, r, o) {
			if (n) {
				var s = n(e, t);
				if ("undefined" != typeof s)return !!s
			}
			if (e === t)return 0 !== e || 1 / e == 1 / t;
			var u = typeof e, c = typeof t;
			if (!(e !== e || e && J[u] || t && J[c]))return !1;
			if (null == e || null == t)return e === t;
			var p = $i.call(e), h = $i.call(t);
			if (p == M && (p = P), h == M && (h = P), p != h)return !1;
			switch (p) {
				case F:
				case O:
					return +e == +t;
				case H:
					return e != +e ? t != +t : 0 == e ? 1 / e == 1 / t : e == +t;
				case R:
				case B:
					return e == xi(t)
			}
			var f = p == L;
			if (!f) {
				var d = Mi.call(e, "__wrapped__"), m = Mi.call(t, "__wrapped__");
				if (d || m)return tt(d ? e.__wrapped__ : e, m ? t.__wrapped__ : t, n, i, r, o);
				if (p != P)return !1;
				var g = e.constructor, v = t.constructor;
				if (g != v && !(jt(g) && g instanceof g && jt(v) && v instanceof v) && "constructor"in e && "constructor"in t)return !1
			}
			var y = !r;
			r || (r = a()), o || (o = a());
			for (var b = r.length; b--;)if (r[b] == e)return o[b] == t;
			var w = 0;
			if (s = !0, r.push(e), o.push(t), f) {
				if (b = e.length, w = t.length, s = w == b, s || i)for (; w--;) {
					var _ = b, x = t[w];
					if (i)for (; _-- && !(s = tt(e[_], x, n, i, r, o));); else if (!(s = tt(e[w], x, n, i, r, o)))break
				}
			} else ar(t, function (t, a, u) {
				return Mi.call(u, a) ? (w++, s = Mi.call(e, a) && tt(e[a], t, n, i, r, o)) : void 0
			}), s && !i && ar(e, function (e, t, n) {
				return Mi.call(n, t) ? s = --w > -1 : void 0
			});
			return r.pop(), o.pop(), y && (l(r), l(o)), s
		}

		function nt(e, t, n, i, r) {
			(Qi(t) ? Gt : ur)(t, function (t, o) {
				var s, a, u = t, l = e[o];
				if (t && ((a = Qi(t)) || lr(t))) {
					for (var c = i.length; c--;)if (s = i[c] == t) {
						l = r[c];
						break
					}
					if (!s) {
						var p;
						n && (u = n(l, t), (p = "undefined" != typeof u) && (l = u)), p || (l = a ? Qi(l) ? l : [] : lr(l) ? l : {}), i.push(t), r.push(l), p || nt(l, t, n, i, r)
					}
				} else n && (u = n(l, t), "undefined" == typeof u && (u = t)), "undefined" != typeof u && (l = u);
				e[o] = l
			})
		}

		function it(e, t) {
			return e + Ni(Vi() * (t - e + 1))
		}

		function rt(n, i, r) {
			var s = -1, u = ut(), p = n ? n.length : 0, h = [], f = !i && p >= y && u === e, d = r || f ? a() : h;
			if (f) {
				var m = o(d);
				u = t, d = m
			}
			for (; ++s < p;) {
				var g = n[s], v = r ? r(g, s, n) : g;
				(i ? !s || d[d.length - 1] !== v : u(d, v) < 0) && ((r || f) && d.push(v), h.push(g))
			}
			return f ? (l(d.array), c(d)) : r && l(d), h
		}

		function ot(e) {
			return function (t, n, i) {
				var r = {};
				n = d.createCallback(n, i, 3);
				var o = -1, s = t ? t.length : 0;
				if ("number" == typeof s)for (; ++o < s;) {
					var a = t[o];
					e(r, a, n(a, o, t), t)
				} else ur(t, function (t, i, o) {
					e(r, t, n(t, i, o), o)
				});
				return r
			}
		}

		function st(e, t, n, i, r, o) {
			var s = 1 & t, a = 2 & t, u = 4 & t, l = 16 & t, c = 32 & t;
			if (!a && !jt(e))throw new ki;
			l && !n.length && (t &= -17, l = n = !1), c && !i.length && (t &= -33, c = i = !1);
			var h = e && e.__bindData__;
			if (h && h !== !0)return h = p(h), h[2] && (h[2] = p(h[2])), h[3] && (h[3] = p(h[3])), !s || 1 & h[1] || (h[4] = r), !s && 1 & h[1] && (t |= 8), !u || 4 & h[1] || (h[5] = o), l && Li.apply(h[2] || (h[2] = []), n), c && qi.apply(h[3] || (h[3] = []), i), h[1] |= t, st.apply(null, h);
			var f = 1 == t || 17 === t ? b : Y;
			return f([e, t, n, i, r, o])
		}

		function at(e) {
			return tr[e]
		}

		function ut() {
			var t = (t = d.indexOf) === yn ? e : t;
			return t
		}

		function lt(e) {
			return "function" == typeof e && Ei.test(e)
		}

		function ct(e) {
			var t, n;
			return e && $i.call(e) == P && (t = e.constructor, !jt(t) || t instanceof t) ? (ar(e, function (e, t) {
				n = t
			}), "undefined" == typeof n || Mi.call(e, n)) : !1
		}

		function pt(e) {
			return nr[e]
		}

		function ht(e) {
			return e && "object" == typeof e && "number" == typeof e.length && $i.call(e) == M || !1
		}

		function ft(e, t, n, i) {
			return "boolean" != typeof t && null != t && (i = n, n = t, t = !1), X(e, t, "function" == typeof n && G(n, i, 1))
		}

		function dt(e, t, n) {
			return X(e, !0, "function" == typeof t && G(t, n, 1))
		}

		function mt(e, t) {
			var n = K(e);
			return t ? or(n, t) : n
		}

		function gt(e, t, n) {
			var i;
			return t = d.createCallback(t, n, 3), ur(e, function (e, n, r) {
				return t(e, n, r) ? (i = n, !1) : void 0
			}), i
		}

		function vt(e, t, n) {
			var i;
			return t = d.createCallback(t, n, 3), bt(e, function (e, n, r) {
				return t(e, n, r) ? (i = n, !1) : void 0
			}), i
		}

		function yt(e, t, n) {
			var i = [];
			ar(e, function (e, t) {
				i.push(t, e)
			});
			var r = i.length;
			for (t = G(t, n, 3); r-- && t(i[r--], i[r], e) !== !1;);
			return e
		}

		function bt(e, t, n) {
			var i = er(e), r = i.length;
			for (t = G(t, n, 3); r--;) {
				var o = i[r];
				if (t(e[o], o, e) === !1)break
			}
			return e
		}

		function wt(e) {
			var t = [];
			return ar(e, function (e, n) {
				jt(e) && t.push(n)
			}), t.sort()
		}

		function _t(e, t) {
			return e ? Mi.call(e, t) : !1
		}

		function xt(e) {
			for (var t = -1, n = er(e), i = n.length, r = {}; ++t < i;) {
				var o = n[t];
				r[e[o]] = o
			}
			return r
		}

		function kt(e) {
			return e === !0 || e === !1 || e && "object" == typeof e && $i.call(e) == F || !1
		}

		function St(e) {
			return e && "object" == typeof e && $i.call(e) == O || !1
		}

		function Ct(e) {
			return e && 1 === e.nodeType || !1
		}

		function Tt(e) {
			var t = !0;
			if (!e)return t;
			var n = $i.call(e), i = e.length;
			return n == L || n == B || n == M || n == P && "number" == typeof i && jt(e.splice) ? !i : (ur(e, function () {
				return t = !1
			}), t)
		}

		function $t(e, t, n, i) {
			return tt(e, t, "function" == typeof n && G(n, i, 2))
		}

		function Et(e) {
			return Bi(e) && !zi(parseFloat(e))
		}

		function jt(e) {
			return "function" == typeof e
		}

		function At(e) {
			return !(!e || !J[typeof e])
		}

		function Nt(e) {
			return It(e) && e != +e
		}

		function Dt(e) {
			return null === e
		}

		function It(e) {
			return "number" == typeof e || e && "object" == typeof e && $i.call(e) == H || !1
		}

		function Mt(e) {
			return e && "object" == typeof e && $i.call(e) == R || !1
		}

		function Lt(e) {
			return "string" == typeof e || e && "object" == typeof e && $i.call(e) == B || !1
		}

		function Ft(e) {
			return "undefined" == typeof e
		}

		function Ot(e, t, n) {
			var i = {};
			return t = d.createCallback(t, n, 3), ur(e, function (e, n, r) {
				i[n] = t(e, n, r)
			}), i
		}

		function qt(e) {
			var t = arguments, n = 2;
			if (!At(e))return e;
			if ("number" != typeof t[2] && (n = t.length), n > 3 && "function" == typeof t[n - 2])var i = G(t[--n - 1], t[n--], 2); else n > 2 && "function" == typeof t[n - 1] && (i = t[--n]);
			for (var r = p(arguments, 1, n), o = -1, s = a(), u = a(); ++o < n;)nt(e, r[o], i, s, u);
			return l(s), l(u), e
		}

		function Ht(e, t, n) {
			var i = {};
			if ("function" != typeof t) {
				var r = [];
				ar(e, function (e, t) {
					r.push(t)
				}), r = Q(r, et(arguments, !0, !1, 1));
				for (var o = -1, s = r.length; ++o < s;) {
					var a = r[o];
					i[a] = e[a]
				}
			} else t = d.createCallback(t, n, 3), ar(e, function (e, n, r) {
				t(e, n, r) || (i[n] = e)
			});
			return i
		}

		function Pt(e) {
			for (var t = -1, n = er(e), i = n.length, r = di(i); ++t < i;) {
				var o = n[t];
				r[t] = [o, e[o]]
			}
			return r
		}

		function Rt(e, t, n) {
			var i = {};
			if ("function" != typeof t)for (var r = -1, o = et(arguments, !0, !1, 1), s = At(e) ? o.length : 0; ++r < s;) {
				var a = o[r];
				a in e && (i[a] = e[a])
			} else t = d.createCallback(t, n, 3), ar(e, function (e, n, r) {
				t(e, n, r) && (i[n] = e)
			});
			return i
		}

		function Bt(e, t, n, i) {
			var r = Qi(e);
			if (null == n)if (r)n = []; else {
				var o = e && e.constructor, s = o && o.prototype;
				n = K(s)
			}
			return t && (t = d.createCallback(t, i, 4), (r ? Gt : ur)(e, function (e, i, r) {
				return t(n, e, i, r)
			})), n
		}

		function zt(e) {
			for (var t = -1, n = er(e), i = n.length, r = di(i); ++t < i;)r[t] = e[n[t]];
			return r
		}

		function Ut(e) {
			for (var t = arguments, n = -1, i = et(t, !0, !1, 1), r = t[2] && t[2][t[1]] === e ? 1 : i.length, o = di(r); ++n < r;)o[n] = e[i[n]];
			return o
		}

		function Wt(e, t, n) {
			var i = -1, r = ut(), o = e ? e.length : 0, s = !1;
			return n = (0 > n ? Wi(0, o + n) : n) || 0, Qi(e) ? s = r(e, t, n) > -1 : "number" == typeof o ? s = (Lt(e) ? e.indexOf(t, n) : r(e, t, n)) > -1 : ur(e, function (e) {
				return ++i >= n ? !(s = e === t) : void 0
			}), s
		}

		function Jt(e, t, n) {
			var i = !0;
			t = d.createCallback(t, n, 3);
			var r = -1, o = e ? e.length : 0;
			if ("number" == typeof o)for (; ++r < o && (i = !!t(e[r], r, e));); else ur(e, function (e, n, r) {
				return i = !!t(e, n, r)
			});
			return i
		}

		function Xt(e, t, n) {
			var i = [];
			t = d.createCallback(t, n, 3);
			var r = -1, o = e ? e.length : 0;
			if ("number" == typeof o)for (; ++r < o;) {
				var s = e[r];
				t(s, r, e) && i.push(s)
			} else ur(e, function (e, n, r) {
				t(e, n, r) && i.push(e)
			});
			return i
		}

		function Vt(e, t, n) {
			t = d.createCallback(t, n, 3);
			var i = -1, r = e ? e.length : 0;
			if ("number" != typeof r) {
				var o;
				return ur(e, function (e, n, i) {
					return t(e, n, i) ? (o = e, !1) : void 0
				}), o
			}
			for (; ++i < r;) {
				var s = e[i];
				if (t(s, i, e))return s
			}
		}

		function Kt(e, t, n) {
			var i;
			return t = d.createCallback(t, n, 3), Yt(e, function (e, n, r) {
				return t(e, n, r) ? (i = e, !1) : void 0
			}), i
		}

		function Gt(e, t, n) {
			var i = -1, r = e ? e.length : 0;
			if (t = t && "undefined" == typeof n ? t : G(t, n, 3), "number" == typeof r)for (; ++i < r && t(e[i], i, e) !== !1;); else ur(e, t);
			return e
		}

		function Yt(e, t, n) {
			var i = e ? e.length : 0;
			if (t = t && "undefined" == typeof n ? t : G(t, n, 3), "number" == typeof i)for (; i-- && t(e[i], i, e) !== !1;); else {
				var r = er(e);
				i = r.length, ur(e, function (e, n, o) {
					return n = r ? r[--i] : --i, t(o[n], n, o)
				})
			}
			return e
		}

		function Qt(e, t) {
			var n = p(arguments, 2), i = -1, r = "function" == typeof t, o = e ? e.length : 0, s = di("number" == typeof o ? o : 0);
			return Gt(e, function (e) {
				s[++i] = (r ? t : e[t]).apply(e, n)
			}), s
		}

		function Zt(e, t, n) {
			var i = -1, r = e ? e.length : 0;
			if (t = d.createCallback(t, n, 3), "number" == typeof r)for (var o = di(r); ++i < r;)o[i] = t(e[i], i, e); else o = [], ur(e, function (e, n, r) {
				o[++i] = t(e, n, r)
			});
			return o
		}

		function en(e, t, n) {
			var r = -1 / 0, o = r;
			if ("function" != typeof t && n && n[t] === e && (t = null), null == t && Qi(e))for (var s = -1, a = e.length; ++s < a;) {
				var u = e[s];
				u > o && (o = u)
			} else t = null == t && Lt(e) ? i : d.createCallback(t, n, 3), Gt(e, function (e, n, i) {
				var s = t(e, n, i);
				s > r && (r = s, o = e)
			});
			return o
		}

		function tn(e, t, n) {
			var r = 1 / 0, o = r;
			if ("function" != typeof t && n && n[t] === e && (t = null), null == t && Qi(e))for (var s = -1, a = e.length; ++s < a;) {
				var u = e[s];
				o > u && (o = u)
			} else t = null == t && Lt(e) ? i : d.createCallback(t, n, 3), Gt(e, function (e, n, i) {
				var s = t(e, n, i);
				r > s && (r = s, o = e)
			});
			return o
		}

		function nn(e, t, n, i) {
			if (!e)return n;
			var r = arguments.length < 3;
			t = d.createCallback(t, i, 4);
			var o = -1, s = e.length;
			if ("number" == typeof s)for (r && (n = e[++o]); ++o < s;)n = t(n, e[o], o, e); else ur(e, function (e, i, o) {
				n = r ? (r = !1, e) : t(n, e, i, o)
			});
			return n
		}

		function rn(e, t, n, i) {
			var r = arguments.length < 3;
			return t = d.createCallback(t, i, 4), Yt(e, function (e, i, o) {
				n = r ? (r = !1, e) : t(n, e, i, o)
			}), n
		}

		function on(e, t, n) {
			return t = d.createCallback(t, n, 3), Xt(e, function (e, n, i) {
				return !t(e, n, i)
			})
		}

		function sn(e, t, n) {
			if (e && "number" != typeof e.length && (e = zt(e)), null == t || n)return e ? e[it(0, e.length - 1)] : f;
			var i = an(e);
			return i.length = Ji(Wi(0, t), i.length), i
		}

		function an(e) {
			var t = -1, n = e ? e.length : 0, i = di("number" == typeof n ? n : 0);
			return Gt(e, function (e) {
				var n = it(0, ++t);
				i[t] = i[n], i[n] = e
			}), i
		}

		function un(e) {
			var t = e ? e.length : 0;
			return "number" == typeof t ? t : er(e).length
		}

		function ln(e, t, n) {
			var i;
			t = d.createCallback(t, n, 3);
			var r = -1, o = e ? e.length : 0;
			if ("number" == typeof o)for (; ++r < o && !(i = t(e[r], r, e));); else ur(e, function (e, n, r) {
				return !(i = t(e, n, r))
			});
			return !!i
		}

		function cn(e, t, n) {
			var i = -1, o = Qi(t), s = e ? e.length : 0, p = di("number" == typeof s ? s : 0);
			for (o || (t = d.createCallback(t, n, 3)), Gt(e, function (e, n, r) {
				var s = p[++i] = u();
				o ? s.criteria = Zt(t, function (t) {
					return e[t]
				}) : (s.criteria = a())[0] = t(e, n, r), s.index = i, s.value = e
			}), s = p.length, p.sort(r); s--;) {
				var h = p[s];
				p[s] = h.value, o || l(h.criteria), c(h)
			}
			return p
		}

		function pn(e) {
			return e && "number" == typeof e.length ? p(e) : zt(e)
		}

		function hn(e) {
			for (var t = -1, n = e ? e.length : 0, i = []; ++t < n;) {
				var r = e[t];
				r && i.push(r)
			}
			return i
		}

		function fn(e) {
			return Q(e, et(arguments, !0, !0, 1))
		}

		function dn(e, t, n) {
			var i = -1, r = e ? e.length : 0;
			for (t = d.createCallback(t, n, 3); ++i < r;)if (t(e[i], i, e))return i;
			return -1
		}

		function mn(e, t, n) {
			var i = e ? e.length : 0;
			for (t = d.createCallback(t, n, 3); i--;)if (t(e[i], i, e))return i;
			return -1
		}

		function gn(e, t, n) {
			var i = 0, r = e ? e.length : 0;
			if ("number" != typeof t && null != t) {
				var o = -1;
				for (t = d.createCallback(t, n, 3); ++o < r && t(e[o], o, e);)i++
			} else if (i = t, null == i || n)return e ? e[0] : f;
			return p(e, 0, Ji(Wi(0, i), r))
		}

		function vn(e, t, n, i) {
			return "boolean" != typeof t && null != t && (i = n, n = "function" != typeof t && i && i[t] === e ? null : t, t = !1), null != n && (e = Zt(e, n, i)), et(e, t)
		}

		function yn(t, n, i) {
			if ("number" == typeof i) {
				var r = t ? t.length : 0;
				i = 0 > i ? Wi(0, r + i) : i || 0
			} else if (i) {
				var o = $n(t, n);
				return t[o] === n ? o : -1
			}
			return e(t, n, i)
		}

		function bn(e, t, n) {
			var i = 0, r = e ? e.length : 0;
			if ("number" != typeof t && null != t) {
				var o = r;
				for (t = d.createCallback(t, n, 3); o-- && t(e[o], o, e);)i++
			} else i = null == t || n ? 1 : t || i;
			return p(e, 0, Ji(Wi(0, r - i), r))
		}

		function wn() {
			for (var n = [], i = -1, r = arguments.length, s = a(), u = ut(), p = u === e, h = a(); ++i < r;) {
				var f = arguments[i];
				(Qi(f) || ht(f)) && (n.push(f), s.push(p && f.length >= y && o(i ? n[i] : h)))
			}
			var d = n[0], m = -1, g = d ? d.length : 0, v = [];
			e:for (; ++m < g;) {
				var b = s[0];
				if (f = d[m], (b ? t(b, f) : u(h, f)) < 0) {
					for (i = r, (b || h).push(f); --i;)if (b = s[i], (b ? t(b, f) : u(n[i], f)) < 0)continue e;
					v.push(f)
				}
			}
			for (; r--;)b = s[r], b && c(b);
			return l(s), l(h), v
		}

		function _n(e, t, n) {
			var i = 0, r = e ? e.length : 0;
			if ("number" != typeof t && null != t) {
				var o = r;
				for (t = d.createCallback(t, n, 3); o-- && t(e[o], o, e);)i++
			} else if (i = t, null == i || n)return e ? e[r - 1] : f;
			return p(e, Wi(0, r - i))
		}

		function xn(e, t, n) {
			var i = e ? e.length : 0;
			for ("number" == typeof n && (i = (0 > n ? Wi(0, i + n) : Ji(n, i - 1)) + 1); i--;)if (e[i] === t)return i;
			return -1
		}

		function kn(e) {
			for (var t = arguments, n = 0, i = t.length, r = e ? e.length : 0; ++n < i;)for (var o = -1, s = t[n]; ++o < r;)e[o] === s && (Oi.call(e, o--, 1), r--);
			return e
		}

		function Sn(e, t, n) {
			e = +e || 0, n = "number" == typeof n ? n : +n || 1, null == t && (t = e, e = 0);
			for (var i = -1, r = Wi(0, ji((t - e) / (n || 1))), o = di(r); ++i < r;)o[i] = e, e += n;
			return o
		}

		function Cn(e, t, n) {
			var i = -1, r = e ? e.length : 0, o = [];
			for (t = d.createCallback(t, n, 3); ++i < r;) {
				var s = e[i];
				t(s, i, e) && (o.push(s), Oi.call(e, i--, 1), r--)
			}
			return o
		}

		function Tn(e, t, n) {
			if ("number" != typeof t && null != t) {
				var i = 0, r = -1, o = e ? e.length : 0;
				for (t = d.createCallback(t, n, 3); ++r < o && t(e[r], r, e);)i++
			} else i = null == t || n ? 1 : Wi(0, t);
			return p(e, i)
		}

		function $n(e, t, n, i) {
			var r = 0, o = e ? e.length : r;
			for (n = n ? d.createCallback(n, i, 1) : Qn, t = n(t); o > r;) {
				var s = r + o >>> 1;
				n(e[s]) < t ? r = s + 1 : o = s
			}
			return r
		}

		function En() {
			return rt(et(arguments, !0, !0))
		}

		function jn(e, t, n, i) {
			return "boolean" != typeof t && null != t && (i = n, n = "function" != typeof t && i && i[t] === e ? null : t, t = !1), null != n && (n = d.createCallback(n, i, 3)), rt(e, t, n)
		}

		function An(e) {
			return Q(e, p(arguments, 1))
		}

		function Nn() {
			for (var e = -1, t = arguments.length; ++e < t;) {
				var n = arguments[e];
				if (Qi(n) || ht(n))var i = i ? rt(Q(i, n).concat(Q(n, i))) : n
			}
			return i || []
		}

		function Dn() {
			for (var e = arguments.length > 1 ? arguments : arguments[0], t = -1, n = e ? en(fr(e, "length")) : 0, i = di(0 > n ? 0 : n); ++t < n;)i[t] = fr(e, t);
			return i
		}

		function In(e, t) {
			var n = -1, i = e ? e.length : 0, r = {};
			for (t || !i || Qi(e[0]) || (t = []); ++n < i;) {
				var o = e[n];
				t ? r[o] = t[n] : o && (r[o[0]] = o[1])
			}
			return r
		}

		function Mn(e, t) {
			if (!jt(t))throw new ki;
			return function () {
				return --e < 1 ? t.apply(this, arguments) : void 0
			}
		}

		function Ln(e, t) {
			return arguments.length > 2 ? st(e, 17, p(arguments, 2), null, t) : st(e, 1, null, null, t)
		}

		function Fn(e) {
			for (var t = arguments.length > 1 ? et(arguments, !0, !1, 1) : wt(e), n = -1, i = t.length; ++n < i;) {
				var r = t[n];
				e[r] = st(e[r], 1, null, null, e)
			}
			return e
		}

		function On(e, t) {
			return arguments.length > 2 ? st(t, 19, p(arguments, 2), null, e) : st(t, 3, null, null, e)
		}

		function qn() {
			for (var e = arguments, t = e.length; t--;)if (!jt(e[t]))throw new ki;
			return function () {
				for (var t = arguments, n = e.length; n--;)t = [e[n].apply(this, t)];
				return t[0]
			}
		}

		function Hn(e, t) {
			return t = "number" == typeof t ? t : +t || e.length, st(e, 4, null, null, null, t)
		}

		function Pn(e, t, n) {
			var i, r, o, s, a, u, l, c = 0, p = !1, h = !0;
			if (!jt(e))throw new ki;
			if (t = Wi(0, t) || 0, n === !0) {
				var d = !0;
				h = !1
			} else At(n) && (d = n.leading, p = "maxWait"in n && (Wi(t, n.maxWait) || 0), h = "trailing"in n ? n.trailing : h);
			var m = function () {
				var n = t - (mr() - s);
				if (0 >= n) {
					r && Ai(r);
					var p = l;
					r = u = l = f, p && (c = mr(), o = e.apply(a, i), u || r || (i = a = null))
				} else u = Fi(m, n)
			}, g = function () {
				u && Ai(u), r = u = l = f, (h || p !== t) && (c = mr(), o = e.apply(a, i), u || r || (i = a = null))
			};
			return function () {
				if (i = arguments, s = mr(), a = this, l = h && (u || !d), p === !1)var n = d && !u; else {
					r || d || (c = s);
					var f = p - (s - c), v = 0 >= f;
					v ? (r && (r = Ai(r)), c = s, o = e.apply(a, i)) : r || (r = Fi(g, f))
				}
				return v && u ? u = Ai(u) : u || t === p || (u = Fi(m, t)), n && (v = !0, o = e.apply(a, i)), !v || u || r || (i = a = null), o
			}
		}

		function Rn(e) {
			if (!jt(e))throw new ki;
			var t = p(arguments, 1);
			return Fi(function () {
				e.apply(f, t)
			}, 1)
		}

		function Bn(e, t) {
			if (!jt(e))throw new ki;
			var n = p(arguments, 2);
			return Fi(function () {
				e.apply(f, n)
			}, t)
		}

		function zn(e, t) {
			if (!jt(e))throw new ki;
			var n = function () {
				var i = n.cache, r = t ? t.apply(this, arguments) : v + arguments[0];
				return Mi.call(i, r) ? i[r] : i[r] = e.apply(this, arguments)
			};
			return n.cache = {}, n
		}

		function Un(e) {
			var t, n;
			if (!jt(e))throw new ki;
			return function () {
				return t ? n : (t = !0, n = e.apply(this, arguments), e = null, n)
			}
		}

		function Wn(e) {
			return st(e, 16, p(arguments, 1))
		}

		function Jn(e) {
			return st(e, 32, null, p(arguments, 1))
		}

		function Xn(e, t, n) {
			var i = !0, r = !0;
			if (!jt(e))throw new ki;
			return n === !1 ? i = !1 : At(n) && (i = "leading"in n ? n.leading : i, r = "trailing"in n ? n.trailing : r), U.leading = i, U.maxWait = t, U.trailing = r, Pn(e, t, U)
		}

		function Vn(e, t) {
			return st(t, 16, [e])
		}

		function Kn(e) {
			return function () {
				return e
			}
		}

		function Gn(e, t, n) {
			var i = typeof e;
			if (null == e || "function" == i)return G(e, t, n);
			if ("object" != i)return ni(e);
			var r = er(e), o = r[0], s = e[o];
			return 1 != r.length || s !== s || At(s) ? function (t) {
				for (var n = r.length, i = !1; n-- && (i = tt(t[r[n]], e[r[n]], null, !0)););
				return i
			} : function (e) {
				var t = e[o];
				return s === t && (0 !== s || 1 / s == 1 / t)
			}
		}

		function Yn(e) {
			return null == e ? "" : xi(e).replace(rr, at)
		}

		function Qn(e) {
			return e
		}

		function Zn(e, t, n) {
			var i = !0, r = t && wt(t);
			t && (n || r.length) || (null == n && (n = t), o = m, t = e, e = d, r = wt(t)), n === !1 ? i = !1 : At(n) && "chain"in n && (i = n.chain);
			var o = e, s = jt(o);
			Gt(r, function (n) {
				var r = e[n] = t[n];
				s && (o.prototype[n] = function () {
					var t = this.__chain__, n = this.__wrapped__, s = [n];
					Li.apply(s, arguments);
					var a = r.apply(e, s);
					if (i || t) {
						if (n === a && At(a))return this;
						a = new o(a), a.__chain__ = t
					}
					return a
				})
			})
		}

		function ei() {
			return n._ = Ti, this
		}

		function ti() {
		}

		function ni(e) {
			return function (t) {
				return t[e]
			}
		}

		function ii(e, t, n) {
			var i = null == e, r = null == t;
			if (null == n && ("boolean" == typeof e && r ? (n = e, e = 1) : r || "boolean" != typeof t || (n = t, r = !0)), i && r && (t = 1), e = +e || 0, r ? (t = e, e = 0) : t = +t || 0, n || e % 1 || t % 1) {
				var o = Vi();
				return Ji(e + o * (t - e + parseFloat("1e-" + ((o + "").length - 1))), t)
			}
			return it(e, t)
		}

		function ri(e, t) {
			if (e) {
				var n = e[t];
				return jt(n) ? e[t]() : n
			}
		}

		function oi(e, t, n) {
			var i = d.templateSettings;
			e = xi(e || ""), n = sr({}, n, i);
			var r, o = sr({}, n.imports, i.imports), a = er(o), u = zt(o), l = 0, c = n.interpolate || j, p = "__p += '", h = _i((n.escape || j).source + "|" + c.source + "|" + (c === $ ? S : j).source + "|" + (n.evaluate || j).source + "|$", "g");
			e.replace(h, function (t, n, i, o, a, u) {
				return i || (i = o), p += e.slice(l, u).replace(N, s), n && (p += "' +\n__e(" + n + ") +\n'"), a && (r = !0, p += "';\n" + a + ";\n__p += '"), i && (p += "' +\n((__t = (" + i + ")) == null ? '' : __t) +\n'"), l = u + t.length, t
			}), p += "';\n";
			var m = n.variable, g = m;
			g || (m = "obj", p = "with (" + m + ") {\n" + p + "\n}\n"), p = (r ? p.replace(_, "") : p).replace(x, "$1").replace(k, "$1;"), p = "function(" + m + ") {\n" + (g ? "" : m + " || (" + m + " = {});\n") + "var __t, __p = '', __e = _.escape" + (r ? ", __j = Array.prototype.join;\nfunction print() { __p += __j.call(arguments, '') }\n" : ";\n") + p + "return __p\n}";
			var v = "\n/*\n//# sourceURL=" + (n.sourceURL || "/lodash/template/source[" + I++ + "]") + "\n*/";
			try {
				var y = vi(a, "return " + p + v).apply(f, u)
			} catch (b) {
				throw b.source = p, b
			}
			return t ? y(t) : (y.source = p, y)
		}

		function si(e, t, n) {
			e = (e = +e) > -1 ? e : 0;
			var i = -1, r = di(e);
			for (t = G(t, n, 1); ++i < e;)r[i] = t(i);
			return r
		}

		function ai(e) {
			return null == e ? "" : xi(e).replace(ir, pt)
		}

		function ui(e) {
			var t = ++g;
			return xi(null == e ? "" : e) + t
		}

		function li(e) {
			return e = new m(e), e.__chain__ = !0, e
		}

		function ci(e, t) {
			return t(e), e
		}

		function pi() {
			return this.__chain__ = !0, this
		}

		function hi() {
			return xi(this.__wrapped__)
		}

		function fi() {
			return this.__wrapped__
		}

		n = n ? Z.defaults(V.Object(), n, Z.pick(V, D)) : V;
		var di = n.Array, mi = n.Boolean, gi = n.Date, vi = n.Function, yi = n.Math, bi = n.Number, wi = n.Object, _i = n.RegExp, xi = n.String, ki = n.TypeError, Si = [], Ci = wi.prototype, Ti = n._, $i = Ci.toString, Ei = _i("^" + xi($i).replace(/[.*+?^${}()|[\]\\]/g, "\\$&").replace(/toString| for [^\]]+/g, ".*?") + "$"), ji = yi.ceil, Ai = n.clearTimeout, Ni = yi.floor, Di = vi.prototype.toString, Ii = lt(Ii = wi.getPrototypeOf) && Ii, Mi = Ci.hasOwnProperty, Li = Si.push, Fi = n.setTimeout, Oi = Si.splice, qi = Si.unshift, Hi = function () {
			try {
				var e = {}, t = lt(t = wi.defineProperty) && t, n = t(e, e, e) && t
			} catch (i) {
			}
			return n
		}(), Pi = lt(Pi = wi.create) && Pi, Ri = lt(Ri = di.isArray) && Ri, Bi = n.isFinite, zi = n.isNaN, Ui = lt(Ui = wi.keys) && Ui, Wi = yi.max, Ji = yi.min, Xi = n.parseInt, Vi = yi.random, Ki = {};
		Ki[L] = di, Ki[F] = mi, Ki[O] = gi, Ki[q] = vi, Ki[P] = wi, Ki[H] = bi, Ki[R] = _i, Ki[B] = xi, m.prototype = d.prototype;
		var Gi = d.support = {};
		Gi.funcDecomp = !lt(n.WinRTError) && A.test(h), Gi.funcNames = "string" == typeof vi.name, d.templateSettings = {
			escape: /<%-([\s\S]+?)%>/g,
			evaluate: /<%([\s\S]+?)%>/g,
			interpolate: $,
			variable: "",
			imports: {_: d}
		}, Pi || (K = function () {
			function e() {
			}

			return function (t) {
				if (At(t)) {
					e.prototype = t;
					var i = new e;
					e.prototype = null
				}
				return i || n.Object()
			}
		}());
		var Yi = Hi ? function (e, t) {
			W.value = t, Hi(e, "__bindData__", W)
		} : ti, Qi = Ri || function (e) {
				return e && "object" == typeof e && "number" == typeof e.length && $i.call(e) == L || !1
			}, Zi = function (e) {
			var t, n = e, i = [];
			if (!n)return i;
			if (!J[typeof e])return i;
			for (t in n)Mi.call(n, t) && i.push(t);
			return i
		}, er = Ui ? function (e) {
			return At(e) ? Ui(e) : []
		} : Zi, tr = {
			"&": "&amp;",
			"<": "&lt;",
			">": "&gt;",
			'"': "&quot;",
			"'": "&#39;"
		}, nr = xt(tr), ir = _i("(" + er(nr).join("|") + ")", "g"), rr = _i("[" + er(tr).join("") + "]", "g"), or = function (e, t, n) {
			var i, r = e, o = r;
			if (!r)return o;
			var s = arguments, a = 0, u = "number" == typeof n ? 2 : s.length;
			if (u > 3 && "function" == typeof s[u - 2])var l = G(s[--u - 1], s[u--], 2); else u > 2 && "function" == typeof s[u - 1] && (l = s[--u]);
			for (; ++a < u;)if (r = s[a], r && J[typeof r])for (var c = -1, p = J[typeof r] && er(r), h = p ? p.length : 0; ++c < h;)i = p[c], o[i] = l ? l(o[i], r[i]) : r[i];
			return o
		}, sr = function (e, t, n) {
			var i, r = e, o = r;
			if (!r)return o;
			for (var s = arguments, a = 0, u = "number" == typeof n ? 2 : s.length; ++a < u;)if (r = s[a], r && J[typeof r])for (var l = -1, c = J[typeof r] && er(r), p = c ? c.length : 0; ++l < p;)i = c[l], "undefined" == typeof o[i] && (o[i] = r[i]);
			return o
		}, ar = function (e, t, n) {
			var i, r = e, o = r;
			if (!r)return o;
			if (!J[typeof r])return o;
			t = t && "undefined" == typeof n ? t : G(t, n, 3);
			for (i in r)if (t(r[i], i, e) === !1)return o;
			return o
		}, ur = function (e, t, n) {
			var i, r = e, o = r;
			if (!r)return o;
			if (!J[typeof r])return o;
			t = t && "undefined" == typeof n ? t : G(t, n, 3);
			for (var s = -1, a = J[typeof r] && er(r), u = a ? a.length : 0; ++s < u;)if (i = a[s], t(r[i], i, e) === !1)return o;
			return o
		}, lr = Ii ? function (e) {
			if (!e || $i.call(e) != P)return !1;
			var t = e.valueOf, n = lt(t) && (n = Ii(t)) && Ii(n);
			return n ? e == n || Ii(e) == n : ct(e)
		} : ct, cr = ot(function (e, t, n) {
			Mi.call(e, n) ? e[n]++ : e[n] = 1
		}), pr = ot(function (e, t, n) {
			(Mi.call(e, n) ? e[n] : e[n] = []).push(t)
		}), hr = ot(function (e, t, n) {
			e[n] = t
		}), fr = Zt, dr = Xt, mr = lt(mr = gi.now) && mr || function () {
				return (new gi).getTime()
			}, gr = 8 == Xi(w + "08") ? Xi : function (e, t) {
			return Xi(Lt(e) ? e.replace(E, "") : e, t || 0)
		};
		return d.after = Mn, d.assign = or, d.at = Ut, d.bind = Ln, d.bindAll = Fn, d.bindKey = On, d.chain = li, d.compact = hn, d.compose = qn, d.constant = Kn, d.countBy = cr, d.create = mt, d.createCallback = Gn, d.curry = Hn, d.debounce = Pn, d.defaults = sr, d.defer = Rn, d.delay = Bn, d.difference = fn, d.filter = Xt, d.flatten = vn, d.forEach = Gt, d.forEachRight = Yt, d.forIn = ar, d.forInRight = yt, d.forOwn = ur, d.forOwnRight = bt, d.functions = wt, d.groupBy = pr, d.indexBy = hr, d.initial = bn, d.intersection = wn, d.invert = xt, d.invoke = Qt, d.keys = er, d.map = Zt, d.mapValues = Ot, d.max = en, d.memoize = zn, d.merge = qt, d.min = tn, d.omit = Ht, d.once = Un, d.pairs = Pt, d.partial = Wn, d.partialRight = Jn, d.pick = Rt, d.pluck = fr, d.property = ni, d.pull = kn, d.range = Sn, d.reject = on, d.remove = Cn, d.rest = Tn, d.shuffle = an, d.sortBy = cn, d.tap = ci, d.throttle = Xn, d.times = si, d.toArray = pn, d.transform = Bt, d.union = En, d.uniq = jn, d.values = zt, d.where = dr, d.without = An, d.wrap = Vn, d.xor = Nn, d.zip = Dn, d.zipObject = In, d.collect = Zt, d.drop = Tn, d.each = Gt, d.eachRight = Yt, d.extend = or, d.methods = wt, d.object = In, d.select = Xt, d.tail = Tn, d.unique = jn, d.unzip = Dn, Zn(d), d.clone = ft, d.cloneDeep = dt, d.contains = Wt, d.escape = Yn, d.every = Jt, d.find = Vt, d.findIndex = dn, d.findKey = gt, d.findLast = Kt, d.findLastIndex = mn, d.findLastKey = vt, d.has = _t, d.identity = Qn, d.indexOf = yn, d.isArguments = ht, d.isArray = Qi, d.isBoolean = kt, d.isDate = St, d.isElement = Ct,d.isEmpty = Tt,d.isEqual = $t,d.isFinite = Et,d.isFunction = jt,d.isNaN = Nt,d.isNull = Dt,d.isNumber = It,d.isObject = At,d.isPlainObject = lr,d.isRegExp = Mt,d.isString = Lt,d.isUndefined = Ft,d.lastIndexOf = xn,d.mixin = Zn,d.noConflict = ei,d.noop = ti,d.now = mr,d.parseInt = gr,d.random = ii,d.reduce = nn,d.reduceRight = rn,d.result = ri,d.runInContext = h,d.size = un,d.some = ln,d.sortedIndex = $n,d.template = oi,d.unescape = ai,d.uniqueId = ui,d.all = Jt,d.any = ln,d.detect = Vt,d.findWhere = Vt,d.foldl = nn,d.foldr = rn,d.include = Wt,d.inject = nn,Zn(function () {
			var e = {};
			return ur(d, function (t, n) {
				d.prototype[n] || (e[n] = t)
			}), e
		}(), !1),d.first = gn,d.last = _n,d.sample = sn,d.take = gn,d.head = gn,ur(d, function (e, t) {
			var n = "sample" !== t;
			d.prototype[t] || (d.prototype[t] = function (t, i) {
				var r = this.__chain__, o = e(this.__wrapped__, t, i);
				return r || null != t && (!i || n && "function" == typeof t) ? new m(o, r) : o
			})
		}),d.VERSION = "2.4.1",d.prototype.chain = pi,d.prototype.toString = hi,d.prototype.value = fi,d.prototype.valueOf = fi,Gt(["join", "pop", "shift"], function (e) {
			var t = Si[e];
			d.prototype[e] = function () {
				var e = this.__chain__, n = t.apply(this.__wrapped__, arguments);
				return e ? new m(n, e) : n
			}
		}),Gt(["push", "reverse", "sort", "unshift"], function (e) {
			var t = Si[e];
			d.prototype[e] = function () {
				return t.apply(this.__wrapped__, arguments), this
			}
		}),Gt(["concat", "slice", "splice"], function (e) {
			var t = Si[e];
			d.prototype[e] = function () {
				return new m(t.apply(this.__wrapped__, arguments), this.__chain__)
			}
		}),d
	}

	var f, d = [], m = [], g = 0, v = +new Date + "", y = 75, b = 40, w = " 	\f\xa0\ufeff\n\r\u2028\u2029\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000", _ = /\b__p \+= '';/g, x = /\b(__p \+=) '' \+/g, k = /(__e\(.*?\)|\b__t\)) \+\n'';/g, S = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g, C = /\w*$/, T = /^\s*function[ \n\r\t]+\w/, $ = /<%=([\s\S]+?)%>/g, E = RegExp("^[" + w + "]*0+(?=.$)"), j = /($^)/, A = /\bthis\b/, N = /['\n\r\t\u2028\u2029\\]/g, D = ["Array", "Boolean", "Date", "Function", "Math", "Number", "Object", "RegExp", "String", "_", "attachEvent", "clearTimeout", "isFinite", "isNaN", "parseInt", "setTimeout"], I = 0, M = "[object Arguments]", L = "[object Array]", F = "[object Boolean]", O = "[object Date]", q = "[object Function]", H = "[object Number]", P = "[object Object]", R = "[object RegExp]", B = "[object String]", z = {};
	z[q] = !1, z[M] = z[L] = z[F] = z[O] = z[H] = z[P] = z[R] = z[B] = !0;
	var U = {leading: !1, maxWait: 0, trailing: !1}, W = {
		configurable: !1,
		enumerable: !1,
		value: null,
		writable: !1
	}, J = {"boolean": !1, "function": !0, object: !0, number: !1, string: !1, undefined: !1}, X = {
		"\\": "\\",
		"'": "'",
		"\n": "n",
		"\r": "r",
		"	": "t",
		"\u2028": "u2028",
		"\u2029": "u2029"
	}, V = J[typeof window] && window || this, K = J[typeof exports] && exports && !exports.nodeType && exports, G = J[typeof module] && module && !module.nodeType && module, Y = G && G.exports === K && K, Q = J[typeof global] && global;
	!Q || Q.global !== Q && Q.window !== Q || (V = Q);
	var Z = h();
	"function" == typeof define && "object" == typeof define.amd && define.amd ? (V._ = Z, define(function () {
		return Z
	})) : K && G ? Y ? (G.exports = Z)._ = Z : K._ = Z : V._ = Z
}.call(this), function () {
	var e = [].indexOf || function (e) {
			for (var t = 0, n = this.length; n > t; t++)if (t in this && this[t] === e)return t;
			return -1
		}, t = [].slice;
	!function (e, t) {
		return "function" == typeof define && define.amd ? define("waypoints", ["jquery"], function (n) {
			return t(n, e)
		}) : t(e.jQuery, e)
	}(window, function (n, i) {
		var r, o, s, a, u, l, c, p, h, f, d, m, g, v, y, b;
		return r = n(i), p = e.call(i, "ontouchstart") >= 0, a = {
			horizontal: {},
			vertical: {}
		}, u = 1, c = {}, l = "waypoints-context-id", d = "resize.waypoints", m = "scroll.waypoints", g = 1, v = "waypoints-waypoint-ids", y = "waypoint", b = "waypoints", o = function () {
			function e(e) {
				var t = this;
				this.$element = e, this.element = e[0], this.didResize = !1, this.didScroll = !1, this.id = "context" + u++, this.oldScroll = {
					x: e.scrollLeft(),
					y: e.scrollTop()
				}, this.waypoints = {
					horizontal: {},
					vertical: {}
				}, this.element[l] = this.id, c[this.id] = this, e.bind(m, function () {
					var e;
					return t.didScroll || p ? void 0 : (t.didScroll = !0, e = function () {
						return t.doScroll(), t.didScroll = !1
					}, i.setTimeout(e, n[b].settings.scrollThrottle))
				}), e.bind(d, function () {
					var e;
					return t.didResize ? void 0 : (t.didResize = !0, e = function () {
						return n[b]("refresh"), t.didResize = !1
					}, i.setTimeout(e, n[b].settings.resizeThrottle))
				})
			}

			return e.prototype.doScroll = function () {
				var e, t = this;
				return e = {
					horizontal: {
						newScroll: this.$element.scrollLeft(),
						oldScroll: this.oldScroll.x,
						forward: "right",
						backward: "left"
					},
					vertical: {
						newScroll: this.$element.scrollTop(),
						oldScroll: this.oldScroll.y,
						forward: "down",
						backward: "up"
					}
				}, !p || e.vertical.oldScroll && e.vertical.newScroll || n[b]("refresh"), n.each(e, function (e, i) {
					var r, o, s;
					return s = [], o = i.newScroll > i.oldScroll, r = o ? i.forward : i.backward, n.each(t.waypoints[e], function (e, t) {
						var n, r;
						return i.oldScroll < (n = t.offset) && n <= i.newScroll ? s.push(t) : i.newScroll < (r = t.offset) && r <= i.oldScroll ? s.push(t) : void 0
					}), s.sort(function (e, t) {
						return e.offset - t.offset
					}), o || s.reverse(), n.each(s, function (e, t) {
						return t.options.continuous || e === s.length - 1 ? t.trigger([r]) : void 0
					})
				}), this.oldScroll = {x: e.horizontal.newScroll, y: e.vertical.newScroll}
			}, e.prototype.refresh = function () {
				var e, t, i, r = this;
				return i = n.isWindow(this.element), t = this.$element.offset(), this.doScroll(), e = {
					horizontal: {
						contextOffset: i ? 0 : t.left,
						contextScroll: i ? 0 : this.oldScroll.x,
						contextDimension: this.$element.width(),
						oldScroll: this.oldScroll.x,
						forward: "right",
						backward: "left",
						offsetProp: "left"
					},
					vertical: {
						contextOffset: i ? 0 : t.top,
						contextScroll: i ? 0 : this.oldScroll.y,
						contextDimension: i ? n[b]("viewportHeight") : this.$element.height(),
						oldScroll: this.oldScroll.y,
						forward: "down",
						backward: "up",
						offsetProp: "top"
					}
				}, n.each(e, function (e, t) {
					return n.each(r.waypoints[e], function (e, i) {
						var r, o, s, a, u;
						return r = i.options.offset, s = i.offset, o = n.isWindow(i.element) ? 0 : i.$element.offset()[t.offsetProp], n.isFunction(r) ? r = r.apply(i.element) : "string" == typeof r && (r = parseFloat(r), i.options.offset.indexOf("%") > -1 && (r = Math.ceil(t.contextDimension * r / 100))), i.offset = o - t.contextOffset + t.contextScroll - r, i.options.onlyOnScroll && null != s || !i.enabled ? void 0 : null !== s && s < (a = t.oldScroll) && a <= i.offset ? i.trigger([t.backward]) : null !== s && s > (u = t.oldScroll) && u >= i.offset ? i.trigger([t.forward]) : null === s && t.oldScroll >= i.offset ? i.trigger([t.forward]) : void 0
					})
				})
			}, e.prototype.checkEmpty = function () {
				return n.isEmptyObject(this.waypoints.horizontal) && n.isEmptyObject(this.waypoints.vertical) ? (this.$element.unbind([d, m].join(" ")), delete c[this.id]) : void 0
			}, e
		}(), s = function () {
			function e(e, t, i) {
				var r, o;
				"bottom-in-view" === i.offset && (i.offset = function () {
					var e;
					return e = n[b]("viewportHeight"), n.isWindow(t.element) || (e = t.$element.height()), e - n(this).outerHeight()
				}), this.$element = e, this.element = e[0], this.axis = i.horizontal ? "horizontal" : "vertical", this.callback = i.handler, this.context = t, this.enabled = i.enabled, this.id = "waypoints" + g++, this.offset = null, this.options = i, t.waypoints[this.axis][this.id] = this, a[this.axis][this.id] = this, r = null != (o = this.element[v]) ? o : [], r.push(this.id), this.element[v] = r
			}

			return e.prototype.trigger = function (e) {
				return this.enabled ? (null != this.callback && this.callback.apply(this.element, e), this.options.triggerOnce ? this.destroy() : void 0) : void 0
			}, e.prototype.disable = function () {
				return this.enabled = !1
			}, e.prototype.enable = function () {
				return this.context.refresh(), this.enabled = !0
			}, e.prototype.destroy = function () {
				return delete a[this.axis][this.id], delete this.context.waypoints[this.axis][this.id], this.context.checkEmpty()
			}, e.getWaypointsByElement = function (e) {
				var t, i;
				return (i = e[v]) ? (t = n.extend({}, a.horizontal, a.vertical), n.map(i, function (e) {
					return t[e]
				})) : []
			}, e
		}(), f = {
			init: function (e, t) {
				var i;
				return t = n.extend({}, n.fn[y].defaults, t), null == (i = t.handler) && (t.handler = e), this.each(function () {
					var e, i, r, a;
					return e = n(this), r = null != (a = t.context) ? a : n.fn[y].defaults.context, n.isWindow(r) || (r = e.closest(r)), r = n(r), i = c[r[0][l]], i || (i = new o(r)), new s(e, i, t)
				}), n[b]("refresh"), this
			}, disable: function () {
				return f._invoke.call(this, "disable")
			}, enable: function () {
				return f._invoke.call(this, "enable")
			}, destroy: function () {
				return f._invoke.call(this, "destroy")
			}, prev: function (e, t) {
				return f._traverse.call(this, e, t, function (e, t, n) {
					return t > 0 ? e.push(n[t - 1]) : void 0
				})
			}, next: function (e, t) {
				return f._traverse.call(this, e, t, function (e, t, n) {
					return t < n.length - 1 ? e.push(n[t + 1]) : void 0
				})
			}, _traverse: function (e, t, r) {
				var o, s;
				return null == e && (e = "vertical"), null == t && (t = i), s = h.aggregate(t), o = [], this.each(function () {
					var t;
					return t = n.inArray(this, s[e]), r(o, t, s[e])
				}), this.pushStack(o)
			}, _invoke: function (e) {
				return this.each(function () {
					var t;
					return t = s.getWaypointsByElement(this), n.each(t, function (t, n) {
						return n[e](), !0
					})
				}), this
			}
		}, n.fn[y] = function () {
			var e, i;
			return i = arguments[0], e = 2 <= arguments.length ? t.call(arguments, 1) : [], f[i] ? f[i].apply(this, e) : n.isFunction(i) ? f.init.apply(this, arguments) : n.isPlainObject(i) ? f.init.apply(this, [null, i]) : n.error(i ? "The " + i + " method does not exist in jQuery Waypoints." : "jQuery Waypoints needs a callback function or handler option.")
		}, n.fn[y].defaults = {
			context: i,
			continuous: !0,
			enabled: !0,
			horizontal: !1,
			offset: 0,
			triggerOnce: !1
		}, h = {
			refresh: function () {
				return n.each(c, function (e, t) {
					return t.refresh()
				})
			}, viewportHeight: function () {
				var e;
				return null != (e = i.innerHeight) ? e : r.height()
			}, aggregate: function (e) {
				var t, i, r;
				return t = a, e && (t = null != (r = c[n(e)[0][l]]) ? r.waypoints : void 0), t ? (i = {
					horizontal: [],
					vertical: []
				}, n.each(i, function (e, r) {
					return n.each(t[e], function (e, t) {
						return r.push(t)
					}), r.sort(function (e, t) {
						return e.offset - t.offset
					}), i[e] = n.map(r, function (e) {
						return e.element
					}), i[e] = n.unique(i[e])
				}), i) : []
			}, above: function (e) {
				return null == e && (e = i), h._filter(e, "vertical", function (e, t) {
					return t.offset <= e.oldScroll.y
				})
			}, below: function (e) {
				return null == e && (e = i), h._filter(e, "vertical", function (e, t) {
					return t.offset > e.oldScroll.y
				})
			}, left: function (e) {
				return null == e && (e = i), h._filter(e, "horizontal", function (e, t) {
					return t.offset <= e.oldScroll.x
				})
			}, right: function (e) {
				return null == e && (e = i), h._filter(e, "horizontal", function (e, t) {
					return t.offset > e.oldScroll.x
				})
			}, enable: function () {
				return h._invoke("enable")
			}, disable: function () {
				return h._invoke("disable")
			}, destroy: function () {
				return h._invoke("destroy")
			}, extendFn: function (e, t) {
				return f[e] = t
			}, _invoke: function (e) {
				var t;
				return t = n.extend({}, a.vertical, a.horizontal), n.each(t, function (t, n) {
					return n[e](), !0
				})
			}, _filter: function (e, t, i) {
				var r, o;
				return (r = c[n(e)[0][l]]) ? (o = [], n.each(r.waypoints[t], function (e, t) {
					return i(r, t) ? o.push(t) : void 0
				}), o.sort(function (e, t) {
					return e.offset - t.offset
				}), n.map(o, function (e) {
					return e.element
				})) : []
			}
		}, n[b] = function () {
			var e, n;
			return n = arguments[0], e = 2 <= arguments.length ? t.call(arguments, 1) : [], h[n] ? h[n].apply(null, e) : h.aggregate.call(null, n)
		}, n[b].settings = {resizeThrottle: 100, scrollThrottle: 30}, r.on("load.waypoints", function () {
			return n[b]("refresh")
		})
	})
}.call(this), function (e) {
	"use strict";
	function t(e) {
		return this instanceof t ? (e || (e = {}), "number" == typeof e && (e = {frameRate: e}), null != e.useNative || (e.useNative = !0), this.options = e, this.frameRate = e.frameRate || t.FRAME_RATE, this._frameLength = 1e3 / this.frameRate, this._isCustomFrameRate = this.frameRate !== t.FRAME_RATE, this._timeoutId = null, this._callbacks = {}, this._lastTickTime = 0, void(this._tickCounter = 0)) : new t(e)
	}

	var n, i;
	!function () {
		var r, o, s = ["webkit", "moz", "ms", "o"];
		try {
			e.top.name, o = e.top
		} catch (a) {
			o = e
		}
		for (n = o.requestAnimationFrame, i = o.cancelAnimationFrame || o.cancelRequestAnimationFrame, r = 0; r < s.length && !n; r++)n = o[s[r] + "RequestAnimationFrame"], i = o[s[r] + "CancelAnimationFrame"] || o[s[r] + "CancelRequestAnimationFrame"];
		n && n(function () {
			t.hasNative = !0
		})
	}(), t.FRAME_RATE = 60, t.shim = function (n) {
		var i = new t(n);
		return e.requestAnimationFrame = function (e) {
			return i.request(e)
		}, e.cancelAnimationFrame = function (e) {
			return i.cancel(e)
		}, i
	}, t.now = Date.now || function () {
		return (new Date).getTime()
	}, t.navigationStart = t.now(), t.perfNow = function () {
		return e.performance && e.performance.now ? e.performance.now() : t.now() - t.navigationStart
	}, t.hasNative = !1, t.prototype.request = function (i) {
		var r, o = this;
		if (++this._tickCounter, t.hasNative && o.options.useNative && !this._isCustomFrameRate)return n(i);
		if (!i)throw new TypeError("Not enough arguments");
		return null == this._timeoutId && (r = this._frameLength + this._lastTickTime - t.now(), 0 > r && (r = 0), this._timeoutId = e.setTimeout(function () {
			var e;
			o._lastTickTime = t.now(), o._timeoutId = null, ++o._tickCounter;
			for (e in o._callbacks)o._callbacks[e] && (t.hasNative && o.options.useNative ? n(o._callbacks[e]) : o._callbacks[e](t.perfNow()), delete o._callbacks[e])
		}, r)), this._callbacks[this._tickCounter] = i, this._tickCounter
	}, t.prototype.cancel = function (e) {
		t.hasNative && this.options.useNative && i(e), delete this._callbacks[e]
	}, "object" == typeof exports && "object" == typeof module ? module.exports = t : "function" == typeof define && define.amd ? define(function () {
		return t
	}) : e.AnimationFrame = t
}(window), function () {
	"use strict";
	function e(t, i) {
		function r(e, t) {
			return function () {
				return e.apply(t, arguments)
			}
		}

		var o;
		if (i = i || {}, this.trackingClick = !1, this.trackingClickStart = 0, this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, this.touchBoundary = i.touchBoundary || 10, this.layer = t, this.tapDelay = i.tapDelay || 200, !e.notNeeded(t)) {
			for (var s = ["onMouse", "onClick", "onTouchStart", "onTouchMove", "onTouchEnd", "onTouchCancel"], a = this, u = 0, l = s.length; l > u; u++)a[s[u]] = r(a[s[u]], a);
			n && (t.addEventListener("mouseover", this.onMouse, !0), t.addEventListener("mousedown", this.onMouse, !0), t.addEventListener("mouseup", this.onMouse, !0)), t.addEventListener("click", this.onClick, !0), t.addEventListener("touchstart", this.onTouchStart, !1), t.addEventListener("touchmove", this.onTouchMove, !1), t.addEventListener("touchend", this.onTouchEnd, !1), t.addEventListener("touchcancel", this.onTouchCancel, !1), Event.prototype.stopImmediatePropagation || (t.removeEventListener = function (e, n, i) {
				var r = Node.prototype.removeEventListener;
				"click" === e ? r.call(t, e, n.hijacked || n, i) : r.call(t, e, n, i)
			}, t.addEventListener = function (e, n, i) {
				var r = Node.prototype.addEventListener;
				"click" === e ? r.call(t, e, n.hijacked || (n.hijacked = function (e) {
					e.propagationStopped || n(e)
				}), i) : r.call(t, e, n, i)
			}), "function" == typeof t.onclick && (o = t.onclick, t.addEventListener("click", function (e) {
				o(e)
			}, !1), t.onclick = null)
		}
	}

	var t = navigator.userAgent.indexOf("Windows Phone") >= 0, n = navigator.userAgent.indexOf("Android") > 0 && !t, i = /iP(ad|hone|od)/.test(navigator.userAgent) && !t, r = i && /OS 4_\d(_\d)?/.test(navigator.userAgent), o = i && /OS ([6-9]|\d{2})_\d/.test(navigator.userAgent), s = navigator.userAgent.indexOf("BB10") > 0;
	e.prototype.needsClick = function (e) {
		switch (e.nodeName.toLowerCase()) {
			case"button":
			case"select":
			case"textarea":
				if (e.disabled)return !0;
				break;
			case"input":
				if (i && "file" === e.type || e.disabled)return !0;
				break;
			case"label":
			case"iframe":
			case"video":
				return !0
		}
		return /\bneedsclick\b/.test(e.className)
	}, e.prototype.needsFocus = function (e) {
		switch (e.nodeName.toLowerCase()) {
			case"textarea":
				return !0;
			case"select":
				return !n;
			case"input":
				switch (e.type) {
					case"button":
					case"checkbox":
					case"file":
					case"image":
					case"radio":
					case"submit":
						return !1
				}
				return !e.disabled && !e.readOnly;
			default:
				return /\bneedsfocus\b/.test(e.className)
		}
	}, e.prototype.sendClick = function (e, t) {
		var n, i;
		document.activeElement && document.activeElement !== e && document.activeElement.blur(), i = t.changedTouches[0], n = document.createEvent("MouseEvents"), n.initMouseEvent(this.determineEventType(e), !0, !0, window, 1, i.screenX, i.screenY, i.clientX, i.clientY, !1, !1, !1, !1, 0, null), n.forwardedTouchEvent = !0, e.dispatchEvent(n)
	}, e.prototype.determineEventType = function (e) {
		return n && "select" === e.tagName.toLowerCase() ? "mousedown" : "click"
	}, e.prototype.focus = function (e) {
		var t;
		i && e.setSelectionRange && 0 !== e.type.indexOf("date") && "time" !== e.type && "month" !== e.type ? (t = e.value.length, e.setSelectionRange(t, t)) : e.focus()
	}, e.prototype.updateScrollParent = function (e) {
		var t, n;
		if (t = e.fastClickScrollParent, !t || !t.contains(e)) {
			n = e;
			do {
				if (n.scrollHeight > n.offsetHeight) {
					t = n, e.fastClickScrollParent = n;
					break
				}
				n = n.parentElement
			} while (n)
		}
		t && (t.fastClickLastScrollTop = t.scrollTop)
	}, e.prototype.getTargetElementFromEventTarget = function (e) {
		return e.nodeType === Node.TEXT_NODE ? e.parentNode : e
	}, e.prototype.onTouchStart = function (e) {
		var t, n, o;
		if (e.targetTouches.length > 1)return !0;
		if (t = this.getTargetElementFromEventTarget(e.target), n = e.targetTouches[0], i) {
			if (o = window.getSelection(), o.rangeCount && !o.isCollapsed)return !0;
			if (!r) {
				if (n.identifier && n.identifier === this.lastTouchIdentifier)return e.preventDefault(), !1;
				this.lastTouchIdentifier = n.identifier, this.updateScrollParent(t)
			}
		}
		return this.trackingClick = !0, this.trackingClickStart = e.timeStamp, this.targetElement = t, this.touchStartX = n.pageX, this.touchStartY = n.pageY, e.timeStamp - this.lastClickTime < this.tapDelay && e.preventDefault(), !0
	}, e.prototype.touchHasMoved = function (e) {
		var t = e.changedTouches[0], n = this.touchBoundary;
		return Math.abs(t.pageX - this.touchStartX) > n || Math.abs(t.pageY - this.touchStartY) > n ? !0 : !1
	}, e.prototype.onTouchMove = function (e) {
		return this.trackingClick ? ((this.targetElement !== this.getTargetElementFromEventTarget(e.target) || this.touchHasMoved(e)) && (this.trackingClick = !1, this.targetElement = null), !0) : !0
	}, e.prototype.findControl = function (e) {
		return void 0 !== e.control ? e.control : e.htmlFor ? document.getElementById(e.htmlFor) : e.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea")
	}, e.prototype.onTouchEnd = function (e) {
		var t, s, a, u, l, c = this.targetElement;
		if (!this.trackingClick)return !0;
		if (e.timeStamp - this.lastClickTime < this.tapDelay)return this.cancelNextClick = !0, !0;
		if (this.cancelNextClick = !1, this.lastClickTime = e.timeStamp, s = this.trackingClickStart, this.trackingClick = !1, this.trackingClickStart = 0, o && (l = e.changedTouches[0], c = document.elementFromPoint(l.pageX - window.pageXOffset, l.pageY - window.pageYOffset) || c, c.fastClickScrollParent = this.targetElement.fastClickScrollParent), a = c.tagName.toLowerCase(), "label" === a) {
			if (t = this.findControl(c)) {
				if (this.focus(c), n)return !1;
				c = t
			}
		} else if (this.needsFocus(c))return e.timeStamp - s > 100 || i && window.top !== window && "input" === a ? (this.targetElement = null, !1) : (this.focus(c), this.sendClick(c, e), i && "select" === a || (this.targetElement = null, e.preventDefault()), !1);
		return i && !r && (u = c.fastClickScrollParent, u && u.fastClickLastScrollTop !== u.scrollTop) ? !0 : (this.needsClick(c) || (e.preventDefault(), this.sendClick(c, e)), !1)
	}, e.prototype.onTouchCancel = function () {
		this.trackingClick = !1, this.targetElement = null
	}, e.prototype.onMouse = function (e) {
		return this.targetElement ? e.forwardedTouchEvent ? !0 : e.cancelable && (!this.needsClick(this.targetElement) || this.cancelNextClick) ? (e.stopImmediatePropagation ? e.stopImmediatePropagation() : e.propagationStopped = !0, e.stopPropagation(), e.preventDefault(), !1) : !0 : !0
	}, e.prototype.onClick = function (e) {
		var t;
		return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, !0) : "submit" === e.target.type && 0 === e.detail ? !0 : (t = this.onMouse(e), t || (this.targetElement = null), t)
	}, e.prototype.destroy = function () {
		var e = this.layer;
		n && (e.removeEventListener("mouseover", this.onMouse, !0), e.removeEventListener("mousedown", this.onMouse, !0), e.removeEventListener("mouseup", this.onMouse, !0)), e.removeEventListener("click", this.onClick, !0), e.removeEventListener("touchstart", this.onTouchStart, !1), e.removeEventListener("touchmove", this.onTouchMove, !1), e.removeEventListener("touchend", this.onTouchEnd, !1), e.removeEventListener("touchcancel", this.onTouchCancel, !1)
	}, e.notNeeded = function (e) {
		var t, i, r;
		if ("undefined" == typeof window.ontouchstart)return !0;
		if (i = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1]) {
			if (!n)return !0;
			if (t = document.querySelector("meta[name=viewport]")) {
				if (-1 !== t.content.indexOf("user-scalable=no"))return !0;
				if (i > 31 && document.documentElement.scrollWidth <= window.outerWidth)return !0
			}
		}
		if (s && (r = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/), r[1] >= 10 && r[2] >= 3 && (t = document.querySelector("meta[name=viewport]")))) {
			if (-1 !== t.content.indexOf("user-scalable=no"))return !0;
			if (document.documentElement.scrollWidth <= window.outerWidth)return !0
		}
		return "none" === e.style.msTouchAction ? !0 : !1
	}, e.attach = function (t, n) {
		return new e(t, n)
	}, "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function () {
		return e
	}) : "undefined" != typeof module && module.exports ? (module.exports = e.attach, module.exports.FastClick = e) : window.FastClick = e
}(), window.Modernizr = function (e, t, n) {
	function i(e) {
		y.cssText = e
	}

	function r(e, t) {
		return typeof e === t
	}

	function o(e, t) {
		return !!~("" + e).indexOf(t)
	}

	function s(e, t) {
		for (var i in e) {
			var r = e[i];
			if (!o(r, "-") && y[r] !== n)return "pfx" == t ? r : !0
		}
		return !1
	}

	function a(e, t, i) {
		for (var o in e) {
			var s = t[e[o]];
			if (s !== n)return i === !1 ? e[o] : r(s, "function") ? s.bind(i || t) : s
		}
		return !1
	}

	function u(e, t, n) {
		var i = e.charAt(0).toUpperCase() + e.slice(1), o = (e + " " + k.join(i + " ") + i).split(" ");
		return r(t, "string") || r(t, "undefined") ? s(o, t) : (o = (e + " " + S.join(i + " ") + i).split(" "), a(o, t, n))
	}

	function l() {
		f.input = function (n) {
			for (var i = 0, r = n.length; r > i; i++)E[n[i]] = !!(n[i]in b);
			return E.list && (E.list = !(!t.createElement("datalist") || !e.HTMLDataListElement)), E
		}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")), f.inputtypes = function (e) {
			for (var i, r, o, s = 0, a = e.length; a > s; s++)b.setAttribute("type", r = e[s]), i = "text" !== b.type, i && (b.value = w, b.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(r) && b.style.WebkitAppearance !== n ? (m.appendChild(b), o = t.defaultView, i = o.getComputedStyle && "textfield" !== o.getComputedStyle(b, null).WebkitAppearance && 0 !== b.offsetHeight, m.removeChild(b)) : /^(search|tel)$/.test(r) || (i = /^(url|email)$/.test(r) ? b.checkValidity && b.checkValidity() === !1 : b.value != w)), $[e[s]] = !!i;
			return $
		}("search tel url email datetime date month week time datetime-local number range color".split(" "))
	}

	var c, p, h = "2.8.2", f = {}, d = !0, m = t.documentElement, g = "modernizr", v = t.createElement(g), y = v.style, b = t.createElement("input"), w = ":)", _ = ({}.toString, " -webkit- -moz- -o- -ms- ".split(" ")), x = "Webkit Moz O ms", k = x.split(" "), S = x.toLowerCase().split(" "), C = {svg: "http://www.w3.org/2000/svg"}, T = {}, $ = {}, E = {}, j = [], A = j.slice, N = function (e, n, i, r) {
		var o, s, a, u, l = t.createElement("div"), c = t.body, p = c || t.createElement("body");
		if (parseInt(i, 10))for (; i--;)a = t.createElement("div"), a.id = r ? r[i] : g + (i + 1), l.appendChild(a);
		return o = ["&#173;", '<style id="s', g, '">', e, "</style>"].join(""), l.id = g, (c ? l : p).innerHTML += o, p.appendChild(l), c || (p.style.background = "", p.style.overflow = "hidden", u = m.style.overflow, m.style.overflow = "hidden", m.appendChild(p)), s = n(l, e), c ? l.parentNode.removeChild(l) : (p.parentNode.removeChild(p), m.style.overflow = u), !!s
	}, D = {}.hasOwnProperty;
	p = r(D, "undefined") || r(D.call, "undefined") ? function (e, t) {
		return t in e && r(e.constructor.prototype[t], "undefined")
	} : function (e, t) {
		return D.call(e, t)
	}, Function.prototype.bind || (Function.prototype.bind = function (e) {
		var t = this;
		if ("function" != typeof t)throw new TypeError;
		var n = A.call(arguments, 1), i = function () {
			if (this instanceof i) {
				var r = function () {
				};
				r.prototype = t.prototype;
				var o = new r, s = t.apply(o, n.concat(A.call(arguments)));
				return Object(s) === s ? s : o
			}
			return t.apply(e, n.concat(A.call(arguments)))
		};
		return i
	}), T.touch = function () {
		var n;
		return "ontouchstart"in e || e.DocumentTouch && t instanceof DocumentTouch ? n = !0 : N(["@media (", _.join("touch-enabled),("), g, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function (e) {
			n = 9 === e.offsetTop
		}), n
	}, T.cssanimations = function () {
		return u("animationName")
	}, T.cssgradients = function () {
		var e = "background-image:", t = "gradient(linear,left top,right bottom,from(#9f9),to(white));", n = "linear-gradient(left top,#9f9, white);";
		return i((e + "-webkit- ".split(" ").join(t + e) + _.join(n + e)).slice(0, -e.length)), o(y.backgroundImage, "gradient")
	}, T.csstransforms = function () {
		return !!u("transform")
	}, T.csstransforms3d = function () {
		var e = !!u("perspective");
		return e && "webkitPerspective"in m.style && N("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function (t) {
			e = 9 === t.offsetLeft && 3 === t.offsetHeight
		}), e
	}, T.csstransitions = function () {
		return u("transition")
	}, T.video = function () {
		var e = t.createElement("video"), n = !1;
		try {
			(n = !!e.canPlayType) && (n = new Boolean(n), n.ogg = e.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), n.h264 = e.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), n.webm = e.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, ""))
		} catch (i) {
		}
		return n
	}, T.svg = function () {
		return !!t.createElementNS && !!t.createElementNS(C.svg, "svg").createSVGRect
	};
	for (var I in T)p(T, I) && (c = I.toLowerCase(), f[c] = T[I](), j.push((f[c] ? "" : "no-") + c));
	return f.input || l(), f.addTest = function (e, t) {
		if ("object" == typeof e)for (var i in e)p(e, i) && f.addTest(i, e[i]); else {
			if (e = e.toLowerCase(), f[e] !== n)return f;
			t = "function" == typeof t ? t() : t, "undefined" != typeof d && d && (m.className += " " + (t ? "" : "no-") + e), f[e] = t
		}
		return f
	}, i(""), v = b = null, function (e, t) {
		function n(e, t) {
			var n = e.createElement("p"), i = e.getElementsByTagName("head")[0] || e.documentElement;
			return n.innerHTML = "x<style>" + t + "</style>", i.insertBefore(n.lastChild, i.firstChild)
		}

		function i() {
			var e = y.elements;
			return "string" == typeof e ? e.split(" ") : e
		}

		function r(e) {
			var t = v[e[m]];
			return t || (t = {}, g++, e[m] = g, v[g] = t), t
		}

		function o(e, n, i) {
			if (n || (n = t), c)return n.createElement(e);
			i || (i = r(n));
			var o;
			return o = i.cache[e] ? i.cache[e].cloneNode() : d.test(e) ? (i.cache[e] = i.createElem(e)).cloneNode() : i.createElem(e), !o.canHaveChildren || f.test(e) || o.tagUrn ? o : i.frag.appendChild(o)
		}

		function s(e, n) {
			if (e || (e = t), c)return e.createDocumentFragment();
			n = n || r(e);
			for (var o = n.frag.cloneNode(), s = 0, a = i(), u = a.length; u > s; s++)o.createElement(a[s]);
			return o
		}

		function a(e, t) {
			t.cache || (t.cache = {}, t.createElem = e.createElement, t.createFrag = e.createDocumentFragment, t.frag = t.createFrag()), e.createElement = function (n) {
				return y.shivMethods ? o(n, e, t) : t.createElem(n)
			}, e.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + i().join().replace(/[\w\-]+/g, function (e) {
				return t.createElem(e), t.frag.createElement(e), 'c("' + e + '")'
			}) + ");return n}")(y, t.frag)
		}

		function u(e) {
			e || (e = t);
			var i = r(e);
			return !y.shivCSS || l || i.hasCSS || (i.hasCSS = !!n(e, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), c || a(e, i), e
		}

		var l, c, p = "3.7.0", h = e.html5 || {}, f = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i, d = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i, m = "_html5shiv", g = 0, v = {};
		!function () {
			try {
				var e = t.createElement("a");
				e.innerHTML = "<xyz></xyz>", l = "hidden"in e, c = 1 == e.childNodes.length || function () {
					t.createElement("a");
					var e = t.createDocumentFragment();
					return "undefined" == typeof e.cloneNode || "undefined" == typeof e.createDocumentFragment || "undefined" == typeof e.createElement
				}()
			} catch (n) {
				l = !0, c = !0
			}
		}();
		var y = {
			elements: h.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
			version: p,
			shivCSS: h.shivCSS !== !1,
			supportsUnknownElements: c,
			shivMethods: h.shivMethods !== !1,
			type: "default",
			shivDocument: u,
			createElement: o,
			createDocumentFragment: s
		};
		e.html5 = y, u(t)
	}(this, t), f._version = h, f._prefixes = _, f._domPrefixes = S, f._cssomPrefixes = k, f.testProp = function (e) {
		return s([e])
	}, f.testAllProps = u, f.testStyles = N, f.prefixed = function (e, t, n) {
		return t ? u(e, t, n) : u(e, "pfx")
	}, m.className = m.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (d ? " js " + j.join(" ") : ""), f
}(this, this.document), Modernizr.addTest("pointerevents", function () {
	var e, t = document.createElement("x"), n = document.documentElement, i = window.getComputedStyle;
	return "pointerEvents"in t.style ? (t.style.pointerEvents = "auto", t.style.pointerEvents = "x", n.appendChild(t), e = i && "auto" === i(t, "").pointerEvents, n.removeChild(t), !!e) : !1
}), Modernizr.addTest("placeholder", function () {
	return !!("placeholder"in(Modernizr.input || document.createElement("input")) && "placeholder"in(Modernizr.textarea || document.createElement("textarea")))
}), window.matchMedia || (window.matchMedia = function () {
	"use strict";
	var e = window.styleMedia || window.media;
	if (!e) {
		var t = document.createElement("style"), n = document.getElementsByTagName("script")[0], i = null;
		t.type = "text/css", t.id = "matchmediajs-test", n.parentNode.insertBefore(t, n), i = "getComputedStyle"in window && window.getComputedStyle(t, null) || t.currentStyle, e = {
			matchMedium: function (e) {
				var n = "@media " + e + "{ #matchmediajs-test { width: 1px; } }";
				return t.styleSheet ? t.styleSheet.cssText = n : t.textContent = n, "1px" === i.width
			}
		}
	}
	return function (t) {
		return {matches: e.matchMedium(t || "all"), media: t || "all"}
	}
}()), function () {
	if (window.matchMedia && window.matchMedia("all").addListener)return !1;
	var e = window.matchMedia, t = e("only all").matches, n = !1, i = 0, r = [], o = function () {
		clearTimeout(i), i = setTimeout(function () {
			for (var t = 0, n = r.length; n > t; t++) {
				var i = r[t].mql, o = r[t].listeners || [], s = e(i.media).matches;
				if (s !== i.matches) {
					i.matches = s;
					for (var a = 0, u = o.length; u > a; a++)o[a].call(window, i)
				}
			}
		}, 30)
	};
	window.matchMedia = function (i) {
		var s = e(i), a = [], u = 0;
		return s.addListener = function (e) {
			t && (n || (n = !0, window.addEventListener("resize", o, !0)), 0 === u && (u = r.push({
				mql: s,
				listeners: a
			})), a.push(e))
		}, s.removeListener = function (e) {
			for (var t = 0, n = a.length; n > t; t++)a[t] === e && a.splice(t, 1)
		}, s
	}
}(), window.matchMedia || (window.matchMedia = function () {
	"use strict";
	var e = window.styleMedia || window.media;
	if (!e) {
		var t = document.createElement("style"), n = document.getElementsByTagName("script")[0], i = null;
		t.type = "text/css", t.id = "matchmediajs-test", n.parentNode.insertBefore(t, n), i = "getComputedStyle"in window && window.getComputedStyle(t, null) || t.currentStyle, e = {
			matchMedium: function (e) {
				var n = "@media " + e + "{ #matchmediajs-test { width: 1px; } }";
				return t.styleSheet ? t.styleSheet.cssText = n : t.textContent = n, "1px" === i.width
			}
		}
	}
	return function (t) {
		return {matches: e.matchMedium(t || "all"), media: t || "all"}
	}
}()), function (e, t, n) {
	"use strict";
	function i(t) {
		"object" == typeof module && "object" == typeof module.exports ? module.exports = t : "function" == typeof define && define.amd && define("picturefill", function () {
			return t
		}), "object" == typeof e && (e.picturefill = t)
	}

	function r(e) {
		var t, n, i, r, o, u = e || {};
		t = u.elements || s.getAllElements();
		for (var l = 0, c = t.length; c > l; l++)if (n = t[l], i = n.parentNode, r = void 0, o = void 0, "IMG" === n.nodeName.toUpperCase() && (n[s.ns] || (n[s.ns] = {}), u.reevaluate || !n[s.ns].evaluated)) {
			if (i && "PICTURE" === i.nodeName.toUpperCase()) {
				if (s.removeVideoShim(i), r = s.getMatch(n, i), r === !1)continue
			} else r = void 0;
			(i && "PICTURE" === i.nodeName.toUpperCase() || !s.sizesSupported && n.srcset && a.test(n.srcset)) && s.dodgeSrcset(n), r ? (o = s.processSourceSet(r), s.applyBestCandidate(o, n)) : (o = s.processSourceSet(n), (void 0 === n.srcset || n[s.ns].srcset) && s.applyBestCandidate(o, n)), n[s.ns].evaluated = !0
		}
	}

	function o() {
		function n() {
			clearTimeout(i), i = setTimeout(a, 60)
		}

		s.initTypeDetects(), r();
		var i, o = setInterval(function () {
			return r(), /^loaded|^i|^c/.test(t.readyState) ? void clearInterval(o) : void 0
		}, 250), a = function () {
			r({reevaluate: !0})
		};
		e.addEventListener ? e.addEventListener("resize", n, !1) : e.attachEvent && e.attachEvent("onresize", n)
	}

	if (e.HTMLPictureElement)return void i(function () {
	});
	t.createElement("picture");
	var s = e.picturefill || {}, a = /\s+\+?\d+(e\d+)?w/;
	s.ns = "picturefill", function () {
		s.srcsetSupported = "srcset"in n, s.sizesSupported = "sizes"in n, s.curSrcSupported = "currentSrc"in n
	}(), s.trim = function (e) {
		return e.trim ? e.trim() : e.replace(/^\s+|\s+$/g, "")
	}, s.makeUrl = function () {
		var e = t.createElement("a");
		return function (t) {
			return e.href = t, e.href
		}
	}(), s.restrictsMixedContent = function () {
		return "https:" === e.location.protocol
	}, s.matchesMedia = function (t) {
		return e.matchMedia && e.matchMedia(t).matches
	}, s.getDpr = function () {
		return e.devicePixelRatio || 1
	}, s.getWidthFromLength = function (e) {
		var n;
		if (!e || e.indexOf("%") > -1 != !1 || !(parseFloat(e) > 0 || e.indexOf("calc(") > -1))return !1;
		e = e.replace("vw", "%"), s.lengthEl || (s.lengthEl = t.createElement("div"), s.lengthEl.style.cssText = "border:0;display:block;font-size:1em;left:0;margin:0;padding:0;position:absolute;visibility:hidden", s.lengthEl.className = "helper-from-picturefill-js"), s.lengthEl.style.width = "0px";
		try {
			s.lengthEl.style.width = e
		} catch (i) {
		}
		return t.body.appendChild(s.lengthEl), n = s.lengthEl.offsetWidth, 0 >= n && (n = !1), t.body.removeChild(s.lengthEl), n
	}, s.detectTypeSupport = function (t, n) {
		var i = new e.Image;
		return i.onerror = function () {
			s.types[t] = !1, r()
		}, i.onload = function () {
			s.types[t] = 1 === i.width, r()
		}, i.src = n, "pending"
	}, s.types = s.types || {}, s.initTypeDetects = function () {
		s.types["image/jpeg"] = !0, s.types["image/gif"] = !0, s.types["image/png"] = !0, s.types["image/svg+xml"] = t.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1"), s.types["image/webp"] = s.detectTypeSupport("image/webp", "data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAAAAAAfQ//73v/+BiOh/AAA=")
	}, s.verifyTypeSupport = function (e) {
		var t = e.getAttribute("type");
		if (null === t || "" === t)return !0;
		var n = s.types[t];
		return "string" == typeof n && "pending" !== n ? (s.types[t] = s.detectTypeSupport(t, n), "pending") : "function" == typeof n ? (n(), "pending") : n
	}, s.parseSize = function (e) {
		var t = /(\([^)]+\))?\s*(.+)/g.exec(e);
		return {media: t && t[1], length: t && t[2]}
	}, s.findWidthFromSourceSize = function (n) {
		for (var i, r = s.trim(n).split(/\s*,\s*/), o = 0, a = r.length; a > o; o++) {
			var u = r[o], l = s.parseSize(u), c = l.length, p = l.media;
			if (c && (!p || s.matchesMedia(p)) && (i = s.getWidthFromLength(c)))break
		}
		return i || Math.max(e.innerWidth || 0, t.documentElement.clientWidth)
	}, s.parseSrcset = function (e) {
		for (var t = []; "" !== e;) {
			e = e.replace(/^\s+/g, "");
			var n, i = e.search(/\s/g), r = null;
			if (-1 !== i) {
				n = e.slice(0, i);
				var o = n.slice(-1);
				if (("," === o || "" === n) && (n = n.replace(/,+$/, ""), r = ""), e = e.slice(i + 1), null === r) {
					var s = e.indexOf(",");
					-1 !== s ? (r = e.slice(0, s), e = e.slice(s + 1)) : (r = e, e = "")
				}
			} else n = e, e = "";
			(n || r) && t.push({url: n, descriptor: r})
		}
		return t
	}, s.parseDescriptor = function (e, t) {
		var n, i = t || "100vw", r = e && e.replace(/(^\s+|\s+$)/g, ""), o = s.findWidthFromSourceSize(i);
		if (r)for (var a = r.split(" "), u = a.length - 1; u >= 0; u--) {
			var l = a[u], c = l && l.slice(l.length - 1);
			if ("h" !== c && "w" !== c || s.sizesSupported) {
				if ("x" === c) {
					var p = l && parseFloat(l, 10);
					n = p && !isNaN(p) ? p : 1
				}
			} else n = parseFloat(parseInt(l, 10) / o)
		}
		return n || 1
	}, s.getCandidatesFromSourceSet = function (e, t) {
		for (var n = s.parseSrcset(e), i = [], r = 0, o = n.length; o > r; r++) {
			var a = n[r];
			i.push({url: a.url, resolution: s.parseDescriptor(a.descriptor, t)})
		}
		return i
	}, s.dodgeSrcset = function (e) {
		e.srcset && (e[s.ns].srcset = e.srcset, e.srcset = "", e.setAttribute("data-pfsrcset", e[s.ns].srcset))
	}, s.processSourceSet = function (e) {
		var t = e.getAttribute("srcset"), n = e.getAttribute("sizes"), i = [];
		return "IMG" === e.nodeName.toUpperCase() && e[s.ns] && e[s.ns].srcset && (t = e[s.ns].srcset), t && (i = s.getCandidatesFromSourceSet(t, n)), i
	}, s.backfaceVisibilityFix = function (e) {
		var t = e.style || {}, n = "webkitBackfaceVisibility"in t, i = t.zoom;
		n && (t.zoom = ".999", n = e.offsetWidth, t.zoom = i)
	}, s.setIntrinsicSize = function () {
		var n = {}, i = function (e, t, n) {
			t && e.setAttribute("width", parseInt(t / n, 10))
		};
		return function (r, o) {
			var a;
			r[s.ns] && !e.pfStopIntrinsicSize && (void 0 === r[s.ns].dims && (r[s.ns].dims = r.getAttribute("width") || r.getAttribute("height")), r[s.ns].dims || (o.url in n ? i(r, n[o.url], o.resolution) : (a = t.createElement("img"), a.onload = function () {
				if (n[o.url] = a.width, !n[o.url])try {
					t.body.appendChild(a), n[o.url] = a.width || a.offsetWidth, t.body.removeChild(a)
				} catch (e) {
				}
				r.src === o.url && i(r, n[o.url], o.resolution), r = null, a.onload = null, a = null
			}, a.src = o.url)))
		}
	}(), s.applyBestCandidate = function (e, t) {
		var n, i, r;
		e.sort(s.ascendingSort), i = e.length, r = e[i - 1];
		for (var o = 0; i > o; o++)if (n = e[o], n.resolution >= s.getDpr()) {
			r = n;
			break
		}
		r && (r.url = s.makeUrl(r.url), t.src !== r.url && (s.restrictsMixedContent() && "http:" === r.url.substr(0, "http:".length).toLowerCase() ? void 0 !== window.console && console.warn("Blocked mixed content image " + r.url) : (t.src = r.url, s.curSrcSupported || (t.currentSrc = t.src), s.backfaceVisibilityFix(t))), s.setIntrinsicSize(t, r))
	}, s.ascendingSort = function (e, t) {
		return e.resolution - t.resolution
	}, s.removeVideoShim = function (e) {
		var t = e.getElementsByTagName("video");
		if (t.length) {
			for (var n = t[0], i = n.getElementsByTagName("source"); i.length;)e.insertBefore(i[0], n);
			n.parentNode.removeChild(n)
		}
	}, s.getAllElements = function () {
		for (var e = [], n = t.getElementsByTagName("img"), i = 0, r = n.length; r > i; i++) {
			var o = n[i];
			("PICTURE" === o.parentNode.nodeName.toUpperCase() || null !== o.getAttribute("srcset") || o[s.ns] && null !== o[s.ns].srcset) && e.push(o)
		}
		return e
	}, s.getMatch = function (e, t) {
		for (var n, i = t.childNodes, r = 0, o = i.length; o > r; r++) {
			var a = i[r];
			if (1 === a.nodeType) {
				if (a === e)return n;
				if ("SOURCE" === a.nodeName.toUpperCase()) {
					null !== a.getAttribute("src") && void 0 !== typeof console && console.warn("The `src` attribute is invalid on `picture` `source` element; instead, use `srcset`.");
					var u = a.getAttribute("media");
					if (a.getAttribute("srcset") && (!u || s.matchesMedia(u))) {
						var l = s.verifyTypeSupport(a);
						if (l === !0) {
							n = a;
							break
						}
						if ("pending" === l)return !1
					}
				}
			}
		}
		return n
	}, o(), r._ = s, i(r)
}(window, window.document, new window.Image), function (e, t, n) {
	var i = window.matchMedia;
	"undefined" != typeof module && module.exports ? module.exports = n(i) : "function" == typeof define && define.amd ? define(function () {
		return t[e] = n(i)
	}) : t[e] = n(i)
}("enquire", this, function (e) {
	"use strict";
	function t(e, t) {
		var n, i = 0, r = e.length;
		for (i; r > i && (n = t(e[i], i), n !== !1); i++);
	}

	function n(e) {
		return "[object Array]" === Object.prototype.toString.apply(e)
	}

	function i(e) {
		return "function" == typeof e
	}

	function r(e) {
		this.options = e, !e.deferSetup && this.setup()
	}

	function o(t, n) {
		this.query = t, this.isUnconditional = n, this.handlers = [], this.mql = e(t);
		var i = this;
		this.listener = function (e) {
			i.mql = e, i.assess()
		}, this.mql.addListener(this.listener)
	}

	function s() {
		if (!e)throw new Error("matchMedia not present, legacy browsers require a polyfill");
		this.queries = {}, this.browserIsIncapable = !e("only all").matches
	}

	return r.prototype = {
		setup: function () {
			this.options.setup && this.options.setup(), this.initialised = !0
		}, on: function () {
			!this.initialised && this.setup(), this.options.match && this.options.match()
		}, off: function () {
			this.options.unmatch && this.options.unmatch()
		}, destroy: function () {
			this.options.destroy ? this.options.destroy() : this.off()
		}, equals: function (e) {
			return this.options === e || this.options.match === e
		}
	}, o.prototype = {
		addHandler: function (e) {
			var t = new r(e);
			this.handlers.push(t), this.matches() && t.on()
		}, removeHandler: function (e) {
			var n = this.handlers;
			t(n, function (t, i) {
				return t.equals(e) ? (t.destroy(), !n.splice(i, 1)) : void 0
			})
		}, matches: function () {
			return this.mql.matches || this.isUnconditional
		}, clear: function () {
			t(this.handlers, function (e) {
				e.destroy()
			}), this.mql.removeListener(this.listener), this.handlers.length = 0
		}, assess: function () {
			var e = this.matches() ? "on" : "off";
			t(this.handlers, function (t) {
				t[e]()
			})
		}
	}, s.prototype = {
		register: function (e, r, s) {
			var a = this.queries, u = s && this.browserIsIncapable;
			return a[e] || (a[e] = new o(e, u)), i(r) && (r = {match: r}), n(r) || (r = [r]), t(r, function (t) {
				a[e].addHandler(t)
			}), this
		}, unregister: function (e, t) {
			var n = this.queries[e];
			return n && (t ? n.removeHandler(t) : (n.clear(), delete this.queries[e])), this
		}
	}, new s
}), function (e) {
	e.fn.prepareTransition = function (t) {
		var n = {
			eventOnly: !1,
			disableExisting: !1
		}, i = e.extend(n, t), r = ["transition-duration", "-moz-transition-duration", "-webkit-transition-duration", "-o-transition-duration"], o = "webkitTransitionEnd transitionend oTransitionEnd";
		return this.each(function () {
			var t = e(this), n = 0;
			e.each(r, function (e, i) {
				n = parseFloat(t.css(i)) || n
			}), 0 !== n ? (i.disableExisting && t.off(o), i.eventOnly || t.addClass("is-transitioning"), t.one(o, function () {
				i.eventOnly || t.removeClass("is-transitioning"), t.trigger("transitionended")
			}).width(), window.setTimeout(function () {
				t.removeClass("is-transitioning"), t.trigger("transitionended")
			}, 1e3 * n + 10)) : t.trigger("transitionended")
		})
	}
}(jQuery), $.fn.serializeJSON = function () {
	var e = {}, t = this.serializeArray();
	return $.each(t, function () {
		e[this.name] ? (e[this.name].push || (e[this.name] = [e[this.name]]), e[this.name].push(this.value || "")) : e[this.name] = this.value || ""
	}), e
}, window.ShopifyMarketing = {}, window.ShopifyMarketing.Helpers = {}, window.ShopifyMarketing.init = function () {
	window.FastClick.attach(document.body), ShopifyMarketing.drawer = new ShopifyMarketing.Drawers("NavDrawer", "left"), ShopifyMarketing.nav = new ShopifyMarketing.Nav, ShopifyMarketing.signup = new ShopifyMarketing.Signup, new ShopifyMarketing.A11yHelpers, _.each($(".js-popover"), function (e) {
		new ShopifyMarketing.Popover($(e))
	})
}, function () {
	this.JST || (this.JST = {}), this.JST["marketing_assets/templates/modal/base"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push('<div class="modal-container" id="ModalContainer" aria-hidden="true">\n  <div class="modal__header">\n    <div class="page-width modal__controls">\n      <button type="button" class="icon icon-close-white" id="CloseModal">\n        <span class="visuallyhidden">', I18n.t("modal.close"), '</span>\n      </button>\n    </div>\n  </div>\n\n  <div class="modal page-width" role="dialog" tabindex="-1" aria-labelledby="ModalTitle"></div>\n</div>\n');
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["marketing_assets/templates/modal/example"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push('<h2 class="modal__heading" id="ModalTitle">', title, "</h2>\n<p>", content, "</p>\n");
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["marketing_assets/templates/signup/errors"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push(""), _.each(err, function (e, t) {
			__p.push('\n  <span class="error hide">', I18n.t("signup.errors." + field + "." + t, {err: err}), "</span>\n")
		}), __p.push("\n");
		return __p.join("")
	}
}.call(this), function () {
	this.JST || (this.JST = {}), this.JST["marketing_assets/templates/signup/modal"] = function (obj) {
		var __p = [];
		with (obj || {})__p.push('<div class="signup-modal__content">\n  <h2 class="modal__heading" id="ModalTitle">', I18n.t("signup.header"), "</h2>\n  ", $(".signup-form").first().get(0).outerHTML, "\n</div>\n");
		return __p.join("")
	}
}.call(this), ShopifyMarketing.Helpers.Cookie = function (e) {
	e || (e = {}), "string" == typeof e && (e = {cookie: e}), void 0 === e.cookie && (e.cookie = "");
	var t = {};
	return t.get = function (t) {
		for (var n = e.cookie.split(/;\s*/), i = 0; i < n.length; i++) {
			var r = n[i].split("="), o = decodeURIComponent(r[0]);
			if (o === t)return decodeURIComponent(r[1])
		}
		return void 0
	}, t.set = function (t, n, i) {
		var r = encodeURIComponent(t) + "=" + encodeURIComponent(n);
		return i || (i = {}), i.expires && (r += "; expires=" + i.expires), i.path && (r += "; path=" + encodeURIComponent(i.path)), e.cookie = r, r
	}, t
}(document), ShopifyMarketing.Helpers.QueryString = function () {
	var e = {};
	return e.parse = function (e, t, n) {
		t = t || "&", n = n || "=";
		var i = {};
		return "string" != typeof e || 0 === e.length ? i : (_.each(e.split(t), function (e) {
			var t = e.split(n), r = decodeURIComponent(t[0], !0), o = decodeURIComponent(t.slice(1).join(n), !0);
			r in i ? _.isArray(i[r]) ? i[r].push(o) : i[r] = [i[r], o] : i[r] = o
		}), i)
	}, e
}(), ShopifyMarketing.Helpers.SwipeSurface = function () {
	var e = function () {
		return this
	};
	return e.prototype.onSwipe = function (e, t) {
		t = t || {};
		var n, i, r, o, s, a, u, l = this, c = t.threshold || 150, p = t.restraint || 100, h = t.allowedTime || 400;
		l.on("touchstart", function (e) {
			var t = e.originalEvent.changedTouches[0];
			n = "none", i = t.pageX, r = t.pageY, u = (new Date).getTime(), e.preventDefault()
		}), l.on("touchmove", function (e) {
			e.preventDefault()
		}), l.on("touchend", function (t) {
			var l = t.originalEvent.changedTouches[0];
			o = l.pageX - i, s = l.pageY - r, a = (new Date).getTime() - u, h >= a && (Math.abs(o) >= c && Math.abs(s) <= p && (n = 0 > o ? "left" : "right"), Math.abs(s) >= c && Math.abs(o) <= p && (n = 0 > s ? "up" : "down"), e(n), t.preventDefault())
		})
	}, e
}(), ShopifyMarketing.Helpers.URL = function () {
	var e = {};
	return e.querystring = function (e) {
		var t = e.indexOf("?");
		return e.substr(t + 1)
	}, e
}(), ShopifyMarketing.A11yHelpers = function () {
	var e = function () {
		this.init(), $(".in-page-link").on("click", _.bind(function (e) {
			this.pageLinkFocus($(e.currentTarget.hash))
		}, this))
	};
	return e.prototype.init = function () {
		var e = window.location.hash;
		e && document.getElementById(e.slice(1)) && this.pageLinkFocus($(e))
	}, e.prototype.trapFocus = function (e, t) {
		var n = t ? "focusin." + t : "focusin";
		e.attr("tabindex", "-1"), $(document).on(n, function (t) {
			e[0] === t.target || e.has(t.target).length || e.focus()
		})
	}, e.prototype.removeTrapFocus = function (e, t) {
		var n = t ? "focusin." + t : "focusin";
		e.removeAttr("tabindex"), $(document).off(n)
	}, e.prototype.pageLinkFocus = function (e) {
		e.length && (e.get(0).tabIndex = -1, e.focus().addClass("js-focus-hidden"), e.one("blur", function () {
			e.removeClass("js-focus-hidden").removeAttr("tabindex")
		}))
	}, e
}();
var _gaq = window._gaq || [];
ShopifyMarketing.Analytics = function () {
	var e = "analytics", t = {external: "[data-ga-event]"}, n = function () {
		this.trackExternal()
	};
	return n.prototype.isValueInteger = function (e) {
		return "number" == typeof e && e % 1 === 0
	}, n.prototype.track = function (e, t, n, i) {
		var r = Array.prototype.slice.call(arguments);
		void 0 === i || this.isValueInteger(i) || r.splice(_.indexOf(r, i)), r.unshift("_trackEvent"), _gaq.push(_.filter(r, _.identity))
	}, n.prototype.trackExternal = function () {
		$(document.body).on("click." + e, t.external, _.bind(function (e) {
			var t = $(e.currentTarget).data();
			this.track(t.gaEvent, t.gaAction, t.gaLabel, t.gaValue)
		}, this))
	}, new n
}(), ShopifyMarketing.Breakpoints = function () {
	var e = {
		desktop: "screen and (min-width: 61.875em)",
		tablet: "screen and (min-width: 46.875em) and (max-width: 61.8125em)",
		tabletUp: "screen and (min-width: 46.875em)",
		tabletDown: "screen and (max-width: 61.8125em)",
		phone: "screen and (max-width: 46.875em)"
	}, t = function (t) {
		this.breakpoints = t || e, this.init()
	};
	return t.prototype = e, t.prototype.init = function () {
		var e = this;
		_.each(this.breakpoints, function (t, n) {
			e[n] = t
		})
	}, t.prototype.isDesktop = function () {
		return window.matchMedia(this.desktop).matches
	}, t
}(), ShopifyMarketing.Config = function () {
	var e = function () {
		window.ShopifyConfig = window.ShopifyConfig || {}, this.data = window.ShopifyConfig
	};
	return e.prototype.get = function (e, t) {
		if ("undefined" == typeof t)throw new Error("Must provide a fallback value");
		return this.data.hasOwnProperty(e) ? this.data[e] : t
	}, e.prototype.set = function (e, t) {
		this.data[e] = t
	}, new e
}(), ShopifyMarketing.Cookies = function () {
	var e = ShopifyMarketing.Helpers.Cookie, t = "subdomain", n = "customerEmail", i = "last_shop", r = function () {
	};
	return r.prototype.getSubdomain = function () {
		return e.get(t) || this.getSubdomainFromLastShopCookie()
	}, r.prototype.getSubdomainCookie = function () {
		return e.get(t)
	}, r.prototype.getSubdomainFromLastShopCookie = function () {
		var t = e.get(i);
		if (t) {
			var n = t.match(/^([^\.]+)\.myshopify\.com$/);
			if (n)return n[1]
		}
	}, r.prototype.getCustomerEmailCookie = function () {
		return e.get(n)
	}, r.prototype.enableLoginCookies = function (i, r) {
		e.set(t, i, {path: "/"}), e.set(n, r, {path: "/"})
	}, r.prototype.disableLoginCookies = function () {
		e.set(t, "", {expires: new Date(0)}), e.set(n, "", {expires: new Date(0)})
	}, new r
}(), function () {
	var e = function () {
		this.data = {}
	};
	e.prototype.translate = function (e, t) {
		var n = e.split("."), i = this.data, r = 0, o = n.length;
		for (o; o > r; r++)i = i[n[r]];
		return t ? _.template(i, t, {interpolate: /\%\{(.+?)\}/g}) : i
	}, e.prototype.t = e.prototype.translate, window.I18n = new e
}(), ShopifyMarketing.keyCodes = {
	BACKSPACE: 8,
	COMMA: 188,
	DELETE: 46,
	DOWN: 40,
	END: 35,
	ENTER: 13,
	ESCAPE: 27,
	HOME: 36,
	LEFT: 37,
	NUMPAD_ADD: 107,
	NUMPAD_DECIMAL: 110,
	NUMPAD_DIVIDE: 111,
	NUMPAD_ENTER: 108,
	NUMPAD_MULTIPLY: 106,
	NUMPAD_SUBTRACT: 109,
	PAGE_DOWN: 34,
	PAGE_UP: 33,
	PERIOD: 190,
	RIGHT: 39,
	SPACE: 32,
	TAB: 9,
	UP: 38
}, function () {
	var e = function () {
		this.$body = $("body")
	};
	e.prototype.init = function () {
		this.$body.on("optimizely", _.bind(this.activate, this))
	}, e.prototype.experiments = {}, e.prototype.activate = function (e, t) {
		_.isFunction(this.experiments[t.variation]) && this.experiments[t.variation]()
	}, window.ShopifyMarketing.Optimizely = new e
}(), ShopifyMarketing.PrettyScrollTo = function () {
	var e = ShopifyMarketing.A11yHelpers, t = function (e, t) {
		var n = {$selector: $(".link-scroll-to"), scrollClass: "js-is-scrolling"};
		this.options = $.extend(n, e), this.callback = "function" == typeof t ? t : $.noop, this.init()
	};
	return t.prototype.init = function () {
		this.options.$selector.on("click", $.proxy(function (e) {
			e.preventDefault(), this.handleClick(e)
		}, this))
	}, t.prototype.handleClick = function (t) {
		var n, i, r, o = t.currentTarget, s = this.callback, a = o.pathname.replace(/(^\/?)/, "/");
		o.host === window.location.host && a === window.location.pathname && o.hash && (i = o.hash, n = $(o.hash), n.length && (t.preventDefault(), r = this.options.offset ? n.offset().top + this.options.offset : n.offset().top, void 0 !== window.history && void 0 !== window.history.replaceState && window.history.replaceState({}, document.title, i), $("body, html").stop().addClass(this.options.scrollClass).animate({scrollTop: r}, 500).promise().then($.proxy(function () {
			$("body, html").removeClass(this.options.scrollClass), e.prototype.pageLinkFocus(n), s(o, n)
		}, this))))
	}, t
}(), ShopifyMarketing.Accordion = function () {
	var e = new ShopifyMarketing.Breakpoints, t = function (t, n) {
		this.config = _.extend({}, {
			itemSelector: ".accordion-item--mobile",
			itemLink: ".accordion-link",
			itemContent: ".accordion-content",
			mobileOnly: !0,
			openFirst: !0
		}, n), this.$el = t, this.$accordionItems = this.$el.find(this.config.itemSelector), this.$accordionLinks = this.$el.find(this.config.itemLink), this.$accordionContents = this.$el.find(this.config.itemContent), this.toggle = _.bind(this.toggle, this), this.enable = _.bind(this.enable, this), this.disable = _.bind(this.disable, this), this.ensureTabIndex = _.bind(this.ensureTabIndex, this), this.removeTabIndex = _.bind(this.removeTabIndex, this), this.removeStyles = _.bind(this.removeStyles, this), this.setInitialAriaStates = _.bind(this.setInitialAriaStates, this), this.removeAriaStates = _.bind(this.removeAriaStates, this), this._onClick = _.bind(this._onClick, this), this._onKeydown = _.bind(this._onKeydown, this), this.config.mobileOnly ? enquire.register(e.tablet, this.disable).register(e.phone, this.enable) : this.enable()
	};
	return t.prototype.getCurrentItem = function (e) {
		var t = $(e.currentTarget);
		return t.closest(this.config.itemSelector)
	}, t.prototype.toggle = function (e) {
		var t = this.getCurrentItem(e), n = !t.hasClass("js-is-active");
		t.toggleClass("js-is-active", n).find(this.config.itemContent).attr("aria-hidden", !n).slideToggle(n).end().find(this.config.itemLink).attr("aria-expanded", n)
	}, t.prototype.enable = function () {
		this.$el.on("click", this.config.itemLink, this._onClick), this.$el.on("keydown", this.config.itemLink, this._onKeydown), this.ensureTabIndex(), this.setInitialAriaStates(), this.config.openFirst && this.$accordionItems.first().not(".js-is-active").find(this.config.itemLink).trigger("click"), this.$el.addClass("js-is-initialized")
	}, t.prototype.disable = function () {
		this.$el.off("click", this.config.itemLink), this.$el.off("keydown", this.config.itemLink), this.removeTabIndex(), this.removeAriaStates(), this.removeStyles(), this.$el.removeClass("js-is-initialized")
	}, t.prototype.ensureTabIndex = function () {
		this.$accordionLinks.prop("tabindex", 0)
	}, t.prototype.removeTabIndex = function () {
		this.$accordionLinks.removeAttr("tabindex")
	}, t.prototype.removeStyles = function () {
		this.$accordionItems.removeAttr("style"), this.$accordionContents.removeAttr("style")
	}, t.prototype.setInitialAriaStates = function () {
		var e = this.config;
		this.$accordionItems.each(function (t, n) {
			var i = $(n), r = _.uniqueId("Accordion");
			i.removeClass("js-is-active"), i.find(e.itemContent).attr({
				"aria-hidden": "true",
				id: r
			}), i.find(e.itemLink).attr({"aria-expanded": "false", "aria-controls": r})
		})
	}, t.prototype.removeAriaStates = function () {
		this.$accordionContents.removeAttr("aria-hidden id"), this.$accordionLinks.removeAttr("aria-controls aria-expanded")
	}, t.prototype._onKeydown = function (e) {
		return e.keyCode === ShopifyMarketing.keyCodes.ENTER ? (e.preventDefault(), this.toggle(e)) : void 0
	}, t.prototype._onClick = function (e) {
		return e.preventDefault(), this.toggle(e)
	}, t
}(), ShopifyMarketing.CarouselBase = function () {
	var e = function (e, t, n) {
		this.config = _.extend({duration: 5e3}, n), this.$el = $(e), this.$items = this.$el.find(t), this.itemsCount = this.$items.length, this.currentIndex = 0
	};
	return e.prototype.prev = function () {
		this.goToIndex(this.currentIndex > 0 ? this.currentIndex - 1 : this.itemsCount - 1)
	}, e.prototype.next = function () {
		this.goToIndex(this.currentIndex < this.itemsCount - 1 ? this.currentIndex + 1 : 0)
	}, e.prototype.start = function () {
		return this.goToIndex(this.currentIndex), this.itemsCount > 1 ? void(this.timeout = setTimeout(_.bind(this._loop, this), this.config.duration)) : void 0
	}, e.prototype.stop = function () {
		return clearTimeout(this.timeout)
	}, e.prototype._loop = function () {
		this.next(), this.timeout = setTimeout(_.bind(this._loop, this), this.config.duration)
	}, e
}(), ShopifyMarketing.Carousel = function () {
	var e = function (e, t) {
		ShopifyMarketing.CarouselBase.apply(this, [e, ".carousel-item"], t), this.$navItems = this.$el.find(".carousel-nav-item"), this.itemsCount > 1 ? (this.nextIndex = this.currentIndex + 1, this.prevIndex = this.itemsCount - 1) : (this.nextIndex = 0, this.prevIndex = 0), this._onClick = _.bind(this._onClick, this), this._navNext = _.bind(this._navNext, this), this._navPrev = _.bind(this._navPrev, this), this._loop = _.bind(this._loop, this), this.next = _.bind(this.next, this), this.prev = _.bind(this.prev, this), this.start = _.bind(this.start, this), this.stop = _.bind(this.stop, this), this.goToIndex = _.bind(this.goToIndex, this), this.$el.on("click", ".carousel-nav-item", this._onClick), this.$el.on("click", ".carousel-arrow-left", this._navPrev), this.$el.on("click", ".carousel-arrow-right", this._navNext), this.goToIndex(0)
	};
	return e.prototype = _.clone(ShopifyMarketing.CarouselBase.prototype), e.prototype.goToIndex = function (e) {
		return this.currentIndex = e >= this.itemsCount ? 0 : 0 > e ? this.itemsCount - 1 : e, this.nextIndex = this.currentIndex + 1 < this.itemsCount ? this.currentIndex + 1 : 0, this.prevIndex = this.currentIndex - 1 >= 0 ? this.currentIndex - 1 : this.itemsCount - 1, this.$navItems.removeClass("js-is-active"), this.$items.removeClass("js-is-active"), this.$items.eq(this.currentIndex).prepareTransition().addClass("js-is-active"), this.$el.attr("data-state", this.currentIndex).trigger("change", this.currentIndex), this.$navItems.eq(this.currentIndex).addClass("js-is-active")
	}, e.prototype._navPrev = function (e) {
		return e.preventDefault(), this.stop(), this._track(), this.prev()
	}, e.prototype._navNext = function (e) {
		return e.preventDefault(), this.stop(), this._track(), this.next()
	}, e.prototype._onClick = function (e) {
		e.preventDefault();
		var t = e.currentTarget.getAttribute("data-state");
		return this.goToIndex(~~parseInt(t, 10)), this._track(), this.stop()
	}, e.prototype._track = function () {
		var e;
		e = "" === this.$navItems.eq(this.currentIndex).text() ? this.currentIndex + 1 : this.$navItems.eq(this.currentIndex).text(), ShopifyMarketing.Analytics.track("carousel", this.$el.prop("id"), e)
	}, e
}(), ShopifyMarketing.Drawers = function () {
	var e = ShopifyMarketing.Breakpoints, t = ShopifyMarketing.A11yHelpers, n = ShopifyMarketing.Config, i = function (e, t, n) {
		var i = {
			close: ".js-drawer-close",
			open: ".js-drawer-open-" + t,
			openClass: "js-drawer-open",
			dirOpenClass: "js-drawer-open-" + t
		};
		return this.$nodes = {
			parent: $("body, html"),
			page: $("#PageContainer")
		}, this.config = _.extend(i, n), this.position = t, this.$drawer = $("#" + e), this.$drawer.length ? (this.drawerIsOpen = !1, void this.init()) : !1
	};
	return i.prototype.init = function () {
		$(this.config.open).on("click", _.bind(this.open, this)), this.$drawer.find(this.config.close).on("click", _.bind(this.close, this)), enquire.register(e.prototype.desktop, _.bind(this.close, this))
	}, i.prototype.open = function (e) {
		return e && e.stopPropagation && (e.stopPropagation(), this.$activeSource = $(e.currentTarget)), this.drawerIsOpen || n.get("drawerIsOpen", !1) ? this.close() : (this.$drawer.prepareTransition(), this.$nodes.parent.addClass(this.config.openClass + " " + this.config.dirOpenClass), this.drawerIsOpen = !0, n.set("drawerIsOpen", this.drawerIsOpen), t.prototype.trapFocus(this.$drawer, "drawer_focus"), this.$activeSource && this.$activeSource.attr("aria-expanded") && this.$activeSource.attr("aria-expanded", "true"), this.$nodes.page.on("touchmove.drawer", function () {
			return !1
		}), void this.$nodes.page.on("click.drawer", _.bind(function () {
			return this.close(), !1
		}, this)))
	}, i.prototype.close = function () {
		this.drawerIsOpen && ($(document.activeElement).trigger("blur"), this.$drawer.addClass("is-transitioning"), this.$nodes.parent.removeClass(this.config.dirOpenClass + " " + this.config.openClass), this.drawerIsOpen = !1, n.set("drawerIsOpen", this.drawerIsOpen), _.delay(_.bind(function () {
			this.$drawer.removeClass("is-transitioning"), t.prototype.removeTrapFocus(this.$drawer, "drawer_focus"), this.$activeSource && (this.$activeSource.focus(), this.$activeSource.attr("aria-expanded") && this.$activeSource.attr("aria-expanded", "false"))
		}, this), 610), this.$nodes.page.off(".drawer"))
	}, i
}(), ShopifyMarketing.EqualHeight = function () {
	var e = ShopifyMarketing.Breakpoints;
	e = new e;
	var t = function (t, n) {
		this.$parent = t, this.$children = n || this.$parent.children(), this.equalize = _.bind(this.equalize, this), this.reset = _.bind(this.reset, this), this._getMaxHeight = _.bind(this._getMaxHeight, this), enquire.register(e.desktop, this.equalize).register(e.tablet, this.equalize).register(e.phone, this.reset)
	};
	return t.prototype.equalize = function () {
		var e;
		this.reset(), e = this._getMaxHeight(), this.$children.css({height: e})
	}, t.prototype.reset = function () {
		this.$children.css({height: ""})
	}, t.prototype._getMaxHeight = function () {
		return Math.max.apply(null, _.map(this.$children, function (e) {
			return $(e).outerHeight()
		}))
	}, t
}(), ShopifyMarketing.InPageMenu = function () {
	var e = function (e, t) {
		this.$el = e;
		var n = _.extend({anchorsWrapper: ".in-page-menu", selectSelector: ".in-page-menu--select"}, t);
		this.$anchorNav = this.$el.find(n.anchorsWrapper), this.$selectNav = this.$el.find(n.selectSelector), this.$anchors = this.$anchorNav.find("a"), this.addEventListeners()
	};
	return e.prototype.addEventListeners = function () {
		this.$selectNav.on("change", _.bind(this.onChange, this)), this.$anchors.on("click", _.bind(this.onClick, this))
	}, e.prototype.onChange = function (e) {
		this.triggerEvent($(e.currentTarget))
	}, e.prototype.onClick = function (e) {
		e.preventDefault();
		var t = $(e.currentTarget);
		this.$anchors.removeClass("js-is-active"), t.addClass("js-is-active"), this.triggerEvent(t)
	}, e.prototype.triggerEvent = function (e) {
		this.$el.trigger("menu:select", [e])
	}, e
}(), window.App = window.App || {}, ShopifyMarketing.Modal = function () {
	var e = ShopifyMarketing.keyCodes, t = ShopifyMarketing.A11yHelpers, n = function (e, t, n) {
		this.$el = e, this.$nodes = {body: $(document.body)};
		var i = {
			modalActiveSourceClass: "js-modal-current-source",
			modalActiveBodyClass: "js-modal-open",
			modalActiveContainerClass: "js-is-active",
			clickingOverlayClosesModal: !0,
			emptyOnClose: !0
		};
		this.options = _.extend(i, n), $("#ModalContainer").length || this.$nodes.body.prepend(JST["marketing_assets/templates/modal/base"]()), this.$siteContainer = $("#SiteContainer"), this.modalDom = {
			$container: $(".modal-container"),
			$modal: $(".modal"),
			$closeBtn: $("#CloseModal")
		}, this.$modalSource = this.$el, this.$modalTrigger = this.$modalSource, this._onClick = _.bind(this._onClick, this), this._onBackdropClick = _.bind(this._onBackdropClick, this), this._onKeyDown = _.bind(this._onKeyDown, this), this.close = _.bind(this.close, this), this.$modalSource.on({
			click: this._onClick,
			keydown: this._onKeyDown
		}), this.modalDom.$closeBtn.on("click", this.close), this.options.clickingOverlayClosesModal && this.modalDom.$container.on("click", this._onBackdropClick), this.modalDom.$container.on("keydown", this._onKeyDown), this.template = t, this.currentIndex = -1, this.$activeSource = this.$modalSource.eq(0)
	};
	return n.prototype.active = !1, n.prototype.open = function (e) {
		this.render(), App.activeModal = this, this.active = !0, e && (this.$modalTrigger = $(e.currentTarget)), this.modalDom.$container.prepareTransition().addClass(this.options.modalActiveContainerClass), this.$nodes.body.addClass(this.options.modalActiveBodyClass), this.$siteContainer.attr("aria-hidden", !0), this.modalDom.$container.attr("aria-hidden", !1), this.modalDom.$modal.focus(), this.modalDom.$container[0].scrollTop = 0, t.prototype.trapFocus(this.modalDom.$container, "modal_focus")
	}, n.prototype.close = function (e) {
		this.active = !1, this.modalDom.$container.one("transitionended", _.bind(function () {
			this.options.emptyOnClose && this.empty(), _.isFunction(e) && e()
		}, this)), this.modalDom.$container.prepareTransition().removeClass(this.options.modalActiveContainerClass), this.$nodes.body.removeClass(this.options.modalActiveBodyClass), this.$modalSource.removeClass(this.options.modalActiveSourceClass), this.$siteContainer.attr("aria-hidden", !1), this.modalDom.$container.attr("aria-hidden", !0), t.prototype.removeTrapFocus(this.modalDom.$container, "modal_focus"), this.$modalTrigger && this.$modalTrigger.length ? this.$modalTrigger.focus() : this.$modalSource.focus(), App.activeModal = null, this.currentIndex = -1
	}, n.prototype.render = function () {
		var e = this.template(this.$activeSource.data());
		this.modalDom.$modal.html(e), this.picturefill()
	}, n.prototype.empty = function () {
		this.modalDom.$modal.empty()
	}, n.prototype.picturefill = function () {
		var e = this.modalDom.$modal.find("picture > img");
		e.length && window.picturefill && window.picturefill({elements: e})
	}, n.prototype._onClick = function (e) {
		e.preventDefault(), this.open(e)
	}, n.prototype._onKeyDown = function (t) {
		if (this.active)switch (t.keyCode) {
			case e.ESCAPE:
				this.close()
		}
	}, n.prototype._onBackdropClick = function (e) {
		e.target === e.delegateTarget && this.active && this.close()
	}, n
}(), jQuery.extend(jQuery.easing, {
	easeInOutSine: function (e, t, n, i, r) {
		return -i / 2 * (Math.cos(Math.PI * t / r) - 1) + n
	}
}), ShopifyMarketing.Nav = function () {
	var e = function (e) {
		var t = {
			selectors: {
				wrapper: ".marketing-nav-wrapper",
				subNavList: "#ShopifySubNavList",
				mobileSelect: "#ShopifyNavMobileSelect",
				mobileClose: "#ShopifyNavMobileClose"
			}
		};
		return this.config = _.merge(t, e), this.$wrapper = $(this.config.selectors.wrapper), this.$subNavList = $(this.config.selectors.subNavList), this.$mobileSelect = $(this.config.selectors.mobileSelect), this.$mobileClose = $(this.config.selectors.mobileClose), this.$wrapper.length ? void this.init() : !1
	};
	return e.prototype.init = function () {
		this.$mobileSelect.on("click", _.bind(this.toggleSubnav, this)), this.$mobileClose.on("click", _.bind(this.hideSubnav, this))
	}, e.prototype.toggleSubnav = function () {
		this.$wrapper.toggleClass("js-is-active"), this.$subNavList.slideToggle({
			easing: "easeInOutSine",
			duration: 300
		})
	}, e.prototype.hideSubnav = function () {
		this.$wrapper.removeClass("js-is-active"), this.$subNavList.slideUp({easing: "easeInOutSine", duration: 300})
	}, e
}(), ShopifyMarketing.NewsletterForm = function () {
	var e = window._gaUTracker || [], t = window._gaq || [], n = function () {
		this.submit = _.bind(this.submit, this), $(".js-email-subscribe").on("submit", this.submit)
	};
	return n.prototype.submit = function (n) {
		n.preventDefault();
		var i = $(n.currentTarget), r = i.closest(".newsletter-block"), o = i.find(".marketing-form__messages"), s = i.find('[type="email"]').val(), a = i.attr("action"), u = this.isMailchimpUrl(a);
		s.length ? (o.empty(), t.push(["_trackevent", "blog", "subscription", "email"]), e("send", "event", "blog", "subscription", "email"), $.ajax({
			type: u ? "GET" : "POST",
			url: a,
			data: u ? i.serialize() : JSON.stringify({email: s}),
			cache: !1,
			dataType: "json",
			contentType: "application/json; charset=utf-8",
			error: function (e) {
				o.append(e.errors ? '<span class="newsletter__error error">' + e.errors + "</span>" : '<span class="newsletter__error error">' + I18n.t("newsletter.errors.generic") + "</span>")
			},
			success: function (e) {
				e.result && "success" !== e.result ? (e.msg = e.msg.replace("0 - ", ""), o.html(e.msg)) : r.addClass("js-success")
			}
		})) : o.html(I18n.t("newsletter.errors.blank"))
	}, n.prototype.isMailchimpUrl = function (e) {
		return e.indexOf("//shopify.us2.list-manage.com") >= 0
	}, n
}(), ShopifyMarketing.Popover = function () {
	var e = ShopifyMarketing.keyCodes, t = function (e, t) {
		this.config = _.extend({}, {position: e.data("position") || "bottom"}, t), this.$el = e, this.$popover = this.$el.find(".popover"), this.$trigger = this.$el.find(".popover__click-trigger"), this.$html = $("html"), this.show = _.bind(this.show, this), this.hide = _.bind(this.hide, this), this._mouseEnter = _.bind(this._mouseEnter, this), this._mouseLeave = _.bind(this._mouseLeave, this), this._onClick = _.bind(this._onClick, this), this._onKeyUp = _.bind(this._onKeyUp, this), this._focusOut = _.bind(this._focusOut, this), this._onBgClick = _.bind(this._onBgClick, this), this.$el.on("mouseenter", this._mouseEnter), this.$el.on("mouseleave", this._mouseLeave), this.$el.on("focus", "*", this.show), this.$el.on("focusout", "*", this._focusOut), this.$trigger.on({
			click: this._onClick,
			keyup: this._onKeyUp
		}), this.$popover.on("keyup", this._onKeyUp), this.$html.on("click", this._onBgClick), this.isOpen = !1, this.init()
	};
	return t.prototype.init = function () {
		var e = _.uniqueId("Popover");
		this.$popover.addClass(this.config.position).attr("id", e), this.$trigger.attr({
			"aria-expanded": "false",
			"aria-describedby": e
		})
	}, t.prototype.show = function () {
		this.$popover.outerWidth(), this.$el.addClass("js-is-active"), this.isOpen = !0, this.$trigger.attr("aria-expanded", "true")
	}, t.prototype.hide = function () {
		this.$trigger.attr("aria-expanded", "false"), this.$el.removeClass("js-is-active"), this.isOpen = !1
	}, t.prototype._focusOut = function (e) {
		e.stopPropagation(), setTimeout(_.bind(function () {
			this.$popover.find(":focus").length || this.hide()
		}, this), 10)
	}, t.prototype._mouseEnter = function () {
		this.show()
	}, t.prototype._mouseLeave = function () {
		this.hide()
	}, t.prototype._onBgClick = function (e) {
		e.target !== this.$popover && this.hide()
	}, t.prototype.toggle = function () {
		this.isOpen ? this.hide() : this.show()
	}, t.prototype._onClick = function () {
		this.toggle()
	}, t.prototype._onKeyUp = function (t) {
		switch (t.keyCode) {
			case e.SPACE:
				t.preventDefault(), t.stopPropagation(), this.toggle();
				break;
			case e.ESCAPE:
				this.hide(), this.$trigger.focus()
		}
	}, t
}(), ShopifyMarketing.SignupAPI = function () {
	var e = ShopifyMarketing.Config, t = ShopifyMarketing.Helpers.QueryString, n = ShopifyMarketing.Helpers.URL, i = function () {
		this.passwordLength = 5, this.shopNameLength = 4
	};
	return i.prototype.baseURI = function () {
		return e.get("SignupBaseURI", "https://app.shopify.com")
	}, i.prototype.validate = function (e) {
		var t = $.Deferred();
		return this.formData = e, this.formData.errors = {}, $.when(this.validateShopName.call(this), this.validateEmail.call(this), this.validatePassword.call(this)).done(_.bind(function (e) {
			_.isEmpty(this.formData.errors) ? e.resolve(this) : e.reject(this)
		}, this, t)), t
	}, i.prototype.validateShopName = function () {
		var e = $.Deferred(), t = this.formData["signup[shop_name]"], n = this.formData["signup[password]"];
		return $.trim(t) ? t.length < this.shopNameLength ? (this.formData.errors.shop_name = {minlength: !0}, e.resolve()) : t === n ? (this.formData.errors.shop_name = {matchesPassword: !0}, e.resolve()) : (this.checkAvailability(t, this.formData["signup[email]"]).done(_.bind(function (t) {
			"unavailable" === t.status ? this.formData.errors.shop_name = {existingAdmin: t.host} : "error" === t.status && (this.formData.errors.shop_name = {message: t.message}), e.resolve()
		}, this)), e) : (this.formData.errors.shop_name = {empty: !0}, e.resolve())
	}, i.prototype.validateEmail = function () {
		var e = this.formData["signup[email]"];
		return $.trim(e) ? /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(e) ? void 0 : (this.formData.errors.email = {invalid: !0}, !1) : (this.formData.errors.email = {empty: !0}, !1)
	}, i.prototype.validatePassword = function () {
		var e = this.formData["signup[password]"];
		return $.trim(e) ? /^[^\s]+$/.test(e) ? e.length < this.passwordLength ? (this.formData.errors.password = {minlength: !0}, !1) : void 0 : (this.formData.errors.password = {spaces: !0}, !1) : (this.formData.errors.password = {empty: !0}, !1)
	}, i.prototype.checkAvailability = function (e, t) {
		return jQuery.getJSON(this.baseURI() + "/services/signup/check_availability.json?callback=?", {
			shop_name: e,
			email: t
		})
	}, i.prototype.getLocation = function () {
		return window.location
	}, i.prototype.track = function (e) {
		var i;
		return i = e ? e : t.parse(n.querystring(this.getLocation().href)), i = $.extend({}, {signup_page: this.getLocation().href}, i), delete i.callback, jQuery.getJSON(this.baseURI() + "/services/signup/track/?callback=?", i)
	}, new i
}();
var _gaq = window._gaq || [];
ShopifyMarketing.SignupForm = function () {
	var e = ShopifyMarketing.Helpers.Cookie, t = ShopifyMarketing.Helpers.QueryString, n = ShopifyMarketing.Helpers.URL, i = function (e, t) {
		var i = {
			gaEvents: {
				success: {
					gaq: ["_trackEvent", "threefield", "threefield", "threefield"],
					tracker: {
						eventCategory: "SignUp",
						eventAction: "success",
						eventLabel: "three field",
						dimension1: "Lead"
					}
				},
				error_shop_name: {
					gaq: ["_trackEvent", "threefield", "error", "badshop_name"],
					tracker: {eventCategory: "SignUp", eventAction: "error", eventLabel: "Bad shop_name"}
				},
				error_email: {
					gaq: ["_trackEvent", "threefield", "error", "bademail"],
					tracker: {eventCategory: "SignUp", eventAction: "error", eventLabel: "Bad email"}
				},
				error_password: {
					gaq: ["_trackEvent", "threefield", "error", "badpassword"],
					tracker: {eventCategory: "SignUp", eventAction: "error", eventLabel: "Bad password"}
				},
				mailcheck: {
					gaq: ["_trackEvent", "threefield", "error", "emailsuggestion"],
					tracker: {eventCategory: "SignUp", eventAction: "error", eventLabel: "Emailsuggestion"}
				}
			}
		};
		this.config = _.extend({}, i, t), this.API = ShopifyMarketing.SignupAPI, this.$form = e, this.$form.addClass("js-signup-inited"), this.$wrappers = this.$form.find(this.SELECTORS.wrappers), this.$userFields = this.$form.find(this.SELECTORS.userFields), this.$submitBtn = this.$form.find(this.SELECTORS.submitBtn), this.bindSubmit(), this.setHiddenFields(n.querystring(window.location.href)), this.track()
	};
	return i.prototype.ERROR_TEMPLATE = "marketing_assets/templates/signup/errors", i.prototype.SELECTORS = {
		messages: ".marketing-form__messages",
		errors: ".marketing-form__messages .error",
		suggest: ".marketing-form__messages .suggest",
		wrappers: ".marketing-input-wrapper",
		userFields: ".marketing-input",
		submitBtn: ".js-signup-submit"
	}, i.prototype.CLASSES = {error: "marketing-input--error"}, i.prototype.focusAtIndex = function (e) {
		this.$userFields.eq(e).focus()
	}, i.prototype.populateFromInline = function (e) {
		var t = '[name="signup[email]"]', n = e.find(t).val(), i = this.$userFields.filter(t);
		i.val(n), n.length ? (this.validateEmail(), this.API.formData.errors.email ? this.focusAtIndex(0) : (i.parents("label").addClass("js-with-label"), this.focusAtIndex(1))) : this.focusAtIndex(0)
	}, i.prototype.setHiddenFields = function (n) {
		var i = t.parse(n);
		this.getHiddenField("forwarded_host").val(window.location.hostname), this.setHiddenFieldIfNotSet("ref", i.ref), this.getHiddenField("ssid").val(i.ssid || e.get("ssid")), e.get("source") && this.setHiddenFieldIfNotSet("source", e.get("source")), i.signup_code && this.setHiddenFieldIfNotSet("signup_code", i.signup_code), window._visit && _.isFunction(window._visit.multitrackToken) && this.getHiddenField("_y").val(window._visit.multitrackToken()), i.signup_type && this.setSignupTypeIfNotSet(i.signup_type)
	}, i.prototype.getHiddenField = function (e) {
		return this.$form.find('[name="' + e + '"]')
	}, i.prototype.setHiddenFieldIfNotSet = function (e, t) {
		var n = this.getHiddenField(e);
		n && _.isEmpty(n.val()) && n.val(t)
	}, i.prototype.setTheme = function (e) {
		return this.getHiddenField("extra[selected_theme]").val(e)
	}, i.prototype.setSignupTypeIfNotSet = function (e) {
		if (!this.signupTypeExists(e)) {
			var t = document.createElement("input");
			return t.setAttribute("type", "hidden"), t.setAttribute("name", "signup_types[]"), t.setAttribute("value", e), this.$form.prepend(t)
		}
	}, i.prototype.signupTypeExists = function (e) {
		var t = this.getHiddenField("signup_types[]"), n = t.filter(function (t, n) {
			return n.value === e
		}).length;
		return n > 0
	}, i.prototype.sendOnSuccessEvents = function () {
		var e = $.Deferred();
		return $.when(this._successTrekkie(), this._successGAasync(), this._successGAuniversal(), this._successOptimizely()).done(_.bind(function () {
			e.resolve()
		}, this)), window.setTimeout(_.bind(function () {
			e.resolve()
		}, this), 300), e
	}, i.prototype._successGAuniversal = function () {
		var e = $.Deferred(), t = this.config.gaEvents.success;
		return _.isFunction(window._gaUTracker) ? (t.tracker.hitCallback = function () {
			e.resolve()
		}, window._gaUTracker("send", "event", t.tracker), e) : e.resolve()
	}, i.prototype._successGAasync = function () {
		var e = $.Deferred(), t = this.config.gaEvents.success;
		return _gaq.push(["_set", "hitCallback", function () {
			e.resolve()
		}]), _gaq.push(t.gaq), e
	}, i.prototype._successOptimizely = function () {
		var e = $.Deferred();
		return window.setTimeout(function () {
			e.resolve()
		}, 150), window.optimizely = window.optimizely || [], _.isFunction(window.optimizely.push) && window.optimizely.push(["trackEvent", "SignedUp"]), e
	}, i.prototype._successTrekkie = function () {
		"undefined" != typeof window.analytics && window.analytics.track("signup")
	}, i.prototype.sendGA = function (e) {
		var t = this.config.gaEvents;
		e in t && (_gaq.push(t[e].gaq), _.isFunction(window._gaUTracker) && window._gaUTracker("send", "event", t[e].tracker))
	}, i.prototype.bindSubmit = function () {
		this.$form.on("submit.signup", _.bind(this.submit, this))
	}, i.prototype.submit = function (e) {
		e.preventDefault();
		var t = this.$form.serializeJSON();
		this.$submitBtn.prop("disabled", !0), this.API.validate(t).always(_.bind(this.hideErrors, this)).done(_.bind(this.success, this)).fail(_.bind(this.error, this))
	}, i.prototype.validateEmail = function () {
		this.API.formData = this.$form.serializeJSON(), this.API.formData.errors = {}, this.API.validateEmail(), this.hideErrors(), this.API.formData.errors.email && this.error(this.API)
	}, i.prototype.removeErrors = function () {
		this.$form.find(this.SELECTORS.errors).remove()
	}, i.prototype.hideErrors = function () {
		this.$submitBtn && !_.isEmpty(this.API.formData.errors) && this.$submitBtn.prop("disabled", !1), this.$userFields.removeClass(this.CLASSES.error)
	}, i.prototype.success = function () {
		this.sendOnSuccessEvents().done(_.bind(function () {
			this.$form.off("submit.signup"), this.$form.submit()
		}, this))
	}, i.prototype.error = function (e) {
		var t;
		this.removeErrors(), _.each(e.formData.errors, _.bind(function (e, t) {
			var n = this.$form.find("." + t), i = n.find(this.SELECTORS.userFields);
			i.addClass(this.CLASSES.error), n.find(this.SELECTORS.messages).prepend(JST[this.ERROR_TEMPLATE]({
				err: e,
				field: t
			})), this.sendGA("error_" + t), this.trackError(t, e, i.val())
		}, this)), t = _.indexOf(this.$userFields, this.$userFields.filter(".marketing-input--error")[0]), t >= 0 && this.focusAtIndex(t)
	}, i.prototype.trackError = function (e, t, n) {
		var i = "shop_name" === e ? n : "", r = _.keys(t).toString();
		if ("undefined" != typeof window.analytics) {
			var o = {category: "threefield_error", shop_name: i};
			window.analytics.track(e + "_" + r, o)
		}
		_gaq.push(["_trackEvent", "threefield_error", e + "_" + r, i]), _.isFunction(window._gaUTracker) && window._gaUTracker("send", "event", {
			eventCategory: "threefield_error",
			eventAction: e + "_" + r,
			eventLabel: i
		})
	}, i.prototype.track = _.once(function () {
		this._track()
	}), i.prototype._track = function () {
		var e = {};
		_.each(["ref", "source", "signup_code", "ssid"], _.bind(function (t) {
			var n = this.getHiddenField(t).val();
			n && n.trim() && (e[t] = n)
		}, this)), this.API.track(e)
	}, i
}(), ShopifyMarketing.SignupModal = function () {
	var e = ShopifyMarketing.Modal, t = ShopifyMarketing.SignupForm, n = ShopifyMarketing.Drawers, i = ShopifyMarketing.Analytics, r = ShopifyMarketing.Breakpoints, o = "form.js-signup-inline", s = ".drawer--right .signup-form", a = function () {
		e.apply(this, arguments), this.Breakpoints = new r, this.drawer = new n("SignupDrawer", "right"), this.drawerForm = new t($(s)), this.$inlineForm = $(o), this.$inlineForm.on("submit", _.bind(this._onInlineSubmit, this)), enquire.register(this.Breakpoints.breakpoints.tablet, _.bind(function () {
			this.active && e.prototype.close.call(this, function () {
				this.drawer.open()
			})
		}, this))
	};
	return a.prototype = _.clone(e.prototype), a.prototype.open = function (n, i) {
		i = i || {};
		var r;
		if (this.Breakpoints.isDesktop()) {
			var o, s;
			e.prototype.open.call(this, n), this.modalDom.$modal.html(this.template()), o = this.modalDom.$modal.find("form.signup-form"), s = this.SignupForm = new t(o, {
				gaEvents: {
					success: {
						gaq: ["_trackEvent", "threefield", "threefield", "threefield_modal"],
						tracker: {
							eventCategory: "SignUp",
							eventAction: "success",
							eventLabel: "three field modal",
							dimension1: "Lead"
						}
					}
				}
			}), s.$form.prop("id", "SignupForm_modal"), o.on("change focus", ".marketing-input", function () {
				$(this).parents("label").addClass("js-with-label")
			}), r = s
		} else this.drawer.open(n), r = this.drawerForm;
		i.theme && r.setTheme(i.theme), i.populate ? r.populateFromInline(this.$inlineForm) : r.focusAtIndex(0)
	}, a.prototype._onClick = function (e) {
		e.preventDefault();
		var t = {}, n = $(e.currentTarget).data("theme-slug");
		n && (t.theme = n), this.open(e, t)
	}, a.prototype._onInlineSubmit = function (e) {
		e.preventDefault(), i.track("threefield", "submit", "inline form"), this.open(e, {populate: !0}), this.$modalTrigger = $(e.currentTarget).find('input[type="submit"]')
	}, a
}(), ShopifyMarketing.Signup = function () {
	var e = ShopifyMarketing.SignupModal, t = ShopifyMarketing.SignupForm, n = function () {
		this.initForms(), this.initInlineForms()
	};
	return n.prototype.initForms = function () {
		this.SignupForms = this.SignupForms || {}, this.SignupModal = new e($(".js-open-signup"), JST["marketing_assets/templates/signup/modal"], {
			modalActiveContainerClass: "signup-modal js-is-active",
			clickingOverlayClosesModal: !1
		})
	}, n.prototype.initInlineForms = function () {
		var e = $(".signup-form");
		_.each(e, _.bind(function (e, n) {
			$(e).hasClass("js-signup-inited") || (this.SignupForms["form_" + n] = new t($(e)))
		}, this))
	}, n
}(), ShopifyMarketing.Slider = function () {
	var e = function (e, t) {
		t = t || {}, ShopifyMarketing.CarouselBase.apply(this, [e, ".slider__item"], t), this.slideWidth = this.$el.width(), this.containerHeight = $(this.$items[0]).height(), this.ratio = this.slideWidth / this.containerHeight, this.navButtons = {}, this.dots = [], this.$el.wrap('<div class="slider__wrapper"><div class="slider__window"></div></div>'), this.$wrapper = this.$el.parent(), this.setup(), this.buildNav(), this._next = _.throttle(_.bind(this.next, this), 500), this._prev = _.throttle(_.bind(this.prev, this), 500), this._onClick = _.throttle(_.bind(this.onClick, this), 500), this._onKeyDown = _.throttle(_.bind(this.onKeyDown, this), 500), this._onSwipe = _.bind(this.onSwipe, this), this._onResize = _.throttle(_.bind(this.onResize, this), 100), $(this.navButtons.right).on("click", this._next), $(this.navButtons.left).on("click", this._prev), this.$wrapper.on("click", ".slider__pagination__item", this._onClick), this.$wrapper.on("mouseover", _.bind(this.stop, this)), Modernizr.touch && (_.extend(this.$wrapper, ShopifyMarketing.Helpers.SwipeSurface.prototype), this.$wrapper.onSwipe(this._onSwipe)), $(document).on("keydown", this._onKeyDown), $(window).on("resize", this._onResize)
	};
	return e.prototype = new ShopifyMarketing.CarouselBase, e.prototype.setup = function () {
		this.$el.css({paddingTop: 1 / this.ratio * 100 + "%"}), _.each(this.$items, function (e, t) {
			$(e).css({left: this.slideWidth * t})
		}, this)
	}, e.prototype.buildNav = function () {
		var e = document.createDocumentFragment(), t = document.createElement("div");
		t.className = "slider__pagination", _.each(["left", "right"], function (t) {
			this.navButtons[t] = document.createElement("button"), this.navButtons[t].className = "slider__arrow slider__arrow--" + t, this.navButtons[t].innerHTML = t, e.appendChild(this.navButtons[t])
		}, this), _.each(this.$items, function (e, n) {
			this.dots[n] = document.createElement("span"), this.dots[n].className = "slider__pagination__item", this.dots[n].setAttribute("data-target", n), t.appendChild(this.dots[n])
		}, this), e.appendChild(t), this.dots[0].classList.add("js-is-active"), this.$wrapper[0].appendChild(e), this.$el.addClass("js-is-active")
	}, e.prototype.adjustDots = function () {
		_.each(this.dots, function (e) {
			e.classList.remove("js-is-active")
		}, this), this.dots[this.currentIndex].classList.add("js-is-active")
	}, e.prototype.onClick = function (e) {
		var t = e.currentTarget.getAttribute("data-target");
		this.goToIndex(t)
	}, e.prototype.onKeyDown = function (e) {
		e.keyCode === ShopifyMarketing.keyCodes.RIGHT && this.next(), e.keyCode === ShopifyMarketing.keyCodes.LEFT && this.prev()
	}, e.prototype.onSwipe = function (e) {
		this.stop(), "left" === e && this.next(), "right" === e && this.prev()
	}, e.prototype.onResize = function () {
		this.slideWidth = this.$el.width(), this.containerHeight = $(this.$items[0]).height(), this.setup()
	}, e.prototype.goToIndex = function (e) {
		var t = e - this.currentIndex, n = t * this.slideWidth, i = parseInt(this.$el.css("left"), 10) - n;
		this.$el.css("left", i), this.currentIndex = e, this.adjustDots()
	}, e
}(), ShopifyMarketing.SocialSharesCount = function () {
	var e = function (e) {
		this.$el = e, this.$socialSharesCount = this.$el.find(".social-shares__count > span"), this.shareCount = parseInt(this.$socialSharesCount.text(), 10), this.shareCount = window.isNaN(this.shareCount) ? 0 : this.shareCount, this.currentUrl = this.getUrl(), this.urls = {
			tw: "https://cdn.api.twitter.com/1/urls/count.json?url=" + this.currentUrl,
			fb: "https://graph.facebook.com/?id=" + this.currentUrl,
			li: "https://www.linkedin.com/countserv/count/share?url=" + this.currentUrl
		}, this.updateCount = _.bind(this.updateCount, this), $.when.apply(this, this.getDeferreds()).then(this.updateCount)
	};
	return e.prototype.getUrl = function () {
		return this.$el.attr("data-url") ? this.$el.data("url") : $(location).attr("href")
	}, e.prototype.getDeferreds = function () {
		var e, t = this, n = [];
		return _.each(this.urls, function (i, r) {
			n.push($.get(i, function (n) {
				e = "fb" === r ? n.shares ? parseInt(n.shares, 10) : 0 : parseInt(n.count, 10), t.shareCount += e
			}, "jsonp"))
		}), n
	}, e.prototype.updateCount = function () {
		this.$socialSharesCount.html(this.shareCount)
	}, e
}(), ShopifyMarketing.StickyNav = function () {
	var e = ShopifyMarketing.PrettyScrollTo, t = function (e) {
		this.config = {
			$pageChromeTop: $("#ShopifyMainNav, #ShopifySubNav"),
			$container: $("#jsStickyMenuContainer"),
			classFixed: "js-is-sticky-container",
			classAbs: "js-is-abs-container",
			classLinkActive: "js-is-active",
			pageTopMargin: 15
		}, this.options = $.extend(e, this.config), this.init()
	};
	return t.prototype.init = function () {
		this.menuDom = {
			$container: this.options.$container,
			$menu: this.options.$container.find(".sticky-menu"),
			$links: this.options.$container.find(".sticky-menu-link"),
			$waypoints: this.options.$container.find(".js-waypoint")
		}, _.every(this.menuDom, length) && (this.getScrollLimits(), this.prettyScroll = new e({
			offset: 0,
			$selector: $(".sticky-menu-link")
		}, $.proxy(function (e) {
			this.updateActiveLink($(e))
		}, this)), this._isMenuFits() && (this.menuDom.$container.addClass("js-is-sticky-init"), this.bindSticky(), this.bindWaypoints()))
	}, t.prototype.updateActiveLink = function (e) {
		this.menuDom.$links.removeClass(this.options.classLinkActive), e.addClass(this.options.classLinkActive)
	}, t.prototype.getScrollLimits = function () {
		var e = this.menuDom;
		this.scrollLimits = {
			containerHeight: this.config.$container.height(),
			menuTop: e.$container.offset().top,
			menuHeight: e.$menu.height(),
			viewHeight: $.waypoints("viewportHeight")
		}
	}, t.prototype._isMenuFits = function () {
		var e = this.scrollLimits;
		return e.menuHeight < e.viewHeight ? !0 : void 0
	}, t.prototype._getPageOffsetTop = function () {
		return this.scrollLimits.menuTop
	}, t.prototype._getPageOffsetBottom = function () {
		return this.scrollLimits.containerHeight + 60
	}, t.prototype.updateStickyNav = function () {
		var e = this.menuDom.$container, t = this.options.classFixed, n = this.options.classAbs, i = $(window).scrollTop();
		return i > this._getPageOffsetBottom() ? void e.addClass(n) : i > this._getPageOffsetTop() ? void e.addClass(t).removeClass(n) : void e.removeClass(n).removeClass(t)
	}, t.prototype.bindSticky = function () {
		var e = _.throttle($.proxy(function () {
			this.getScrollLimits(), this.updateStickyNav()
		}, this), 100);
		$(window).on("scroll", $.proxy(this.updateStickyNav, this)).on("resize", e).on("load", e)
	}, t.prototype.bindWaypoints = function () {
		_.each(this.menuDom.$waypoints, $.proxy(function (e, t) {
			var n = $(e);
			n.waypoint($.proxy(function () {
				$(this.prettyScroll.options.scrollClass).length || this.updateActiveLink(this.menuDom.$links.eq(t))
			}, this), {offset: "20%"})
		}, this))
	}, t
}(), ShopifyMarketing.TabbedCarousel = function () {
	var e = new ShopifyMarketing.Breakpoints, t = function (e) {
		this.$el = $(e), this.$tabNav = this.$el.find(".tabbed-carousel__nav"), this.$tabNavItems = this.$el.find(".tabbed-carousel__nav-item"), this.$tabItems = this.$el.find(".tabbed-carousel__item"), this.$nextLabel = this.$el.find(".carousel-arrow-right > span"), this.$prevLabel = this.$el.find(".carousel-arrow-left > span"), this.setInitialState = _.bind(this.setInitialState, this), this.removeState = _.bind(this.removeState, this), this.updateState = _.bind(this.updateState, this), this._onKeydown = _.bind(this._onKeydown, this), this.enable()
	};
	return t.prototype.enable = function () {
		this.accordion = new ShopifyMarketing.Accordion(this.$el), enquire.register([e.tablet, e.desktop].join(","), this.setInitialState), enquire.register(e.phone, this.removeState)
	}, t.prototype.setInitialState = function () {
		this.carousel = new ShopifyMarketing.Carousel(this.$el), this.$tabNav.attr("role", "tablist");
		for (var e = 0; e < this.carousel.itemsCount; e++) {
			var t = _.uniqueId("TabbedCarousel");
			this.$tabNavItems.eq(e).attr({
				"aria-controls": t,
				role: "tab",
				tabindex: e === this.carousel.currentIndex ? "0" : "-1"
			}), this.$tabItems.eq(e).attr({id: t, role: "tabpanel", tabindex: "0"})
		}
		this.$el.on("change", this.updateState), this.$tabNavItems.on("keydown", this._onKeydown), this.updateState()
	}, t.prototype.removeState = function () {
		this.$tabNav.removeAttr("role"), this.$tabNavItems.removeAttr("aria-controls aria-selected role"), this.$tabItems.removeAttr("aria-hidden id role")
	}, t.prototype.updateState = function () {
		this.$nextLabel.text($.trim(this.carousel.$navItems.eq(this.carousel.nextIndex).text())), this.$prevLabel.text($.trim(this.carousel.$navItems.eq(this.carousel.prevIndex).text())), this.$tabNavItems.attr({
			"aria-selected": "false",
			tabindex: "-1"
		}).eq(this.carousel.currentIndex).attr({
			"aria-selected": "true",
			tabindex: "0"
		}), this.$tabItems.attr("aria-hidden", "true").eq(this.carousel.currentIndex).attr("aria-hidden", "false")
	}, t.prototype._onKeydown = function (e) {
		var t, n = ShopifyMarketing.keyCodes;
		switch (e.keyCode) {
			case n.UP:
			case n.LEFT:
				t = this.carousel.prevIndex;
				break;
			case n.DOWN:
			case n.RIGHT:
				t = this.carousel.nextIndex;
				break;
			case n.HOME:
				t = 0;
				break;
			case n.END:
				t = this.carousel.itemsCount - 1
		}
		return void 0 !== t ? (e.preventDefault(), this.$tabNavItems.eq(t).trigger("click").trigger("focus")) : void 0
	}, t
}(), ShopifyMarketing.TruncatableText = function () {
	var e = function (e, t) {
		this.$wrapper = e, this.$trigger = t, this.$wrapper.length && this.$trigger.on("click", _.bind(this.showText, this))
	};
	return e.prototype.showText = function () {
		this.$wrapper.addClass("js-is-active")
	}, e
}(), ShopifyMarketing.Video = function () {
	var e = function (e) {
		this.$video = e, this.$video.length && this.init()
	};
	return e.prototype.init = function () {
		if (Modernizr.touch || !Modernizr.video) {
			var e = new Image, t = ["js-is-active"].concat(this.$video[0].className.split(/\s+/));
			t.push(_.indexOf(t, "inline-video") > -1 ? "inline-video--fallback" : "background-video--fallback"), e.setAttribute("src", this.$video.attr("data-poster")), e.setAttribute("alt", this.$video.attr("aria-label")), e.className = t.join(" "), this.$video.after(e), this.$video.remove()
		} else this.$video.find('[type="video/webm"]').attr("src", this.$video.data("webm-src")), this.$video.find('[type="video/mp4"]').attr("src", this.$video.data("mp4-src")), this.$video.on("loadeddata", _.bind(this.videoReady, this)), this.$video.get(0).load()
	}, e.prototype.videoReady = function () {
		this.$video.addClass("js-is-active")
	}, e
}(), function () {
	var e;
	$(window).load(function () {
		e = $(document.body), e.addClass("js-is-loaded")
	}), setTimeout(function () {
		e = e || $(document.body), e.hasClass("js-is-loaded") || e.addClass("js-is-loaded")
	}, 1e3)
}(), function () {
	var e = [], t = {};
	if (window.optimizely = window.optimizely || [], window.optimizely.data) {
		var n = window.optimizely.data.state.variationIdsMap;
		e.forEach(function (e) {
			window.optimizely.push(["activate", e]);
			var i = t[n[e]];
			_.isFunction(i) && i()
		})
	}
}();
var cookie = ShopifyMarketing.Helpers.Cookie, QueryString = ShopifyMarketing.Helpers.QueryString, URLHelper = ShopifyMarketing.Helpers.URL;
window.ShopifyMarketing = window.ShopifyMarketing || {}, function (e) {
	var t = ShopifyMarketing.Config, n = ShopifyMarketing.PrettyScrollTo, i = ShopifyMarketing.StickyNav, r = ShopifyMarketing.EqualHeight, o = ShopifyMarketing.Breakpoints, s = ShopifyMarketing.Video, a = ShopifyMarketing.Carousel, u = $("#ShopifyMainNav"), l = {
		activeModal: null,
		init: function () {
			this.initRodeo(), this.initCountrySelect(), this.config.signupHost && t.set("SignupBaseURI", "https://" + this.config.signupHost), ShopifyMarketing.init(), e.breakpoints = new o(this.breakpoints), e.prettyScrollTo = new n({
				offset: -u.height(),
				$selector: $(".link-scroll-to")
			}, ShopifyMarketing.A11yHelpers.prototype.pageLinkFocus), new s($(".background-video")), new i, this.initTestimonials(), this.initOptimizely()
		},
		initCountrySelect: function () {
			var e = $(document.getElementById("CountrySelect"));
			e.on("change", function (e) {
				e.preventDefault(), window.location.href = $(e.currentTarget).val()
			})
		},
		initTestimonials: function () {
			var e = $(".testimonial-items");
			if (e.length) {
				_.each($(".testimonial-items"), function (e) {
					var t = $(e);
					new r(t, t.children(".testimonial-item"))
				});
				var t = new a($(".testimonial-items"), {duration: 8e3});
				t.start()
			}
		},
		initOptimizely: function () {
			var e = cookie.get("optimizelyEndUserId"), t = cookie.get("optimizelyBuckets");
			e && "" !== e && t && "" !== t && (_visit.keyValue("optimizelyEndUserId", e), _visit.keyValue("optimizelyBuckets", t))
		},
		initRodeo: function () {
			var e = QueryString.parse(URLHelper.querystring(window.location.href)), t = {
				id: {
					domain: "www.shopify.co.id",
					name: "Indonesia"
				},
				"in": {domain: "www.shopify.in", name: "India"},
				my: {domain: "www.shopify.my", name: "Malaysia"},
				sg: {domain: "www.shopify.co.sg", name: "Singapore"}
			}, n = e.sh_rd;
			if (n && t[n]) {
				var i = new Date;
				i.setDate(i.getDate() + 7), cookie.set("noredirect", n, {expires: i})
			}
		}
	};
	$.extend(e, l), $("body").trigger("optimizely", {variation: 2871320712})
}(window.App = window.App || {});