var hn = /#.*$/, fn = /([?&])_=[^&]*/, dn = /^(.*?):[ \t]*([^\r\n]*)$/gm, mn = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, gn = /^(?:GET|HEAD)$/, vn = /^\/\//, yn = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, bn = {}, wn = {}, _n = "*/".concat("*"), xn = window.location.href, kn = yn.exec(xn.toLowerCase()) || [];

$.ajaxSetup({
	url: xn,
	type: "GET",
	isLocal: mn.test(kn[1]),
	global: !0,
	processData: !0,
	async: !0,
	contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	accepts: {
		"*": _n,
		text: "text/plain",
		html: "text/html",
		xml: "application/xml, text/xml",
		json: "application/json, text/javascript"
	},
	contents: {xml: /xml/, html: /html/, json: /json/},
	responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
	converters: {"* text": String, "text html": !0, "text json": $.parseJSON, "text xml": $.parseXML},
	flatOptions: {url: !0, context: !0},
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});


if (!Array.prototype.find) {
	Array.prototype.find = function(predicate) {
		if (this == null) {
			throw new TypeError('Array.prototype.find called on null or undefined');
		}
		if (typeof predicate !== 'function') {
			throw new TypeError('predicate must be a function');
		}
		var list = Object(this);
		var length = list.length >>> 0;
		var thisArg = arguments[1];
		var value;

		for (var i = 0; i < length; i++) {
			value = list[i];
			if (predicate.call(thisArg, value, i, list)) {
				return value;
			}
		}
		return undefined;
	};
}

var Application = new function () {
	this.init = function () {
		return this.setup();
	};
	this.setup = function () {

	};

};

Application.init();
var UI = new function () {
	this.theme = 'default';
	this.init = function () {
		return this.setup();
	};
	this.setup = function () {
		$('body').addClass('theme-' + this.theme);

		return this.tabbedCarousel();
	};
	this.tabbedCarousel = function () {
		var element = $('.carousel-with-tabs'),
			tabs = element.find('nav .carousel-tab'),
			items = element.find('.carousel-items'),
			prev = element.find('.prev'),
			next = element.find('.next'),
			firstItem = items.find('li:first'),
			lastItem = items.find('li:last');

		setNavLabels();

		$(tabs).on('click', function (event) {
			event.preventDefault();

			var target = $(this).data('target');

			tabs.removeClass('active');
			$(this).addClass('active');

			items.find('li').removeClass('active');
			$(target).addClass('active');
		});

		$(next).on('click', function (event) {
			event.preventDefault();

			var activeItem = items.find('li.active'),
				activeTab = $('a[data-target="#' + activeItem.attr('id') + '"'),
				nextTab = activeTab.next('a'),
				nextItem = activeItem.next('li');

			activeItem.removeClass('active');
			tabs.removeClass('active');

			if (nextItem.length) {
				nextTab.addClass('active');
				nextItem.addClass('active');
			}
			else {
				tabs.first().addClass('active');
				firstItem.addClass('active');
			}

			setNavLabels();
		});

		$(prev).on('click', function (event) {
			event.preventDefault();

			var activeItem = items.find('li.active'),
				activeTab = $('a[data-target="#' + activeItem.attr('id') + '"'),
				prevTab = activeTab.prev('a'),
				prevItem = activeItem.prev('li');

			tabs.removeClass('active');
			activeItem.removeClass('active');

			if (prevItem.length) {
				prevTab.addClass('active');
				prevItem.addClass('active');
			}
			else {
				tabs.last().addClass('active');
				lastItem.addClass('active');
			}

			setNavLabels();
		});

		function setNavLabels() {
			var activeItem = items.find('li.active'),
				activeTab = $('a[data-target="#' + activeItem.attr('id') + '"'),
				prevTab = activeTab.prev('a'),
				nextTab = activeTab.next();

			if (prevTab.length) {
				$(prev).find('span').text(prevTab.find('strong').text());
			}
			else {
				$(prev).find('span').text(tabs.last().find('strong').text());
			}

			if (nextTab.length) {
				$(next).find('span').text(nextTab.find('strong').text());
			}
			else {
				$(next).find('span').text(tabs.first().find('strong').text());
			}
		}
	}
	this.form = function (form) {
		var form = form,
			initialValues = getInputValues(),
			submitBtn = form.find('button[type="submit"]');

		updateDirtyState(matchEnabledValues(getInputValues(), initialValues));

		form.off("bindings:change");
		form.on("input change paste", function () {
			updateDirtyState(matchEnabledValues(getInputValues(), initialValues));
		});

		form.on('submit', function (event) {
			event.preventDefault();

			var form = $(this),
				originalBtn = submitBtn.html(),
				btnWidth = submitBtn.width();

			submitBtn.width(btnWidth).html('<i class="fa fa-spinner fa-spin"></i>');

			// Track request with Pace
			Pace.track(function () {
				$.ajax({
					type: "POST",
					url: form.attr('action'),
					dataType: 'json',
					data: form.serialize()
				}).always(function () {
					submitBtn.html(originalBtn);
					initialValues = getInputValues();
					updateDirtyState(matchEnabledValues(getInputValues(), initialValues));
				}).done(function (data, textStatus, jqXHR) {
					if (typeof data.url !== 'undefined') {
						window.location = data.url;
					}
				}).fail(function (jqXHR, textStatus, errorThrown) {

				});
			});
		});

		function getInputValues() {
			if (typeof form === 'undefined') {
				return null
			}

			var t = form.find("input, select, textarea").not(".js-no-dirty");

			return _.map(t, function (t) {
				var e;
				return "checkbox" === (e = t.type) || "radio" === e ? {
					name: t.name,
					value: t.checked,
					disabled: t.disabled,
					type: t.type
				} : {name: t.name, value: t.value, disabled: t.disabled, type: t.type}

			});
		}

		function updateDirtyState(dirty) {
			if (dirty) {
				submitBtn.attr('disabled', 'disabled').removeClass('btn-success').addClass('btn-default disabled');
			}
			else {
				submitBtn.removeAttr('disabled').removeClass('btn-default disabled').addClass('btn-success');
			}
		}

		function matchEnabledValues(t, e) {
			return _.size(t) !== _.size(e) ? !1 : _(t).zip(e).every(function (t) {
				var e, n;

				return n = t[0], e = t[1], n.disabled || n.value === e.value
			});
		}
	};
};

UI.init();

//# sourceMappingURL=app.js.map