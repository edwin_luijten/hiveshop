<?php namespace Shop\Domain\Entity\Subscription;

use Shop\Domain\Entity\Plan\Plan;
use Src\Doctrine\Traits\Timestamps;

class Subscription {

    use Timestamps;

    /**
     * @var SubscriptionId
     */
    private $id;
    /**
     * @var \Datetime
     */
    private $startDate;
    /**
     * @var \Datetime
     */
    private $endDate;
    /**
     * @var string
     */
    private $period;
    /**
     * @var \Shop\Domain\Entity\plan\Plan
     */
    private $plan;

    /**
     * @return SubscriptionId
     */
    public function getId()
    {
        return SubscriptionId::fromString($this->id);
    }

    /**
     * @param SubscriptionId $id
     */
    public function setId(SubscriptionId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return \Datetime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param \Datetime $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return \Datetime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \Datetime $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param string $period
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    }

    /**
     * @return \Shop\Domain\Entity\plan\Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param \Shop\Domain\Entity\plan\Plan $plan
     */
    public function setPlan(Plan $plan)
    {
        $this->plan = $plan;
    }
}
