@extends('hiveshop.template')

@section('content')
<section>
    <div id="header-overview">
        <div class="container">
            <h1>Use Hive to create your online store</h1>
            <img src="/assets/h/img/header-overview@1x.png" />
        </div>
    </div>

    <!-- Form -->
    <div class="container">
        <h2 class="text-center">Start your 14 day trial!</h2>

        <div class="row">
            <form role="form" data-toggle="validator" method="post" action="{{ URL::route('services.signup.setup') }}" class="marketing-form" id="user-signup">
                <div class="col-xs-12 col-md-2 col-md-offset-2 form-group">
                    <input type="email" name="email" class="form-control" placeholder="Your email" required="required" data-remote="/services/signup/check_availability.json" data-error="Email already used">
                    <small class="help-block with-errors"></small>
                </div>
                <div class="col-xs-12 col-md-2 form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" data-minlength="6" required="required">
                    <small class="help-block with-errors"></small>
                </div>
                <div class="col-xs-12 col-md-2 form-group">
                    <input type="text" name="store-name"class="form-control" placeholder="Store name" required="required" data-remote="/services/signup/check_availability.json" data-error="Store name already taken">
                    <small class="help-block with-errors"></small>
                </div>
                <div class="col-xs-12 col-md-2 form-group">
                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-success btn-large btn-block" name="register">Create your store</button>
                </div>
            </form>
        </div>
    </div>
    <!-- end form -->
    <div class="container page-section">
         <header class="text-center">
             <h2>Your ecommerce website</h2>
             <span>Impress your customers with a beautiful online store and a secure shopping cart</span>
         </header>

         <div class="carousel-with-tabs">
            <nav>
                <a href="#" class="col-md-4 carousel-tab active" data-target="#tab-storefront">
                    <i class="icon-store"></i>
                    <strong>Storefront</strong>
                </a>

                <a href="#" class="col-md-4 carousel-tab" data-target="#tab-products">
                    <i class="icon-diamond"></i>
                    <strong>Products</strong>
                </a>

                <a href="#" class="col-md-4 carousel-tab" data-target="#tab-orders">
                    <i class="icon-clipboard"></i>
                    <strong>Orders</strong>
                </a>
            </nav>

            <a href="" class="prev"><span>Orders</span></a>
            <a href="" class="next"><span>Products</span></a>

            <ul class="carousel-items">
                <li class="row active" id="tab-storefront">
                    <div class="col-md-4 pull-left animated slideInUp">
                        <h3>A home for your brand</h3>
                        <h4>Customize your store</h4>

                        <p class="animated fadeInUp">
                            You have complete control over the look and feel of your website, from its domain name to its layout, colors and content.
                        </p>

                        <h4>No design skills needed</h4>

                        <p class="animated fadeInUp">
                            Choose from over 100 professional store templates, or build your own design using HTML and CSS.
                        </p>
                    </div>

                    <picture class="carousel-image">
                        <!--[if IE 9]><video style="display: none;"><![endif]-->
                        <!--[if IE 9]></video><![endif]-->
                        <img src="/assets/h/img/laptop-storefront@2x.jpg" alt="" class="pull-left animated fast fadeInRight">
                    </picture>
                </li>
                <li class="row" id="tab-products">
                    <picture class="carousel-image">
                        <!--[if IE 9]><video style="display: none;"><![endif]-->
                        <!--[if IE 9]></video><![endif]-->
                        <img src="/assets/h/img/laptop-products@2x.jpg" alt="" class="animated fast fadeInLeft">
                    </picture>

                    <div class="col-md-4 carousel-content pull-right animated slideInUp">
                        <h3>Products</h3>
                        <h4>Showcase your products</h4>

                        <p class="animated fadeInUp">
                            Your products are displayed in an online catalog that you can organize by category, so they're easy for customers to browse and always look their best.
                        </p>

                        <h4>Sell one product or millions</h4>

                        <p class="animated fadeInUp">
                            Hiveshop fits your business, whether you're a mom-and-pop shop, a weekend crafter, or a big brand name. There's no limit to the number or type of products you can sell in your online store.
                        </p>
                     </div>
                </li>
                <li class="row" id="tab-orders">
                    <div class="col-md-4 pull-left animated slideInUp">
                        <h3>A seamless checkout experience</h3>
                        <h4>Accept payments instantly</h4>

                        <p class="animated fadeInUp">
                            From Bitcoin to PayPal to iDEAL, we integrate with over 70 external payment gateways from around the world.
                        </p>

                        <h4>Your data is safe and secure</h4>

                        <p class="animated fadeInUp">
                            All credit card and transaction information is protected by the same level of security used by banks. Hiveshop is certified Level 1 PCI compliant.
                        </p>
                    </div>

                    <picture class="carousel-image">
                        <!--[if IE 9]><video style="display: none;"><![endif]-->
                        <!--[if IE 9]></video><![endif]-->
                        <img src="/assets/h/img/laptop-checkout@2x.jpg" alt="" class="animated fast fadeInRight">
                    </picture>
                </li>
            </ul>
         </div>
    </div>

</section>
@endsection