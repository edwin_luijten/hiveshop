<?php namespace App\Console\Commands;

use Shop\Domain\Services\Seeds\TenantSeeder;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputTenant;
use Symfony\Component\Stopwatch\Stopwatch;

class TenantsSeederCommand extends BaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'seeder:tenants';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the tenants table with 10 tenants each time this command is run.';

    /**
     * The Entity Manager
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $tenantSeeder;

    public function __construct(TenantSeeder $tenantSeeder)
    {
        parent::__construct();

        $this->tenantSeeder = $tenantSeeder;
    }

    public function fire()
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('tenantSeeding');

        $this->info('Seeding tenants');

        $this->tenantSeeder->seed();

        $event = $stopwatch->stop('tenantSeeding');

        $this->info('Done.');
        $this->info(
            PHP_EOL . 'Stopwatch: ' . PHP_EOL .
            'Duration: ' . $this->formatDuration($event->getDuration()) . PHP_EOL .
            'Memory used: ' . $this->formatMemory($event->getMemory())
        );
    }

    protected function getArguments()
    {
        return [

        ];
    }

    protected function getTenants()
    {
        return [

        ];
    }
}