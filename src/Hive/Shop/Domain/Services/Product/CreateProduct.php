<?php namespace Shop\Domain\Services\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Product\OptionRepositoryInterface;
use Shop\Domain\Entity\Product\OptionValue;
use Shop\Domain\Entity\Product\OptionValueRepositoryInterface;
use Shop\Domain\Entity\Product\Product;
use Shop\Domain\Entity\Product\ProductRepositoryInterface;
use Shop\Domain\Entity\Product\Variant;
use Shop\Domain\Entity\Product\VariantRepositoryInterface;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;
use Shop\Domain\Services\Traits\StoreAwareService;

class CreateProduct extends AbstractCreatorService implements ServiceInterface
{
    use StoreAwareService;

    private $entityManager;
    private $productRepository;
    private $variantRepository;
    private $optionRepository;
    private $optionValueRepository;

    private $variantGenerator;

    protected $product;

    public function __construct(
        EntityManager $entityManager,
        ProductRepositoryInterface $productRepository,
        VariantRepositoryInterface $variantRepository,
        OptionRepositoryInterface $optionRepository,
        OptionValueRepositoryInterface $optionValueRepository,
        VariantGenerator $variantGenerator
    ) {
        $this->entityManager = $entityManager;
        $this->productRepository = $productRepository;
        $this->variantRepository = $variantRepository;
        $this->optionRepository = $optionRepository;
        $this->optionValueRepository = $optionValueRepository;

        $this->variantGenerator = $variantGenerator;
    }

    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param string  $name
     * @param integer $price
     * @param string  $description
     * @param string  $availableOn
     * @param string  $metaDescription
     * @param string  $metaKeywords
     * @param array   $options
     * @param bool    $createVariants
     *
     * @return \Shop\Domain\Entity\Product\Product
     */
    public function create(
        $name,
        $price,
        $description,
        $availableOn,
        $metaDescription,
        $metaKeywords,
        $options = [],
        $createVariants = false
    ) {
        $availableOn = new \DateTime($availableOn);

        $id = $this->productRepository->nextUuid();

        $product = new Product();
        $product->setId($id);
        $product->setName($name);
        $product->setSlug(str_replace(' ', '-', $name));
        $product->setDescription($description);
        $product->setAvailableOn($availableOn);
        $product->setMetaDescription($metaDescription);
        $product->setMetaKeywords($metaKeywords);
        $product->setStore($this->store);

        if (! empty($options)) {
            foreach ($options as $optKey => $opt) {
                $option = $this->optionRepository->getById($opt['id']);

                if (! empty($opt['values']) && ! empty($option)) {
                    // Start fresh
                    $option->setValues(new ArrayCollection());

                    foreach ($opt['values'] as $valKey => $value) {
                        $optionValue = new OptionValue();

                        $id = $this->optionValueRepository->nextUuid();
                        $optionValue->setId($id);
                        $optionValue->setValue($value);
                        $optionValue->setOption($option);

                        // Attach value to option
                        $option->addValue($optionValue);

                        // Attach value to product
                        $product->addOptionValue($optionValue);
                    }
                }

                // Attach option to product
                $product->addOption($option);
            }
        }

        if ($createVariants === true) {
            $this->variantGenerator->generate($product);

            if ($product->hasVariants()) {
                $product->setHasVariants(true);

                foreach ($product->getVariants() as $variant) {
                    $id = $this->variantRepository->nextUuid();

                    $variant->setId($id);
                    $variant->setPrice($price);
                    $variant->setStore($this->store);
                }
            }
        } else {
            $masterVariant = new Variant();
            $id = $this->variantRepository->nextUuid();

            $masterVariant->setId($id);
            $masterVariant->setPrice($price);
            $masterVariant->setIsMaster(true);
            $masterVariant->setStore($this->store);
            $masterVariant->setProduct($product);

            $product->addVariant($masterVariant);
        }

        $this->product = $product;

        return $this;
    }

    public function run()
    {
        $product = $this->getProduct();

        // Store the store
        $this->entityManager->persist($product);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getProduct();
    }
}
