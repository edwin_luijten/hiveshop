<?php namespace Shop\Domain\Entity\Geo;

use Doctrine\Common\Collections\ArrayCollection;
use Shop\Domain\AggregateRoot;
use Shop\Domain\Identifier;

class Country implements AggregateRoot
{
    /**
     * @var CountryId
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $code;
    /**
     * @var float
     */
    private $tax;
    /**
     * @var string
     */
    private $taxName;
    /**
     * @var string
     */
    private $currency;
    /**
     * @var string
     */
    private $unitSystem;
    /**
     * @var string
     */
    private $zoneLabel;
    /**
     * @var string
     */
    private $zoneKey;
    /**
     * @var string
     */
    private $group;
    /**
     * @var string
     */
    private $exampleZip;
    /**
     * @var boolean
     */
    private $enabled;
    /**
     * @var ArrayCollection
     */
    private $zones;

    public function __construct()
    {
        $this->zones = new ArrayCollection();
    }

    /**
     * Return the Aggregate Root identifer
     *
     * @return Identifier
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getExampleZip()
    {
        return $this->exampleZip;
    }

    /**
     * @param string $exampleZip
     */
    public function setExampleZip($exampleZip)
    {
        $this->exampleZip = $exampleZip;
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param string $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return decimal
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param decimal $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return string
     */
    public function getTaxName()
    {
        return $this->taxName;
    }

    /**
     * @param string $taxName
     */
    public function setTaxName($taxName)
    {
        $this->taxName = $taxName;
    }

    /**
     * @return string
     */
    public function getUnitSystem()
    {
        return $this->unitSystem;
    }

    /**
     * @param string $unitSystem
     */
    public function setUnitSystem($unitSystem)
    {
        $this->unitSystem = $unitSystem;
    }

    /**
     * @return string
     */
    public function getZoneKey()
    {
        return $this->zoneKey;
    }

    /**
     * @param string $zoneKey
     */
    public function setZoneKey($zoneKey)
    {
        $this->zoneKey = $zoneKey;
    }

    /**
     * @return string
     */
    public function getZoneLabel()
    {
        return $this->zoneLabel;
    }

    /**
     * @param string $zoneLabel
     */
    public function setZoneLabel($zoneLabel)
    {
        $this->zoneLabel = $zoneLabel;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return ArrayCollection
     */
    public function getZones()
    {
        return $this->zones;
    }

    /**
     * @param Zone $zone
     */
    public function addZone(Zone $zone)
    {
        if (!$this->hasZone($zone)) {
            $this->zones->add($zone);
        }
    }

    /**
     * @param Zone $zone
     * @return bool
     */
    public function hasZone(Zone $zone)
    {
        return $this->zones->contains($zone);
    }
}
