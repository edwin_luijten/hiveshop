<?php namespace Shop\Domain\Services\Geo;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Geo\AddressId;
use Shop\Domain\Entity\Geo\AddressRepositoryInterface;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;

class UpdateAddress extends AbstractCreatorService implements ServiceInterface
{
    private $entityManager;
    private $addressRepository;
    private $countryRepository;
    protected $address;

    public function __construct(
        EntityManager $entityManager,
        AddressRepositoryInterface $addressRepository,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->entityManager = $entityManager;
        $this->addressRepository = $addressRepository;
        $this->countryRepository = $countryRepository;
    }

    public function getAddress()
    {
        return $this->address;
    }

    /**
     *
     * @param AddressId  $addressId
     * @param null $phone
     * @param null $street
     * @param null $country
     * @param null $legalName
     * @param null $city
     * @param null $postal
     *
     * @return \Shop\Domain\Entity\Geo\Address
     */
    public function update(
        AddressId $addressId,
        $phone = null,
        $street = null,
        $country = null,
        $legalName = null,
        $city = null,
        $postal = null
    ) {
        $address = $this->addressRepository->getById($addressId->toString());
        if (!is_null($country)) {
            $address->setCountry($this->countryRepository->getByCode($country));
        }

        $address->setLegalName($legalName);
        $address->setPhone($phone);
        $address->setStreet($street);
        $address->setCity($city);
        $address->setPostal($postal);


        $this->address = $address;

        return $this;
    }

    public function run()
    {
        $address = $this->getAddress();

        // Store the store
        $this->entityManager->merge($address);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getAddress();
    }
}
