<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\MessageBag;

class ValidationErrors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            return $next($request);
        } catch (ValidationException $e) {
            $messages = (array) json_decode($e->getMessage());

            $errors = new MessageBag($messages);

            return redirect()->back()->withInput()->withErrors($errors);
        }
    }
}
