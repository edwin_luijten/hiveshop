@extends('shop.admin.template')

@section('page-breadcrumb')
    <ul>
        <li><i class="fa fa-cog"></i> {{ _('Settings') }} / {{ _('Account') }} / </li>
        <li class="action">{{ $account->getFullName() }}</li>
    </ul>
@endsection

@section('content')
    <form id="account-profile" data-toggle="validator" action="{{ URL::route('admin.settings.account.profile', ['subdomain' => $store->getSubdomain(), 'id' => $account->getId()->toString()]) }}" method="post">
    <header class="navbar navbar-fixed-top navbar-default">
        <ul class="nav navbar-nav-custom">
            <li class="hidden-xs animation-fadeInQuick navbar-breadcrumb">
                @yield('page-breadcrumb')
            </li>
        </ul>
        <ul class="nav navbar-nav-custom pull-right">
            <li><button type="submit" class="btn btn-default btn-effect-ripple disabled" disabled="disabled">{{ _('Save') }}</button></li>
        </ul>
        <div id="progress">

        </div>
    </header>
    <div id="page-content">
        <div class="row section">
            <div class="col-md-3">
                <h3>{{ _('Account information') }}</h3>

                <p class="text-muted"><small>{{ _('All information relevant to the account.') }}</small></p>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-section">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="first-name">{{ _('First name') }}</label>
                                    <input type="text" name="first-name" value="{{ $account->getFirstName() }}" class="form-control" id="first-name" required="required" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="last-name">{{ _('Last name') }}</label>
                                    <input type="text" name="last-name" value="{{ $account->getLastName() }}" class="form-control" id="last-name" required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">{{ _('Email address') }}</label>
                                    <div class="enableStaticInput">
                                        {{ $account->getEmail() }} <a href="javascript:void(0)" data-target="#email">{{ _('Change email') }}</a>
                                    </div>
                                    <input type="text" name="email" value="{{ $account->getEmail() }}" class="form-control hidden" id="email"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="phone">{{ _('Phone') }} ({{ _('optional') }})</label>
                                    <input type="text" name="phone" value="" class="form-control" id="phone" />
                                </div>
                            </div>
                        </div>
                        <div class="row enableStaticInput">
                            <div class="col-md-12">
                                <a href="javascript:void(0)" data-target="#passwords">{{ _('Change password') }}</a>
                            </div>
                        </div>
                        <div class="row hidden" id="passwords">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password">{{ _('New password') }}</label>
                                    <input type="password" name="password" class="form-control" id="password"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="confirm-password">{{ _('Confirm new password') }}</label>
                                    <input type="text" name="confirm-password" class="form-control" id="confirm-password" />
                                </div>
                            </div>
                        </div>
                        <div class="spacing"></div>
                        <!-- @todo implement -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label for="example-radio1">
                                        <input type="checkbox" id="example-radio1" name="example-radios" value="option1"> <strong>{{ _('Please keep me up to date about important developments by email.') }}</strong>
                                    </label>
                                    <span class="help-block">
                                        {{ _('We periodically send out important news about Hiveshop to our users via email. We keep the email volume to an absolute minimum.') }}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row section">
            <div class="col-md-3">
                <h3>{{ _('Recent login history') }}</h3>

                <p class="text-muted"><small>{{ _('Access from an ISP, Location, or IP address you don\'t recognize may indicate that the account has been compromised and should be reviewed further.') }}</small></p>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-section">
                        <table class="table">
                            <thead>
                                <td>{{ _('Date') }}</td>
                                <td>{{ _('IP') }}</td>
                                <td>{{ _('ISP') }}</td>
                                <td>{{ _('Location') }}</td>
                                <td></td>
                            </thead>
                            <tbody>
                                @foreach($authHistory as $history)
                                <tr>
                                    <td>{{ date_for_human($history->getCreatedAt()->format('Y-m-d H:i:s'), 3)}}</td>
                                    <td>{{ $history->getIP() }}</td>
                                    <td>{{ $history->getISP() }}</td>
                                    <td>{{ $history->getLocation() }}</td>
                                    <td><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="{{ $history->getBrowser() }} {{ $history->getOs() }}"></i></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <footer class="row">
            <div class="col-md-12 text-right">
                {!! csrf_field() !!}
                <button type="submit" class="btn btn-default btn-effect-ripple disabled" disabled="disabled">{{ _('Save') }}</button>
            </div>
        </footer>

    </div>

    </form>
@endsection

@section('script')
<script type="text/javascript">
    UI.form($('#account-profile'));
</script>
@endsection