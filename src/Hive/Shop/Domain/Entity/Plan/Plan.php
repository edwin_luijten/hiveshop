<?php namespace Shop\Domain\Entity\Plan;

use Doctrine\Common\Collections\ArrayCollection;
use Shop\Domain\ValueObjects\Price;

class Plan
{
    /**
     * @var integer
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $slug;
    /**
     * @var
     */
    private $description;
    /**
     * @var integer
     */
    private $price;
    /**
     * @var bool
     */
    private $highlight;
    /**
     * @var decimal
     */
    private $fee;
    /**
     * @var decimal
     */
    private $onlineFeePercentage;
    /**
     * @var integer
     */
    private $onlineFeePrice;
    /**
     * @var decimal
     */
    private $inPersonFeePercentage;
    /**
     * @var integer
     */
    private $inPersonFeePrice;
    /**
     * @var ArrayCollection
     */
    private $features;

    public function __construct()
    {
        $this->features = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getFeatures()
    {
        return $this->features;
    }

    /**
     * @param ArrayCollection $features
     */
    public function setFeatures($features)
    {
        $this->features = $features;
    }

    /**
     * @param \Shop\Domain\Entity\Plan\Feature $feature
     *
     * @return bool
     */
    public function hasFeature(Feature $feature)
    {
        return $this->features->contains($feature);
    }

    /**
     * @param \Shop\Domain\Entity\Plan\Feature $feature
     */
    public function addFeature(Feature $feature)
    {
        if (!$this->hasFeature($feature)) {
            $this->features->add($feature);
        }
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return (!is_null($this->price) ? new Price($this->price) : null);
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return boolean
     */
    public function getHighlight()
    {
        return $this->highlight;
    }

    /**
     * @param boolean $highlight
     */
    public function setHighlight($highlight)
    {
        $this->highlight = $highlight;
    }

    /**
     * @return decimal
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * @param decimal $fee
     */
    public function setFee($fee)
    {
        $this->fee = $fee;
    }

    /**
     * @return decimal
     */
    public function getOnlineFeePercentage()
    {
        return $this->onlineFeePercentage;
    }

    /**
     * @param decimal $onlineFeePercentage
     */
    public function setOnlineFeePercentage($onlineFeePercentage)
    {
        $this->onlineFeePercentage = $onlineFeePercentage;
    }

    /**
     * @return int
     */
    public function getOnlineFeePrice()
    {
        return (!is_null($this->onlineFeePrice) ? new Price($this->onlineFeePrice) : null);
    }

    /**
     * @param int $onlineFeePrice
     */
    public function setOnlineFeePrice($onlineFeePrice)
    {
        $this->onlineFeePrice = $onlineFeePrice;
    }

    /**
     * @return decimal
     */
    public function getInPersonFeePercentage()
    {
        return $this->inPersonFeePercentage;
    }

    /**
     * @param decimal $inPersonFeePercentage
     */
    public function setInPersonFeePercentage($inPersonFeePercentage)
    {
        $this->inPersonFeePercentage = $inPersonFeePercentage;
    }

    /**
     * @return int
     */
    public function getInPersonFeePrice()
    {
        return (!is_null($this->inPersonFeePrice) ? new Price($this->inPersonFeePrice) : null);
    }

    /**
     * @param int $inPersonFeePrice
     */
    public function setInPersonFeePrice($inPersonFeePrice)
    {
        $this->inPersonFeePrice = $inPersonFeePrice;
    }
}
