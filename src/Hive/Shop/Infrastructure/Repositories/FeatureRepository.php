<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\Plan\Feature;
use Shop\Domain\Entity\Plan\FeatureRepositoryInterface;

class FeatureRepository extends AbstractBaseRepository implements FeatureRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        // TODO: Implement nextUuid() method.
    }

    public function getEntity()
    {
        return new Feature();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('f')->from(Feature::class, 'f')
            ->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        //@todo implement getPaginated
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('f')->from(Feature::class, 'f')
            ->where(
                $queryBuilder->expr()->eq('a.id', ':id')
            )
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    public function getBySlug($slug)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('f')->from(Feature::class, 'f')
            ->where(
                $queryBuilder->expr()->eq('f.slug', ':slug')
            )
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleResult();
    }

    public function getCount()
    {
        //@todo implement getCount()
    }

}
