<?php namespace Shop\Domain\Services\Shipping;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Shipping\ShippingRate;
use Shop\Domain\Entity\Shipping\ShippingRateId;
use Shop\Domain\Entity\Shipping\ShippingRateRepositoryInterface;
use Shop\Domain\Entity\Store\Store;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;

class CreateShippingRate extends AbstractCreatorService implements ServiceInterface
{
    private $entityManager;
    private $shippingRateRepository;
    private $countryRepository;
    protected $shippingRate;

    public function __construct(
        EntityManager $entityManager,
        ShippingRateRepositoryInterface $shippingRateRepository,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->entityManager = $entityManager;
        $this->shippingRateRepository = $shippingRateRepository;
        $this->countryRepository = $countryRepository;
    }

    public function getShippingRate()
    {
        return $this->shippingRate;
    }

    /**
     * @param \Shop\Domain\Entity\Store\Store $store
     * @param                                 $name
     * @param                                 $type
     * @param null                            $weightLow
     * @param null                            $weightHigh
     * @param null                            $minOrderSubtotal
     * @param null                            $maxOrderSubtotal
     * @param null                            $price
     * @param                                 $country
     *
     * @return $this
     */
    public function create(
        Store $store,
        $name,
        $type,
        $weightLow = null,
        $weightHigh = null,
        $minOrderSubtotal = null,
        $maxOrderSubtotal = null,
        $price = null,
        $country = null
    ) {
        $country = $this->countryRepository->getByCode($country);

        $shippingRate = new ShippingRate();
        $shippingRate->setId(ShippingRateId::generate());
        $shippingRate->setName($name);
        $shippingRate->setType($type);
        $shippingRate->setWeightLow($weightLow);
        $shippingRate->setWeightHigh($weightHigh);
        $shippingRate->setMinOrderSubtotal($minOrderSubtotal);
        $shippingRate->setMaxOrderSubtotal($maxOrderSubtotal);
        $shippingRate->setPrice($price);
        $shippingRate->setStore($store);

        if (!is_null($country)) {
            $shippingRate->setCountry($country);
        }

        $this->shippingRate = $shippingRate;

        return $this;
    }

    public function run()
    {
        $rate = $this->getShippingRate();

        // Store the store
        $this->entityManager->persist($rate);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getShippingRate();
    }
}
