<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['domain' => 'www.' . config('app.domain')], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'IndexController@getIndex']);
    Route::get('overview', ['as' => 'overview', 'uses' => 'IndexController@getOverview']);
    Route::get('features', ['as' => 'features', 'uses' => 'IndexController@getFeatures']);
    Route::get('pricing', ['as' => 'pricing', 'uses' => 'IndexController@getPricing']);
    Route::get('login', ['as' => 'login', 'uses' => 'IndexController@getLogin']);
    Route::post('login', ['as' => 'login', 'uses' => 'IndexController@postLogin']);

    Route::get('seed', ['as' => 'seed', 'uses' => 'IndexController@getInitial']);

    Route::get('products', ['as' => 'products', 'uses' => 'ProductController@getIndex']);
    Route::get('product/{id}', ['as' => 'product', 'uses' => 'ProductController@getItem']);
});
//services/signup/setup
// creating account
// initializing store
// applying store settings
Route::group(['prefix' => 'services'], function () {
    Route::group(['prefix' => 'signup'], function () {
        Route::get('check_availability.json',
            ['as' => 'services.signup.checkAvailability', 'uses' => 'Services\SignUpController@getCheckAvailability']);
        Route::post('setup', ['as' => 'services.signup.setup', 'uses' => 'Services\SignUpController@postSetup']);
        Route::post('create', ['as' => 'services.signup.create', 'uses' => 'Services\SignUpController@postCreate']);
    });
});

Route::group(['domain' => '{subdomain}.' . config('app.domain'), 'middleware' => ['subdomain']], function () {
    Route::group(['prefix' => 'admin'], function () {

        Route::get('login', ['as' => 'admin.login', 'uses' => 'Shop\Admin\AuthController@getLogin']);
        Route::post('login', ['as' => 'admin.login', 'uses' => 'Shop\Admin\AuthController@postLogin']);

        Route::get('logout', ['as' => 'admin.logout', 'uses' => 'Shop\Admin\AuthController@getLogout']);

        Route::group(['middleware' => ['auth']], function () {
            Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'Shop\Admin\DashboardController@getIndex']);
            Route::get('account-setup',
                ['as' => 'admin.account-setup', 'uses' => 'Shop\Admin\AccountSetupController@getIndex']);
            Route::post('account-setup',
                ['as' => 'admin.account-setup', 'uses' => 'Shop\Admin\AccountSetupController@postDetails']);

            // Settings
            Route::group(['prefix' => 'settings'], function () {
                Route::get('/', [
                    'as' => 'admin.settings.index',
                    'uses' => 'Shop\Admin\Settings\GeneralController@getGeneral'
                ]);
                Route::get('general', [
                    'as' => 'admin.settings.general',
                    'uses' => 'Shop\Admin\Settings\GeneralController@getGeneral'
                ]);
                Route::post('general', [
                    'as' => 'admin.settings.general',
                    'uses' => 'Shop\Admin\Settings\GeneralController@postGeneral'
                ]);

                Route::get('shipping', [
                    'as' => 'admin.settings.shipping',
                    'uses' => 'Shop\Admin\Settings\ShippingController@getShipping'
                ]);
                Route::post('shipping', [
                    'as' => 'admin.settings.update-shipping',
                    'uses' => 'Shop\Admin\Settings\ShippingController@postUpdateShipping'
                ]);

                Route::get('account', [
                    'as' => 'admin.settings.account',
                    'uses' => 'Shop\Admin\Settings\GeneralController@getAccount'
                ]);
                Route::post('account', [
                    'as' => 'admin.settings.account',
                    'uses' => 'Shop\Admin\Settings\GeneralController@postAccount'
                ]);

                Route::get('account/{id}', [
                    'as' => 'admin.settings.account.profile',
                    'uses' => 'Shop\Admin\Settings\GeneralController@getProfile'
                ]);
                Route::post('account/{id}', [
                    'as' => 'admin.settings.account.profile',
                    'uses' => 'Shop\Admin\Settings\GeneralController@postProfile'
                ]);

                Route::get('account/plan', [
                    'as' => 'admin.settings.account.plan',
                    'uses' => 'Shop\Admin\Settings\PlanController@getPlan'
                ]);
                Route::post('account/plan', [
                    'as' => 'admin.settings.account.plan',
                    'uses' => 'Shop\Admin\Settings\PlanController@postPlan'
                ]);
            });

            Route::get('subscription/pick-period/{plan}/{period}', [
                'as'   => 'admin.subscription.pick',
                'uses' => 'Shop\Admin\Subscription\SubscriptionController@getPick'
            ]);
        });
    });
});