<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;
use Illuminate\Contracts\Cache\Factory;

abstract class AbstractBaseRepository
{

    protected $entityManager;
    protected $maxResultsPerPage = 200;
    protected $cache;

    public function __construct(EntityManager $entityManager, Factory $cache)
    {
        $this->entityManager = $entityManager;
        $this->cache = $cache->store(env('CACHE_DRIVER'));

        //$entityManagerConfig = $this->entityManager->getConfiguration();

        //$entityManagerConfig->setQueryCacheImpl(new \Doctrine\Common\Cache\ApcCache());
        //$entityManagerConfig->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());
    }
}
