<?php namespace Shop\Domain\Services\Plan;

use Doctrine\ORM\EntityManager;
use Illuminate\Support\Str;
use Shop\Domain\Entity\Geo\Address;
use Shop\Domain\Entity\Geo\AddressRepositoryInterface;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Plan\Feature;
use Shop\Domain\Entity\Plan\Plan;
use Shop\Domain\Entity\Plan\PlanRepositoryInterface;
use Shop\Domain\Entity\Store\Setting;
use Shop\Domain\Entity\Store\SettingRepositoryInterface;
use Shop\Domain\Entity\Store\Store;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;

class CreatePlan extends AbstractCreatorService implements ServiceInterface
{
    private $entityManager;
    private $planRepository;
    protected $plan;

    public function __construct(
        EntityManager $entityManager,
        PlanRepositoryInterface $planRepository
    ) {
        $this->entityManager = $entityManager;
        $this->planRepository = $planRepository;
    }

    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param       $name
     * @param       $slug
     * @param       $description
     * @param       $price
     * @param       $highlight
     * @param       $onlineFeePercentage
     * @param       $onlineFeePrice
     * @param       $inPersonFeePercentage
     * @param       $inPersonFeePrice
     * @param array $features
     *
     * @return $this
     */
    public function create(
        $name,
        $slug,
        $description,
        $price,
        $highlight,
        $onlineFeePercentage,
        $onlineFeePrice,
        $inPersonFeePercentage,
        $inPersonFeePrice,
        $features = []
    ) {
        $plan = new Plan();
        $plan->setName($name);
        $plan->setSlug($slug);
        $plan->setDescription($description);
        $plan->setPrice($price);
        $plan->setHighlight($highlight);
        $plan->setOnlineFeePercentage($onlineFeePercentage);
        $plan->setOnlineFeePrice($onlineFeePrice);
        $plan->setInPersonFeePercentage($inPersonFeePercentage);
        $plan->setInPersonFeePrice($inPersonFeePrice);

        if ( ! empty($features)) {
            foreach ($features as $feature) {
                $plan->addFeature($feature);
            }
        }

        $this->plan = $plan;

        return $this;
    }

    public function run()
    {
        $store = $this->getPlan();

        // Store the store
        $this->entityManager->persist($store);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getPlan();
    }
}
