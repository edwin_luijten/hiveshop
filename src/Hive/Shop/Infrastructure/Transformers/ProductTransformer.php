<?php namespace Shop\Infrastructure\Transformers;

use League\Fractal\TransformerAbstract;
use Shop\Domain\Entity\Product\Product;

class ProductTransformer extends TransformerAbstract
{
    protected $availableIncludes  = [
        'variants',
        'options',
    ];

    public function transform(Product $product)
    {
        return [
            'id'              => $product->getId()->toString(),
            'name'            => $product->getName(),
            'slug'            => $product->getSlug(),
            'description'     => $product->getDescription(),
            'availableOn'     => $product->getAvailableOn(),
            'metaDescription' => $product->getMetaDescription(),
            'metaKeywords'    => $product->getMetaKeywords(),
        ];
    }

    public function includeVariants(Product $product)
    {
        return $this->collection($product->getVariants(), new VariantTransformer());
    }

    public function includeOptions(Product $product)
    {
        return $this->collection($product->getOptions(), new OptionTransformer());
    }
}
