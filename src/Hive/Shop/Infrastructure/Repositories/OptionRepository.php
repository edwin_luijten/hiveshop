<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\Product\Option;
use Shop\Domain\Entity\Product\OptionId;
use Shop\Domain\Entity\Product\OptionRepositoryInterface;

class OptionRepository extends AbstractBaseRepository implements OptionRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return OptionId::generate();
    }

    public function getEntity()
    {
        return new Option();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('o')->from(Option::class, 'o')->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        //@todo implement getPaginated
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('o')->from(Option::class, 'o')
            ->where($queryBuilder->expr()->eq('o.id', ':id'))
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    public function getCount()
    {
        //@todo implement getCount()
    }
}
