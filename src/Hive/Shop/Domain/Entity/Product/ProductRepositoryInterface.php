<?php namespace Shop\Domain\Entity\Product;

use Shop\Infrastructure\Repositories\RepositoryInterface;

interface ProductRepositoryInterface extends RepositoryInterface
{

}
