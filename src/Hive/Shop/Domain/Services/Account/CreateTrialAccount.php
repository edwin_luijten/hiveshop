<?php namespace Shop\Domain\Services\Account;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Account\Account;
use Shop\Domain\Entity\Account\AccountId;
use Shop\Domain\Entity\Plan\PlanRepositoryInterface;
use Shop\Domain\Entity\Subscription\Subscription;
use Shop\Domain\Entity\Subscription\SubscriptionId;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\HashingService;
use Shop\Domain\Services\ServiceInterface;
use Shop\Domain\ValueObjects\Email;
use Shop\Domain\ValueObjects\Password;

class CreateTrialAccount extends AbstractCreatorService implements ServiceInterface
{
    use ServiceTrait;

    protected $account;

    private $entityManager;
    private $planRepository;
    private $hashingService;

    public function __construct(
        EntityManager $entityManager,
        PlanRepositoryInterface $planRepository,
        HashingService $hashingService
    ) {
        $this->entityManager = $entityManager;
        $this->planRepository = $planRepository;
        $this->hashingService = $hashingService;
    }

    /**
     * @param $email
     * @param $password
     *
     * @return $this
     */
    public function create($email, $password)
    {
        $account = new Account();
        $account->setId(AccountId::generate());
        $account->setEmail(new Email($email));
        $account->setPassword($this->hashingService->hash(new Password($password)));
        $account->setCreatedAt(new \DateTime('now'));

        $startDate = new \DateTime('now');

        $subscription = new Subscription();
        $subscription->setId(SubscriptionId::generate());
        $subscription->setStartDate($startDate);
        $subscription->setEndDate($startDate->modify('+14 day'));
        $subscription->setPeriod('trial');
        $subscription->setPlan($this->planRepository->getBySlug('trial'));
        $subscription->setCreatedAt(new \DateTime('now'));

        $account->setSubscription($subscription);

        $this->account = $account;

        return $this;
    }
}
