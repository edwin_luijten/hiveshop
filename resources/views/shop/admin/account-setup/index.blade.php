@extends('shop.admin.blank')

@section('content')
<div class="container">
    <div class="signup-wrapper">
        <div class="signup-wrapper-window">
            <form id="detailsForm" method="post" action="{{ $formAction }}" data-toggle="validator">
            <header>
                <h1>Tell us a little about yourself</h1>
            </header>
            <section>
                <div class="row">
                    <div class="col-xs-12 col-md-6 {{ ($errors->default->has('first_name') ? 'has-error': '') }}">
                        <div class="form-group">
                            <label for="first_name">First name</label>
                            <input type="text" name="first_name" id="first_name" class="form-control" required="required"/>

                            <small class="help-block with-errors">
                                @if($errors->default->has('first_name'))
                                    @foreach($errors->default->get('first_name') as $message)
                                        {!! ucfirst($message) !!}
                                    @endforeach
                                @endif
                            </small>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group {{ ($errors->default->has('last_name') ? 'has-error': '') }}">
                            <label for="last_name">Last name</label>
                            <input type="text" name="last_name" id="last_name" class="form-control" required="required"/>

                            <small class="help-block with-errors">
                                @if($errors->default->has('last_name'))
                                    @foreach($errors->default->get('last_name') as $message)
                                        {!! ucfirst($message) !!}
                                    @endforeach
                                @endif
                            </small>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group {{ ($errors->default->has('street') ? 'has-error': '') }}">
                            <label for="street">Street address</label>
                            <input type="text" name="street" id="street" class="form-control" required="required"/>

                            <small class="help-block with-errors">
                                @if($errors->default->has('street'))
                                    @foreach($errors->default->get('street') as $message)
                                        {!! ucfirst($message) !!}
                                    @endforeach
                                @endif
                            </small>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" name="city" id="city" class="form-control" />
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="postal">Zip/Postal code</label>
                            <input type="text" name="postal" id="postal" class="form-control" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="country">Country</label>
                            <select name="country" id="country" class="form-control">
                                <option>Select country</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->getCode() }}">{{ $country->getName() }}</option>
                                @endforeach

                                <?php /*
                                <option value="CA">Canada</option>
                                <option value="US">United States</option>
                                <option value="GB">United Kingdom</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AD">Andorra</option>
                                <option value="AO">Angola</option>
                                <option value="AR">Argentina</option>
                                <option value="AM">Armenia</option>
                                <option value="AW">Aruba</option>
                                <option value="AG">Antigua And Barbuda</option>
                                <option value="AU">Australia</option>
                                <option value="AT">Austria</option>
                                <option value="AZ">Azerbaijan</option>
                                <option value="BD">Bangladesh</option>
                                <option value="BS">Bahamas</option>
                                <option value="BH">Bahrain</option>
                                <option value="BB">Barbados</option>
                                <option value="BE">Belgium</option>
                                <option value="BZ">Belize</option>
                                <option value="BM">Bermuda</option>
                                <option value="BT">Bhutan</option>
                                <option value="BO">Bolivia</option>
                                <option value="BA">Bosnia And Herzegovina</option>
                                <option value="BW">Botswana</option>
                                <option value="BR">Brazil</option>
                                <option value="BN">Brunei</option>
                                <option value="BG">Bulgaria</option>
                                <option value="MM">Burma</option>
                                <option value="KH">Cambodia</option>
                                <option value="CM">Republic of Cameroon</option>
                                <option value="KY">Cayman Islands</option>
                                <option value="CL">Chile</option>
                                <option value="CN">China</option>
                                <option value="CO">Colombia</option>
                                <option value="CG">Congo</option>
                                <option value="CR">Costa Rica</option>
                                <option value="CI">Côte d&#39;Ivoire</option>
                                <option value="HR">Croatia</option>
                                <option value="CW">Curaçao</option>
                                <option value="CY">Cyprus</option>
                                <option value="CZ">Czech Republic</option>
                                <option value="DK">Denmark</option>
                                <option value="DM">Dominica</option>
                                <option value="DO">Dominican Republic</option>
                                <option value="EC">Ecuador</option>
                                <option value="EG">Egypt</option>
                                <option value="SV">El Salvador</option>
                                <option value="EE">Estonia</option>
                                <option value="ET">Ethiopia</option>
                                <option value="FO">Faroe Islands</option>
                                <option value="FJ">Fiji</option>
                                <option value="FI">Finland</option>
                                <option value="FR">France</option>
                                <option value="PF">French Polynesia</option>
                                <option value="GM">Gambia</option>
                                <option value="GE">Georgia</option>
                                <option value="DE">Germany</option>
                                <option value="GH">Ghana</option>
                                <option value="GI">Gibraltar</option>
                                <option value="GR">Greece</option>
                                <option value="GL">Greenland</option>
                                <option value="GD">Grenada</option>
                                <option value="GP">Guadeloupe</option>
                                <option value="GT">Guatemala</option>
                                <option value="GG">Guernsey</option>
                                <option value="GY">Guyana</option>
                                <option value="HT">Haiti</option>
                                <option value="HN">Honduras</option>
                                <option value="HK">Hong Kong</option>
                                <option value="HU">Hungary</option>
                                <option value="IS">Iceland</option>
                                <option value="IN">India</option>
                                <option value="ID">Indonesia</option>
                                <option value="IE">Ireland</option>
                                <option value="IM">Isle Of Man</option>
                                <option value="IL">Israel</option>
                                <option value="IT">Italy</option>
                                <option value="JM">Jamaica</option>
                                <option value="JP">Japan</option>
                                <option value="JE">Jersey</option>
                                <option value="JO">Jordan</option>
                                <option value="KZ">Kazakhstan</option>
                                <option value="KE">Kenya</option>
                                <option value="KV">Kosovo</option>
                                <option value="KW">Kuwait</option>
                                <option value="KG">Kyrgyzstan</option>
                                <option value="LV">Latvia</option>
                                <option value="LB">Lebanon</option>
                                <option value="LR">Liberia</option>
                                <option value="LI">Liechtenstein</option>
                                <option value="LT">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="MO">Macao</option>
                                <option value="MK">Macedonia, Republic Of</option>
                                <option value="MG">Madagascar</option>
                                <option value="MY">Malaysia</option>
                                <option value="MV">Maldives</option>
                                <option value="MT">Malta</option>
                                <option value="MU">Mauritius</option>
                                <option value="MX">Mexico</option>
                                <option value="MD">Moldova, Republic of</option>
                                <option value="MC">Monaco</option>
                                <option value="MN">Mongolia</option>
                                <option value="ME">Montenegro</option>
                                <option value="MA">Morocco</option>
                                <option value="MZ">Mozambique</option>
                                <option value="NA">Namibia</option>
                                <option value="NP">Nepal</option>
                                <option value="AN">Netherlands Antilles</option>
                                <option value="NL">Netherlands</option>
                                <option value="NZ">New Zealand</option>
                                <option value="NI">Nicaragua</option>
                                <option value="NE">Niger</option>
                                <option value="NG">Nigeria</option>
                                <option value="NO">Norway</option>
                                <option value="OM">Oman</option>
                                <option value="PK">Pakistan</option>
                                <option value="PS">Palestinian Territory, Occupied</option>
                                <option value="PA">Panama</option>
                                <option value="PG">Papua New Guinea</option>
                                <option value="PY">Paraguay</option>
                                <option value="PE">Peru</option>
                                <option value="PH">Philippines</option>
                                <option value="PL">Poland</option>
                                <option value="PT">Portugal</option>
                                <option value="QA">Qatar</option>
                                <option value="RE">Reunion</option>
                                <option value="RO">Romania</option>
                                <option value="RU">Russia</option>
                                <option value="RW">Rwanda</option>
                                <option value="KN">Saint Kitts And Nevis</option>
                                <option value="LC">Saint Lucia</option>
                                <option value="MF">Saint Martin</option>
                                <option value="ST">Sao Tome And Principe</option>
                                <option value="WS">Samoa</option>
                                <option value="SA">Saudi Arabia</option>
                                <option value="RS">Serbia</option>
                                <option value="SC">Seychelles</option>
                                <option value="SG">Singapore</option>
                                <option value="SK">Slovakia</option>
                                <option value="SI">Slovenia</option>
                                <option value="ZA">South Africa</option>
                                <option value="KR">South Korea</option>
                                <option value="ES">Spain</option>
                                <option value="LK">Sri Lanka</option>
                                <option value="VC">St. Vincent</option>
                                <option value="SR">Suriname</option>
                                <option value="SE">Sweden</option>
                                <option value="CH">Switzerland</option>
                                <option value="TW">Taiwan</option>
                                <option value="TH">Thailand</option>
                                <option value="TZ">Tanzania, United Republic Of</option>
                                <option value="TT">Trinidad and Tobago</option>
                                <option value="TN">Tunisia</option>
                                <option value="TR">Turkey</option>
                                <option value="TM">Turkmenistan</option>
                                <option value="TC">Turks and Caicos Islands</option>
                                <option value="UG">Uganda</option>
                                <option value="UA">Ukraine</option>
                                <option value="AE">United Arab Emirates</option>
                                <option value="UY">Uruguay</option>
                                <option value="UZ">Uzbekistan</option>
                                <option value="VU">Vanuatu</option>
                                <option value="VE">Venezuela</option>
                                <option value="VN">Vietnam</option>
                                <option value="VG">Virgin Islands, British</option>
                                <option value="ZM">Zambia</option>
                                <option value="ZW">Zimbabwe</option>
                                */ ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group" id="province" style="display: none;">
                            <label id="zoneLabel" class="marketing-label">State/Province</label>
                            <select name="province" class="form-control"><option></option></select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group {{ ($errors->default->has('phone') ? 'has-error': '') }}">
                            <label for="phone">Phone number</label>
                            <input type="tel" name="phone" id="phone" class="form-control" required="required"/>

                            <small class="help-block with-errors">
                                @if($errors->default->has('phone'))
                                    @foreach($errors->default->get('phone') as $message)
                                        {!! ucfirst($message) !!}
                                    @endforeach
                                @endif
                            </small>
                        </div>
                        <small class="help-block">Used to identify your store if you call us</small>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        &nbsp;
                    </div>
                </div>
            </section>

            <footer>
                <div class="row">
                    <div class="col-xs-12 text-right">
                        {{ csrf_field() }}
                        <button type="submit" name="next" class="btn btn-success">Enter my store</button>
                    </div>
                </div>
            </footer>

            </form>
        </div> <!-- b-form-inner -->
    </div> <!-- signup wrapper -->
</div>
@endsection

@section('script')
<script type="text/javascript">
  /* <![CDATA[ */
    var CountryDB = {
        countries: <?php echo $countriesDB; ?>,
        validatedPostalCodes: ["US", "CA", "GB"],
        getCountry: function(name) {

            return this.countries.find(function(country){
                return (country.name == name || country.code == name);
            });
        }

    }
  /* ]]> */
    $('#detailsForm').on("change", '[name="country"]', Setup.onCountrySelection);
</script>
@endsection
