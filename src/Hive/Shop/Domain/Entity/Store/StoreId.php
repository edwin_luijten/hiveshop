<?php namespace Shop\Domain\Entity\Store;

use Ramsey\Uuid\Uuid;
use Shop\Domain\Identifier;
use Shop\Domain\UuidIdentifier;

class StoreId extends UuidIdentifier implements Identifier
{
    /**
     * @var Uuid
     */
    protected $value;

    /**
     * Create a new Id
     *
     * @param \Ramsey\Uuid\Uuid $value
     */
    public function __construct(Uuid $value)
    {
        $this->value = $value;
    }
}
