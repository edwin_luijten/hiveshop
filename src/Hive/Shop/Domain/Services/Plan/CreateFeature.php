<?php namespace Shop\Domain\Services\Plan;

use Doctrine\ORM\EntityManager;
use Illuminate\Support\Str;
use Shop\Domain\Entity\Geo\Address;
use Shop\Domain\Entity\Geo\AddressRepositoryInterface;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Plan\Feature;
use Shop\Domain\Entity\Plan\FeatureRepositoryInterface;
use Shop\Domain\Entity\Plan\Plan;
use Shop\Domain\Entity\Plan\PlanRepositoryInterface;
use Shop\Domain\Entity\Store\Setting;
use Shop\Domain\Entity\Store\SettingRepositoryInterface;
use Shop\Domain\Entity\Store\Store;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;

class CreateFeature extends AbstractCreatorService implements ServiceInterface
{
    private $entityManager;
    private $featureRepository;
    protected $feature;

    public function __construct(
        EntityManager $entityManager,
        FeatureRepositoryInterface $featureRepository
    ) {
        $this->entityManager = $entityManager;
        $this->featureRepository = $featureRepository;
    }

    public function getFeature()
    {
        return $this->feature;
    }

    /**
     * @param       $name
     * @param       $slug
     * @param       $description
     * @param       $type
     * @param       $value
     * @param       $price
     *
     * @return $this
     */
    public function create(
        $name,
        $slug,
        $description,
        $type,
        $value,
        $price
    ) {

        $feature = new Feature();
        $feature->setName($name);
        $feature->setSlug($slug);
        $feature->setDescription($description);
        $feature->setType($type);
        $feature->setValue($value);
        $feature->setPrice($price);

        $this->feature = $feature;

        return $this;
    }

    public function run()
    {
        $store = $this->getFeature();

        // Store the store
        $this->entityManager->persist($store);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getFeature();
    }
}
