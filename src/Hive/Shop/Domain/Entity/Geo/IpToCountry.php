<?php namespace Shop\Domain\Entity\Geo;

class IpToCountry {

    /**
     * @var decimal
     */
    private $ipFrom;
    /**
     * @var decimal
     */
    private $ipTo;
    /**
     * @var string
     */
    private $countryCode;
    /**
     * @var string
     */
    private $countryName;
    /**
     * @var string
     */
    private $regionName;
    /**
     * @var string
     */
    private $cityName;
    /**
     * @var double
     */
    private $latitude;
    /**
     * @var double
     */
    private $longitude;
    /**
     * @var string
     */
    private $zipCode;
    /**
     * @var string
     */
    private $timeZone;

    /**
     * @return decimal
     */
    public function getIpFrom()
    {
        return $this->ipFrom;
    }

    /**
     * @param decimal $ipFrom
     */
    public function setIpFrom($ipFrom)
    {
        $this->ipFrom = $ipFrom;
    }

    /**
     * @return decimal
     */
    public function getIpTo()
    {
        return $this->ipTo;
    }

    /**
     * @param decimal $ipTo
     */
    public function setIpTo($ipTo)
    {
        $this->ipTo = $ipTo;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return string
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * @param string $countryName
     */
    public function setCountryName($countryName)
    {
        $this->countryName = $countryName;
    }

    /**
     * @return string
     */
    public function getRegionName()
    {
        return $this->regionName;
    }

    /**
     * @param string $regionName
     */
    public function setRegionName($regionName)
    {
        $this->regionName = $regionName;
    }

    /**
     * @return string
     */
    public function getCityName()
    {
        return $this->cityName;
    }

    /**
     * @param string $cityName
     */
    public function setCityName($cityName)
    {
        $this->cityName = $cityName;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param string $timeZone
     */
    public function setTimeZone($timeZone)
    {
        $this->timeZone = $timeZone;
    }
}