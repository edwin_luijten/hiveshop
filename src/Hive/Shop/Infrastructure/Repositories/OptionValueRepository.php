<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\Product\OptionValue;
use Shop\Domain\Entity\Product\OptionValueId;
use Shop\Domain\Entity\Product\OptionValueRepositoryInterface;

class OptionValueRepository extends AbstractBaseRepository implements OptionValueRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return OptionValueId::generate();
    }

    public function getEntity()
    {
        return new OptionValue();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        //@todo implement getAll()
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        //@todo implement getPaginated()
    }

    public function getById($id)
    {
        //@todo implement getById()
    }

    public function getCount()
    {
        //@todo implement getCount()
    }
}
