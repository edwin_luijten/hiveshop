<?php namespace Shop\Domain\Entity\Store;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Shop\Domain\AggregateRoot;
use Shop\Domain\Entity\Geo\Address;
use Shop\Domain\Identifier;
use Shop\Domain\ValueObjects\Email;
use Src\Doctrine\Traits\SoftDeletes;
use Src\Doctrine\Traits\Timestamps;

class Setting implements AggregateRoot
{
    /**
     * @var SettingId
     */
    private $id;

    /**
     * @var Email
     */
    private $accountEmail;

    /**
     * @var Email
     */
    private $customerEmail;

    /**
     * @var string
     */
    private $timezone;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var string
     */
    private $unitSystem;

    /**
     * @var string
     */
    private $weightUnit;
    /**
     * @var string
     */
    private $orderIdPrefix;
    /**
     * @var string
     */
    private $orderIdSuffix;
    /**
     * @var Store
     */
    private $store;

    /**
     * Return the Aggregate Root identifer
     *
     * @return Identifier
     */
    public function getId()
    {
        return SettingId::fromString($this->id);
    }

    /**
     * @param \Shop\Domain\Entity\Store\SettingId $id
     */
    public function setId(SettingId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return Email
     */
    public function getAccountEmail()
    {
        return $this->accountEmail;
    }

    /**
     * @param Email $accountEmail
     */
    public function setAccountEmail($accountEmail)
    {
        $this->accountEmail = $accountEmail;
    }

    /**
     * @return Email
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @param Email $customerEmail
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getUnitSystem()
    {
        return $this->unitSystem;
    }

    /**
     * @param string $unitSystem
     */
    public function setUnitSystem($unitSystem)
    {
        $this->unitSystem = $unitSystem;
    }

    /**
     * @return string
     */
    public function getWeightUnit()
    {
        return $this->weightUnit;
    }

    /**
     * @param string $weightUnit
     */
    public function setWeightUnit($weightUnit)
    {
        $this->weightUnit = $weightUnit;
    }

    /**
     * @return string
     */
    public function getOrderIdPrefix()
    {
        return $this->orderIdPrefix;
    }

    /**
     * @param string $orderIdPrefix
     */
    public function setOrderIdPrefix($orderIdPrefix)
    {
        $this->orderIdPrefix = $orderIdPrefix;
    }

    /**
     * @return string
     */
    public function getOrderIdSuffix()
    {
        return $this->orderIdSuffix;
    }

    /**
     * @param string $orderIdSuffix
     */
    public function setOrderIdSuffix($orderIdSuffix)
    {
        $this->orderIdSuffix = $orderIdSuffix;
    }

    /**
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param Store $store
     */
    public function setStore(Store $store)
    {
        $this->store = $store;
    }
}
