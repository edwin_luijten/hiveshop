@extends('shop.template')

@section('content')
<section class="b-infoblock">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-9">
                <div class="b-shortcode-example">
                    <div class=" f-primary-b b-title-b-hr f-title-b-hr b-null-top-indent">
                        {{ $product->getName() }}
                    </div>

                    <div class="b-product-card b-default-top-indent">
                        <div class="b-product-card__visual-wrap">
                            <div class="flexslider b-product-card__visual flexslider-zoom">

                            <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 1200%; -webkit-transition-duration: 0.6s; transition-duration: 0.6s; -webkit-transform: translate3d(-1116px, 0px, 0px); transform: translate3d(-1116px, 0px, 0px);">
                                    <li class="" style="width: 372px; float: left; display: block;">
                                        <a class="fancybox" rel="product" href="img/shop/shop_1.png" title="Duis id nulla id enim sodales"><img src="/assets/s/img/shop/shop_1.png" draggable="false"></a>
                                    </li>
                                    <li style="width: 372px; float: left; display: block;" class="">
                                        <a class="fancybox" rel="product" href="img/shop/shop_2.png" title="Duis id nulla id enim sodales"><img src="/assets/s/img/shop/shop_2.png" draggable="false"></a>
                                    </li>
                                    <li style="width: 372px; float: left; display: block;" class="">
                                        <a class="fancybox" rel="product" href="img/shop/shop_3.png" title="Duis id nulla id enim sodales"><img src="/assets/s/img/shop/shop_3.png" draggable="false"></a>
                                    </li>
                                    <li style="width: 372px; float: left; display: block;" class="flex-active-slide">
                                        <a class="fancybox" rel="product" href="img/shop/shop_4.png" title="Duis id nulla id enim sodales"><img src="/assets/s/img/shop/shop_4.png" draggable="false"></a>
                                    </li>
                                    <li style="width: 372px; float: left; display: block;" class="">
                                        <a class="fancybox" rel="product" href="img/shop/shop_3.png" title="Duis id nulla id enim sodales"><img src="/assets/s/img/shop/shop_3.png" draggable="false"></a>
                                    </li>
                                    <li style="width: 372px; float: left; display: block;" class="">
                                        <a class="fancybox" rel="product" href="img/shop/shop_4.png" title="Duis id nulla id enim sodales"><img src="/assets/s/img/shop/shop_4.png" draggable="false"></a>
                                    </li>
                                </ul></div><ul class="flex-direction-nav"><li><a class="flex-prev" href="#">Previous</a></li><li><a class="flex-next" href="#">Next</a></li></ul></div>
                            <div class="flexslider flexslider-thumbnail b-product-card__visual-thumb carousel-sm">

                            <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides" style="width: 1200%; -webkit-transition-duration: 0.6s; transition-duration: 0.6s; -webkit-transform: translate3d(0px, 0px, 0px); transform: translate3d(0px, 0px, 0px);">
                                    <li class="" style="width: 78px; float: left; display: block;">
                                        <img src="/assets/s/img/shop/shop_1_sm.png" draggable="false">
                                    </li>
                                    <li style="width: 78px; float: left; display: block;" class="">
                                        <img src="/assets/s/img/shop/shop_2_sm.png" draggable="false">
                                    </li>
                                    <li style="width: 78px; float: left; display: block;" class="">
                                        <img src="/assets/s/img/shop/shop_3_sm.png" draggable="false">
                                    </li>
                                    <li style="width: 78px; float: left; display: block;" class="flex-active-slide">
                                        <img src="/assets/s/img/shop/shop_4_sm.png" draggable="false">
                                    </li>
                                    <li style="width: 78px; float: left; display: block;" class="">
                                        <img src="/assets/s/img/shop/shop_3_sm.png" draggable="false">
                                    </li>
                                    <li style="width: 78px; float: left; display: block;" class="">
                                        <img src="/assets/s/img/shop/shop_4_sm.png" draggable="false">
                                    </li>
                                </ul></div><ul class="flex-direction-nav"><li><a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a></li><li><a class="flex-next" href="#" tabindex="-1">Next</a></li></ul></div>
                        </div>

                        <div class="b-product-card__info">
                            <h4 class="f-primary-b b-h4-special f-h4-special">information</h4>
                            <div class="b-product-card__info_row">
                                <div class="b-product-card__info_title f-primary-b f-title-smallest">Price</div>
                                <span class="f-product-card__info_price c-default f-primary-b">{{ $product->getMasterVariant()->getPrice()->getFormatted() }}</span>
                            </div>
                            <div class="b-product-card__info_row">
                                <div class="b-product-card__info_title f-primary-b f-title-smallest">Reviews</div>
                                <div class="b-stars-group f-stars-group b-margin-right-standard">
                                    <i class="fa fa-star is-active-stars"></i>
                                    <i class="fa fa-star is-active-stars"></i>
                                    <i class="fa fa-star is-active-stars"></i>
                                    <i class="fa fa-star is-active-stars"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <span class="f-primary-b c-tertiary f-title-smallest"> <a href="#">(12 reviews)</a></span>
                            </div>
                            <div class="b-product-card__info_row">
                                <div class="b-product-card__info_description f-product-card__info_description">
                                    {{ $product->getDescription()}}
                                </div>
                            </div>
                            <div class="b-product-card__info_row row">
                            @if($product->hasOptions())
                                @foreach($product->getOptions() as $option)

                                        <div class="b-form-row b-form-select c-arrow-secondary col-xs-12 col-md-6">
                                            <label for="">{{ $option->getName() }}</label>
                                            <select class="j-select" name="options[{{ $option->getId() }}">
                                                <?php

                                                    foreach($product->getOptionValues() as $value)
                                                    {
                                                        if($value->getOption()->getId()->toString() === $option->getId()->toString())
                                                        {
                                                        ?>
                                                        <option value="{{ $value->getValue() }}">{{ $value->getValue() }}</option>
                                                        <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>

                                @endforeach
                            </div>
                            @endif
                            <div class="b-product-card__info_row">
                                <div class="b-product-card__info_count">
                                    <div class="input-number-box"><input type="number" min="1" class="form-control form-control--secondary" placeholder="1" style="display: none;"><input class="input-number form-control form-control--secondary" type="text" placeholder="1" min="1"><div class="input-number-more"></div><div class="input-number-less"></div></div>
                                </div>
                                <div class="b-product-card__info_add b-margin-right-standard">
                                    <div class=" b-btn f-btn b-btn-sm-md f-btn-sm-md">
                                        <i class="fa fa-shopping-cart"></i> Add to cart
                                    </div>
                                </div>
                                <div class="b-product-card__info_like  b-btn f-btn b-btn-sm-md f-btn-sm-md b-btn--icon-only">
                                    <i class="fa fa-heart"></i>
                                </div>
                            </div>
                            <div class="b-product-card__info_row">
                                <div class="b-product-card__info_code">
                                    <input type="text" class="form-control form-control--secondary" placeholder="Enter your coupon code">
                                </div>
                                <div class="b-product-card__info_submit  b-btn f-btn b-btn-sm-md f-btn-sm-md b-btn--icon-only">
                                    <i class="fa fa-arrow-right"></i>
                                </div>
                            </div>
                            <div class="b-product-card__info_row">
                                <div class="b-product-card__info_title f-primary-b f-title-smallest">Categories</div>
                                <a class="f-more f-title-smallest" href="">Dress</a>, <a class="f-more f-title-smallest" href="">Lorem</a>
                            </div>
                            <div class="b-product-card__info_row">
                                <div class="b-product-card__info_title f-primary-b f-title-smallest">Tags</div>
                                <div class="b-tag-container">
                                    <a class="f-tag b-tag" href="">Dress</a>
                                </div>
                            </div>
                            <div class="b-product-card__info_row">
                                <div class="b-btn-group-hor f-btn-group-hor">
                                    <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
                                        <i class="fa fa-dribbble"></i>
                                    </a>
                                    <a href="#" class="b-btn-group-hor__item f-btn-group-hor__item">
                                        <i class="fa fa-behance"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-xs-12 col-md-3">
            </div>
        </div>
    </div>
</section>
@endsection