<?php namespace Shop\Domain\Services\Account;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Account\AccountRepositoryInterface;
use Shop\Domain\Services\AbstractUpdaterService;
use Shop\Domain\Services\HashingService;
use Shop\Domain\Services\ServiceInterface;
use Shop\Domain\ValueObjects\Email;
use Shop\Domain\ValueObjects\Password;

class UpdateAccount extends AbstractUpdaterService implements ServiceInterface
{
    use ServiceTrait;

    protected $account;

    private $entityManager;
    private $accountRepository;
    private $hashingService;

    public function __construct(
        EntityManager $entityManager,
        AccountRepositoryInterface $accountRepository,
        HashingService $hashingService
    ) {
        $this->entityManager = $entityManager;
        $this->accountRepository = $accountRepository;
        $this->hashingService = $hashingService;
    }

    /**
     * @param                                          $accountId
     * @param null                                     $email
     * @param null                                     $password
     * @param null                                     $firstName
     * @param null                                     $lastName
     *
     * @return $this
     */
    public function update(
        $accountId,
        $email = null,
        $password = null,
        $firstName = null,
        $lastName = null
    ) {
        $account = $this->accountRepository->getById($accountId);

        if (! is_null($email) && !empty($email) && $email !== $account->getEmail()->toString()) {
            $email = new Email($email);
            $account->setEmail($email);
        }

        if (! is_null($password) && !empty($password)) {
            $password = $this->hashingService->hash(new Password($password));
            $account->setPassword($password);
        }

        $account->setFirstName($firstName);
        $account->setLastName($lastName);
        $account->setUpdatedAt(new \DateTime('now'));

        $this->account = $account;

        return $this;
    }
}
