<?php namespace Shop\Domain\Services\Account;

trait ServiceTrait {
    public function getAccount()
    {
        return $this->account;
    }

    public function run()
    {
        $account = $this->getAccount();

        $this->entityManager->merge($account);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        $account = $this->getAccount();

        $this->entityManager->detach($account);

        return $account;
    }
}
