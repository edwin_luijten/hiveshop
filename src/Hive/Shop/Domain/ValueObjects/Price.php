<?php namespace Shop\Domain\ValueObjects;

use Assert\Assertion;
use Shop\I18n\Number;

class Price extends AbstractValueObject
{
    private $cents;
    private $decimal;
    private $formatted;
    private $symbol;
    private $decimalPoint;

    private $formatterOptions = [
        'currency' => null,
        'options'  => [],
    ];

    public function __construct($cents)
    {
        Assertion::integer($cents);

        $this->setDefaults($cents);
    }

    public function formatter($options)
    {
        if (! empty($options['currency'])) {
            $this->formatterOptions['currency'] = $options['currency'];
            unset($options['currency']);
        }

        $this->formatterOptions['options'] = $options;
        $this->setDefaults($this->cents);

        return $this;
    }

    public function setDefaults($cents)
    {
        $this->decimalPoint = localeconv()['decimal_point'];
        $this->setCents($cents);
        $this->setDecimal();
        $this->setFormatted();
        $this->setDecimal();
        $this->setSymbol();

    }

    public function setCents($cents)
    {
        $this->cents = $cents;
    }

    public function getCents()
    {
        return $this->cents;
    }

    public function setSymbol()
    {
        $this->symbol = preg_replace('#[a-z0-9.' . $this->decimalPoint . ']*#i', '', $this->formatted);
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    public function setDecimal()
    {
        $this->decimal = $this->stripSymbol($this->getFormatted());
    }

    public function getDecimal()
    {
        return $this->decimal;
    }

    public function setFormatted()
    {
        $numberFormatter = new Number();
        $formatted = $numberFormatter->currency($this->getCents(), $this->formatterOptions['currency'],
            $this->formatterOptions['options']);

        $this->formatted = $formatted;
    }

    public function getFormatted()
    {
        $numberFormatter = new Number();
        $formatted = $numberFormatter->currency($this->getCents(), $this->formatterOptions['currency'],
            $this->formatterOptions['options']);

        return $formatted;
    }

    private function stripSymbol($formatted)
    {
        $symbol = preg_replace('#[a-z0-9.' . $this->decimalPoint . ']*#i', '', $formatted);

        return str_replace($symbol, '', $formatted);
    }
}
