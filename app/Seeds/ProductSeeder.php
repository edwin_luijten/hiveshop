<?php namespace Shop\Domain\Services\Seeds;

use Shop\Domain\Entity\Product\OptionRepositoryInterface;
use Shop\Domain\Entity\Product\ProductRepositoryInterface;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;
use Shop\Domain\Services\Product\CreateProduct;

class ProductSeeder extends BaseSeeder {

    private $productRepository;
    private $optionRepository;
    private $createProduct;
    private $defaultAmount = 100;
    private $options;

    public function __construct(StoreRepositoryInterface $storeRepository, ProductRepositoryInterface $productRepository, OptionRepositoryInterface $optionRepository, CreateProduct $createProduct)
    {
        $this->productRepository = $productRepository;
        $this->optionRepository = $optionRepository;
        $this->createProduct = $createProduct;

        parent::__construct($storeRepository);
    }

    public function seed($amount = 100)
    {
        $this->setRandomStore();

        $this->setOptions();

        $maxProducts = (is_null($amount) ? $this->defaultAmount : $amount);

        // current amount
        $currentAmount = $this->productRepository->getCount();

        $i = 1;
        $currentAmount;

        $products = [];

        while($i <= $maxProducts)
        {
            $i++;

            if(! is_null($this->randomStore))
            {
                $options = $this->getRandomOptions();

                $createVariants = boolval(rand(0,1));
                $products[$i] = $this->createProduct->forStore($this->randomStore)
                    ->create('product ' . ($currentAmount+$i), 200, 'desc', '2015-12-12', 'meta', 'meta', $options, $createVariants);
            }
        }

        return $products;
    }

    private function getRandomOptions()
    {
        $amount = rand(1,3);

        $options = [];

        $i = 0;
        while ($i <= $amount)
        {
            $i++;
            $n = rand(0,2);

            $options[$this->options[$n]]['id'] = $this->options[$n];
            $options[$this->options[$n]]['values'] = $this->getRandomOptionValues();
        }

        return $options;
    }

    private function getRandomOptionValues()
    {
        $amount = rand(1,3);

        $values = [];

        $i = 0;
        while ($i <= $amount)
        {
            $i++;
            $values[$i] = str_random(5);
        }

        return $values;
    }

    private function setOptions()
    {
        $options = $this->optionRepository->getAll();

        foreach($options as $option) {
            $this->options[] = $option->getId()->toString();
            $this->randomStore = $option->getStore();
        }
    }
}