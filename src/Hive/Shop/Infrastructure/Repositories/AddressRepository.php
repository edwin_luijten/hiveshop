<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\Geo\Address;
use Shop\Domain\Entity\Geo\AddressId;
use Shop\Domain\Entity\Geo\AddressRepositoryInterface;

class AddressRepository extends AbstractBaseRepository implements AddressRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return AddressId::generate();
    }
    
    public function getEntity()
    {
        return new Address();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('a')->from(Address::class, 'a')
            ->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        //@todo implement getPaginated
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('a')->from(Address::class, 'a')
            ->where(
                $queryBuilder->expr()->eq('a.id', ':id')
            )
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    public function getCount()
    {
        //@todo implement getCount()
    }
}
