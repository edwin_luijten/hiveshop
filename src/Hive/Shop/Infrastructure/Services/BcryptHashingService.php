<?php namespace Shop\Infrastructure\Services;

use Illuminate\Hashing\BcryptHasher;
use Shop\Domain\Services\HashingService;
use Shop\Domain\ValueObjects\HashedPassword;
use Shop\Domain\ValueObjects\Password;

class BcryptHashingService implements HashingService
{
    /**
     * @var \Illuminate\Hashing\BcryptHasher
     */
    private $hasher;

    /**
     * Create a new BcryptHashingService
     *
     * @param \Illuminate\Hashing\BcryptHasher $hasher
     *
     * @return \Shop\Infrastructure\Services\BcryptHashingService
     */
    public function __construct(BcryptHasher $hasher)
    {
        $this->hasher = $hasher;
    }
    /**
     * Create a new HashedPassword
     *
     * @param Password $password
     * @return HashedPassword
     */
    public function hash(Password $password)
    {
        return new HashedPassword($this->hasher->make($password->toString()));
    }
}
