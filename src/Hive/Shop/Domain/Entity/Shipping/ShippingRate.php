<?php namespace Shop\Domain\Entity\Shipping;

use Shop\Domain\Entity\Geo\Country;
use Shop\Domain\Entity\Store\Store;
use Shop\Domain\ValueObjects\Price;

class ShippingRate
{
    /**
     * @var ShippingRateId
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $type;
    /**
     * @var decimal
     */
    private $weightLow;
    /**
     * @var decimal
     */
    private $weightHigh;
    /**
     * @var integer
     */
    private $minOrderSubtotal;
    /**
     * @var integer
     */
    private $maxOrderSubtotal;
    /**
     * @var integer
     */
    private $price;
    /**
     * @var Country
     */
    private $country;
    /**
     * @var Store
     */
    private $store;

    /**
     * @return ShippingRateId
     */
    public function getId()
    {
        return ShippingRateId::fromString($this->id);
    }

    /**
     * @param ShippingRateId $id
     */
    public function setId(ShippingRateId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return decimal
     */
    public function getWeightLow()
    {
        return $this->weightLow;
    }

    /**
     * @param decimal $weightLow
     */
    public function setWeightLow($weightLow)
    {
        $this->weightLow = $weightLow;
    }

    /**
     * @return decimal
     */
    public function getWeightHigh()
    {
        return $this->weightHigh;
    }

    /**
     * @param decimal $weightHigh
     */
    public function setWeightHigh($weightHigh)
    {
        $this->weightHigh = $weightHigh;
    }

    /**
     * @return int
     */
    public function getMinOrderSubtotal()
    {
        return (!is_null($this->minOrderSubtotal) ? new Price($this->minOrderSubtotal) : null);
    }

    /**
     * @param int $minOrderSubtotal
     */
    public function setMinOrderSubtotal($minOrderSubtotal)
    {
        $this->minOrderSubtotal = $minOrderSubtotal;
    }

    /**
     * @return int
     */
    public function getMaxOrderSubtotal()
    {
        return (!is_null($this->maxOrderSubtotal) ? new Price($this->maxOrderSubtotal) : null);
    }

    /**
     * @param int $maxOrderSubtotal
     */
    public function setMaxOrderSubtotal($maxOrderSubtotal)
    {
        $this->maxOrderSubtotal = $maxOrderSubtotal;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return (!is_null($this->price) ? new Price($this->price) : null);
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;
    }

    /**
     * @return Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * @param Store $store
     */
    public function setStore(Store $store)
    {
        $this->store = $store;
    }

}