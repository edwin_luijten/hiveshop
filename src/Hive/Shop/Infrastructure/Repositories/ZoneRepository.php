<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\Geo\ZoneRepositoryInterface;
use Shop\Domain\Entity\Geo\Zone;
use Shop\Domain\Entity\Geo\ZoneId;

class ZoneRepository extends AbstractBaseRepository implements ZoneRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return ZoneId::generate();
    }

    public function getEntity()
    {
        return new Zone();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('z')->from(Zone::class, 'z')->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        //@todo implement getPaginated
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('z')
            ->from(Zone::class, 'z')
            ->where($queryBuilder->expr()->eq('z.id', ':id'))
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    public function getCount()
    {
        //@todo implement getCount()
    }
}
