<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

use Shop\Domain\Entity\Store\Setting;
use Shop\Domain\Entity\Store\SettingId;
use Shop\Domain\Entity\Store\SettingRepositoryInterface;

class SettingRepository extends AbstractBaseRepository implements SettingRepositoryInterface
{

    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return SettingId::generate();
    }

    public function getEntity()
    {
        return new Setting();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('s')->from(Setting::class, 's');

        return $queryBuilder->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function getPaginated($page = 1, $limit = 15)
    {
        if ($limit > $this->maxResultsPerPage) {
            $limit = $this->maxResultsPerPage;
        }

        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('s')->from(Setting::class, 's')
        ;

        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pager = new Pagerfanta($adapter);
        $pager->setCurrentPage($page);
        $pager->setMaxPerPage($limit);

        return $pager;
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('s')->from(Setting::class, 's')
            ->where(
                $queryBuilder->expr()->eq('s.id', ':id')
            )
            ->setParameter('id', $id)
        ;

        return $queryBuilder->getQuery()->getSingleResult();
    }

    public function getCount()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('count(s.id)')->from(Store::class, 's');

        return (integer) $queryBuilder->getQuery()->getSingleScalarResult();
    }
}
