<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	    <meta name="csrf-token" content="{{ csrf_token() }}">

		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

	    <link href="/assets/c/css/shared.css" rel="stylesheet" type="text/css"/>
	    <link href="/assets/h/css/app.css" rel="stylesheet" type="text/css"/>
	</head>

	<body>
	    <header>
	        <div class="container">
                <nav class="pull-left">
                    <ul>
                        <li><a href="{{ URL::route('overview') }}">Overview</a></li>
                        <li><a href="{{ URL::route('features') }}">Features</a></li>
                        <li><a href="{{ URL::route('pricing') }}">Pricing</a></li>
                    </ul>
                </nav>

                <nav class="pull-right">
                    <ul>
                        <li><a href="{{ Url::route('login') }}">Login</a></li>
                    </ul>
                </nav>
            </div>
        </header>

        @yield('content')

        <footer>

        </footer>

        <script src="/assets/c/js/shared.js" type="text/javascript"></script>
        <script src="/assets/h/js/app.js" type="text/javascript"></script>

        @yield('script')

	</body>
</html>