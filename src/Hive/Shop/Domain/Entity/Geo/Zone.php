<?php namespace Shop\Domain\Entity\Geo;

use Shop\Domain\AggregateRoot;

class Zone implements AggregateRoot
{
    /**
     * @var ZoneId
     */
    private $id;
    /**
     * @var float
     */
    private $tax;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $taxName;
    /**
     * @var string
     */
    private $taxType;
    /**
     * @var string
     */
    private $code;
    /**
     * @var Country
     */
    private $country;
    /**
     * @return ZoneId
     */
    public function getId()
    {
        return ZoneId::fromString($this->id);
    }

    /**
     * @param ZoneId $id
     */
    public function setId(ZoneId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return decimal
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param decimal $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * @return string
     */
    public function getTaxName()
    {
        return $this->taxName;
    }

    /**
     * @param string $taxName
     */
    public function setTaxName($taxName)
    {
        $this->taxName = $taxName;
    }

    /**
     * @return string
     */
    public function getTaxType()
    {
        return $this->taxType;
    }

    /**
     * @param string $taxType
     */
    public function setTaxType($taxType)
    {
        $this->taxType = $taxType;
    }

    public function setCountry(Country $country)
    {
        $this->country = $country;
    }
}
