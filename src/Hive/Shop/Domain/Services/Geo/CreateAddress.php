<?php namespace Shop\Domain\Services\Geo;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Account\Account;
use Shop\Domain\Entity\Geo\Address;
use Shop\Domain\Entity\Geo\AddressRepositoryInterface;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Store\Store;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;

class CreateAddress extends AbstractCreatorService implements ServiceInterface
{
    private $entityManager;
    private $addressRepository;
    private $countryRepository;
    protected $address;

    public function __construct(
        EntityManager $entityManager,
        AddressRepositoryInterface $addressRepository,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->entityManager = $entityManager;
        $this->addressRepository = $addressRepository;
        $this->countryRepository = $countryRepository;
    }

    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param      $phone
     * @param      $street
     * @param      $country
     * @param      $object
     * @param null $legalName
     * @param null $city
     * @param null $postal
     *
     * @return \Shop\Domain\Entity\Geo\Address
     */
    public function create($phone, $street, $country, $object, $legalName = null, $city = null, $postal = null)
    {

        $id = $this->addressRepository->nextUuid();
        $country = $this->countryRepository->getByCode($country);

        $address = new Address();
        $address->setId($id);
        $address->setPhone($phone);
        $address->setLegalName((!empty($legalName) ? $legalName : null));
        $address->setStreet($street);
        $address->setCity((!empty($city) ? $city : null));
        $address->setPostal((!empty($postal) ? $postal : null));
        $address->setCountry($country);

        if ($object instanceof Store) {
            $address->setStore($object);
        } elseif ($object instanceof Account) {
            $address->setAccount($object);
        }

        $this->address = $address;

        return $this;
    }

    public function run()
    {
        $store = $this->getAddress();

        // Store the store
        $this->entityManager->persist($store);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getAddress();
    }
}
