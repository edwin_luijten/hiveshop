<?php namespace App\Http\Controllers\Shop\Admin;

use App\Http\Controllers\Controller;
use BrowscapPHP\Browscap;
use Illuminate\Auth\Guard;
use Illuminate\Http\Response;
use Illuminate\Routing\UrlGenerator;

class DashboardController extends Controller
{
    private $response;
    private $urlGenerator;
    private $guard;

    public function __construct(
        Response $response,
        UrlGenerator $urlGenerator,
        Guard $guard
    ) {
        $this->response = $response;
        $this->urlGenerator = $urlGenerator;
        $this->guard = $guard;
    }

    public function getIndex()
    {
        return view('shop.admin.pages.dashboard');
    }
}
