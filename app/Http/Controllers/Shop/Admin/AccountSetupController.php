<?php namespace App\Http\Controllers\Shop\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStoreDetailsRequest;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\UrlGenerator;
use JMS\Serializer\Serializer;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Shipping\ShippingRateTypes;
use Shop\Domain\Services\Account\UpdateAccount;
use Shop\Domain\Services\ServiceFactory;
use Shop\Domain\Services\Geo\UpdateAddress;
use Shop\Domain\Services\Shipping\CreateShippingRate;
use Shop\Domain\Services\Store\UpdateSetting;

class AccountSetupController extends Controller
{
    private $response;
    private $countryRepository;
    private $urlGenerator;
    private $serializer;
    private $guard;

    public function __construct(
        Response $response,
        UrlGenerator $urlGenerator,
        Guard $guard,
        Serializer $serializer,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->response = $response;
        $this->urlGenerator = $urlGenerator;
        $this->guard = $guard;
        $this->serializer = $serializer;
        $this->countryRepository = $countryRepository;
    }

    public function getIndex(Request $request)
    {
        $formAction = $this->urlGenerator->route('admin.account-setup', [
            'subdomain' => $request->user()->getStore()->getSubdomain()
        ]);

        $countries = $this->countryRepository->getAll();
        $countriesDB = $this->serializer->serialize($countries, 'json');

        return view('shop.admin.account-setup.index', [
            'formAction' => $formAction,
            'countries' => $countries,
            'countriesDB' => $countriesDB,
        ]);
    }

    /**
     * @param StoreStoreDetailsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDetails(StoreStoreDetailsRequest $request)
    {
        $input = $request->except('_token');
        $account = $request->user();
        $store = $account->getStore();

        $accountService = new ServiceFactory(UpdateAccount::class);
        $accountService->update(
            $account->getId(),
            $account->getEmail()->toString(),
            null,
            array_get($input, 'first_name'),
            array_get($input, 'last_name')
        )->run()->flush();

        if (!empty($account->getStore()->getAddress())) {
            // Create a store address
            $addressService = new ServiceFactory(UpdateAddress::class);
            $addressService->update(
                $store->getAddress()->getId(),
                $input['phone'],
                $input['street'],
                $input['country'],
                array_get($input, 'legal_name'),
                array_get($input, 'city'),
                array_get($input, 'postal')
            )->run()->flush();
        }

        // Redirect to store
        return redirect()->route('admin.dashboard', ['subdomain' => $store->getSubdomain()]);
    }
}
