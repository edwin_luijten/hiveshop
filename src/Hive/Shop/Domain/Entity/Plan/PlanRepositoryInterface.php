<?php namespace Shop\Domain\Entity\Plan;

use Shop\Infrastructure\Repositories\RepositoryInterface;

interface PlanRepositoryInterface extends RepositoryInterface
{
    public function getBySlug($slug);
}
