var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	/**
	 * Shared assets
	 */
	mix.styles([
		'bootstrap.min.css',
		'animate.min.css',
		'fonts.css',
	], 'public/assets/c/css/shared.css', './resources/assets/css/shared/');

	mix.scripts([
		'jquery-2.1.4.min.js',
		'bootstrap.min.js',
		'underscore-1.8.3.min.js',
		'formvalidation.min.js',
	], 'public/assets/c/js/shared.js', './resources/assets/js/shared/');

	/**
	 * Hiveshop assets
	 */
	mix.styles([
		'app.css',
	], 'public/assets/h/css/app.css', './resources/assets/css/hiveshop/');

	mix.scripts([
		'app.js',
		'ui.js',
	], 'public/assets/h/js/app.js', './resources/assets/js/hiveshop/');

	/**
	 * Shop assets
	 */
	mix.styles([
		'setup.css',
	], 'public/assets/s/css/app.css', './resources/assets/css/shop/');

	mix.scripts([
		'setup.js',
	], 'public/assets/s/js/app.js', './resources/assets/js/shop/');

	/**
	 * Shop Admin assets
	 */
	mix.styles([
		'themes/flat.css',
		'plugins.css',
		'main.css',
		'themes.css',
		'app.css',
	], 'public/assets/sa/css/admin.css', './resources/assets/css/shop/admin/');

	mix.scripts([
		'plugins.js',
		'app.js',
	], 'public/assets/sa/js/app.js', './resources/assets/js/shop/admin/');

	mix.copy('resources/assets/fonts', 'public/assets/c/fonts');
	mix.copy('resources/assets/img/shared', 'public/assets/c/img');
	mix.copy('resources/assets/img/hiveshop', 'public/assets/h/img');
	mix.copy('resources/assets/img/shop/admin', 'public/assets/sa/img');
});
