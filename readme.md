## Todo
- [ ] Refactor the service layer
- [ ] Cache decorator
- [ ] Implement domain events
- [ ] Create unit tests

## Roadmap
### Settings
- [x] General
- [ ] Account
     on email change -> send notification email to account
     on password change -> send notification email to account
- [ ] Shipping
-     add countries
-         by region/continent eg. European countries etc will add all the countries of that region/continent
-     implement shipping tools eg. sentcloud, parcelware etc.
- [ ] Taxes
-     use commerceguys tax package for managing tax rates based on country

## Seeding ip_to_country database
Download the IP2LOCATION-LITE-DB11.zip from the downloads section
```sql
LOAD DATA LOCAL INFILE '[path-to]/IP2LOCATION-LITE-DB11.CSV' INTO TABLE homestead.ip_to_country FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n';
```