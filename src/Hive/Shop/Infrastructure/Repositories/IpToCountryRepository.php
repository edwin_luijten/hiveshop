<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\Geo\IpToCountry;
use Shop\Domain\Entity\Geo\IpToCountryRepositoryInterface;

class IpToCountryRepository extends AbstractBaseRepository implements IpToCountryRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('i')->from(IpToCountry::class, 'i')
            ->getQuery()->execute();
    }

    public function getByIp($ip)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('i')->from(IpToCountry::class, 'i')
            ->where(
                $queryBuilder->expr()->lte('inet_aton(:ip)', 'i.ipTo')
            )
            ->setParameter('ip', $ip)
            ->setMaxResults(1)
            ->getQuery()->getSingleResult();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        //@todo implement getPaginated
    }

    public function getCount()
    {
        //@todo implement getCount()
    }

    public function nextUuid()
    {
        // TODO: Implement nextUuid() method.
    }

    public function getEntity()
    {
        // TODO: Implement getEntity() method.
    }

    public function getById($id)
    {
        // TODO: Implement getById() method.
    }
}
