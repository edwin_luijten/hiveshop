<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\Product\Variant;
use Shop\Domain\Entity\Product\VariantId;
use Shop\Domain\Entity\Product\VariantRepositoryInterface;

class VariantRepository extends AbstractBaseRepository implements VariantRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return VariantId::generate();
    }

    public function getEntity()
    {
        return new Variant();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('pv', 'pvo')->from(Variant::class, 'pv')
            ->innerJoin('pv.optionValues', 'pvo')
        ;

        return $queryBuilder->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        if ($limit > $this->maxResultsPerPage) {
            $limit = $this->maxResultsPerPage;
        }

        $offset = ($page - 1) * $limit;

        $resultMapping = new ResultSetMapping();
        $resultMapping->addEntityResult(Variant::class, 'pv');
        $resultMapping->addFieldResult('pv', 'id', 'id');

        $nativeQuery = $this->entityManager->createNativeQuery(
            "select pv.* from variants as pv " .
            "inner join (select id from variants limit :limit offset :offset) as small_tbl using (id) " .
            "inner join option_value_variant as ovv on pv.id = ovv.variant_id " .
            "inner join option_values as ov on ov.id = ovv.option_value_id",
            $resultMapping
        );

        $nativeQuery->setParameter('limit', $limit);
        $nativeQuery->setParameter('offset', $offset);

        return $nativeQuery->getResult();
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('pv', 'pvo')->from(Variant::class, 'pv')
            ->where($queryBuilder->expr()->eq('pv.id', ':pvid'))
            ->innerJoin('pv.optionValues', 'pvo')
            ->setParameter('pvid', $id)
        ;

        return $queryBuilder->getQuery()->getFirstResult();
    }

    public function getBySlug($slug)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('pv', 'pvo')->from(Variant::class, 'p')
            ->where($queryBuilder->expr()->eq('p.slug', ':pvslug'))
            ->innerJoin('pv.optionValues', 'pvo')
            ->setParameter('pvslug', $slug)
        ;

        return $queryBuilder->getQuery()->getFirstResult();
    }

    public function getCount()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder->select('count(pv.id)')
            ->from(Variant::class, 'pv')
            ;

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function save(Variant $entity)
    {
        // TODO: Implement save() method.
    }

    public function update(Variant $entity)
    {
        // TODO: Implement update() method.
    }

    public function delete(Variant $entity)
    {
        // TODO: Implement delete() method.
    }
}
