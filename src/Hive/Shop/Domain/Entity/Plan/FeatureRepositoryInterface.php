<?php namespace Shop\Domain\Entity\Plan;

use Shop\Infrastructure\Repositories\RepositoryInterface;

interface FeatureRepositoryInterface extends RepositoryInterface
{
    public function getBySlug($slug);
}
