<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Shop\Domain\Entity\Product\Option;
use Shop\Domain\Entity\Product\OptionValue;
use Shop\Domain\Entity\Product\Product;
use Shop\Domain\Entity\Product\ProductRepositoryInterface;
use Shop\Domain\Services\Product\CreateProduct;
use Shop\Domain\Services\ProductSeeder;
use Shop\Infrastructure\Transformers\ProductTransformer;
use Shop\Domain\Services\VariantGenerator;

class ProductController extends Controller
{
    private $response;
    private $productRepository;
    private $createProduct;

    public function __construct(
        Response $response,
        ProductRepositoryInterface $productRepository,
        CreateProduct $createProduct
    ) {
        $this->response = $response;
        $this->productRepository = $productRepository;
        $this->createProduct = $createProduct;
    }

    public function getIndex(Request $request)
    {

        $products = $this->productRepository->getRandomItems(6);

        return view('shop.pages.home', ['products' => $products]);
    }

    public function getItem(Request $request, $id)
    {
        $product = $this->productRepository->getById($id);

        return view('shop.pages.product', ['product' => $product]);
    }

    public function postCreate(Request $request)
    {
        $payload = $request->get('payload');

        if(! is_null($payload))
        {

        }
    }
}