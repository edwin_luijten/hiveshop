@extends('shop.admin.template')

@section('page-breadcrumb')
    <ul>
        <li class="action"><i class="fa fa-home"></i> Dashboard</li>
    </ul>
@endsection

@section('content')
    <header class="navbar navbar-fixed-top navbar-default">
        <ul class="nav navbar-nav-custom">
            <li class="hidden-xs animation-fadeInQuick navbar-breadcrumb">
                @yield('page-breadcrumb')
            </li>
        </ul>
        <div id="progress">

        </div>
    </header>
    <div id="page-content" class="home-index-page">
        <div class="welcome-header welcome-header-trial">
            <h2>Welcome</h2>
        </div>

        <div class="home-cards col-xs-12 col-md-4 col-md-offset-4 animated fast" data-animation="fadeInUp">
            <div class="next-card next-card-notifications">
                <div class="next-card-section callout">
                    <div class="row">
                        <div class="col-md-7">
                            <h2 class="heading">
                                You have {{ subscription_countdown($account->getSubscription()->getStartDate(), $account->getSubscription()->getEndDate()) }} days left of your trial
                            </h2>
                        </div>
                        <div class="col-md-5 text-right">
                            <a href="" class="btn btn-success btn-effect-riple">Select a plan</a>
                        </div>
                    </div>


                </div>
            </div>

            <div class="next-card completed">
                <div class="next-card-section">
                    <h3 class="heading">Add a product to see it in your store</h3>

                    <a href="" class="btn btn-xs btn-square btn-primary btn-effect-ripple">Add a product</a>
                </div>
            </div>

            <div class="next-card">
                <div class="next-card-section">
                    <h3 class="heading">Customize the look of your website</h3>

                    <a href="" class="btn btn-xs btn-square btn-primary btn-effect-ripple">Select a theme</a>
                </div>
            </div>

            <div class="next-card">
                <div class="next-card-section">
                    <h3 class="heading">Tell us about your store</h3>

                    <a href="" class="btn btn-xs btn-square btn-primary btn-effect-ripple">Describe</a>
                </div>
            </div>
        </div>
    </div>
    <div class="plan-selection" id="launchbar">
        <div class="wrapper clearfix">
            <span class="plan-selection-message">
                <a href="/admin">{{ trial_countdown($account->getCreatedAt()) }} days left</a> of your trial, ready to start selling?
            </span>

            <a class="btn btn-success btn-effect-ripple" href="/admin/settings/account/plan">Select a plan</a>
        </div>
    </div>
@endsection