<?php namespace Src\Doctrine\Traits;

use DateTime;

trait SoftDeletes
{
    private $deletedAt;

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(DateTime $deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    public function isDeleted()
    {
        return new DateTime > $this->deletedAt;
    }
}