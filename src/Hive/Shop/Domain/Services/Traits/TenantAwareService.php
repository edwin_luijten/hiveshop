<?php namespace Shop\Domain\Services\Traits;

use Shop\Domain\Entity\Store\Store;

trait StoreAwareService
{

    private $store;

    public function forStore(Store $store)
    {
        $this->store = $store;

        return $this;
    }
}
