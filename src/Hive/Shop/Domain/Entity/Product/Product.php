<?php namespace Shop\Domain\Entity\Product;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Shop\Domain\AggregateRoot;
use Shop\Domain\Entity\Store\Traits\StoreAware;
use Src\Doctrine\Traits\SoftDeletes;
use Src\Doctrine\Traits\Timestamps;

class Product implements AggregateRoot
{
    use Timestamps;
    use SoftDeletes;
    use StoreAware;

    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $slug;
    /**
     * @var string
     */
    private $description;
    /**
     * @var Datetime
     */
    private $availableOn;
    /**
     * @var string
     */
    private $metaDescription;
    /**
     * @var string
     */
    private $metaKeywords;
    /**
     * @var bool
     */
    private $hasVariants = false;
    /**
     * @var ArrayCollection
     */
    private $variants;
    /**
     * @var ArrayCollection
     */
    private $options;
    /**
     * @var ArrayCollection
     */
    private $optionValues;

    public function __construct()
    {
        $this->variants = new ArrayCollection();
        $this->options = new ArrayCollection();
        $this->optionValues = new ArrayCollection();
    }

    /**
     * @return Datetime
     */
    public function getAvailableOn()
    {
        return $this->availableOn;
    }

    /**
     * @param $availableOn
     */
    public function setAvailableOn($availableOn)
    {
        $this->availableOn = $availableOn;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return \Shop\Domain\Identifier
     */
    public function getId()
    {
        return ProductId::fromString($this->id);
    }

    /**
     * @param \Shop\Domain\Entity\Product\ProductId $id
     */
    public function setId(ProductId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param $metaKeywords
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return bool
     */
    public function getHasVariants()
    {
        return $this->hasVariants;
    }

    /**
     * @param $hasVariants
     */
    public function setHasVariants($hasVariants)
    {
        $this->hasVariants = $hasVariants;
    }

    /**
     * @param \Shop\Domain\Entity\Product\Variant $variant
     *
     * @return bool
     */
    public function hasVariant(Variant $variant)
    {
        return $this->variants->contains($variant);
    }

    /**
     * @return bool
     */
    public function hasVariants()
    {
        return ! $this->variants->isEmpty();
    }

    /**
     * @param \Shop\Domain\Entity\Product\Variant $variant
     */
    public function addVariant(Variant $variant)
    {
        if (!$this->hasVariant($variant)) {
            $this->variants->add($variant);
        }
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getVariants()
    {
        return $this->variants;
    }

    /**
     * @param Option $option
     *
     * @return bool
     */
    public function hasOption(Option $option)
    {
        return $this->options->contains($option);
    }

    /**
     * @return bool
     */
    public function hasOptions()
    {
        return ! $this->options->isEmpty();
    }

    /**
     * @param Option $option
     */
    public function addOption(Option $option)
    {
        if (!$this->hasOption($option)) {
            $this->options->add($option);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return ArrayCollection
     */
    public function getOptionValues()
    {
        return $this->optionValues;
    }

    /**
     * @param \Shop\Domain\Entity\Product\OptionValue $optionValue
     *
     * @return bool
     */
    public function hasOptionValue(OptionValue $optionValue)
    {
        return $this->optionValues->contains($optionValue);
    }

    /**
     * @param \Shop\Domain\Entity\Product\OptionValue $optionValue
     */
    public function addOptionValue(OptionValue $optionValue)
    {
        if (!$this->hasOptionValue($optionValue)) {
            $this->optionValues->add($optionValue);
        }
    }

    /**
     * @return mixed|null
     */
    public function getMasterVariant()
    {
        foreach ($this->variants as $variant) {
            if ($variant->isMaster()) {
                return $variant;
            }
        }

        return null;
    }
}
