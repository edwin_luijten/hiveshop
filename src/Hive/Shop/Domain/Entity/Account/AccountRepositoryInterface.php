<?php namespace Shop\Domain\Entity\Account;

use Shop\Infrastructure\Repositories\RepositoryInterface;

interface AccountRepositoryInterface extends RepositoryInterface
{

}
