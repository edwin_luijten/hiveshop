<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\Plan\Plan;
use Shop\Domain\Entity\Plan\PlanRepositoryInterface;

class PlanRepository extends AbstractBaseRepository implements PlanRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        // TODO: Implement nextUuid() method.
    }
    
    public function getEntity()
    {
        return new Plan();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('p', 'pf')->from(Plan::class, 'p')
            ->leftJoin('p.features', 'pf')
            ->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        //@todo implement getPaginated
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('p', 'pf')->from(Plan::class, 'p')
            ->where(
                $queryBuilder->expr()->eq('p.id', ':id')
            )
            ->leftJoin('p.features', 'pf')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    public function getBySlug($slug)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('p', 'pf')->from(Plan::class, 'p')
            ->where(
                $queryBuilder->expr()->eq('p.slug', ':slug')
            )
            ->leftJoin('p.features', 'pf')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getSingleResult();
    }

    public function getCount()
    {
        //@todo implement getCount()
    }

}
