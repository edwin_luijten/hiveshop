<?php namespace Shop\Domain\Services\Geo;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Geo\Country;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Geo\Zone;
use Shop\Domain\Entity\Geo\ZoneRepositoryInterface;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;

class CreateCountry extends AbstractCreatorService implements ServiceInterface
{
    private $entityManager;
    private $countryRepository;
    private $zoneRepository;

    protected $country;

    public function __construct(
        EntityManager $entityManager,
        CountryRepositoryInterface $countryRepository,
        ZoneRepositoryInterface $zoneRepository
    ) {
        $this->entityManager = $entityManager;
        $this->countryRepository = $countryRepository;
        $this->zoneRepository = $zoneRepository;
    }

    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param       $name
     * @param       $code
     * @param       $tax
     * @param       $taxName
     * @param       $currency
     * @param       $unitSystem
     * @param       $zoneLabel
     * @param       $zoneKey
     * @param       $group
     * @param       $exampleZip
     * @param array $zones
     * @param       $enabled
     *
     * @return $this
     */
    public function create(
        $name,
        $code,
        $tax,
        $taxName,
        $currency,
        $unitSystem,
        $zoneLabel,
        $zoneKey,
        $group,
        $exampleZip,
        array $zones,
        $enabled = false
    ) {
        $id = $this->countryRepository->nextUuid();

        $country = new Country();
        $country->setName($name);
        $country->setCode($code);
        $country->setTax($tax);
        $country->setTaxName($taxName);
        $country->setCurrency($currency);
        $country->setUnitSystem($unitSystem);
        $country->setZoneLabel($zoneLabel);
        $country->setZoneKey($zoneKey);
        $country->setGroup($group);
        $country->setExampleZip($exampleZip);
        $country->setEnabled($enabled);

        if (! empty($zones)) {
            foreach ($zones as $z) {
                $zone = new Zone();

                $id = $this->zoneRepository->nextUuid();

                $zone->setId($id);
                $zone->setName($z['name']);
                $zone->setTax($z['tax']);
                $zone->setTaxName(! empty($z['tax_name']) ? $z['tax_name'] : null);
                $zone->setTaxType(! empty($z['tax_type']) ? $z['tax_type'] : null);
                $zone->setCode($z['code']);
                $zone->setCountry($country);
                $country->addZone($zone);
            }
        }

        $this->country = $country;

        return $this;
    }

    /**
     * persist or merge the entity
     */
    public function run()
    {
        $this->entityManager->persist($this->getCountry());

        return $this;
    }

    /**
     * flush the entity manager
     */
    public function flush()
    {
        $this->entityManager->flush();

        return $this->getCountry();
    }
}
