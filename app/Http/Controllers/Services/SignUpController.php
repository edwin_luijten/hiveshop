<?php namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Guard;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Shop\Domain\Entity\Account\AccountRepositoryInterface;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Geo\IpToCountryRepositoryInterface;
use Shop\Domain\Entity\Plan\PlanRepositoryInterface;
use Shop\Domain\Entity\Shipping\ShippingRateTypes;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;
use Shop\Domain\Events\Account\AccountWasCreated;
use Shop\Domain\Services\Account\AddStoreToAccountService;
use Shop\Domain\Services\Account\CreateAccount;
use Shop\Domain\Services\Account\CreateTrialAccount;
use Shop\Domain\Services\Account\UpdateAccount;
use Shop\Domain\Services\ServiceFactory;
use Shop\Domain\Services\Shipping\CreateShippingRate;
use Shop\Domain\Services\Store\CreateStore;
use Shop\Domain\Services\Store\UpdateSetting;
use Shop\Infrastructure\Services\TransactionService;

class SignUpController extends Controller
{

    private $response;
    private $urlGenerator;
    private $transactionService;
    private $auth;
    private $accountRepository;
    private $storeRepository;
    private $planRepository;
    private $ipToCountryRepository;
    private $countryRepository;
    public function __construct(
        Response $response,
        UrlGenerator $urlGenerator,
        TransactionService $transactionService,
        Guard $auth,
        AccountRepositoryInterface $accountRepository,
        StoreRepositoryInterface $storeRepository,
        PlanRepositoryInterface $planRepository,
        IpToCountryRepositoryInterface $ipToCountryRepository,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->response = $response;
        $this->urlGenerator = $urlGenerator;
        $this->transactionService = $transactionService;
        $this->auth = $auth;
        $this->accountRepository = $accountRepository;
        $this->storeRepository = $storeRepository;
        $this->planRepository = $planRepository;
        $this->ipToCountryRepository = $ipToCountryRepository;
        $this->countryRepository = $countryRepository;
    }

    /**
     * Create a new trial account
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     */
    public function postSetup(Request $request)
    {
        $input = $request->except('_token');

        if (empty($request->user())) {
            // Create account
            $accountService = new ServiceFactory(CreateTrialAccount::class);

            $account = $accountService->create($input['email'], $input['password'])->run()->flush();

            $this->auth->login($account);
        } else {
            $account = $request->user();
        }

        $signupParams['_g'] = $account->getId()->toString();
        $signupParams['email'] = $account->getEmail()->toString();
        $signupParams['store-name'] = $this->createSlug($input['store-name']);

        return view('hiveshop.pages.services.signup.setup', [
            'signupParams' => json_encode($signupParams),
        ]);
    }

    /**
     * Create a store and add it to the account
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function postCreate(Request $request)
    {
        $account = $request->user();

        if (is_null($request->user())) {
            return $this->response->create([
                'status'  => 'unavailable',
                'message' => 'Something went wrong',
                'host'    => null,
            ], 500);
        }

        $input = $request->except('_token');

        if (!$account->hasStore()) {
            // Get country based on ip lookup
            // @todo get real ip of client
            $locatedCountry = $this->ipToCountryRepository->getByIp($this->getClientDemoIp());

            // Create store with address and attach account
            // the creation and update are wrapped in a transaction
            $storeService = new ServiceFactory(CreateStore::class);
            $storeService->create(
                $input['store-name'],
                null,
                null,
                $locatedCountry->getCountryCode(),
                null,
                null,
                null,
                null,
                null,
                null,
                $account->getEmail()->toString(),
                $account->getEmail()->toString()
            );

            $accountService = new ServiceFactory(AddStoreToAccountService::class);
            $accountService->update($input['_g'], $storeService->getStore());

            // Create Default shipping rates for selected country
            $heavyShippingRateService = new ServiceFactory(CreateShippingRate::class);
            $heavyShippingRateService->create(
                $storeService->getStore(),
                _('Heavy Goods Shipping'),
                ShippingRateTypes::WEIGHT_BASED,
                5.0,
                20.0,
                null,
                null,
                2000,
                $storeService->getStore()->getAddress()->getCountry()->getCode()
            );

            $standardShippingRateService = new ServiceFactory(CreateShippingRate::class);
            $standardShippingRateService->create(
                $storeService->getStore(),
                _('Standard Shipping'),
                ShippingRateTypes::WEIGHT_BASED,
                0.0,
                5.0,
                null,
                null,
                1000,
                $storeService->getStore()->getAddress()->getCountry()->getCode()
            )->run()->flush();

            // Create Default shipping rates for rest of the world
            $internationalShippingRateService = new ServiceFactory(CreateShippingRate::class);
            $internationalShippingRateService->create(
                $storeService->getStore(),
                _('International Shipping'),
                ShippingRateTypes::WEIGHT_BASED,
                0.0,
                20.0,
                null,
                null,
                2000,
                null
            );

            $this->transactionService->addService($storeService->getService());
            $this->transactionService->addService($accountService->getService());
            $this->transactionService->addService($heavyShippingRateService->getService());
            $this->transactionService->addService($standardShippingRateService->getService());
            $this->transactionService->addService($internationalShippingRateService->getService());

            $this->transactionService->run();

            $account = $accountService->getAccount();

            //event(new AccountWasCreated($account));
        }

        $this->auth->loginUsingId($account->getId());
        $this->logAuthRequest($request, $request->user(), $request->user()->getStore());

        return json_encode([
            'signup' => [
                'admin_url' => $this->urlGenerator->route(
                    'admin.account-setup',
                    ['subdomain' => $account->getStore()->getSubdomain()]
                ),
            ]
        ]);
    }

    /**
     * Check if email or store name are taken
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCheckAvailability(Request $request)
    {
        $email = $request->input('email');
        $shopName = $request->input('store-name');

        $status = 'available';
        $httpStatus = 200;
        $message = null;
        $host = null;

        if (is_null($email) && ! is_null($shopName)) {
            try {
                $this->storeRepository->getBySubdomain($shopName);

                $status = 'unavailable';
                $httpStatus = 404;
            } catch (\Doctrine\ORM\NoResultException $e) {
                // sssshhtt
            }
        } elseif (is_null($shopName) && ! is_null($email)) {
            try {
                $this->accountRepository->getByEmail($email);

                $status = 'unavailable';
                $httpStatus = 404;
            } catch (\Doctrine\ORM\NoResultException $e) {
                // sssshhtt
            }
        }

        return $this->response->create([
            'status'  => $status,
            'message' => $message,
            'host'    => $host,
        ], $httpStatus);
    }
}
