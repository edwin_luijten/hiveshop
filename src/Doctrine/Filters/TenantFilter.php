<?php namespace Src\Doctrine\Filters;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class TenantFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        // Check if the entity uses the Tenant trait
        if (!$this->usesTenant($targetEntity->rootEntityName)) {
            return "";
        }

        return $targetTableAlias.'.tenant_id = ' . $this->getParameter('tenantId');
    }

    private function usesTenant($entity)
    {
        return array_key_exists('Src\Doctrine\Traits\Tenant', class_uses($entity));
    }
}