<?php namespace Shop\Domain\Services;

use Shop\Domain\ValueObjects\Password;

interface HashingService
{
    /**
     * Create a new hashed password
     *
     * @param Password $password
     * @return \Shop\Domain\ValueObjects\HashedPassword
     */
    public function hash(Password $password);
}
