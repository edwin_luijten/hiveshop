<?php namespace Shop\Domain\Services\Account;

use Shop\Domain\ValueObjects\Password;

interface HashingService
{
    /**
     * Create a new hashed password
     *
     * @param Password $password
     *
     * @return HashedPassword
     */
    public function hash(Password $password);
}
