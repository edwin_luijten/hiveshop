<?php namespace App\Http\Controllers\Shop\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileRequest;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\UrlGenerator;
use MyProject\Proxies\__CG__\stdClass;
use Shop\Domain\Entity\Account\AccountRepositoryInterface;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\History\AuthHistoryRepositoryInterface;
use Shop\Domain\Entity\Shipping\ShippingRateRepositoryInterface;
use Shop\Domain\Services\Account\UpdateAccount;
use Shop\Domain\Services\ServiceFactory;
use Shop\Domain\Services\Shipping\UpdateShippingRate;
use Shop\Domain\Services\Store\UpdateStore;

class ShippingController extends Controller
{
    private $response;
    private $urlGenerator;
    private $guard;
    private $countryRepository;
    private $shippingRateRepository;

    public function __construct(
        Response $response,
        UrlGenerator $urlGenerator,
        Guard $guard,
        CountryRepositoryInterface $countryRepository,
        ShippingRateRepositoryInterface $shippingRateRepository
    ) {
        $this->response = $response;
        $this->urlGenerator = $urlGenerator;
        $this->guard = $guard;
        $this->countryRepository = $countryRepository;
        $this->shippingRateRepository = $shippingRateRepository;
    }

    public function getShipping(Request $request)
    {
        $shippingRates = $this->shippingRateRepository->getByStoreId($request->user()->getStore()->getId()->toString());

        $rates = [];

        foreach ($shippingRates as $shippingRate) {
            if (!is_null($shippingRate->getCountry())) {
                $rates[$shippingRate->getCountry()->getId()]['name'] = $shippingRate->getCountry()->getName();
                $rates[$shippingRate->getCountry()->getId()]['code'] = $shippingRate->getCountry()->getCode();
                $rates[$shippingRate->getCountry()->getId()]['rates'][] = $shippingRate;
            } else {
                $rates['rest-of-the-world']['name'] = _('Rest of the world');
                $rates['rest-of-the-world']['code'] = 'rest';
                $rates['rest-of-the-world']['rates'][] = $shippingRate;
            }
        }

        return view('shop.admin.pages.settings-shipping', [
            'shippingRates' => $rates,
        ]);
    }

    public function postUpdateShipping(Request $request)
    {
        $input = $request->only(
            'name',
            'type',
            'weight_low',
            'weight_high',
            'min_order_subtotal',
            'max_order_subtotal',
            'price',
            'rate'
        );

        $shippingRateService = new ServiceFactory(UpdateShippingRate::class);
        $shippingRateService->update(
            $input['rate'],
            $input['name'],
            $input['type'],
            (!empty($input['weight_low']) ? $input['weight_low'] : null),
            (!empty($input['weight_high']) ? $input['weight_high'] : null),
            (!empty($input['min_order_subtotal']) ? $input['min_order_subtotal'] : null),
            (!empty($input['min_order_subtotal']) ? $input['min_order_subtotal'] : null),
            $input['price']
        )->run()->flush();

        if ($request->ajax()) {
            return $this->response->create([
                'message' => _('Settings saved'),
            ], 200);
        } else {
            return redirect()->route('admin.settings.shipping', [
                'subdomain' => $request->user()->getStore()->getSubdomain()
            ]);
        }
    }
}
