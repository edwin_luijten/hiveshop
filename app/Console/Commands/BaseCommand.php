<?php namespace App\Console\Commands;

use Illuminate\Console\Command;

abstract class BaseCommand extends Command
{
    protected function formatMemory($bytes)
    {
        return round($bytes / 1000 / 1000, 2) . ' MB';
    }

    protected function formatDuration($microseconds)
    {
        return $microseconds / 1000 . ' s';
    }
}