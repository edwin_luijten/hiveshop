@extends('shop.admin.template')

@section('page-breadcrumb')
    <ul>
        <li><i class="fa fa-cog"></i> {{ _('Settings') }} /</li>
        <li class="action">{{ _('General') }}</li>
    </ul>
@endsection

@section('content')
    <form id="general-settings" data-toggle="validator" action="{{ URL::route('admin.settings.general', ['subdomain' => $store->getSubdomain()]) }}" method="post">
    <header class="navbar navbar-fixed-top navbar-default">
        <ul class="nav navbar-nav-custom">
            <li class="hidden-xs animation-fadeInQuick navbar-breadcrumb">
                @yield('page-breadcrumb')
            </li>
        </ul>
        <ul class="nav navbar-nav-custom pull-right">
            <li><button type="submit" class="btn btn-default btn-effect-ripple disabled" disabled="disabled">{{ _('Save') }}</button></li>
        </ul>
        <div id="progress">

        </div>
    </header>
    <div id="page-content">
        <div class="row section">
            <div class="col-md-3">
                <h3>{{ _('Store details') }}</h3>

                <p class="text-muted"><small>{{ _('Edit your store information. The store name shows up on your storefront, while the title and meta description help define how your store shows up on search engines.') }}</small></p>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-section">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="store-name">{{ _('Name') }}</label>
                                    <input type="text" name="store-name" value="{{ $store->getName() }}" class="form-control" id="store-name" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">{{ _('Account email') }}</label>
                                    <input type="email" name="email" value="{{ $account->getEmail() }}" class="form-control" id="email" />
                                </div>
                                <small class="help-block">
                                    {{ _('Email used for Shopify to contact you about your account') }}
                                </small>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="customer-email">{{ _('Customer email') }}</label>
                                    <input type="email" name="customer-email" value="{{ $store->getSettings()->getCustomerEmail() }}" class="form-control" id="customer-email" />
                                </div>
                                <small class="help-block">
                                    {{ _('Your customers will see this when you email them (e.g.: order confirmations)') }}
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row section">
            <div class="col-md-3">
                <h3>{{ _('Store address') }}</h3>

                <p class="text-muted"><small>{{ _('This address will appear on your invoices and will be used to calculate your shipping rates.') }}</small></p>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-section">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="legal-name">{{ _('Legal name of business') }}</label>
                                    <input type="text" name="legal-name" value="{{ $store->getAddress()->getLegalName() }}" class="form-control" id="legal-name" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="phone">{{ _('Phone') }}</label>
                                    <input type="text" name="phone" value="{{ $store->getAddress()->getPhone() }}" class="form-control" id="phone" required="required" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="city">{{ _('Street') }}</label>
                                    <input type="text" name="street" value="{{ $store->getAddress()->getStreet() }}" class="form-control" id="street" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="city">{{ _('City') }}</label>
                                    <input type="text" name="city" value="{{ $store->getAddress()->getCity() }}" class="form-control" id="city" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="postal">{{ _('Postal/Zipcode') }}</label>
                                    <input type="text" name="postal" value="{{ $store->getAddress()->getPostal() }}" class="form-control" id="postal" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="country">{{ _('Country') }}</label>
                                    <select name="country" class="form-control" id="country">
                                        @foreach($countries as $country)
                                            <option value="{{ $country->getCode() }}" {{ ($store->getAddress()->getCountry()->getCode() === $country->getCode() ? 'selected="selected"': '') }}>{{ $country->getName() }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row section">
            <div class="col-md-3">
                <h3>{{ _('Standards & formats') }}</h3>

                <p class="text-muted"><small>{{ _('Standards and formats are used to calculate such things as product price, shipping weight and the time an order was placed.') }}</small></p>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-section">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="timezone">{{ _('Timezone') }}</label>
                                    <select name="timezone" class="select-chosen" placeholder="{{ _('Select timezone') }}" id="timezone">
                                        @foreach($timezones as $key => $timezone)
                                            <option value="{{ $key }}" {{ ($store->getSettings()->getTimezone() === $key ? 'selected="selected"' : '') }}>{{ $timezone }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="currency">{{ _('Currency') }}</label>
                                    <select name="currency"  class="select-chosen" placeholder="{{ _('Select currency') }}"  id="currency">
                                        @foreach($currencies as $key => $name)
                                            <option value="{{ $key }}" {{ ($store->getSettings()->getCurrency() === $key ? 'selected="selected"': '') }}>({{ $key }}) {{ $name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="unit-system">{{ _('Unit system') }}</label>
                                    <select name="unit-system" class="form-control" id="unit-system">
                                        <option value="imperial" {{ ($store->getSettings()->getUnitSystem() === 'imperial' ? 'selected="selected"': '') }}>{{ _('Imperial System') }}</option>
                                        <option value="metric" {{ ($store->getSettings()->getUnitSystem() === 'metric' ? 'selected="selected"': '') }}>{{ _('Metric System') }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="weight-unit">{{ _('Default Weight Unit') }}</label>
                                    <select name="weight-unit" class="form-control" id="weight-unit">
                                        @foreach($defaultWeightList as $key => $list)
                                            @if($store->getAddress()->getCountry()->getUnitSystem() == $key)
                                                @foreach($defaultWeightList[$key] as $unitKey => $weightUnit)
                                                    <option value="{{ $unitKey }}">{{ $weightUnit }}</option>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-12">
                                <strong>{{ _('Edit order ID format (optional)') }}</strong>
                                <p class="text-muted">{{ _('Order numbers start at #1001 by default. While you can\'t change the order number itself, you can add a prefix or suffix to create IDs like "EN1001" or "1001-A."') }}</p>
                                @set('orderIdPrefix', $store->getSettings()->getOrderIdPrefix())
                                @set('orderIdSuffix', $store->getSettings()->getOrderIdSuffix())
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="order-id-prefix">{{ _('Prefix') }}</label>
                                            <input type="text" name="order-id-prefix" value="{{ $orderIdPrefix }}" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="order-id-suffix">{{ _('Suffix') }}</label>
                                            <input type="text" name="order-id-suffix" value="{{ $orderIdSuffix }}" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <p class="text-muted">{{ sprintf(_('Your order ID will appear as %s1001%s, %s1002%s, %s1003%s ...'), $orderIdPrefix, $orderIdSuffix, $orderIdPrefix, $orderIdSuffix, $orderIdPrefix, $orderIdSuffix) }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="row">
            <div class="col-md-12 text-right">
                {!! csrf_field() !!}
                <button type="submit" class="btn btn-default btn-effect-ripple disabled" disabled="disabled">{{ _('Save') }}</button>
            </div>
        </footer>

    </div>

    </form>
@endsection

@section('script')
<script type="text/javascript">
    UI.form($('#general-settings'));
</script>
@endsection