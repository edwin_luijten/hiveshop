<?php namespace Shop\Domain\Services\Store;

use Doctrine\ORM\EntityManager;
use Illuminate\Support\Str;
use Shop\Domain\Entity\Geo\Address;
use Shop\Domain\Entity\Geo\AddressRepositoryInterface;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Store\Store;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;

class CreateSetting extends AbstractCreatorService implements ServiceInterface
{
    private $entityManager;
    private $settingRepository;
    protected $setting;

    public function __construct(
        EntityManager $entityManager,
        SettingRepositoryInterface $settingRepository
    ) {
        $this->entityManager = $entityManager;
        $this->settingRepository = $settingRepository;
    }

    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * @param string $name
     *
     * @return \Shop\Domain\Entity\Product\Store
     */
    public function create($name)
    {
        $id = $this->settingRepository->nextUuid();

        $store = new Store();
        $store->setId($id);
        $store->setName($name);
        $store->setSubdomain(Str::slug($name));

        $this->store = $store;

        return $this;
    }

    public function run()
    {
        $store = $this->getStore();

        // Store the store
        $this->entityManager->persist($store);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getStore();
    }
}
