<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

        <link href="/assets/c/css/shared.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/h/css/app.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/s/css/app.css" rel="stylesheet" type="text/css"/>
        <link href="/assets/sa/css/admin.css" rel="stylesheet" type="text/css"/>
        <script>
            window.paceOptions = {
                ajax: true,
                target: '#progress'
            }
        </script>
    </head>

    <body>
        <div id="page-wrapper" class="page-loading">
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">

                <div id="sidebar">
                    <div id="sidebar-brand" class="themed-background">
                        <a href="{{ URL::route('admin.dashboard', ['subdomain' => $store->getSubdomain()]) }}" class="sidebar-title">
                            <img src="/assets/c/img/hiveshop.svg" />

                        </a>
                    </div>
                    <div id="sidebar-scroll">
                        <!-- Sidebar Content -->
                        <div class="sidebar-content">
                            <!-- Sidebar Navigation -->
                            <ul class="sidebar-nav">
                                <li>
                                    <a href="{{ URL::route('admin.dashboard', ['subdomain' => $store->getSubdomain()]) }}" class="{{ is_active_url(['admin']) }}"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">{{ _('Dashboard') }}</span></a>
                                </li>

                                <li class="sidebar-separator">
                                    <i class="fa fa-ellipsis-h"></i>
                                </li>

                                <li class="selling-channel">
                                    <a href="javascript:void(0)" class="sidebar-nav-menu">
                                        <i class="fa fa-chevron-right sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                                        <i class="gi gi-globe sidebar-nav-icon"></i>
                                        <span class="sidebar-nav-mini-hide">{{ _('Online Store') }}</span>
                                    </a>
                                    <ul class="sidebar-nav-sub">
                                        <li class="sidebar-nav-header sidebar-nav-lg-hide">
                                            {{ _('Online Store') }}
                                        </li>
                                        <li>
                                            <a href="">{{ _('Overview') }}</a>
                                        </li>
                                        <li>
                                            <a href="">{{ _('Blog Posts') }}</a>
                                        </li>
                                        <li>
                                            <a href="">{{ _('Pages') }}</a>
                                        </li>
                                        <li>
                                            <a href="">{{ _('Themes') }}</a>
                                        </li>
                                        <li>
                                            <a href="">{{ _('Navigation') }}</a>
                                        </li>
                                        <li>
                                            <a href="">{{ _('Domains') }}</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="sidebar-separator">
                                    <i class="fa fa-ellipsis-h"></i>
                                </li>

                                <li>
                                    <a href="javascript:void(0)" class="sidebar-nav-menu {{ route_contains('settings') ? 'active' : '' }}">
                                        <i class="fa fa-chevron-right sidebar-nav-indicator sidebar-nav-mini-hide"></i>
                                        <i class="fa fa-cog sidebar-nav-icon"></i>
                                        <span class="sidebar-nav-mini-hide">{{ _('Settings') }}</span>
                                    </a>
                                    <ul class="sidebar-nav-sub">
                                        <li class="sidebar-nav-header sidebar-nav-lg-hide">
                                            {{ _('Settings') }}
                                        </li>
                                        <li>
                                            <a href="{{ URL::route('admin.settings.general', ['subdomain' => $store->getSubdomain()]) }}" class="{{ route_contains('general') ? 'active' : '' }}">
                                                {{ _('General') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::route('admin.settings.shipping', ['subdomain' => $store->getSubdomain()]) }}" class="{{ route_contains('shipping') ? 'active' : '' }}">
                                                {{ _('Shipping') }}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::route('admin.settings.account', ['subdomain' => $store->getSubdomain()]) }}" class="{{ route_contains('account') ? 'active' : '' }}">
                                                {{ _('Account') }}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div id="sidebar-extra-info" class="sidebar-content">
                        <div class="push-bit">
                            <div class="dropup">
                                <a href="javascript:void(0)" class="dropdown-toggle gravatar" data-toggle="dropdown">
                                    <img src="/assets/sa/img/no-gravatar.png" class="img-circle" />
                                    <span class="sidebar-nav-mini-hide">
                                        <small>
                                        {{ $store->getSubdomain() }}<br/>
                                        {{ $account->getFullName() }}
                                        </small>
                                    </span>
                                    <i class="caret sidebar-nav-mini-hide"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <li class="dropdown-header">
                                        <strong>ADMINISTRATOR</strong>
                                    </li>
                                    <li>
                                        <a href="page_app_email.php">
                                            <i class="fa fa-inbox fa-fw pull-right"></i>
                                            Inbox
                                        </a>
                                    </li>
                                    <li>
                                        <a href="page_app_social.php">
                                            <i class="fa fa-pencil-square fa-fw pull-right"></i>
                                            Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('admin.logout', ['subdomain' => $store->getSubdomain()]) }}">
                                            <i class="fa fa-power-off fa-fw pull-right"></i>
                                            Log out
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="main-container">
                    @yield('content')
                </div>
            </div>
        </div>
        @yield('outside-wrapper')


        <script src="/assets/c/js/shared.js" type="text/javascript"></script>
        <script src="/assets/h/js/app.js" type="text/javascript"></script>
        <script src="/assets/s/js/app.js" type="text/javascript"></script>
        <script src="/assets/sa/js/app.js" type="text/javascript"></script>

        @yield('script')

    </body>
</html>