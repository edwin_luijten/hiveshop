<?php namespace App\Seeds;

use Shop\Domain\Entity\Plan\FeatureRepositoryInterface;
use Shop\Domain\Entity\Plan\PlanRepositoryInterface;
use Shop\Domain\Services\ServiceFactory;
use Shop\Domain\Services\Plan\CreateFeature;
use Shop\Domain\Services\Plan\CreatePlan;

class PlansSeeder
{

    private $planRepository;
    private $featureRepository;

    public function __construct(PlanRepositoryInterface $planRepository, FeatureRepositoryInterface $featureRepository)
    {
        $this->planRepository = $planRepository;
        $this->featureRepository = $featureRepository;
    }

    public function seed()
    {
        $features = [
            [
                'name'        => 'Up to 25 products',
                'slug'        => 'up-to-25-products',
                'type'        => 'product-limit',
                'value'       => 25,
                'price'       => null,
                'description' => null,
            ],
            [
                'name'        => 'Unlimited products',
                'slug'        => 'unlimited-products',
                'type'        => 'product-limit',
                'value'       => 0,
                'price'       => null,
                'description' => null,
            ],
            [
                'name'        => '1 GB file storage',
                'slug'        => '1-gb-file-storage',
                'type'        => 'disk-space',
                'value'       => 1024,
                'price'       => null,
                'description' => null,
            ],
            [
                'name'        => '5 GB file storage',
                'slug'        => '5-gb-file-storage',
                'type'        => 'disk-space',
                'value'       => 5120,
                'price'       => null,
                'description' => null,
            ],
            [
                'name'        => 'Unlimited file storage',
                'slug'        => 'unlimited-file-storage',
                'type'        => 'disk-space',
                'value'       => 0,
                'price'       => null,
                'description' => null,
            ],
            [
                'name'        => '24/7 support',
                'slug'        => '24-7-support',
                'type'        => 'support',
                'value'       => 0,
                'price'       => null,
                'description' => null,
            ],
            [
                'name'        => 'Discount code engine',
                'slug'        => 'discount-code-engine',
                'type'        => 'addon',
                'value'       => 0,
                'price'       => null,
                'description' => null,
            ],
            [
                'name'        => 'No transaction fees',
                'slug'        => 'no-transaction-fees',
                'type'        => 'fee',
                'value'       => 0,
                'price'       => null,
                'description' => 'Use Hiveshop Payments and pay no transaction fee. If you choose an external payment
                gateway, there will be a 2.0% transaction fee.',
            ],
            [
                'name'        => 'Hiveshop POS',
                'slug'        => 'hiveshop-pos',
                'type'        => 'addon',
                'value'       => 0,
                'price'       => null,
                'description' => 'Sell in person using your iPhone or iPad and Hiveshop\'s free card reader.',
            ],
            [
                'name'        => 'Gift cards',
                'slug'        => 'gift-cards',
                'type'        => 'addon',
                'value'       => 0,
                'price'       => null,
                'description' => null,
            ],
        ];

        foreach ($features as $feature) {
            $createFeatureService = new ServiceFactory(CreateFeature::class);
            $createFeatureService->create($feature['name'], $feature['slug'], $feature['description'], $feature['type'],
                $feature['value'], $feature['price'])->run()->flush();
        }

        $plans = [
            [
                'name'                  => 'Trial',
                'slug'                  => 'trial',
                'description'           => '14 day trial.',
                'price'                 => 0,
                'highlight'             => false,
                'onlineFeePercentage'   => 0,
                'onlineFeePrice'        => 0,
                'inPersonFeePercentage' => null,
                'inPersonFeePrice'      => null,
                'features'              => [
                    $this->featureRepository->getBySlug('up-to-25-products'),
                    $this->featureRepository->getBySlug('1-gb-file-storage'),
                ]
            ],
            [
                'name'                  => 'Starter',
                'slug'                  => 'starter',
                'description'           => 'Starter plan',
                'price'                 => 1400,
                'highlight'             => false,
                'onlineFeePercentage'   => 2.9,
                'onlineFeePrice'        => 30,
                'inPersonFeePercentage' => null,
                'inPersonFeePrice'      => null,
                'features'              => [
                    $this->featureRepository->getBySlug('up-to-25-products'),
                    $this->featureRepository->getBySlug('1-gb-file-storage'),
                    $this->featureRepository->getBySlug('no-transaction-fees'),
                ]
            ],
            [
                'name'                  => 'Basic',
                'slug'                  => 'basic',
                'description'           => 'Start selling your products today with basic Hiveshop features.',
                'price'                 => 2900,
                'highlight'             => false,
                'onlineFeePercentage'   => 2.9,
                'onlineFeePrice'        => 30,
                'inPersonFeePercentage' => 2.7,
                'inPersonFeePrice'      => null,
                'features'              => [
                    $this->featureRepository->getBySlug('hiveshop-pos'),
                    $this->featureRepository->getBySlug('1-gb-file-storage'),
                    $this->featureRepository->getBySlug('unlimited-products'),
                    $this->featureRepository->getBySlug('24-7-support'),
                    $this->featureRepository->getBySlug('discount-code-engine'),
                    $this->featureRepository->getBySlug('no-transaction-fees'),
                ]
            ],
            [
                'name'                  => 'Professional',
                'slug'                  => 'professional',
                'description'           => 'Get more sales with features like gift cards, professional reports, fraud analysis tools and abandoned cart recovery.',
                'price'                 => 7900,
                'highlight'             => true,
                'onlineFeePercentage'   => 2.6,
                'onlineFeePrice'        => 30,
                'inPersonFeePercentage' => 2.4,
                'inPersonFeePrice'      => null,
                'features'              => [
                    $this->featureRepository->getBySlug('hiveshop-pos'),
                    $this->featureRepository->getBySlug('5-gb-file-storage'),
                    $this->featureRepository->getBySlug('unlimited-products'),
                    $this->featureRepository->getBySlug('24-7-support'),
                    $this->featureRepository->getBySlug('discount-code-engine'),
                    $this->featureRepository->getBySlug('no-transaction-fees'),
                    $this->featureRepository->getBySlug('gift-cards'),
                ]
            ],
            [
                'name'                  => 'Unlimited',
                'slug'                  => 'unlimited',
                'description'           => 'Save time and money with advanced report builder and carrier calculated shipping.',
                'price'                 => 17900,
                'highlight'             => false,
                'onlineFeePercentage'   => 2.4,
                'onlineFeePrice'        => 30,
                'inPersonFeePercentage' => 2.2,
                'inPersonFeePrice'      => null,
                'features'              => [
                    $this->featureRepository->getBySlug('hiveshop-pos'),
                    $this->featureRepository->getBySlug('unlimited-file-storage'),
                    $this->featureRepository->getBySlug('unlimited-products'),
                    $this->featureRepository->getBySlug('24-7-support'),
                    $this->featureRepository->getBySlug('discount-code-engine'),
                    $this->featureRepository->getBySlug('no-transaction-fees'),
                    $this->featureRepository->getBySlug('gift-cards'),
                ]
            ],
        ];

        foreach ($plans as $plan) {
            $createPlanService = new ServiceFactory(CreatePlan::class);
            $createPlanService->create(
                $plan['name'],
                $plan['slug'],
                $plan['description'],
                $plan['price'],
                $plan['highlight'],
                $plan['onlineFeePercentage'],
                $plan['onlineFeePrice'],
                $plan['inPersonFeePercentage'],
                $plan['inPersonFeePrice'],
                $plan['features']
            )->run()->flush();
        }
    }
}