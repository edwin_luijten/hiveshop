<?php namespace Shop\Domain\Services\Seeds;

use Shop\Domain\Entity\Product\OptionRepositoryInterface;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;
use Shop\Domain\Services\Product\CreateOption;

class OptionSeeder extends BaseSeeder {

    private $optionRepository;
    private $createOption;

    public function __construct(StoreRepositoryInterface $storeRepository, OptionRepositoryInterface $optionRepository, CreateOption $createOption)
    {
        $this->optionRepository = $optionRepository;
        $this->createOption = $createOption;

        parent::__construct($storeRepository);
    }

    public function seed()
    {
        $this->setRandomStore();

        $options = [
            'Size',
            'Material',
            'Color',
        ];

        foreach($options as $option)
        {
            if(! is_null($this->randomStore)) {
                $this->createOption->forStore($this->randomStore)->create($option);
            }
        }
    }
}