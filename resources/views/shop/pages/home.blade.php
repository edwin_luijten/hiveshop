@extends('shop.template')

@section('content')
<!-- head -->
<section>
    <div id="header-overview">
        <div class="container">
            <h1>Use Hive to create your online store</h1>
            <img src="/assets/h/img/header-overview@1x.png" />
        </div>
    </div>

    <!-- Form -->
    <div class="container">
        <h2 class="text-center">Start your 14 day trial!</h2>

        <div class="row">
            <form method="post" action="{{ URL::route('services.signup.setup') }}" class="marketing-form" id="user-signup">
                <div class="col-xs-12 col-md-2 col-md-offset-2">
                    <input type="email" name="email" class="form-control" placeholder="Your email" data-validation="required email">
                </div>
                <div class="col-xs-12 col-md-2">
                    <input type="text" name="password" class="form-control" placeholder="Password" data-validation="required">
                </div>
                <div class="col-xs-12 col-md-2">
                    <input type="text" name="store-name"class="form-control" placeholder="Store name" data-validation="required">
                </div>
                <div class="col-xs-12 col-md-2">

                    {!! csrf_field() !!}
                    <button type="submit" class="btn btn-success btn-large btn-block" name="register">Create your store</button>
                </div>
            </form>
        </div>
    </div>
    <!-- end form -->

    <div class="page-section">
        <h2>Your ecommerce website</h2>
        <span>Impress your customers with a beautiful online store and a secure shopping cart</span>
    </div>
</section>
<!-- end head -->


</section>
@endsection