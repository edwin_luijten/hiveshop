@extends('shop.admin.blank')

@section('content')
    <div id="login-container">
        <h1 class="h2 text-light text-center push-top-bottom animation-pullDown">
            <i class="fa fa-cube text-light-op"></i> <strong>{{ _('Hiveshop') }}</strong>
        </h1>
        <div class="block animation-fadeInQuick">
            <div class="block-title">
                <h2>{{ _('Please Login') }}</h2>
                <div id="loading"></div>
            </div>
            <form id="form-login" action="{{ URL::route('admin.login', ['subdomain' => $subdomain]) }}" data-toggle="validator" method="post" class="form-horizontal" novalidate="novalidate">
                <div class="form-group {{ ($errors->default->has('email') ? 'has-error': '') }}">
                    <div class="col-xs-12">
                        <label for="email">Email Address</label>
                        <input type="email" name="email" id="email" class="form-control" required="required" />
                        <small class="help-block with-errors">
                            @if($errors->default->has('email'))
                                @foreach($errors->default->get('email') as $message)
                                    {!! ucfirst($message) !!}
                                @endforeach
                            @endif
                        </small>
                    </div>
                </div>
                <div class="form-group {{ ($errors->default->has('password') ? 'has-error': '') }}">
                    <div class="col-xs-12">
                        <label for="password">Password</label>
                        <div class="input-group">
                            <input type="password" name="password" id="password" class="form-control" required="required" />
                                    <span class="input-group-btn">
                                       <a href="#" class="btn btn-default">Forgot?</a>
                                    </span>
                        </div>
                        <small class="help-block with-errors">
                            @if($errors->default->has('password'))
                                @foreach($errors->default->get('password') as $message)
                                    {!! ucfirst($message) !!}
                                @endforeach
                            @endif
                        </small>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-xs-8">
                        <label class="csscheckbox csscheckbox-primary">
                            <input type="checkbox" id="login-remember-me" name="login-remember-me"><span></span> Remember Me?
                        </label>
                    </div>
                    <div class="col-xs-4 text-right">
                        {!! csrf_field() !!}
                        <input type="hidden" name="subdomain" value="{{ $subdomain }}" />
                        <button type="submit" class="btn btn-effect-ripple btn-sm btn-success" style="overflow: hidden; position: relative;">Log In</button>
                    </div>
                </div>
            </form>
            <hr>
            <div class="push text-center">or Login with</div>
            <div class="row push">
                <div class="col-xs-6">
                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-sm btn-info btn-block" style="overflow: hidden; position: relative;"><i class="fa fa-facebook"></i> Facebook</a>
                </div>
                <div class="col-xs-6">
                    <a href="javascript:void(0)" class="btn btn-effect-ripple btn-sm btn-primary btn-block" style="overflow: hidden; position: relative;"><i class="fa fa-twitter"></i> Twitter</a>
                </div>
            </div>
        </div>
        <footer class="text-muted text-center animation-pullUp">
            <small><span id="year-copy">2014-15</span> � <a href="http://goo.gl/RcsdAh" target="_blank">AppUI 2.4</a></small>
        </footer>
    </div>
@endsection