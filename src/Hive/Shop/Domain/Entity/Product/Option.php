<?php namespace Shop\Domain\Entity\Product;

use Doctrine\Common\Collections\ArrayCollection;

use Shop\Domain\AggregateRoot;
use Shop\Domain\Entity\Store\Traits\StoreAware;
use Src\Doctrine\Traits\SoftDeletes;
use Src\Doctrine\Traits\Timestamps;

class Option implements AggregateRoot
{
    use Timestamps;
    use SoftDeletes;
    use StoreAware;

    /**
     * @var OptionId
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var ArrayCollection
     */
    private $values;

    public function __construct()
    {
        $this->values = new ArrayCollection();
    }

    /**
     * @return \Shop\Domain\Identifier
     */
    public function getId()
    {
        return OptionId::fromString($this->id);
    }

    /**
     * @param \Shop\Domain\Entity\Product\OptionId $id
     */
    public function setId(OptionId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @param \Shop\Domain\Entity\Product\OptionValue $optionValue
     *
     * @return bool
     */
    public function hasValue(OptionValue $optionValue)
    {
        return $this->values->contains($optionValue);
    }

    /**
     * @param \Shop\Domain\Entity\Product\OptionValue $optionValue
     */
    public function addValue(OptionValue $optionValue)
    {
        if (! $this->hasValue($optionValue)) {
            $this->values->add($optionValue);
        }
    }
}
