<?php namespace Shop\Domain\Services\Store;

use Doctrine\ORM\EntityManager;
use Illuminate\Support\Str;
use Shop\Domain\Entity\Geo\Address;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;
use Shop\Domain\Entity\Store\SettingId;
use Shop\Domain\Entity\Store\SettingRepositoryInterface;
use Shop\Domain\Entity\Store\StoreId;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;
use Shop\Domain\Services\AbstractUpdaterService;
use Shop\Domain\Services\Geo\CreateAddress;
use Shop\Domain\Services\ServiceFactory;
use Shop\Domain\Services\ServiceInterface;

class UpdateSetting extends AbstractUpdaterService implements ServiceInterface
{
    protected $setting;

    private $entityManager;
    private $settingRepository;
    private $countryRepository;

    public function __construct(
        EntityManager $entityManager,
        SettingRepositoryInterface $settingRepository,
        CountryRepositoryInterface $countryRepository
    ) {
        $this->entityManager = $entityManager;
        $this->settingRepository = $settingRepository;
        $this->countryRepository = $countryRepository;
    }

    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * @param \Shop\Domain\Entity\Store\SettingId $settingId
     * @param null $timezone
     * @param null $currency
     * @param null $weightUnit
     * @param null $accountEmail
     * @param null $customerEmail
     * @param null $unitSystem
     * @param null $orderIdPrefix
     * @param null $orderIdSuffix
     *
     * @return $this
     */
    public function update(
        SettingId $settingId,
        $timezone = null,
        $currency = null,
        $weightUnit = null,
        $accountEmail = null,
        $customerEmail = null,
        $unitSystem = null,
        $orderIdPrefix = null,
        $orderIdSuffix = null
    ) {
        $settings = $this->settingRepository->getById($settingId->toString());
        $settings->setTimezone($timezone);
        $settings->setCurrency($currency);
        $settings->setWeightUnit($weightUnit);
        $settings->setAccountEmail($accountEmail);
        $settings->setCustomerEmail($customerEmail);
        $settings->setUnitSystem($unitSystem);
        $settings->setOrderIdPrefix($orderIdPrefix);
        $settings->setOrderIdSuffix($orderIdSuffix);

        $this->setting = $settings;

        return $this;
    }

    public function run()
    {
        $store = $this->getSetting();

        $this->entityManager->merge($store);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getSetting();
    }
}
