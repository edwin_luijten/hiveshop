<?php namespace App\Http\Controllers\Shop\Admin\Subscription;

use App\Http\Controllers\Controller;
use Doctrine\ORM\NoResultException;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\UrlGenerator;
use Omnipay\Omnipay;
use Shop\Domain\Entity\Plan\PlanRepositoryInterface;

class SubscriptionController extends Controller
{
    private $response;
    private $urlGenerator;
    private $guard;
    private $planRepository;
    private $paymentGateway;

    public function __construct(
        Response $response,
        UrlGenerator $urlGenerator,
        Guard $guard,
        PlanRepositoryInterface $planRepository
    ) {
        $this->response = $response;
        $this->urlGenerator = $urlGenerator;
        $this->guard = $guard;
        $this->planRepository = $planRepository;

        $this->paymentGateway = Omnipay::create('PayPro');
        $this->paymentGateway->initialize([
                'apiKey'   => env('PAYPRO_API_KEY'),
                'testMode' => env('APP_DEBUG'),
            ]);
    }

    public function getPick()
    {
        return view('shop.admin.pages.subscription-pick', [
            'issuers' => array_where($this->paymentGateway->fetchIssuers()->send()->getIssuers(), function($key, $value){
                return ($value->getPaymentMethod() === 'ideal');
            }),
        ]);
    }

    public function postPick(Request $request)
    {
        $input = $request->only('');

        try {
            $plan = $this->planRepository->getBySlug('');

        } catch ( NoResultException $e) {

        }


    }
}
