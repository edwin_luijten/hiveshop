<?php namespace Shop\Domain\Events\Account;

use Shop\Domain\DomainEvent;
use Shop\Domain\Entity\Account\AccountId;
use Shop\Domain\ValueObjects\Email;

final class AccountWasCreated implements DomainEvent
{
    private $accountId;
    private $email;

    public function __construct(AccountId $accountId, Email $email)
    {
        $this->accountId = $accountId;
        $this->email = $email;
    }

    /**
     * @return \Shop\Domain\Entity\Account\AccountId
     */
    public function getAggregateId()
    {
        return $this->accountId;
    }

    /**
     * @return \Shop\Domain\ValueObjects\Email
     */
    public function getEmail()
    {
        return $this->email;
    }
}
