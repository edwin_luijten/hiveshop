<?php

namespace App\Http\Middleware;

use Closure;
use Locale;

class setSubdomain
{

    /**
     * Create a new filter instance.
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        view()->share('subdomain', $request->route('subdomain'));

        return $next($request);
    }
}
