<?php namespace Shop\Domain\Services\Seeds;

use Shop\Domain\Entity\Store\StoreRepositoryInterface;

abstract class BaseSeeder {

    protected $randomStore = null;
    protected $storeRepository;

    public function __construct(StoreRepositoryInterface $storeRepository)
    {
        $this->storeRepository = $storeRepository;
    }

    protected function setRandomStore()
    {
        $stores = $this->storeRepository->getAll();

        if(!empty($stores))
        {
            $nStores = (count($stores) - 1);
            $index = rand(0, $nStores);

            $this->randomStore = $stores[$index];
        }

    }
}