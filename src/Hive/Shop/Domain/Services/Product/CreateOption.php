<?php namespace Shop\Domain\Services\Product;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Product\Option;
use Shop\Domain\Entity\Product\OptionRepositoryInterface;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;
use Shop\Domain\Services\Traits\StoreAwareService;

class CreateOption extends AbstractCreatorService implements ServiceInterface
{
    use StoreAwareService;

    private $entityManager;
    private $optionRepository;

    protected $option;

    public function __construct(EntityManager $entityManager, OptionRepositoryInterface $optionRepository)
    {
        $this->entityManager = $entityManager;
        $this->optionRepository = $optionRepository;
    }

    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param string  $name
     *
     * @return \Shop\Domain\Entity\Product\Option
     */
    public function create($name)
    {
        $id = $this->optionRepository->nextUuid();

        $option = new Option();
        $option->setId($id);
        $option->setName($name);
        $option->setStore($this->store);

        $this->option = $option;

        return $this;
    }

    public function run()
    {
        $option = $this->getOption();

        // Store the store
        $this->entityManager->persist($option);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getOption();
    }
}
