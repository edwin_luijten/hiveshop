<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\Geo\Country;
use Shop\Domain\Entity\Geo\CountryId;
use Shop\Domain\Entity\Geo\CountryRepositoryInterface;

class CountryRepository extends AbstractBaseRepository implements CountryRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return CountryId::generate();
    }
    
    public function getEntity()
    {
        return new Country();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('c', 'cz')->from(Country::class, 'c')
            ->where($queryBuilder->expr()->eq('c.enabled', true))
            ->leftJoin('c.zones', 'cz')
            ->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        //@todo implement getPaginated
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('c', 'cz')->from(Country::class, 'c')
            ->where(
                $queryBuilder->expr()->eq('c.id', ':id')
            )
            ->leftJoin('c.zones', 'cz')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }


    public function getByName($name)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('c', 'cz')->from(Country::class, 'c')
            ->where(
                $queryBuilder->expr()->eq('c.name', ':name')
            )
            ->leftJoin('c.zones', 'cz')
            ->setParameter('name', $name)
            ->getQuery()
            ->getSingleResult();
    }

    public function getByCode($code)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('c', 'cz')->from(Country::class, 'c')
            ->where(
                $queryBuilder->expr()->eq('c.code', ':code')
            )
            ->leftJoin('c.zones', 'cz')
            ->setParameter('code', $code)
            ->getQuery()
            ->getSingleResult();
    }

    public function getCount()
    {
        //@todo implement getCount()
    }
}
