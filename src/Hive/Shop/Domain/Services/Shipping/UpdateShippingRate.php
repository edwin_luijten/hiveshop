<?php namespace Shop\Domain\Services\Shipping;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Shipping\ShippingRateId;
use Shop\Domain\Entity\Shipping\ShippingRateRepositoryInterface;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;

class UpdateShippingRate extends AbstractCreatorService implements ServiceInterface
{
    private $entityManager;
    private $shippingRateRepository;
    protected $shippingRate;

    public function __construct(
        EntityManager $entityManager,
        ShippingRateRepositoryInterface $shippingRateRepository
    ) {
        $this->entityManager = $entityManager;
        $this->shippingRateRepository = $shippingRateRepository;
    }

    public function getShippingRate()
    {
        return $this->shippingRate;
    }

    /**
     * @param \Shop\Domain\Entity\Shipping\ShippingRateId $shippingRateId
     * @param                                             $name
     * @param                                             $type
     * @param null                                        $weightLow
     * @param null                                        $weightHigh
     * @param null                                        $minOrderSubtotal
     * @param null                                        $maxOrderSubtotal
     * @param null                                        $price
     *
     * @return $this
     */
    public function update(
        ShippingRateId $shippingRateId,
        $name,
        $type,
        $weightLow = null,
        $weightHigh = null,
        $minOrderSubtotal = null,
        $maxOrderSubtotal = null,
        $price = null
    ) {
        $shippingRate = $this->shippingRateRepository->getById($shippingRateId->toString());

        $shippingRate->setName($name);
        $shippingRate->setType($type);
        $shippingRate->setWeightLow($weightLow);
        $shippingRate->setWeightHigh($weightHigh);
        $shippingRate->setMinOrderSubtotal($minOrderSubtotal);
        $shippingRate->setMaxOrderSubtotal($maxOrderSubtotal);
        $shippingRate->setPrice($price);

        $this->shippingRate = $shippingRate;

        return $this;
    }

    public function run()
    {
        $shippingRate = $this->getShippingRate();

        $this->entityManager->merge($shippingRate);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getShippingRate();
    }
}
