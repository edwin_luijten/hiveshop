@extends('hiveshop.template')

@section('content')

<section>
    <div class="container">
        <div class="page-section">
            <header class="text-center">
                    <h1>Login to your store</h1>
            </header>

            <form class="col-xs-6 col-xs-offset-3 login-form" method="post" action="{{ URL::route('login') }}" data-toggle="validator">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group {{ ($errors->default->has('subdomain') ? 'has-error': '') }}">
                            <label for="subdomain">Store Address</label>
                            <div class="input-group">
                                <input type="text" name="subdomain" id="subdomain" class="form-control" required="required" />
                                <span class="input-group-addon">.{{ config('app.domain') }}</span>
                            </div>
                            <small class="help-block with-errors">
                                @if($errors->default->has('subdomain'))
                                    @foreach($errors->default->get('subdomain') as $message)
                                        {!! ucfirst($message) !!}
                                    @endforeach
                                @endif
                            </small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group {{ ($errors->default->has('email') ? 'has-error': '') }}">
                            <label for="email">Email Address</label>
                            <input type="email" name="email" id="email" class="form-control" required="required" />
                            <small class="help-block with-errors">
                                @if($errors->default->has('email'))
                                    @foreach($errors->default->get('email') as $message)
                                        {!! ucfirst($message) !!}
                                    @endforeach
                                @endif
                            </small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group {{ ($errors->default->has('password') ? 'has-error': '') }}">
                            <label for="password">Password</label>
                            <div class="input-group">
                                <input type="password" name="password" id="password" class="form-control" required="required" />
                                <span class="input-group-btn">
                                   <a href="#" class="btn btn-default">Forgot?</a>
                                </span>
                            </div>
                            <small class="help-block with-errors">
                                @if($errors->default->has('password'))
                                    @foreach($errors->default->get('password') as $message)
                                        {!! ucfirst($message) !!}
                                    @endforeach
                                @endif
                            </small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <footer class="col-xs-12 text-right">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-success">Login</button>
                    </footer>
                </div>
            </form>
        </div>
    </div>
</section>

@endsection