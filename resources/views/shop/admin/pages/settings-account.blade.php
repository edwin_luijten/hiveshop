@extends('shop.admin.template')

@section('page-breadcrumb')
    <ul>
        <li><i class="fa fa-cog"></i> {{ _('Settings') }} /</li>
        <li class="action">{{ _('Account') }}</li>
    </ul>
@endsection

@section('content')
    <form id="account-settings" data-toggle="validator" action="{{ URL::route('admin.settings.account', ['subdomain' => $store->getSubdomain()]) }}" method="post">
    <header class="navbar navbar-fixed-top navbar-default">
        <ul class="nav navbar-nav-custom">
            <li class="hidden-xs animation-fadeInQuick navbar-breadcrumb">
                @yield('page-breadcrumb')
            </li>
        </ul>
        <ul class="nav navbar-nav-custom pull-right">
            <li><button type="submit" class="btn btn-default btn-effect-ripple disabled" disabled="disabled">{{ _('Save') }}</button></li>
        </ul>
        <div id="progress">

        </div>
    </header>
    <div id="page-content">
        <div class="row section">
            <div class="col-md-3">
                <h3>{{ _('Account overview') }}</h3>

                <p class="text-muted"><small>{{ _('An overview of your current plan. Grow your business by upgrading your plan features. View our terms of service and privacy policy.') }}</small></p>

                <a href="{{ URL::route('admin.settings.account.plan', ['subdomain' => $subdomain]) }}" class="btn btn-default btn-effect-ripple">{{ _('Change plan type') }}</a>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-section">
                        <div class="row">
                            <div class="col-md-4">
                                <strong>{{ _('Member since') }}</strong><br/>
                                {{ $account->getCreatedAt()->format('Y-m-d') }}
                            </div>
                            <div class="col-md-4">
                                <strong>{{ _('Current plan') }}</strong><br/>
                                {{ $account->getSubscription()->getPlan()->getName() }}
                            </div>
                            <div class="col-md-4">
                                <strong>{{ _('Account status') }}</strong>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row section">
            <div class="col-md-3">
                <h3>{{ _('Billing information') }}</h3>

                <p class="text-muted"><small>{{ _('A summary of your payment information, including any credit cards linked to your Hiveshop account.') }}</small></p>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-section">

                    </div>
                </div>
            </div>
        </div>

        <div class="row section">
            <div class="col-md-3">
                <h3>{{ _('Staff members') }}</h3>

                <p class="text-muted"><small>{{ _('You can give other people admin access to your Hiveshop store.') }}</small></p>
            </div>
            <div class="col-md-9">
                <div class="block">
                    <div class="block-section">
                        <table class="table">
                            <thead>
                                <td>{{ _('Name') }}</td>
                                <td>{{ _('Email') }}</td>
                                <td>{{ _('Admin Access') }}</td>
                                <td>{{ _('Last Login Date') }}</td>
                            </thead>
                            <tbody>
                                @foreach($accounts as $account)
                                <tr>
                                    <td><a href="{{ URL::route('admin.settings.account.profile', ['subdomain' => $store->getSubdomain(), 'id' => $account->getId()->toString()]) }}">{{ $account->getFullName() }}</a></td>
                                    <td>{{ $account->getEmail() }}</td>
                                    <td>{{ _('Account owner') }}</td>
                                    <td>
                                        @if(!empty($account->getAuthHistory()))
                                            @foreach($account->getAuthHistory() as $history)
                                             {{ $history->getCreatedAt()->format('d-m-Y H:i:s') }}
                                                <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="{{ $history->getBrowser() }} {{ $history->getOs() }}"></i>
                                            @endforeach
                                        @endif

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <footer class="row">
            <div class="col-md-12 text-right">
                {!! csrf_field() !!}
                <button type="submit" class="btn btn-default btn-effect-ripple disabled" disabled="disabled">{{ _('Save') }}</button>
            </div>
        </footer>

    </div>

    </form>
@endsection

@section('script')
<script type="text/javascript">
    UI.form($('#account-settings'));
</script>
@endsection