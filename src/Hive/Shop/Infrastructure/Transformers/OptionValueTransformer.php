<?php namespace Shop\Infrastructure\Transformers;

use League\Fractal\TransformerAbstract;
use Shop\Domain\Entity\Product\OptionValue;

class OptionValueTransformer extends TransformerAbstract
{
    public function transform(OptionValue $optionValue)
    {
        return [
            'id'              => $optionValue->getId()->toString(),
            'value'            => $optionValue->getValue(),
            'optionId' => $optionValue->getOption()->getId()->toString(),
        ];
    }
}
