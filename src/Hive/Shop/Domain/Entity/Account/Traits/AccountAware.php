<?php namespace Shop\Domain\Entity\Store\Traits;

use Doctrine\ORM\Mapping as ORM;
use Shop\Domain\Entity\Account\Account;

trait AccountAware
{
    /**
     * @ORM\ManyToOne(targetEntity="Shop\Domain\Entity\Account\Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=true)
     */
    private $account;

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param \Shop\Domain\Entity\Account\Account $account
     */
    public function setAccount(Account $account = null)
    {
        $this->account = $account;
    }
}
