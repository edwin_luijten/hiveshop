<h1>Welcome!</h1>

{{ \Carbon\Carbon::now()->format('Y-m-d H:i:s') }}

<p>
    Please confirm your account.
</p>

{{ $account->getEmail() }}