<?php namespace Shop\Domain\Entity\Store;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Shop\Domain\AggregateRoot;
use Shop\Domain\Entity\Geo\Address;
use Shop\Domain\Identifier;
use Src\Doctrine\Traits\SoftDeletes;
use Src\Doctrine\Traits\Timestamps;

class Store implements AggregateRoot
{
    use Timestamps;
    use SoftDeletes;

    /**
     * @var StoreId
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $subdomain;

    /**
     * @var \Shop\Domain\Entity\Geo\Address
     */
    private $address;

    /**
     * @var Setting
     */
    private $settings;

    /**
     * @var ArrayCollection
     */
    private $shippingRates;

    public function __construct()
    {
        $this->shippingRates = new ArrayCollection();
    }

    /**
     * Return the Aggregate Root identifer
     *
     * @return Identifier
     */
    public function getId()
    {
        return StoreId::fromString($this->id);
    }

    /**
     * @param \Shop\Domain\Entity\Store\StoreId $id
     */
    public function setId(StoreId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSubdomain()
    {
        return $this->subdomain;
    }

    /**
     * @param $subdomain
     */
    public function setSubdomain($subdomain)
    {
        $this->subdomain = $subdomain;
    }

    /**
     * @return \Shop\Domain\Entity\Geo\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param \Shop\Domain\Entity\Geo\Address $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return Setting
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param \Shop\Domain\Entity\Store\Setting $setting
     */
    public function setSettings(Setting $setting)
    {
        $this->settings = $setting;
    }
}
