<?php namespace Shop\Domain\Entity\Product;

use Shop\Domain\AggregateRoot;
use Src\Doctrine\Traits\SoftDeletes;
use Src\Doctrine\Traits\Timestamps;

class OptionValue implements AggregateRoot
{
    use Timestamps;
    use SoftDeletes;

    /**
     * @var OptionValueId
     */
    private $id;
    /**
     * @var string
     */
    private $value;
    /**
     * @var Option
     */
    private $option;

    /**
     * @return \Shop\Domain\Identifier
     */
    public function getId()
    {
        return OptionValueId::fromString($this->id);
    }

    /**
     * @param \Shop\Domain\Entity\Product\OptionValueId $id
     */
    public function setId(OptionValueId $id)
    {
        $this->id = $id->toString();
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return \Shop\Domain\Entity\Product\Option
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param \Shop\Domain\Entity\Product\Option $option
     */
    public function setOption(Option $option)
    {
        $this->option = $option;
    }
}
