<?php

namespace App\Providers;

use EntityManager;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use JMS\Serializer\Builder\CallbackDriverFactory;
use JMS\Serializer\Metadata\Driver\AnnotationDriver;
use Mitch\LaravelDoctrine\IlluminateRegistry;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $debugStack = new \Doctrine\DBAL\Logging\DebugStack();
        EntityManager::getConnection()->getConfiguration()->setSQLLogger($debugStack);
        $debugbar = app('debugbar');
        $debugbar->addCollector(new \DebugBar\Bridge\DoctrineCollector($debugStack));


        $this->app[AuthManager::class]->extend('doctrine', function ($app) {
            return new \Src\Doctrine\DoctrineUserProvider(
                $app['Illuminate\Contracts\Hashing\Hasher'],
                $app[IlluminateRegistry::class],
                config('auth.model')
            );
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Shop\Domain\Services\HashingService', 'Shop\Infrastructure\Services\BcryptHashingService');
        $this->app->bind('Shop\Domain\Entity\Store\StoreRepositoryInterface', 'Shop\Infrastructure\Repositories\StoreRepository');
        $this->app->bind('Shop\Domain\Entity\Store\SettingRepositoryInterface', 'Shop\Infrastructure\Repositories\SettingRepository');
        $this->app->bind('Shop\Domain\Entity\Product\ProductRepositoryInterface', 'Shop\Infrastructure\Repositories\ProductRepository');
        $this->app->bind('Shop\Domain\Entity\Product\VariantRepositoryInterface', 'Shop\Infrastructure\Repositories\VariantRepository');
        $this->app->bind('Shop\Domain\Entity\Product\OptionRepositoryInterface', 'Shop\Infrastructure\Repositories\OptionRepository');
        $this->app->bind('Shop\Domain\Entity\Product\OptionValueRepositoryInterface', 'Shop\Infrastructure\Repositories\OptionValueRepository');
        $this->app->bind('Shop\Domain\Entity\Account\AccountRepositoryInterface', 'Shop\Infrastructure\Repositories\AccountRepository');
        $this->app->bind('Shop\Domain\Entity\Geo\CountryRepositoryInterface', 'Shop\Infrastructure\Repositories\CountryRepository');
        $this->app->bind('Shop\Domain\Entity\Geo\ZoneRepositoryInterface', 'Shop\Infrastructure\Repositories\ZoneRepository');
        $this->app->bind('Shop\Domain\Entity\Geo\AddressRepositoryInterface', 'Shop\Infrastructure\Repositories\AddressRepository');
        $this->app->bind('Shop\Domain\Entity\Geo\IpToCountryRepositoryInterface', 'Shop\Infrastructure\Repositories\IpToCountryRepository');
        $this->app->bind('Shop\Domain\Entity\Plan\PlanRepositoryInterface', 'Shop\Infrastructure\Repositories\PlanRepository');
        $this->app->bind('Shop\Domain\Entity\Plan\FeatureRepositoryInterface', 'Shop\Infrastructure\Repositories\FeatureRepository');
        $this->app->bind('Shop\Domain\Entity\History\AuthHistoryRepositoryInterface', 'Shop\Infrastructure\Repositories\AuthHistoryRepository');
        $this->app->bind('Shop\Domain\Entity\Shipping\ShippingRateRepositoryInterface', 'Shop\Infrastructure\Repositories\ShippingRateRepository');

        $this->app->singleton(\JMS\Serializer\Serializer::class, function(){

            return \JMS\Serializer\SerializerBuilder::create()
                ->setCacheDir(storage_path('app/cache/serializer'))
                ->addMetadataDir(base_path('resources/serializer'))
                ->build();
        });
    }
}
