<?php namespace Shop\Domain\Entity\Store\Traits;

use Shop\Domain\Entity\Store\Store;

trait StoreAware
{
    private $store;

    /**
     * @return \Shop\Domain\Entity\Store\Store
     */
    public function getStore()
    {
        return $this->store;
    }
    /**
     * @param Store $store
     */
    public function setStore(Store $store = null)
    {
        $this->store = $store;
    }

    public function hasStore()
    {
        return !is_null($this->store);
    }
}
