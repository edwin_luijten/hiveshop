var hn = /#.*$/, fn = /([?&])_=[^&]*/, dn = /^(.*?):[ \t]*([^\r\n]*)$/gm, mn = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, gn = /^(?:GET|HEAD)$/, vn = /^\/\//, yn = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, bn = {}, wn = {}, _n = "*/".concat("*"), xn = window.location.href, kn = yn.exec(xn.toLowerCase()) || [];

$.ajaxSetup({
	url: xn,
	type: "GET",
	isLocal: mn.test(kn[1]),
	global: !0,
	processData: !0,
	async: !0,
	contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	accepts: {
		"*": _n,
		text: "text/plain",
		html: "text/html",
		xml: "application/xml, text/xml",
		json: "application/json, text/javascript"
	},
	contents: {xml: /xml/, html: /html/, json: /json/},
	responseFields: {xml: "responseXML", text: "responseText", json: "responseJSON"},
	converters: {"* text": String, "text html": !0, "text json": $.parseJSON, "text xml": $.parseXML},
	flatOptions: {url: !0, context: !0},
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});


if (!Array.prototype.find) {
	Array.prototype.find = function(predicate) {
		if (this == null) {
			throw new TypeError('Array.prototype.find called on null or undefined');
		}
		if (typeof predicate !== 'function') {
			throw new TypeError('predicate must be a function');
		}
		var list = Object(this);
		var length = list.length >>> 0;
		var thisArg = arguments[1];
		var value;

		for (var i = 0; i < length; i++) {
			value = list[i];
			if (predicate.call(thisArg, value, i, list)) {
				return value;
			}
		}
		return undefined;
	};
}

var Application = new function () {
	this.init = function () {
		return this.setup();
	};
	this.setup = function () {

	};

};

Application.init();