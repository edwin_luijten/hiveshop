<?php namespace Src\Doctrine\Traits;

use DateTime;

trait Timestamps
{
    private $createdAt;

    private $updatedAt;

    public function prePersist()
    {
        $now = new Datetime;
        $this->createdAt = $now;
        $this->updatedAt = $now;
    }

    public function preUpdate()
    {
        $this->updatedAt = new DateTime;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}