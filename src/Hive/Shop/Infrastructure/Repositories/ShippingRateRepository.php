<?php namespace Shop\Infrastructure\Repositories;

use Doctrine\ORM\EntityManager;

use Illuminate\Contracts\Cache\Factory;
use Shop\Domain\Entity\Shipping\ShippingRate;
use Shop\Domain\Entity\Shipping\ShippingRateId;
use Shop\Domain\Entity\Shipping\ShippingRateRepositoryInterface;

class ShippingRateRepository extends AbstractBaseRepository implements ShippingRateRepositoryInterface
{
    public function __construct(EntityManager $entityManager, Factory $cacheManager)
    {
        parent::__construct($entityManager, $cacheManager);
    }

    public function nextUuid()
    {
        return ShippingRateId::generate();
    }

    public function getEntity()
    {
        return new ShippingRate();
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('s')->from(ShippingRate::class, 's')->getQuery()->execute();
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function getPaginated($page = 1, $limit = 1)
    {
        //@todo implement getPaginated
    }

    public function getById($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('s')->from(ShippingRate::class, 's')
            ->where(
                $queryBuilder->expr()->eq('s.id', ':id')
            )
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    public function getByStoreId($id)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        return $queryBuilder->select('s', 'sc')->from(ShippingRate::class, 's')
            ->where(
                $queryBuilder->expr()->eq('s.store', ':id')
            )
            ->setParameter('id', $id)
            ->leftJoin('s.country', 'sc')
            ->getQuery()
            ->execute();
    }

    public function getCount()
    {
        //@todo implement getCount()
    }
}
