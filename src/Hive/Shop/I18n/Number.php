<?php namespace Shop\I18n;

class Number
{

    public $defaultLocale = 'en_US';
    protected $defaultCurrency;
    private $formatters = [];
    private $locale;

    public function __construct()
    {
        $this->locale = \Locale::getDefault();
    }

    /**
     * Formats a number into the correct locale format
     *
     * Options:
     *
     * - `places` - Minimum number or decimals to use, e.g 0
     * - `precision` - Maximum Number of decimal places to use, e.g. 2
     * - `pattern` - An ICU number pattern to use for formatting the number. e.g #,###.00
     * - `locale` - The locale name to use for formatting the number, e.g. fr_FR
     * - `before` - The string to place before whole numbers, e.g. '['
     * - `after` - The string to place after decimal numbers, e.g. ']'
     *
     * @param float $value   A floating point number.
     * @param array $options An array with options.
     *
     * @return string Formatted number
     */
    public function format($value, array $options = [])
    {
        $formatter = $this->formatter($options);
        $options += ['before' => '', 'after' => ''];

        return $options['before'] . $formatter->format($value) . $options['after'];
    }

    /**
     * Formats a number into a currency format.
     *
     * ### Options
     *
     * - `locale` - The locale name to use for formatting the number, e.g. fr_FR
     * - `fractionSymbol` - The currency symbol to use for fractional numbers.
     * - `fractionPosition` - The position the fraction symbol should be placed
     *    valid options are 'before' & 'after'.
     * - `before` - Text to display before the rendered number
     * - `after` - Text to display after the rendered number
     * - `zero` - The text to use for zero values, can be a string or a number. e.g. 0, 'Free!'
     * - `places` - Number of decimal places to use. e.g. 2
     * - `precision` - Maximum Number of decimal places to use, e.g. 2
     * - `pattern` - An ICU number pattern to use for formatting the number. e.g #,###.00
     * - `useIntlCode` - Whether or not to replace the currency symbol with the international
     *   currency code.
     *
     * @param float       $value    Value to format.
     * @param string|null $currency International currency name such as 'USD', 'EUR', 'JPY', 'CAD'
     * @param array       $options  Options list.
     *
     * @return string Number formatted as a currency.
     */
    public function currency($value, $currency = null, array $options = [])
    {
        $value = (float) $value / 100;
        $currency = $currency ?: $this->defaultCurrency();

        if (isset($options['zero']) && ! $value) {
            return $options['zero'];
        }

        $formatter = $this->formatter(['type' => 'currency'] + $options);

        $abs = abs($value);

        if (! empty($options['fractionSymbol']) && $abs > 0 && $abs < 1) {
            $value = $value * 100;
            $pos = isset($options['fractionPosition']) ? $options['fractionPosition'] : 'after';

            return $this->format($value, ['precision' => 0, $pos => $options['fractionSymbol']]);
        }

        $before = isset($options['before']) ? $options['before'] : null;
        $after = isset($options['after']) ? $options['after'] : null;

        return $before . $formatter->formatCurrency($value, $currency) . $after;
    }

    /**
     * Getter/setter for default currency
     *
     * @param string|bool|null $currency Default currency string to be used by currency()
     *                                   if $currency argument is not provided. If boolean false is passed, it will
     *                                   clear the currently stored value
     *
     * @return string Currency
     */
    public function defaultCurrency($currency = null)
    {
        if (! empty($currency)) {
            return $this->defaultCurrency = $currency;
        }

        if ($currency === false) {
            return $this->defaultCurrency = null;
        }

        if (empty($this->defaultCurrency)) {
            $locale = ini_get('intl.default_locale') ?: $this->defaultLocale;
            $formatter = new \NumberFormatter($locale, \NumberFormatter::CURRENCY);

            $this->defaultCurrency = $formatter->getTextAttribute(\NumberFormatter::CURRENCY_CODE);
        }

        return $this->defaultCurrency;
    }

    /**
     * Returns a formatter object that can be reused for similar formatting task
     * under the same locale and options. This is often a speedier alternative to
     * using other methods in this class as only one formatter object needs to be
     * constructed.
     *
     * The options array accepts the following keys:
     *
     * - `locale` - The locale name to use for formatting the number, e.g. fr_FR
     * - `type` - The formatter type to construct, set it to `currency` if you need to format
     *    numbers representing money.
     * - `places` - Number of decimal places to use. e.g. 2
     * - `precision` - Maximum Number of decimal places to use, e.g. 2
     * - `pattern` - An ICU number pattern to use for formatting the number. e.g #,###.00
     * - `useIntlCode` - Whether or not to replace the currency symbol with the international
     *   currency code.
     *
     * @param array $options An array with options.
     *
     * @return \NumberFormatter The configured formatter instance
     */
    public function formatter($options = [])
    {
        $this->locale = isset($options['locale']) ? $options['locale'] : ini_get('intl.default_locale');

        if (! $this->locale) {
            $this->locale = $this->defaultLocale;
        }

        $type = \NumberFormatter::DECIMAL;

        if (! empty($options['type']) && $options['type'] === 'currency') {
            $type = \NumberFormatter::CURRENCY;
        }

        if (! isset($this->formatters[$this->locale][$type])) {
            $this->formatters[$this->locale][$type] = new \NumberFormatter($this->locale, $type);
        }

        $formatter = $this->formatters[$this->locale][$type];

        $hasPlaces = isset($options['places']);
        $hasPrecision = isset($options['precision']);
        $hasPattern = ! empty($options['pattern']) || ! empty($options['useIntlCode']);

        if ($hasPlaces || $hasPrecision || $hasPattern) {
            $formatter = clone $formatter;
        }

        if ($hasPlaces) {

            $formatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, $options['places']);
        }

        if ($hasPrecision) {
            $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, $options['precision']);
        }

        if (! empty($options['pattern'])) {
            $formatter->setPattern($options['pattern']);
        }

        if (! empty($options['useIntlCode'])) {
            // One of the odd things about ICU is that the currency marker in patterns
            // is denoted with ¤, whereas the international code is marked with ¤¤,
            // in order to use the code we need to simply duplicate the character wherever
            // it appears in the pattern.
            $pattern = trim(str_replace('¤', '¤¤ ', $formatter->getPattern()));
            $formatter->setPattern($pattern);
        }

        return $formatter;
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    public function getLocale()
    {
        return $this->locale;
    }
}
