<?php namespace Shop\Domain\Services\Geo;

use Doctrine\ORM\EntityManager;
use Shop\Domain\Entity\Account\Account;
use Shop\Domain\Entity\History\AuthHistory;
use Shop\Domain\Entity\History\AuthHistoryId;
use Shop\Domain\Entity\History\AuthHistoryRepositoryInterface;
use Shop\Domain\Entity\Store\Store;
use Shop\Domain\Services\AbstractCreatorService;
use Shop\Domain\Services\ServiceInterface;

class LogAuthRequest extends AbstractCreatorService implements ServiceInterface
{
    private $entityManager;
    private $authHistoryRepository;
    protected $authHistory;

    public function __construct(
        EntityManager $entityManager,
        AuthHistoryRepositoryInterface $authHistoryRepository
    ) {
        $this->entityManager = $entityManager;
        $this->authHistoryRepository = $authHistoryRepository;
    }

    public function getAuthHistory()
    {
        return $this->authHistory;
    }

    /**
     * @param Account $account
     * @param Store $store
     * @param null $ip
     * @param null $isp
     * @param null $location
     * @param null $browser
     * @param null $os
     *
     * @return $this
     */
    public function create(
        Account $account,
        Store $store,
        $ip = null,
        $isp = null,
        $location = null,
        $browser = null,
        $os = null
    ) {

        $authHistory = new AuthHistory();
        $authHistory->setId(AuthHistoryId::generate());
        $authHistory->setIp($ip);
        $authHistory->setIsp($isp);
        $authHistory->setLocation($location);
        $authHistory->setBrowser($browser);
        $authHistory->setOs($os);
        $authHistory->setCreatedAt(new \DateTime('now'));
        $authHistory->setAccount($account);
        $authHistory->setStore($store);

        $this->authHistory = $authHistory;

        return $this;
    }

    public function run()
    {
        $store = $this->getAuthHistory();

        $this->entityManager->persist($store);

        return $this;
    }

    public function flush()
    {
        $this->entityManager->flush();

        return $this->getAuthHistory();
    }
}
