@extends('shop.blank')

@section('content')
<div class="container">
    <div class="wait-page-wrapper">
        <div class="wait-page-creating">

            <div class="col-xs-12 col-md-6 col-md-offset-3 text-center">
                <div class="differential">
                    <div class="differential__gear--large"></div>
                    <div class="differential__gear--medium"></div>
                    <div class="differential__gear--small"></div>
                </div>

                <section class="js-section-loading">
                    <h1 class="heading-1">Sit Tight! We're creating your store</h1>
                    <p>1 of 3: Creating your account</p>
                    <p>2 of 3: Initializing your store</p>
                    <p>3 of 3: Applying store settings</p>
                </section>
            </div>

        </div>
        <div class="wait-page-error section-heading">
            <h1>Error :(</h1>
            <p>
              There was a problem creating your store.<br> Please <a class="body-link" href="">try again</a> or contact support at
            </p>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  /* <![CDATA[ */
    var signupParams = <?php echo $signupParams; ?>;
    Setup.init();
  /* ]]> */
@endsection

</script>
@endsection
