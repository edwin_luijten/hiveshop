<?php

namespace App\Http\Controllers;

use BrowscapPHP\Browscap;
use DateTime;
use DateTimeZone;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Str;
use Shop\Domain\Services\Geo\LogAuthRequest;
use Shop\Domain\Services\ServiceFactory;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function shopUrl($subdomain)
    {
        return 'http://' . $this->createSlug($subdomain) . '.' . config('app.domain');
    }

    public function getClientDemoIp()
    {
        return '82.72.193.229';
    }

    public function logAuthRequest(Request $request, $account, $store)
    {
        $browsCap = new Browscap();
        $info = $browsCap->getBrowser();

        $authHistoryService = new ServiceFactory(LogAuthRequest::class);
        $authHistoryService->create(
            $account,
            $store,
            $request->getClientIp(),
            null,
            null,
            $info->browser . ' ' . $info->version,
            $info->platform
        )->run()->flush();
    }

    public function createSlug($string)
    {
        return Str::slug($string);
    }

    public function getSubdomainFromUrl(Request $request)
    {
        $url = $request->fullUrl();

        preg_match('/(?:http[s]*\:\/\/)*(.*?)\.(?=[^\/]*\..{2,5})/i', $url, $match);

        return $match[1];
    }

    public function getTimezoneList()
    {
        static $regions = array(
            DateTimeZone::AFRICA,
            DateTimeZone::AMERICA,
            DateTimeZone::ANTARCTICA,
            DateTimeZone::ASIA,
            DateTimeZone::ATLANTIC,
            DateTimeZone::AUSTRALIA,
            DateTimeZone::EUROPE,
            DateTimeZone::INDIAN,
            DateTimeZone::PACIFIC,
        );

        $timezones = array();
        foreach( $regions as $region )
        {
            $timezones = array_merge( $timezones, DateTimeZone::listIdentifiers( $region ) );
        }

        $timezone_offsets = array();
        foreach( $timezones as $timezone )
        {
            $tz = new DateTimeZone($timezone);
            $timezone_offsets[$timezone] = $tz->getOffset(new DateTime);
        }

        // sort timezone by timezone name
        ksort($timezone_offsets);

        $timezone_list = array();
        foreach( $timezone_offsets as $timezone => $offset )
        {
            $offset_prefix = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate( 'H:i', abs($offset) );

            $pretty_offset = "UTC${offset_prefix}${offset_formatted}";

            $t = new DateTimeZone($timezone);
            $c = new DateTime(null, $t);
            $current_time = $c->format('g:i A');

            $timezone_list[$timezone] = "(${pretty_offset}) $timezone - $current_time";
        }

        return $timezone_list;
    }
}
