<?php namespace Shop\Infrastructure\Transformers;

use League\Fractal\TransformerAbstract;
use Shop\Domain\Entity\Product\Variant;

class VariantTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'optionValues',
    ];

    public function transform(Variant $variant)
    {
        return [
            'id'       => $variant->getId()->toString(),
            'isMaster' => $variant->getIsMaster(),
            'price'    => [
                'cents'     => $variant->getPrice()->getCents(),
                'formatted' => $variant->getPrice()->getFormatted()
            ],
        ];
    }

    public function includeOptionValues(Variant $variant)
    {
        return $this->collection($variant->getOptionValues(), new OptionValueTransformer());
    }
}
