<?php namespace App\Console\Commands;

use Shop\Domain\Services\Seeds\OptionSeeder;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Stopwatch\Stopwatch;

class OptionsSeederCommand extends BaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'seeder:options';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed the products table with 100 products each time this command is run.';

    /**
     * The Entity Manager
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $optionSeeder;

    public function __construct(OptionSeeder $optionSeeder)
    {
        parent::__construct();

        $this->optionSeeder = $optionSeeder;
    }

    public function fire()
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('optionSeeding');

        $this->info('Seeding options');

        $this->optionSeeder->seed();

        $event = $stopwatch->stop('optionSeeding');

        $this->info('Done.');
        $this->info(
            PHP_EOL . 'Stopwatch: ' . PHP_EOL .
            'Duration: ' . $this->formatDuration($event->getDuration()) . PHP_EOL .
            'Memory used: ' . $this->formatMemory($event->getMemory())
        );
    }

    protected function getArguments()
    {
        return [

        ];
    }

    protected function getOptions()
    {
        return [

        ];
    }
}