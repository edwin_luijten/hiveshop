<?php namespace Shop\Domain\Listeners;

use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Shop\Domain\Events\NewAccountCreated;

class EmailAccountCreationConfirmation implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue;

    private $mailer;

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }
    public function handle(NewAccountCreated $event)
    {
        $account = $event->getAccount();

        $this->mailer->send(['text' => 'emails.account.confirmation'], ['account' => $account], function ($message) {
            $message->from('info@hive-shop.dev', 'Hive Shop');
            $message->to('edwin.luijten@live.nl', 'Edwin Luijten');
            $message->subject('Confirm your account');
        });
    }
}
