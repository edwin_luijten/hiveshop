<?php namespace App\Http\Controllers\Shop\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginToStoreRequest;
use Illuminate\Auth\Guard;
use Illuminate\Http\Response;
use Shop\Domain\Entity\Plan\FeatureRepositoryInterface;
use Shop\Domain\Entity\Plan\PlanRepositoryInterface;
use Shop\Domain\Entity\Product\ProductRepositoryInterface;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;

class AuthController extends Controller
{
    private $response;
    private $productRepository;
    private $storeRepository;
    private $planRepository;
    private $featureRepository;
    private $auth;

    public function __construct(
        Response $response,
        ProductRepositoryInterface $productRepository,
        StoreRepositoryInterface $storeRepository,
        PlanRepositoryInterface $planRepository,
        FeatureRepositoryInterface $featureRepository,
        Guard $auth
    ) {
        $this->response = $response;
        $this->productRepository = $productRepository;
        $this->storeRepository = $storeRepository;
        $this->planRepository = $planRepository;
        $this->featureRepository = $featureRepository;
        $this->auth = $auth;
    }

    public function getLogin($subdomain)
    {
        return view('shop.admin.pages.login');
    }

    public function postLogin(LoginToStoreRequest $request)
    {
        $input = $request->only(['subdomain', 'email', 'password']);

        try {
            $store = $this->storeRepository->getBySubdomain($input['subdomain']);

            if ($this->auth->attempt([
                'email' => $input['email'],
                'password' => $input['password'],
                'store' => $store
            ])) {
                $this->logAuthRequest($request, $request->user(), $store);

                return redirect()->route('admin.dashboard', ['subdomain' => $store->getSubdomain()]);
            }
        } catch (\Doctrine\ORM\NoResultException $e) {
            return redirect()->back();
        }
    }

    public function getLogout($subdomain)
    {
        $this->auth->logout();

        return redirect()->route('admin.login', ['subdomain' => $subdomain]);
    }
}
