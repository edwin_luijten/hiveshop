<?php namespace Shop\Infrastructure\Repositories;

interface RepositoryInterface
{
    public function nextUuid();

    public function getEntity();

    public function getAll();

    public function getPaginated($page = 1, $limit = 1);

    public function getById($id);

    public function getCount();
}
