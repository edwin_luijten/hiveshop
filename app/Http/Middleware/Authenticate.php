<?php

namespace App\Http\Middleware;

use Closure;
use Doctrine\ORM\NoResultException;
use Illuminate\Contracts\Auth\Guard;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    private $storeRepository;

    /**
     * Create a new filter instance.
     *
     * @param  Guard                                             $auth
     * @param \Shop\Domain\Entity\Store\StoreRepositoryInterface $storeRepository
     */
    public function __construct(Guard $auth, StoreRepositoryInterface $storeRepository)
    {
        $this->auth = $auth;
        $this->storeRepository = $storeRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                // Redirect based on subdomain
                $subdomain = get_subdomain($request->fullUrl());

                if ($subdomain !== 'www') {
                    try {
                        $store = $this->storeRepository->getBySubdomain($subdomain);

                        return redirect()->route('admin.login', ['subdomain' => $store->getSubdomain()]);
                    } catch (NoResultException $e) {
                        // Throw 404
                    }

                }

                return redirect()->guest('login');
            }
        }

        view()->share('account', $request->user());
        view()->share('store', $request->user()->getStore());

        return $next($request);
    }
}
