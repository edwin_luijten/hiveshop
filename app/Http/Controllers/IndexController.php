<?php namespace App\Http\Controllers;

use App\Http\Requests\LoginToStoreRequest;
use App\Seeds\CountriesSeeder;
use App\Seeds\PlansSeeder;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Shop\Domain\Entity\Plan\FeatureRepositoryInterface;
use Shop\Domain\Entity\Plan\PlanRepositoryInterface;
use Shop\Domain\Entity\Product\ProductRepositoryInterface;
use Shop\Domain\Entity\Store\StoreRepositoryInterface;

class IndexController extends Controller
{
    private $response;
    private $productRepository;
    private $storeRepository;
    private $planRepository;
    private $featureRepository;
    private $auth;

    public function __construct(
        Response $response,
        ProductRepositoryInterface $productRepository,
        StoreRepositoryInterface $storeRepository,
        PlanRepositoryInterface $planRepository,
        FeatureRepositoryInterface $featureRepository,
        Guard $auth,
        CountriesSeeder $countriesSeeder,
        PlansSeeder $plansSeeder
    ) {
        $this->response = $response;
        $this->productRepository = $productRepository;
        $this->storeRepository = $storeRepository;
        $this->planRepository = $planRepository;
        $this->featureRepository = $featureRepository;
        $this->auth = $auth;
        $this->countriesSeeder = $countriesSeeder;
        $this->plansSeeder = $plansSeeder;
    }

    public function getIndex(Request $request)
    {
        return view('hiveshop.pages.home');
    }

    public function getOverview()
    {
        return $this->getIndex();
    }

    public function getPricing()
    {
        return view('hiveshop.pages.pricing', [
            'plans' => $this->planRepository->getAll(),
            'features' => $this->featureRepository->getAll(),
        ]);
    }

    public function getLogin()
    {
        return view('hiveshop.pages.login');
    }

    public function postLogin(LoginToStoreRequest $request)
    {
        $input = $request->only(['subdomain', 'email', 'password']);

        try {
            $store = $this->storeRepository->getBySubdomain($input['subdomain']);

            if ($this->auth->attempt([
                'email' => $input['email'],
                'password' => $input['password'],
                'store' => $store
            ])) {
                return redirect()->route('admin.dashboard', ['subdomain' => $store->getSubdomain()]);
            }
        } catch (\Doctrine\ORM\NoResultException $e) {
            return redirect()->back();
        }
    }

    public function getInitial()
    {
        $this->countriesSeeder->seed();
        $this->plansSeeder->seed();
    }
}
